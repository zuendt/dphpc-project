#!/bin/bash

filter_only_backtrace() {
    hide_mode=0
    while read l;do
        if [ "$l" = 'Memory map:' ];then
            break
        elif [ "$l" = 'Register dump:' ];then
            hide_mode=1
        elif [ "$l" = 'Backtrace:' ];then
            echo #empty line
            hide_mode=0
        fi
        if [ "$hide_mode" = '0' ];then
            echo -e "$l"
        fi
    done
}

main_run() { #$1: binary, $2+: other optional args
    bin_to_run="${1}"
    args="${@:2}"
    if [ "${2%%.mps.gz}" != "${2}" ];then
        #overwrite $1
        args[0]='/dev/stdin'
    fi

    if [ "$(cat /etc/hostname)" = 'davinci' ];then
        glpk_so='../felix_tests/glpk-4.35/src/.libs/libglpk.so.0'
        segfault_so='../libSegFault.so'
    else
        glpk_so='../glpk-4.35/src/.libs/libglpk.so.0'
    fi
    echo "Running '${bin_to_run} ${args[@]}'"
    LD_PRELOAD="${segfault_so} ${glpk_so}" SEGFAULT_SIGNALS="all" ${bin_to_run} "${args[@]}"
}

filter_symbols() {
    #and highlight red stuff
    c++filt|sed -e $'s,\./project,\033[31m./project\033[0m,g'
}


if [ "${2%%.mps.gz}" != "${2}" ];then
    echo "Decompressing with 'gzip "${2}" -dc'"
    gzip "${2}" -dc|main_run "${@}" 2>&1|filter_only_backtrace|filter_symbols
else
    main_run "${@}" 2>&1|filter_only_backtrace|filter_symbols
fi

