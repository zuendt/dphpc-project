#include <thrust/find.h> 
#include <thrust/reduce.h>
#include <thrust/functional.h>
#include "simplex.cuh"
#include <climits>
#include <cassert>
#include <iostream>
#include <iomanip>
#include "lib/cuPrintf.cuh"

//falls es probleme gibt: nehmt --expt-relaxed-constexpr aus dem makefile raus und versucht die error von den const expressions zu beheben. 

///THIS IMPLEMENTATION FULLY RUNS ON DEVICE




//condition for find_if function
struct negative{
	__device__
	bool operator()(int x)
	{
		return x < 0;
	}
};





//compare for reduce
	//for any l in the range 0 to xB.size - 1, it calculates ratios if and only if alj > 0.
	//the the operator()(x,y) takes two indices x and y and chooses the minimum of the corresponding ratios bx/axj and by/ayj
	//if only one ratio is calculated, then this one will count as the "smaller" one
	//if there is no ratio calculated, then x counts as the "smaller" one
struct minratio: public thrust::binary_function<DenseVector::value_t , DenseVector::value_t , DenseVector::value_t >{
	       	const DenseVector::value_t* const B_invA_copy;
    		const DenseVector::value_t* const xB_copy;
			__device__ __host__
    		minratio(const DenseVector::value_t* const B_invA, const DenseVector::value_t* const xB) : B_invA_copy(B_invA), xB_copy(xB) {}

     		__device__ __host__
     		DenseVector::value_t operator()(int x, int y) {
                //DenseVector::value_t ratiox = std::numeric_limits<DenseVector::value_t>::max();
                //DenseVector::value_t ratioy = std::numeric_limits<DenseVector::value_t>::max();
				// Float max I found on cpp reference
				DenseVector::value_t ratiox = 3.40282e+38;
				DenseVector::value_t ratioy = 3.40282e+38;
                 if(x != -1 && y != -1) {
                 	const DenseVector::value_t * const B_invAstart =  B_invA_copy;
                 	const DenseVector::value_t * const xBstart =  xB_copy;
                 		
                 	 //get the values for comparison
                     DenseVector::value_t ax = *((DenseVector::value_t*)B_invAstart + x);
                     DenseVector::value_t ay = *((DenseVector::value_t*)B_invAstart + y);
                     DenseVector::value_t bx = *((DenseVector::value_t*)xBstart + x);
                     DenseVector::value_t by = *((DenseVector::value_t*)xBstart + y);
					 
					 //calculate ratios if allowed
                     if (ax > 0) {
                         ratiox = bx / ax;
                     }
                     if (ay > 0) {
                         ratioy = by / ay;
                     }
					 
					 
                     if (ratiox < ratioy) return x;
                     else return y;
                 }
                 else if (x != -1){
                     return y;
                 }
                 else {
                     return x;
                 }
                

             }
             
	};
	
	
//if all values in v are non-negative -> returns false else writes index of pivot which is (the most) negative value into j and returns true

bool selectEnteringVar(DenseVector* v, int j, const bool blandsrule){
		//printf("Size of" );
		//DenseVector::value_t locVals [v->size];
		//for (unsigned int i=0; i<v->size; ++i) {locVals[i] = (DenseVector::value_t)0.0;}
		//cudaMemcpy(locVals, v->values, v->size, cudaMemcpyDeviceToHost);
		//DenseVector::value_t* start_h = (DenseVector::value_t*) locVals;
		//DenseVector::value_t* end_h = start_h + v->size;
		
		DenseVector::value_t* start_d = (DenseVector::value_t*) v->values;
		DenseVector::value_t* end_d = start_d + v->size;
		printf("Size of" ); // << "Size of v: " << v->size << std::endl;
		//std::cout << "end - start" << std::setw(2) << end_d - start_d << std::endl;

		// Debug: print all the values 
			//std::cout << " v values are: " << std::endl;
			for (unsigned i = 0; i < v->size; ++i) {
				//std::cout << *(start_h+i) << ", ";
			}
			//std::cout << std::endl;
		
		//unsicher ob das genau bland's rule entspricht, da wir in einer anderen Reihenfolge (als beim Simplex Tableau) speichern
		if(blandsrule){
	 		
	 		//pointer to the first element which is negative executed on GPU, if there is no such negative element-> it == end
			DenseVector::value_t *it = thrust::find_if(thrust::device, start_d, end_d, negative());

			if (it == end_d) return false;
			else{
				
				j = it - start_d;
				return true;
			}
	}
	
	//Vorschlag: könnte man auch wie bei selectLeavingVar mit Indx vector machen 
	else{	
		//Different approach with min_element
		//std::cout << "started reduce" << std::endl;
		
		val_T * pivot = thrust::min_element(thrust::device, start_d, end_d);
		//std::cout << "finished min_element with pivot " << *pivot << std::endl;
		
		// Debug: print all the values 
		//std::cout << " v values are: " << std::endl;
		for (unsigned i = 0; i < v->size; ++i) {
			//std::cout << *(start_d+i) << ", ";
		}
		//std::cout << std::endl;

		//the algorithm reached optimal value
		if (/*pivot <= TOL &&*/ *pivot >= -TOL) return false;

	
		j = pivot-start_d;
		return true;
	}
		
}

//B_invA is the jth column of the matrix B_invA
//xB is the vector xB as explained in the revised simplex
//i is the row of the "ideal" pivot
//index vector contains the integers from  0 to xB.size-1 in increasing order
 
//Optimierungsüberlegung: da eigentlich B_invA Sparse ist?, lohnt es sich überhaupt durch das ganze ding zu iterieren?

//The function writes the index of the pivot into i. The pivot is the ith element in B_invA namely aij that has the lowest ratio bi/aij such that aij > 0, where bi is the i-th element in de Vector xB
// It returns true if it can find such a pivot. Else it returns false and i = -1. 

bool selectLeavingVar(const DenseVector B_invA, const DenseVector xB, int i, const DenseVector index){
	
	thrust::device_ptr<const DenseVector::value_t> B_invA_ptr(B_invA.values);
    thrust::device_ptr<const DenseVector::value_t> xB_ptr(xB.values);

    const DenseVector::value_t* start = thrust::raw_pointer_cast(B_invA_ptr);
    const DenseVector::value_t* end = start + B_invA.size;
	
	//if there is an error while running on thrust:host, consider using start_h, end_h -> memcopy to host required
	const DenseVector::value_t* it = thrust::min_element(thrust::device, start, end, minratio(B_invA_ptr.get(), xB_ptr.get()));
	i = it - start;
	if(it == end) return false;
	else return true;
        
}
