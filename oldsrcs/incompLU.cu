#include "simplex.cuh"

void LU::performLU(cusparseHandle_t* handle, const SparseMatrix* A) {

    // Delete old lu data
    CHECK_CUDA(cudaFree(csrVal));
    CHECK_CUDA(cudaFree(csrRowPtr));
    CHECK_CUDA(cudaFree(csrColInd));

    // Copy values from A to the LU
    cusparseIndexType_t dummyRIT;
    cusparseIndexType_t dummyCIT;
    cusparseIndexBase_t dummyIB;
    cudaDataType dummyDT;
    val_T* tmpV;
    int* tmpR;
    int* tmpC;
    cusparseCsrGet(A->spDescr, &m, &m, &nnz, (void **)&tmpR, (void **)&tmpC, (void **)&tmpV, &dummyRIT, &dummyCIT, &dummyIB, &dummyDT);
    // Initialize LU elements
    CHECK_CUDA(cudaMalloc((void**)&csrVal, nnz * sizeof(val_T)))
    CHECK_CUDA(cudaMalloc((void**)&csrRowPtr, (m+1) * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&csrColInd, nnz * sizeof(int)))
    
    cudaMemcpy(csrVal, tmpV, nnz * sizeof(val_T), cudaMemcpyDeviceToDevice);
    cudaMemcpy(csrRowPtr, tmpR, (m+1) * sizeof(int), cudaMemcpyDeviceToDevice);
    cudaMemcpy(csrColInd, tmpC, nnz * sizeof(int), cudaMemcpyDeviceToDevice);
    PRINT_FUNC_PREFIX;
    std::cout << "Copied A to Lu, nnz = " << nnz*sizeof(value_t)  << std::endl;

    // Debug: print the LU matrix (should be identity in total for 1. step)
    val_T tmpVals[nnz];
    int tmpRow [m+1];
    int tmpCol [nnz];
    cudaMemcpy(tmpVals, csrVal, nnz * sizeof(val_T), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpRow, csrRowPtr, (m+1) * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpCol, csrColInd, nnz * sizeof(int), cudaMemcpyDeviceToHost);
    int rowI = 0;
    std::cout << std::endl << "A = " << std::endl;
    for (unsigned i = 0; i < nnz; i++) {
        std::cout << "[" << rowI << "," << tmpCol[i] << "]=" << tmpVals[i] << "; ";
        if (tmpRow[rowI+1] == i+1) {
            ++rowI;
        }
    }
    rowI = 0;
    std::cout << std::endl << std::endl;

    // Create info
    csrilu02Info_t info = 0;
    CHECK_CUSPARSE(cusparseCreateCsrilu02Info(&info))

    // Buffer info
    int pBufferSizeInBytes = 0;
    void* pBuffer = NULL;

    // Query buffer size for the decomposition
    CHECK_CUSPARSE(cusparseScsrilu02_bufferSize(*handle, (int) m,
                    (int) nnz, A->descr, csrVal, (int *) csrRowPtr,
                    (int *) csrColInd, info, &pBufferSizeInBytes))

    // Allocate pBuffer
    CHECK_CUDA(cudaMalloc((void**)&pBuffer, pBufferSizeInBytes))

    // Do analysis
    const cusparseSolvePolicy_t policy = CUSPARSE_SOLVE_POLICY_NO_LEVEL;
    CHECK_CUSPARSE(cusparseScsrilu02_analysis(*handle, (int) m,
                    (int) nnz, A->descr, csrVal, (int *) csrRowPtr,
                    (int *) csrColInd, info, policy, pBuffer))

    // Zero pivot stuff? (Dont understand what it does)

    // Do decomposition
    // Again, dont no what a numerical zero is...
    int numerical_zero;

    CHECK_CUSPARSE(cusparseScsrilu02(*handle, (int) m, (int) nnz,
                    A->descr, csrVal, (int *) csrRowPtr, (int *) csrColInd,
                    info, policy, pBuffer))

    // Zero pivot stuff? (Dont understand what it does)

    // Free pBuffer
    CHECK_CUDA(cudaFree(pBuffer))

    descr_L = 0;
    CHECK_CUSPARSE(cusparseCreateCsr(&descr_L, m, m, nnz, (void *)this->csrRowPtr, (void *)this->csrColInd, (void *)this->csrVal, indexType, indexType, CUSPARSE_INDEX_BASE_ZERO, valueType))
    PRINT_PREFIX;
    std::cout << "Created L descriptor" << std::endl;
    CHECK_CUSPARSE(cusparseSpMatSetAttribute(descr_L, CUSPARSE_SPMAT_FILL_MODE, &fillModeL, sizeof(fillModeL)))
    PRINT_PREFIX;
    std::cout << "L FillType set" << std::endl;
    CHECK_CUSPARSE(cusparseSpMatSetAttribute(descr_L, CUSPARSE_SPMAT_DIAG_TYPE, &diagTypeL, sizeof(diagTypeL)))
    PRINT_PREFIX;
    std::cout << "L Matrix generated" << std::endl;


    // Matrix descriptor for U:
    descr_U = 0;
    CHECK_CUSPARSE(cusparseCreateCsr(&descr_U, m, m, nnz, this->csrRowPtr, this->csrColInd, this->csrVal, indexType, indexType, CUSPARSE_INDEX_BASE_ZERO, valueType))
    CHECK_CUSPARSE(cusparseSpMatSetAttribute(descr_U, CUSPARSE_SPMAT_FILL_MODE, &fillModeR, sizeof(fillModeR)))
    CHECK_CUSPARSE(cusparseSpMatSetAttribute(descr_U, CUSPARSE_SPMAT_DIAG_TYPE, &diagTypeR, sizeof(diagTypeR)))
    PRINT_PREFIX;
    std::cout << "U Matrix generated" << std::endl;

    // Debug: print the LU matrix (should be identity in total for 1. step)
    cudaMemcpy(tmpVals, csrVal, nnz * sizeof(val_T), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpRow, csrRowPtr, (m+1) * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpCol, csrColInd, nnz * sizeof(int), cudaMemcpyDeviceToHost);
    rowI = 0;
    std::cout << std::endl << "LU = " << std::endl;
    for (unsigned i = 0; i < nnz; i++) {
        std::cout << "[" << rowI << "," << tmpCol[i] << "]=" << tmpVals[i] << "; ";
        if (tmpRow[rowI+1] == i+1) {
            ++rowI;
        }
    }
    std::cout << std::endl << std::endl;

}

void LU::solve(cusparseHandle_t* handle, const bool transpose, const DenseVector& rhs, DenseVector& intermediate, DenseVector& x) {

    std::cout << "\n Started solving LU with tranpose(0/1): " << transpose << std::endl;

    // We solve L*U*x = rhs, with z = U*x

    cusparseSpSVDescr_t SpSV_descr_L = 0;
    CHECK_CUSPARSE(cusparseSpSV_createDescr(&SpSV_descr_L))
    cusparseSpSVDescr_t SpSV_descr_U = 0;
    CHECK_CUSPARSE(cusparseSpSV_createDescr(&SpSV_descr_U))

    DenseVector::value_t alpha = 1.0;

    // Intermediate vector (z = U*x or L^T*x)
    // DenseVector z;
    // z.size = this->m;
    // cudaMalloc(&z.values, z.size*sizeof(DenseVector::value_t));
    // z.valueType = CUDA_R_64F;
    // cusparseCreateDnVec(z.descr, z.size, z.values, z.valueType);

    // Debug: print the LU matrix (should be identity in total for 1. step)
    val_T tmpVals[nnz];
    int tmpRow [m+1];
    int tmpCol [nnz];
    cudaMemcpy(tmpVals, csrVal, nnz * sizeof(val_T), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpRow, csrRowPtr, (m+1) * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpCol, csrColInd, nnz * sizeof(int), cudaMemcpyDeviceToHost);
    int rowI = 0;
    std::cout << std::endl << "LU = " << std::endl;
    for (unsigned i = 0; i < nnz; i++) {
        std::cout << "[" << rowI << "," << tmpCol[i] << "]=" << tmpVals[i] << "; ";
        if (tmpRow[rowI+1] == i+1) {
            ++rowI;
        }
    }
    std::cout << std::endl << std::endl;

    // Debug: print out rhs vector 
    // printVector(&rhs, "cB ");
    val_T tmpVec[rhs.size];
    val_T* tmpVec_d;
    cusparseDnVecGetValues(rhs.DnDescr, (void **)&tmpVec_d);
    cudaMemcpy(tmpVec, tmpVec_d, rhs.size * sizeof(val_T), cudaMemcpyDeviceToHost);
    std::cout << std::endl << "RHS = " << std::endl;
    for (unsigned i = 0 ; i < rhs.size; i++) {
        std::cout << tmpVec[i] << ", ";
    }
    std::cout << "Address of RHS vals: " << tmpVec_d;
    std::cout << std::endl << std::endl;

    // if transpose == true ...:
    if (transpose) {
        cusparseOperation_t op = CUSPARSE_OPERATION_TRANSPOSE;

        // Query buffer size for both steps
        size_t buffSizeU = 0;
        size_t buffSizeL = 0;
        
        CHECK_CUSPARSE(cusparseSpSV_bufferSize(*handle, op, &alpha, this->descr_U, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U, &buffSizeU))
        CHECK_CUSPARSE(cusparseSpSV_bufferSize(*handle, op, &alpha, this->descr_L, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L, &buffSizeL))

        // Allocate buffers
        void* bufferL = NULL;
        void* bufferU = NULL;
        CHECK_CUDA(cudaMalloc((void**)&bufferL, buffSizeL))
        CHECK_CUDA(cudaMalloc((void**)&bufferU, buffSizeU))

        // Do analysis
        CHECK_CUSPARSE(cusparseSpSV_analysis(*handle, op, &alpha, this->descr_U, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U, bufferU))
        CHECK_CUSPARSE(cusparseSpSV_analysis(*handle, op, &alpha, this->descr_L, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L, bufferL))

        // Solve in order
        CHECK_CUSPARSE(cusparseSpSV_solve(*handle, op, &alpha, this->descr_U, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U))
        CHECK_CUSPARSE(cusparseSpSV_solve(*handle, op, &alpha, this->descr_L, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L))

        // Free workspace buffers
        CHECK_CUDA(cudaFree(bufferL))
        CHECK_CUDA(cudaFree(bufferU))

    } else {
        cusparseOperation_t op = CUSPARSE_OPERATION_NON_TRANSPOSE;

        // Query buffer size for both steps
        size_t buffSizeL = 0;
        size_t buffSizeU = 0;
        
        // CHECK_CUSPARSE(cusparseSpSV_bufferSize(*handle, op, &alpha, this->descr_L, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L, &buffSizeL))
        // CHECK_CUSPARSE(cusparseSpSV_bufferSize(*handle, op, &alpha, this->descr_U, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U, &buffSizeU))

        // std::cout << "\n How much memory it want to allocate for the solving: " << buffSizeL << std::endl << std::endl;

        // Allocate buffers
        void* bufferL = NULL;
        void* bufferU = NULL;
        // CHECK_CUDA(cudaMalloc((void**)&bufferL, buffSizeL))
        // CHECK_CUDA(cudaMalloc((void**)&bufferU, buffSizeU))

        // std::cout << "alloced workspaces" << std::endl;

        // // Do analysis
        // CHECK_CUSPARSE(cusparseSpSV_analysis(*handle, op, &alpha, this->descr_L, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L, bufferL))
        // CHECK_CUSPARSE(cusparseSpSV_analysis(*handle, op, &alpha, this->descr_U, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U, bufferU))

        // std::cout << "analyzed" << std::endl;

        // // Solve in order
        // CHECK_CUSPARSE(cusparseSpSV_solve(*handle, op, &alpha, this->descr_L, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L))
        // CHECK_CUSPARSE(cusparseSpSV_solve(*handle, op, &alpha, this->descr_U, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U))

        // std::cout << "solved" << std::endl;

        // // Free workspace buffers
        // CHECK_CUDA(cudaFree(bufferL))
        // CHECK_CUDA(cudaFree(bufferU))

        // std::cout << "freed workspace" << std::endl;


        CHECK_CUSPARSE(cusparseSpSV_bufferSize(*handle, op, &alpha, this->descr_L, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L, &buffSizeL))
        std::cout << "\n How much memory it want to allocate for the solving: " << buffSizeL << std::endl << std::endl;
        CHECK_CUDA(cudaMalloc((void**)&bufferL, buffSizeL))
        std::cout << "alloced workspaces" << std::endl;
        CHECK_CUSPARSE(cusparseSpSV_analysis(*handle, op, &alpha, this->descr_L, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L, bufferL))
        std::cout << "analyzed" << std::endl;
        CHECK_CUSPARSE(cusparseSpSV_solve(*handle, op, &alpha, this->descr_L, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L))
        std::cout << "solved" << std::endl;
        CHECK_CUDA(cudaFree(bufferL))

        CHECK_CUSPARSE(cusparseSpSV_bufferSize(*handle, op, &alpha, this->descr_U, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U, &buffSizeU))
        std::cout << "\n How much memory it want to allocate for the solving: " << buffSizeL << std::endl << std::endl;
        CHECK_CUDA(cudaMalloc((void**)&bufferU, buffSizeU))
        std::cout << "alloced workspaces" << std::endl;
        CHECK_CUSPARSE(cusparseSpSV_analysis(*handle, op, &alpha, this->descr_U, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U, bufferU))
        std::cout << "analyzed" << std::endl;
        CHECK_CUSPARSE(cusparseSpSV_solve(*handle, op, &alpha, this->descr_U, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U))
        std::cout << "solved" << std::endl;
        CHECK_CUDA(cudaFree(bufferU))
        std::cout << "freed workspace" << std::endl;

    }
    printVector(&x, "Solved for x");

    std::cout << "Finished solving LU" << std::endl;
}
