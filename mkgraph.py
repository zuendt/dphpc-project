#!/usr/bin/python3

import sys
import os
import matplotlib.pyplot as plt
import numpy as np
import statistics
import scipy.stats

fontsizes=[12,11,10]

trsl={
        'scip':'SCIP',
        'project':'This Work',
        'scipy':'SciPy'}
"""
PROGRESS: violin plot try?
https://matplotlib.org/stable/gallery/statistics/violinplot.html

errorbars
https://matplotlib.org/stable/gallery/lines_bars_and_markers/errorbar_subsample.html#sphx-glr-gallery-lines-bars-and-markers-errorbar-subsample-py

"""


WORKDIR=os.path.dirname(__file__)
DATASOURCE='/results/run*davinci*_{solver}_*'
#DATASOURCE='/results/run*florian-Swift*_{solver}_*'
sys.path.append(WORKDIR+'/benchmark/miplib')

import get_time #local

def confidence_interval(ls,confidence=0.95) :
    n=len(ls)
    ci=scipy.stats.t.interval(confidence=confidence,
            df=n-1,
            loc=statistics.mean(ls),
            scale=scipy.stats.sem(ls))
    if np.isnan(ci[0]) or np.isnan(ci[1]):
        print('nan',ls)
        return [None,None]
    return ci

def sort_data_by_mean(data) :
    full_repr_data=[(statistics.mean(samples) if len(samples)>0 else -1,
        test_bin,test_name,samples)
            for test_bin,tests in data.items()
            for test_name,samples in tests.items()]

    full_repr_data.sort()
    result={test_bin:{} for mean_value,test_bin,*_ in full_repr_data}
    for mean_value,test_bin,test_name,samples in full_repr_data :
        result[test_bin][test_name]=samples
    return result
def filter_numbers_smaller_than(data,callback) :
    return {k:{k2:v2 for k2,v2 in v.items() if callback(v2)} for k,v in data.items()}

def plot_point_cloud(extremes=[1,3],show_two=True) :
    """ Draw a plot or points
    extremes=[smallest,biggest] is the count of data traces to label on the plots,
    in their longest runtime. The smallest will also have an arrow to the corresp. point.
    show_two is whether to show 2 different y-scale plots of the data, for an overview
    """
    if show_two :
        fig, axs = plt.subplots(2,1,gridspec_kw={'height_ratios': [1, 2]})
    else :
        fig, ax = plt.subplots()
        axs=[ax]
    max_normalized={}
    #for name,samples in get_time.extract_out([WORKDIR+'/benchmark/miplib/results/run*.scip.*.out']) :
    all_show_data=[]
    for name,samples in get_time.extract_runbench([WORKDIR+DATASOURCE.format(solver='project')]) :
        show_name=name.strip('.gz').strip('.mps')
        len_samples=len(samples)
        samples.sort()
        m=statistics.median(samples)
        max_normalized[samples[-1]/m]=show_name
        show_data=[i/m for i in samples]
        all_show_data.append(show_data)
        for ax in axs :
            ax.plot(range(len(samples)),show_data,label=show_name,
                    alpha=0.3,linestyle='-',linewidth=0.5,
                    marker='+',markersize=0.5)
    samples_tot=[[vs[i] for vs in all_show_data]
            for i in range(len(all_show_data[0]))]
    samples_avg=[statistics.mean(i) for i in samples_tot]
    samples_conf_low=[confidence_interval(i)[0] for i in samples_tot]
    samples_conf_high=[confidence_interval(i)[1] for i in samples_tot]
    for ax in axs :
        for d,width in ((samples_conf_low,0.5),(samples_conf_high,0.5),(samples_avg,1)) :
            ax.plot(d,label=show_name,
                    alpha=1,linestyle='-',linewidth=width,
                    marker='',markersize=3,color='darkblue')
        ax.annotate('This Work',xy=(0,samples_avg[0]),
                fontsize=fontsizes[2],
                va='top',ha='left')
    print('mps files tested',len(max_normalized))
    #now the scip data
    all_show_data=[]
    for name,samples in get_time.extract_runbench([WORKDIR+DATASOURCE.format(solver='scip')]) :
        show_name=name.strip('.gz').strip('.mps')
        len_samples=len(samples)
        samples.sort()
        m=statistics.median(samples)
        show_data=[i/m for i in samples]
        all_show_data.append(show_data)
    samples_tot=[[vs[i] for vs in all_show_data]
            for i in range(len(all_show_data[0]))]
    samples_avg=[statistics.mean(i) for i in samples_tot]
    samples_conf_low=[confidence_interval(i)[0] for i in samples_tot]
    samples_conf_high=[confidence_interval(i)[1] for i in samples_tot]
    for ax in axs :
        for d,width in ((samples_conf_low,0.5),(samples_conf_high,0.5),(samples_avg,2)) :
            ax.plot(d,label=show_name,
                    alpha=1,linestyle='-',linewidth=width,
                    marker='',markersize=3,color='brown')
        ax.annotate('SCIP',xy=(0,samples_avg[0]),
                fontsize=fontsizes[2],
                va='top',ha='left')

    # line labels for outliers
    for ix,k in enumerate(sorted(max_normalized.keys(),reverse=True)) :
        if ix<extremes[1] :
            axs[0].annotate(max_normalized[k],xy=(len_samples-1,k),
                    fontsize=fontsizes[2],
                    va='bottom' if ix!=0 else 'top',ha='right')
            print(max_normalized[k],k)
        elif ix>=len(max_normalized)-extremes[0] :
            axs[-1].annotate(max_normalized[k],
                    xy=(len_samples-1,k),
                    xytext=(len_samples-1,k-0.02),
                    arrowprops=dict(facecolor='black',width=0.05,headlength=4,headwidth=4,
                        shrink=10),
                    fontsize=fontsizes[2],
                    va='top',ha='right')
            print(max_normalized[k],k)

    if show_two :
        #by default only makes 1,2 with such a big fontsize
        axs[0].set(yticks=[1,1.5,2])
        #ax.set_yscale('function', functions=(forward, inverse))
        #axs[0].set_ybound(lower=1.00)
        axs[1].set_ybound(0.95,1.06)
        #fig.text(0.06, 0.5, 'runtime/median (no unit)', ha='right', va='center',
                #rotation='vertical', fontsize=fontsizes[1])
    axs[0].set_title(f'Runtime distribution, sorted and normalised n={len_samples}',
            pad=10,fontsize=fontsizes[0])
    axs[0].set_xlabel(f'quantiles',fontsize=fontsizes[1])
    axs[0].set_ylabel(f'run time/median run time',fontsize=fontsizes[1])


    #ax.legend()
    for ax in axs :
        ax.set_xticks([i for i in range(0,101,10)],[f'{i}%' for i in range(0,101,10)])
        if show_two :
            ax.axhline(y=1.05)
        ax.grid(True)

def plot_multiple_solvers() :
    data={'scip':{},'project':{},'scipy':{}}
    for solver in data.keys() :
        for name,samples in get_time.extract_runbench([WORKDIR+DATASOURCE.format(solver=solver)]) :
            show_name=name.strip('.gz').strip('.mps')
            data[solver][show_name]=samples

    data=sort_data_by_mean(data)
    #only extract mean for now
    #data={k:{k2:statistics.mean(v2) for k2,v2 in v.items()} for k,v in data.items()}

    data=filter_numbers_smaller_than(data,lambda i:len(i)==0 or statistics.mean(i)<40.0)

    fig,axs=dict_dict_plot(data)


def dict_dict_plot(data) :
    test_names=list(data[list(data.keys())[0]].keys())

    x = np.arange(len(test_names))  # the label locations
    width = 0.25  # the width of the bars
    multiplier = 0

    fig, axs = plt.subplots(2,1,layout='constrained',
            gridspec_kw={'height_ratios': [2, 1]})
    ax=axs[0]

    for attribute, measurement in data.items():
        offset = width * multiplier
        vs=list(measurement.values())
        len_samples=len(vs[0])
        confidences,d=[],[]
        lookup_confidences={}
        for i in vs :
            ci=confidence_interval(i,confidence=0.95)
            m=statistics.mean(i)
            confidences.append((m-ci[0],ci[1]-m))
            d.append(m)
            lookup_confidences[m]=confidences[-1]
        rects = ax.bar(x + offset, d,width=width, label=trsl[attribute],
                yerr=[[i[0] for i in confidences],[i[1] for i in confidences]])
        axs[1].bar(x + offset, [statistics.variance(i) if len(i)>1 else 0 for i in vs],
                width=width, label=attribute,bottom=1e-8)
        #rects = ax.errorbar(x + offset, [statistics.mean(i) for i in vs],
        #        yerr=[statistics.variance(i)*5 for i in vs],label=attribute)
        if attribute=='scip' :
            axs[0].bar_label(rects, padding=3,rotation=90,
                    fmt=lambda i:f'{round(i*1e3,2)}±{round(max(lookup_confidences[i])*1e3,2)} ms',
                    color='tab:blue')
                    #fmt=lambda i:i)
        axs[1].semilogy(base=10.0)
        axs[1].set(yticks=[10**(-i) for i in (3,4,5,6,7,8)])
        multiplier += 1

    # Add some text for labels, title and custom x-axis tick labels, etc.
    axs[0].set_title(f'Performance mean over n={len_samples} runs',fontsize=fontsizes[0])
    #some formatting:
    axs[0].set_xticks(x, test_names,rotation=30,
            rotation_mode='anchor',horizontalalignment='right',
            fontsize=fontsizes[2])

    axs[1].set_xticklabels([])
    axs[1].set_xlabel('and variance (log scale)',fontsize=fontsizes[1])

    for ax in axs :
        ax.tick_params(labelsize=fontsizes[2])
        ax.grid(True)
        ax.set_ylabel('time (s)',fontsize=fontsizes[2])
    axs[0].legend(loc='best',bbox_to_anchor=(0., 0.7, 1, 0.1),
            ncols=3,fontsize=fontsizes[2])

    return fig,axs


def does_not_work() :
    fig,ax=plt.subplots(1,1,figsize=(2,1),sharey=True)

    for bench_bin,samples in data.items():
        offset = width * multiplier
        rects = ax.bar(x + offset, samples[0], width, label=bench_bin)
        ax.bar_label(rects, padding=3)
        multiplier += 1

    ax.bar(data.keys(),data.values())

    fig.suptitle('blah')

if __name__=='__main__' :
    if len(sys.argv)<3 :
        print('error not enough arguments')
        exit(1)
    if sys.argv[1]=='multiple_solvers' :
        plot_multiple_solvers()
    if sys.argv[1]=='point_cloud' :
        plot_point_cloud(extremes=[0,0],show_two=False)

    if sys.argv[2]=='show' :
        plt.show()
    else :
        plt.savefig(os.path.dirname(__file__)+'/results/'+sys.argv[1]+'.svg',format='svg')
