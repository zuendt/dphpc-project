
#include "simplex.cuh"
#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <string>
#include <cassert>

// void transpose(cusparseHandle_t* handle, SparseMatrix** A) {
// 	int64_t h_n, h_m, h_nnz;
//     int* d_csrRowPtr;
//     int* d_csrColInd;
//     val_T* d_csrVal;

//     cusparseIndexType_t h_rowPtrType, h_colIndType;
//     cusparseIndexBase_t h_idxBase;
//     cudaDataType h_valueType;

//     CHECK_CUSPARSE(cusparseCsrGet((*A)->spDescr,
//                                   &h_m,
//                                   &h_n,
//                                   &h_nnz,
//                                   (void**)&d_csrRowPtr,
//                                   (void**)&d_csrColInd,
//                                   (void**)&d_csrVal,
//                                   &h_rowPtrType,
//                                   &h_colIndType,
//                                   &h_idxBase,
//                                   &h_valueType))

//     //transform A into CSC
//     int* d_cscColPtr;
//     int* d_cscRowInd;
//     val_T* d_cscVal;

//     CHECK_CUDA(cudaMalloc((void**)&d_cscColPtr, (h_n + 1) * sizeof(int)))
//     CHECK_CUDA(cudaMalloc((void**)&d_cscRowInd, h_nnz     * sizeof(int)))
//     CHECK_CUDA(cudaMalloc((void**)&d_cscVal,    h_nnz     * sizeof(val_T)))

// 	size_t pBufferSize;
//     void* pBuffer = NULL;

//     cusparseCsr2CscAlg_t algo{CUSPARSE_CSR2CSC_ALG1};
    
//     CHECK_CUSPARSE(cusparseCsr2cscEx2_bufferSize(*handle,
//                                                  h_m,
//                                                  h_n,
//                                                  h_nnz,
//                                                  d_csrVal,
//                                                  d_csrRowPtr,
//                                                  d_csrColInd,
//                                                  d_cscVal,
//                                                  d_cscColPtr,
//                                                  d_cscRowInd,
//                                                  h_valueType,
//                                                  CUSPARSE_ACTION_NUMERIC,
//                                                  h_idxBase,
//                                                  algo,
//                                                  &pBufferSize))
    
//     // Allocate pBuffer
//     CHECK_CUDA(cudaMalloc((void**)&pBuffer, pBufferSize))
    
//     CHECK_CUSPARSE(cusparseCsr2cscEx2(*handle,
//                                     h_m,
//                                     h_n,
//                                     h_nnz,
//                                     d_csrVal,
//                                     d_csrRowPtr,
//                                     d_csrColInd,
//                                     d_cscVal,
//                                     d_cscColPtr,
//                                     d_cscRowInd,
//                                     h_valueType,
//                                     CUSPARSE_ACTION_NUMERIC,
//                                     h_idxBase,
//                                     algo,
//                                     pBuffer))
	
// 	CHECK_CUDA(cudaFree(pBuffer))
	
// 	delete *A;

// 	*A = new SparseMatrix(h_n, h_m, h_nnz, d_cscColPtr, d_cscRowInd, d_cscVal);
// }



// Dont know if it makes sens to fix this here, but would work
#define MAX_NUM_ITERATIONS 1e7

int main (int argc, char** argv) {
    if (argc<2) {
        std::cout << "error please provide a filename"<<std::endl;
        return 1;
    }
	//get filename from the first input parameter
	char* filename = argv[1];

	bool debug = false;
	if (argc > 2) {
		std::string action(argv[2]);
		if (action == "debug" || action == "d") {
			debug = true;
		}
	}

	const char* profile;
	if (argc > 3)
		profile = argv[3];
	else
	 	profile = "0";
	//std::string outputFile = argv[1];

	// initialize handle before doing ANYTHING with cusparse
	cusparseHandle_t handle = NULL;
    cusparseCreate(&handle);

	// Same for cusolver handle
	cusolverSpHandle_t solvHandle = NULL;
    cusolverSpCreate(&solvHandle);

	// Manipulations for name of output file
	//int startTest = outputFile.find("test");
	//outputFile = outputFile.replace(startTest, 4, "sol");
	//std::string testName = outputFile.erase(startTest-1, 4);
	
	SparseMatrix *A = nullptr;
	DenseVector *b = nullptr;
	DenseVector *c = nullptr;
	
	//read A b and c from file
	mps_reader(filename, &A, &b, &c);

	// transpose(&handle, &A);
	
    //PRINT_FUNC_PREFIX;
	//std::cout << "read succesfully  ->  A, b, c initialzed" << std::endl;

//	std::cout << "testing if A, b, c is still correct:" << std::endl; 
	

	//std::cout << "testing b vector:" << std::endl;
    //printVector(b, "b", true);
	
	//create Identity Matrix
	int n = A->m;
    //PRINT_PREFIX;
	//std::cout << "Identity matrix dimension=" << n << "   A->m:" << A->m << ", A->n:" << A->n << std::endl;
	SparseMatrix B(n);

    //PRINT_PREFIX;
	//std::cout << "B sparsMat inizialized" << std::endl;

	// Create cB vector
	DenseVector::value_t zeros[n];
	for (unsigned i = 0; i < n; i++) {
		zeros[i] = 0.0;
	}
	DenseVector cB(n, zeros);
    //PRINT_PREFIX;
	//std::cout << "cB vec inizialized" << std::endl;

	// Create basis vector
	BasisVector* basis = nullptr;

    //PRINT_PREFIX;
	//std::cout << "basis initialized" << std::endl;

	// Creare xb vector
	DenseVector xb(*b);

    //PRINT_PREFIX;
	//std::cout << "xb dense vector initialized" << std::endl;

	int status;

	// Do phase 1
			PRINT_PREFIX; std::cout << "STARTING phase1" << std::endl;
    bool feasible = simplexPhase1(&handle, &solvHandle, A, &B, b, c, &cB, &basis, &xb, MAX_NUM_ITERATIONS, &status, *profile, debug);
    		PRINT_PREFIX; std::cout << "FINISHED phase1" << std::endl;

	// DEBUG -------------------------------------------------
	//std::cout << "testing b vector (shouldn't have any changes):" << std::endl;
	uint64_t testbSize = b->size;
    //printVector(b, "b", true);
	// DEBUG END ---------------------------------------------

	// If problem is feasible, do phase 2
	bool result;
	DenseVector::value_t opt_val = 0;
	DenseVector::value_t* solution;
	if (feasible) {
				PRINT_PREFIX; std::cout << "\n STARTING phase2 simplex on feasible problem\n" << std::endl;
		result = simplexPhase2(&handle, &solvHandle, A, &B, b, c, &cB, basis, &xb, MAX_NUM_ITERATIONS, &status, *profile, debug);
				PRINT_PREFIX; std::cout << "FINISHEd phase2\n" << std::endl;
	} else {
		// Fill vector with values of original variables
		//std::cout << "\n\n\n Starting problem was not possible to convert to feasible -> no solution found:\n" << std::endl;
		gatherSolution(&xb, basis, &solution, A->n+A->m);
		write_sol(status, opt_val, solution, A->n, A->m);
		return 0;
	}
	
	if (result) {
		// Compute optimal value
		dot(&cB, &xb, &opt_val);
		//std::cout << "Computed optimal value=" << opt_val << std::endl;
	}

	// Fill vector with values of original variables
	//std::cout << "removing Slack variables form xb" << std::endl;
	gatherSolution(&xb, basis, &solution, A->n+A->m);

	//std::cout << "writing solution file" << std::endl;
	write_sol(status, opt_val, solution, A->n, A->m);
	//std::cout << "Delete" << std::endl;
	//std::cout << "Deleting dynamically allocated objects" << std::endl;
	
	delete solution;
	solution = nullptr;
	//std::cout << "Deleted solution" << std::endl;
	delete c;
	c = nullptr;
	//std::cout << "Deleted c" << std::endl;
	
	delete b;
	b = nullptr;
	//std::cout << "Deleted b" << std::endl;
	
	delete A;
	A = nullptr;
	//std::cout << "Deleted A" << std::endl;

	delete basis;
	basis = nullptr;
	
	
	cusolverSpDestroy(solvHandle);
	cusparseDestroy(handle);

    return 0;
}
