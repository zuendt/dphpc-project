#include "simplex.cuh"


void printBasis(BasisVector* basis, std::string name) {
    uint64_t Size = basis->m_;

    int* vals;
    // CHECK_CUDA(cudaMalloc((void**)&vals, vSize * sizeof(val_T)))
    CHECK_CUSPARSE(cusparseSpVecGetValues(basis->descr, (void**)&vals))

    int basisVals[Size];
    // std::cout << name << " has now size: " << vSize << std::endl;
    // for (unsigned int i=0; i<vSize; ++i) {vVals[i] = 0.0f;}
    cudaMemcpy(basisVals, vals, Size * sizeof(val_T), cudaMemcpyDeviceToHost);
    // Debug: print all the values 
    std::cout <<"[device_ptr="<<vals<<",host="<<basisVals<<"] "<<name<<" values are: ";
    unsigned int c=0;
    for (unsigned int i = 0; i < Size; ++i) {
        if (c % 15 == 0)
            std::cout << std::endl;
        std::cout << basisVals[i] << ", ";
        ++c;
    }
    std::cout << std::endl;
}

void TestGatherSolution(const DenseVector* xb, BasisVector* basis, DenseVector::value_t** solution, const int size) {

	*solution = new DenseVector::value_t[size];
	DenseVector::value_t* solution_d;
    std::cout << "Cuda Malloc coming: " << std::endl;
	CHECK_CUDA(cudaMalloc((void **)&solution_d, size*sizeof(DenseVector::value_t)))
    std::cout << "Making zeros" << std::endl;
	makeZeros<<<size, 1>>>(solution_d);
    std::cout << "Getting vals basis" << std::endl;
	int* basisVals_d;
	CHECK_CUSPARSE(cusparseSpVecGetValues((basis->descr), (void **)&basisVals_d))
    std::cout << "Getting vals xb" << std::endl;
	val_T* xbVals_d;
	CHECK_CUSPARSE(cusparseDnVecGetValues(xb->DnDescr, (void**)&xbVals_d))
    std::cout << "setting values" << std::endl;
    cudaDeviceSynchronize();
    std::cout << "Basis: " << std::endl;
    printBasis(basis, "Basis");
    std::cout << std::endl << "xb: " << std::endl;
    printVector(xb, "xb");
	setBasicVals<<<basis->m_, 1>>>(xbVals_d, basisVals_d, solution_d);
    std::cout << "Copying solution" << std::endl;
    cudaDeviceSynchronize();
	cudaMemcpy(*solution, solution_d, size*sizeof(DenseVector::value_t), cudaMemcpyDeviceToHost);
	
}

int main(){

    int m = 4;
    int n = 5;

    BasisVector basis(m, n);

    int newIdxs[m] = {0, 2, 4, 8};
    int* newIdxs_d;
    cudaMalloc((void **)&newIdxs_d, m * sizeof(int));
    cudaMemcpy(newIdxs_d, newIdxs, m * sizeof(int), cudaMemcpyHostToDevice);
    cusparseSpVecSetValues(basis.descr, (void*)newIdxs_d);
    

    val_T xbVals[m];
    for (unsigned i = 0; i < m; i++) {
        xbVals[i] = m-i;
    }

    DenseVector xb(m, xbVals);

    val_T* solution;

    TestGatherSolution(&xb, &basis, &solution, n+m);

    for (int i=0; i < n; i++) {
        if (std::abs(solution[i])<TOL) {
            solution[i]=0.0f;
        }
        std::cout << solution[i] << "\n";
    }
	std::cout << "Slack variables: " << "\n";
	for (int i=0; i < m; i++) {
        if (std::abs(solution[n+i])<TOL) {
            solution[n+i]=0.0f;
        }
        std::cout << solution[n+i] << "\n";
    }

    return 0;
}