#include "lu.cuh"

void performLU(cusparseHandle_t* handle, const SparseMatrix* A, LU** lu) {
    
    // Debug: print the A matrix (should be identity in total for 1. step)
    val_T tmpAVals[A->nnz];
    int tmpARow [A->m+1];
    int tmpACol [A->nnz];
    cudaMemcpy(tmpAVals, A->csrVal, A->nnz * sizeof(val_T), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpARow, A->csrRowPtr, (A->m+1) * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpACol, A->csrColInd, A->nnz * sizeof(int), cudaMemcpyDeviceToHost);
    int rowIA = 0;
    std::cout << std::endl << "A = " << std::endl;
    for (unsigned i = 0; i < A->nnz; i++) {
        std::cout << "[" << tmpARow[rowIA] << "," << tmpACol[i] << "]=" << tmpAVals[i] << "; ";
        if (tmpARow[rowIA+1] == i+1) {
            ++rowIA;
        }
    }
    std::cout << std::endl << std::endl;

    // Create info
    csrilu02Info_t info = 0;
    CHECK_CUSPARSE(cusparseCreateCsrilu02Info(&info))

    // Buffer info
    int pBufferSizeInBytes = 0;
    void* pBuffer = NULL;

    // Query buffer size for the decomposition
    CHECK_CUSPARSE(cusparseScsrilu02_bufferSize(*handle, (int) A->m,
                    (int) A->nnz, A->descr, A->csrVal, (int *) A->csrRowPtr,
                    (int *) A->csrColInd, info, &pBufferSizeInBytes))

    // Allocate pBuffer
    CHECK_CUDA(cudaMalloc((void**)&pBuffer, pBufferSizeInBytes))

    // Do analysis
    const cusparseSolvePolicy_t policy = CUSPARSE_SOLVE_POLICY_NO_LEVEL;
    CHECK_CUSPARSE(cusparseScsrilu02_analysis(*handle, (int) A->m,
                    (int) A->nnz, A->descr, A->csrVal, (int *) A->csrRowPtr,
                    (int *) A->csrColInd, info, policy, pBuffer))

    // Zero pivot stuff? (Dont understand what it does)

    // Do decomposition
    // Again, dont no what a numerical zero is...
    int numerical_zero;

    // Copy values from A to the LU
    CHECK_CUDA(cudaMemcpy((*lu)->csrVal, A->csrVal, A->m * sizeof(val_T), cudaMemcpyDeviceToDevice))

    CHECK_CUSPARSE(cusparseScsrilu02(*handle, (int) A->m, (int) A->nnz,
                    A->descr, (*lu)->csrVal, (int *) A->csrRowPtr, (int *) A->csrColInd,
                    info, policy, pBuffer))

    // Zero pivot stuff? (Dont understand what it does)

    // Free pBuffer
    CHECK_CUDA(cudaFree(pBuffer))

    // Return LU object:
    (*lu)->m = A->m;
    (*lu)->nnz = A->nnz;
    (*lu)->csrRowPtr = A->csrRowPtr;
    (*lu)->csrColInd = A->csrColInd;

    // Debug: print the LU matrix (should be identity in total for 1. step)
    val_T tmpVals[(*lu)->nnz];
    int tmpRow [(*lu)->m+1];
    int tmpCol [(*lu)->nnz];
    cudaMemcpy(tmpVals, (*lu)->csrVal, (*lu)->nnz * sizeof(val_T), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpRow, (*lu)->csrRowPtr, ((*lu)->m+1) * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpCol, (*lu)->csrColInd, (*lu)->nnz * sizeof(int), cudaMemcpyDeviceToHost);
    int rowI = 0;
    std::cout << std::endl << "LU = " << std::endl;
    for (unsigned i = 0; i < (*lu)->nnz; i++) {
        std::cout << "[" << tmpRow[rowI] << "," << tmpCol[i] << "]=" << tmpVals[i] << "; ";
        if (tmpRow[rowI+1] == i+1) {
            ++rowI;
        }
    }
    std::cout << std::endl << std::endl;

}

void LU::solve(cusparseHandle_t* handle, const bool transpose, const DenseVector& rhs, DenseVector& intermediate, DenseVector& x) {

    std::cout << "\n Started solving LU with tranpose(0/1): " << transpose << std::endl;

    // We solve L*U*x = rhs, with z = U*x

    cusparseSpSVDescr_t SpSV_descr_L = 0;
    CHECK_CUSPARSE(cusparseSpSV_createDescr(&SpSV_descr_L))
    cusparseSpSVDescr_t SpSV_descr_U = 0;
    CHECK_CUSPARSE(cusparseSpSV_createDescr(&SpSV_descr_U))

    DenseVector::value_t alpha = 1.0;

    // Intermediate vector (z = U*x or L^T*x)
    // DenseVector z;
    // z.size = this->m;
    // cudaMalloc(&z.values, z.size*sizeof(DenseVector::value_t));
    // z.valueType = CUDA_R_64F;
    // cusparseCreateDnVec(z.descr, z.size, z.values, z.valueType);

    // Debug: print the LU matrix (should be identity in total for 1. step)
    val_T tmpVals[nnz];
    int tmpRow [m+1];
    int tmpCol [nnz];
    cudaMemcpy(tmpVals, csrVal, nnz * sizeof(val_T), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpRow, csrRowPtr, (m+1) * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpCol, csrColInd, nnz * sizeof(int), cudaMemcpyDeviceToHost);
    int rowI = 0;
    std::cout << std::endl << "LU = " << std::endl;
    for (unsigned i = 0; i < nnz; i++) {
        std::cout << "[" << tmpRow[rowI] << "," << tmpCol[i] << "]=" << tmpVals[i] << "; ";
        if (tmpRow[rowI+1] == i+1) {
            ++rowI;
        }
    }
    std::cout << std::endl << std::endl;

    // Debug: print out rhs vector 
    // printVector(&rhs, "cB ");
    val_T tmpVec[rhs.size];
    cudaMemcpy(tmpVec, rhs.values, rhs.size * sizeof(val_T), cudaMemcpyDeviceToHost);
    std::cout << std::endl << "RHS = " << std::endl;
    for (unsigned i = 0 ; i < rhs.size; i++) {
        std::cout << tmpVec[i] << ", ";
    }
    std::cout << "Address of RHS vals: " << rhs.values;
    std::cout << std::endl << std::endl;

    // if transpose == true ...:
    if (transpose) {
        cusparseOperation_t op = CUSPARSE_OPERATION_TRANSPOSE;

        // Query buffer size for both steps
        size_t buffSizeU = 0;
        size_t buffSizeL = 0;
        
        CHECK_CUSPARSE(cusparseSpSV_bufferSize(*handle, op, &alpha, this->descr_U, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U, &buffSizeU))
        CHECK_CUSPARSE(cusparseSpSV_bufferSize(*handle, op, &alpha, this->descr_L, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L, &buffSizeL))

        // Allocate buffers
        void* bufferL = NULL;
        void* bufferU = NULL;
        CHECK_CUDA(cudaMalloc((void**)&bufferL, buffSizeL))
        CHECK_CUDA(cudaMalloc((void**)&bufferU, buffSizeU))

        // Do analysis
        CHECK_CUSPARSE(cusparseSpSV_analysis(*handle, op, &alpha, this->descr_U, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U, bufferU))
        CHECK_CUSPARSE(cusparseSpSV_analysis(*handle, op, &alpha, this->descr_L, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L, bufferL))

        // Solve in order
        CHECK_CUSPARSE(cusparseSpSV_solve(*handle, op, &alpha, this->descr_U, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U))
        CHECK_CUSPARSE(cusparseSpSV_solve(*handle, op, &alpha, this->descr_L, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L))

        // Free workspace buffers
        CHECK_CUDA(cudaFree(bufferL))
        CHECK_CUDA(cudaFree(bufferU))

    } else {
        cusparseOperation_t op = CUSPARSE_OPERATION_NON_TRANSPOSE;

        // Query buffer size for both steps
        size_t buffSizeL = 0;
        size_t buffSizeU = 0;
        
        CHECK_CUSPARSE(cusparseSpSV_bufferSize(*handle, op, &alpha, this->descr_L, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L, &buffSizeL))
        CHECK_CUSPARSE(cusparseSpSV_bufferSize(*handle, op, &alpha, this->descr_U, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U, &buffSizeU))

        std::cout << "\n How much memory it want to allocate for the solving: " << buffSizeL << std::endl << std::endl;

        // Allocate buffers
        void* bufferL = NULL;
        void* bufferU = NULL;
        CHECK_CUDA(cudaMalloc((void**)&bufferL, buffSizeL))
        CHECK_CUDA(cudaMalloc((void**)&bufferU, buffSizeU))

        // Do analysis
        CHECK_CUSPARSE(cusparseSpSV_analysis(*handle, op, &alpha, this->descr_L, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L, bufferL))
        CHECK_CUSPARSE(cusparseSpSV_analysis(*handle, op, &alpha, this->descr_U, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U, bufferU))

        // Solve in order
        CHECK_CUSPARSE(cusparseSpSV_solve(*handle, op, &alpha, this->descr_L, rhs.DnDescr, intermediate.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_L))
        CHECK_CUSPARSE(cusparseSpSV_solve(*handle, op, &alpha, this->descr_U, intermediate.DnDescr, x.DnDescr, this->valueType, CUSPARSE_SPSV_ALG_DEFAULT, SpSV_descr_U))

        // Free workspace buffers
        CHECK_CUDA(cudaFree(bufferL))
        CHECK_CUDA(cudaFree(bufferU))
    }
    printVector(&x, "Solved for x");

    std::cout << "Finished solving LU" << std::endl;
}
