import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import spsolve
import sys
np.set_printoptions(threshold=sys.maxsize)

# Set the size of the matrix and vectors
n = 34

indptr = np.array([0, 2, 4, 5, 7, 10, 14, 17, 20, 24, 28, 32, 35, 39, 43, 47, 52, 56, 60, 64, 68, 72, 76, 80, 84, 88, 92, 96, 100, 104, 108, 112, 115, 119, 123])

indices = np.array([0, 6, 1, 5, 2, 3, 11, 5, 6, 11, 5, 6, 7, 11, 5, 6, 11, 5, 6, 11, 5, 6, 8, 11, 5, 6, 9, 11, 5, 6, 10, 11, 5, 6, 11, 5, 6, 11, 12, 5, 6, 11, 13, 5, 6, 11, 14, 4, 5, 6, 11, 15, 5, 6, 11, 16, 5, 6, 11, 17, 5, 6, 11, 18, 5, 6, 11, 19, 5, 6, 11, 20, 5, 6, 11, 21, 5, 6, 11, 22, 5, 6, 11, 23, 5, 6, 11, 24, 5, 6, 11, 25, 5, 6, 11, 26, 5, 6, 11, 27, 5, 6, 11, 28, 5, 6, 11, 29, 5, 6, 11, 30, 5, 6, 31, 5, 6, 11, 32, 5, 6, 11, 33])

data = np.array([1, -1, 1, -1, 1, 1, 1, -93, -17, 2, -44, -75, 1, 77, -79, -9, 73, -12, -87, 59, -8, -58, 1, 43, -95, -79, 1, 64, -2, -69, 1, 75, -15, -37, 6, -38, -88, 5, 1, -15, -75, 78, 1, -53, -45, 71, 1, 1, -88, -35, 12, 1, -43, -73, 30, 1, -26, -26, 7, 1, -31, -39, 69, 1, -77, -78, 36, 1, -10, -85, 73, 1, -77, -58, 19, 1, -71, -72, 15, 1, -22, -8, 16, 1, -76, -46, 84, 1, -41, -11, 55, 1, -65, -55, 32, 1, -93, -39, 53, 1, -50, -57, 43, 1, -69, -96, 21, 1, -44, -87, 73, 1, -61, -16, 1, -58, -27, 59, 1, -63, -26, 48, 1])

B = csr_matrix((data, indices, indptr), shape=(n,n))

B.toarray()

B.transpose()

cB = np.ndarray(shape=(n, 1), buffer=np.array([0, 0, 0, 0, 1, -759, -786, 1, 1, 1, 1, 649, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]))

# Perform matrix multiplication and subtraction
result = spsolve(B, cB)

# Print vectors and matrix
print(repr(B.nnz))
print("\nrowPtr:")
print(repr(B.indptr))
print("\ncolInd:")
print(repr(B.indices))
print("\nvals:")
print(repr(B.data))
print("\nResult (B⁻1 * cB):")
print(repr(result))
