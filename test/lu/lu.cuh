#ifndef LU_CUH
#define LU_CUH

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

// conversions between cuda and thrust need raw_pointer_cast
#include <thrust/device_ptr.h>
#include <thrust/tuple.h>

#include <cusparse.h>

#include <string>

typedef float val_T;

// For Debuging only
template <typename T, typename S>
void printVector(T* v, S name) {
    uint64_t vSize = v->size;
    float vVals[vSize];
    std::cout << name << " has now size: " << vSize << std::endl;
    for (unsigned int i=0; i<vSize; ++i) {vVals[i] = 0.0f;}
    cudaMemcpy(vVals, v->values, vSize * sizeof(val_T), cudaMemcpyDeviceToHost);
    // Debug: print all the values 
    std::cout << name << " values are: " << std::endl;
    for (unsigned int i = 0; i < vSize; ++i) {
        std::cout << vVals[i] << ", ";
    }
    std::cout << std::endl;
}

// Tolerance for floating point zero checks
#define TOL 1e-10

#define CHECK_CUDA(func)                                                        \
{                                                                               \
    cudaError_t status = (func);                                                \
    if (status != cudaSuccess) {                                                \
        PRINT_RED_PREFIX;                                                       \
        std::cout<<"CUDA API failed with error: ";                              \
        std::cout<<cudaGetErrorString(status)<<" ("<<status<<")"<<std::endl;    \
    }                                                                           \
}

#define CHECK_CUSPARSE(func)                                                    \
{                                                                               \
    cusparseStatus_t status = (func);                                           \
    if (status != CUSPARSE_STATUS_SUCCESS) {                                    \
        PRINT_RED_PREFIX;                                                       \
        std::cout<<"CUSPARSE API failed with error:" ;                          \
        std::cout<<cusparseGetErrorString(status)<<" ("<<status<<")"<<std::endl;\
    }                                                                           \
}


#define PRINT_RED_PREFIX { std::cout << "\033[31m["<< __FILE__ <<"("<< __LINE__ <<")] \033[0m"; }

#define PRINT_PREFIX { std::cout << "\033[32m["<< __FILE__ <<"("<< __LINE__ <<")] \033[0m"; }

#define PRINT_FUNC_PREFIX { std::cout << "\033[32m["<< __FILE__ <<"("<< \
    __LINE__ << ":" << __FUNCTION__ <<")] \033[0m"; }



// Abstract format for a csr matrix (usable in HOST context)
struct SparseMatrix {
    typedef val_T value_t;

    SparseMatrix();
    SparseMatrix(unsigned int dim); // Initailizes Identitymatrix of size dim
    SparseMatrix(unsigned int rows, unsigned int cols, unsigned int nonZeros, int* rowPtr, int* colInd, value_t* val);

    // SparseMatrix(const SparseMatrix* SpMat);
    ~SparseMatrix();

    // Number of rows:
    uint64_t m;
    // Number of cols:
    uint64_t n;
    // Number of non-zero entries:
    uint64_t nnz;
    // Descriptor:
    cusparseMatDescr_t descr;
    // Value array:
    value_t* csrVal;
    // Row pointer arry:
    int* csrRowPtr;
    // Col index array:
    int* csrColInd;
};

// Abstract format for a dense vector
struct DenseVector {
    typedef val_T value_t;     // should be float!

    DenseVector();
    DenseVector(unsigned int n);
    DenseVector(unsigned int n, value_t* val);
    // Copy Constructor:
    DenseVector(const DenseVector& b);
    ~DenseVector();

    // Size:
    uint64_t size;
    // values on GPU
    value_t* values;
    // Descriptor:
    // cusparseDnVecDescr_t* descr;
    cusparseDnVecDescr_t DnDescr;   // use this one!
    // Cuda equivalent to value_t value type (CUDA_R_32F for val_T):
    cudaDataType valueType;
};

//
struct LU {
    typedef val_T value_t;

    // Fill types for L an U:
    cusparseFillMode_t fillModeL = CUSPARSE_FILL_MODE_LOWER;
    cusparseFillMode_t fillModeR = CUSPARSE_FILL_MODE_UPPER;
    // Diag types of L and U:
    cusparseDiagType_t diagTypeL = CUSPARSE_DIAG_TYPE_UNIT;
    cusparseDiagType_t diagTypeR = CUSPARSE_DIAG_TYPE_NON_UNIT;

    // Number of rows/cols:
    uint64_t m;
    // Number of non-zero entries:
    uint64_t nnz;
    // Descriptors:
    cusparseSpMatDescr_t descr_L;
    cusparseSpMatDescr_t descr_U;
    // Value array:
    value_t* csrVal;
    // Row pointer arry:
    int* csrRowPtr;
    // Col index array:
    int* csrColInd;
    // Cuda value type (CUDA_R_32F for val_T):
    cudaDataType valueType = CUDA_R_32F;
    // Cuda index type (CUSPARSE_INDEX_32I for int):
    cusparseIndexType_t indexType = CUSPARSE_INDEX_32I;

    // Constructor:
    LU(uint64_t m, uint64_t nnz, value_t* csrVal, int* csrRowPtr, int* crsColInd) :  m(m), nnz(nnz), csrVal(csrVal), csrRowPtr(csrRowPtr), csrColInd(csrColInd) {
        // Matrix descriptor for L:
        descr_L = 0;
        cusparseCreateCsr(&descr_L, m, m, nnz, csrRowPtr, csrColInd, csrVal, indexType, indexType, CUSPARSE_INDEX_BASE_ZERO, valueType);
        cusparseSpMatSetAttribute(descr_L, CUSPARSE_SPMAT_FILL_MODE, (void*)&fillModeL, sizeof(fillModeL));
        cusparseSpMatSetAttribute(descr_L, CUSPARSE_SPMAT_DIAG_TYPE, (void *)diagTypeL, sizeof(diagTypeL));

        // Matrix descriptor for U:
        descr_U = 0;
        cusparseCreateCsr(&descr_U, m, m, nnz, csrRowPtr, csrColInd, csrVal, indexType, indexType, CUSPARSE_INDEX_BASE_ZERO, valueType);
        cusparseSpMatSetAttribute(descr_U, CUSPARSE_SPMAT_FILL_MODE, (void*)&fillModeR, sizeof(fillModeR));
        cusparseSpMatSetAttribute(descr_U, CUSPARSE_SPMAT_DIAG_TYPE, (void *)diagTypeR, sizeof(diagTypeR));

    }
    LU(const SparseMatrix* A);
    ~LU();

    // Solve fundtion
    void solve(cusparseHandle_t* handle, bool transpose, const DenseVector& rhs, DenseVector& intermediate, DenseVector& x);
};


// Performs incomplete LU of a given matrix A in CRS format
void performLU(cusparseHandle_t* handle, const SparseMatrix* A, LU** lu);

#endif
