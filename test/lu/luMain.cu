#include "simplex.cuh"
#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <string>
#include <cassert>

void expLU () {

    // Suppose that A is m x m sparse matrix represented by CSR format,
    // Assumption:
    // - handle is already created by cusparseCreate(),
    // - (d_csrRowPtr, d_csrColInd, d_csrVal) is CSR of A on device memory,
    // - d_x is right hand side vector on device memory,
    // - d_y is solution vector on device memory.
    // - d_z is intermediate result on device memory.

    val_T vals[9] = {1, 2, 4,
                     3, 8, 14,
                     2, 6, 13};
    
    int rowP[4] = {0, 3, 6, 9};

    int colI[9] = {0, 1, 2, 0, 1, 2, 0, 1, 2};

    SparseMatrix B(3, 3, 9, rowP, colI, vals);

    val_T* d_csrVal;
    CHECK_CUDA(cudaMalloc(&d_csrVal, 9 * sizeof(val_T)))
    CHECK_CUDA(cudaMemcpy(d_csrVal, vals, 9 * sizeof(val_T), cudaMemcpyHostToDevice))

    int* d_csrRowPtr;
    CHECK_CUDA(cudaMalloc(&d_csrRowPtr, 4 * sizeof(int)))
    CHECK_CUDA(cudaMemcpy(d_csrRowPtr, rowP, 4 * sizeof(int), cudaMemcpyHostToDevice))

    int* d_csrColInd;
    CHECK_CUDA(cudaMalloc(&d_csrColInd, 9 * sizeof(int)))
    CHECK_CUDA(cudaMemcpy(d_csrColInd, colI, 9 * sizeof(int), cudaMemcpyHostToDevice))

    cusparseHandle_t handle = NULL;
    CHECK_CUSPARSE(cusparseCreate(&handle))

    int m = 3;
    int nnz = 9;


    cusparseMatDescr_t descr_M = 0;
    //cusparseMatDescr_t descr_L = 0;
    //cusparseMatDescr_t descr_U = 0;
    csrilu02Info_t info_M  = 0;
    //csrsv2Info_t  info_L  = 0;
    //csrsv2Info_t  info_U  = 0;
    int pBufferSize_M;
    // int pBufferSize_L;
    // int pBufferSize_U;
    int pBufferSize;
    void *pBuffer = 0;
    int structural_zero;
    int numerical_zero;
    const double alpha = 1.;
    const cusparseSolvePolicy_t policy_M = CUSPARSE_SOLVE_POLICY_NO_LEVEL;
    // const cusparseSolvePolicy_t policy_L = CUSPARSE_SOLVE_POLICY_NO_LEVEL;
    // const cusparseSolvePolicy_t policy_U = CUSPARSE_SOLVE_POLICY_USE_LEVEL;
    // const cusparseOperation_t trans_L  = CUSPARSE_OPERATION_NON_TRANSPOSE;
    // const cusparseOperation_t trans_U  = CUSPARSE_OPERATION_NON_TRANSPOSE;

    // step 1: create a descriptor which contains
    // - matrix M is base-1
    // - matrix L is base-1
    // - matrix L is lower triangular
    // - matrix L has unit diagonal
    // - matrix U is base-1
    // - matrix U is upper triangular
    // - matrix U has non-unit diagonal
    CHECK_CUSPARSE(cusparseCreateMatDescr(&descr_M))
    CHECK_CUSPARSE(cusparseSetMatIndexBase(descr_M, CUSPARSE_INDEX_BASE_ZERO))
    CHECK_CUSPARSE(cusparseSetMatType(descr_M, CUSPARSE_MATRIX_TYPE_GENERAL))

    // cusparseCreateMatDescr(&descr_L);
    // cusparseSetMatIndexBase(descr_L, CUSPARSE_INDEX_BASE_ONE);
    // cusparseSetMatType(descr_L, CUSPARSE_MATRIX_TYPE_GENERAL);
    // cusparseSetMatFillMode(descr_L, CUSPARSE_FILL_MODE_LOWER);
    // cusparseSetMatDiagType(descr_L, CUSPARSE_DIAG_TYPE_UNIT);

    // cusparseCreateMatDescr(&descr_U);
    // cusparseSetMatIndexBase(descr_U, CUSPARSE_INDEX_BASE_ONE);
    // cusparseSetMatType(descr_U, CUSPARSE_MATRIX_TYPE_GENERAL);
    // cusparseSetMatFillMode(descr_U, CUSPARSE_FILL_MODE_UPPER);
    // cusparseSetMatDiagType(descr_U, CUSPARSE_DIAG_TYPE_NON_UNIT);

    // step 2: create a empty info structure
    // we need one info for csrilu02 and two info's for csrsv2
    CHECK_CUSPARSE(cusparseCreateCsrilu02Info(&info_M))
    // cusparseCreateCsrsv2Info(&info_L);
    // cusparseCreateCsrsv2Info(&info_U);

    std::cout << std::endl << "initialized everything in example" <<std::endl;

    // step 3: query how much memory used in csrilu02 and csrsv2, and allocate the buffer
    CHECK_CUSPARSE(cusparseScsrilu02_bufferSize(handle, m, nnz,
        descr_M, d_csrVal, d_csrRowPtr, d_csrColInd, info_M, &pBufferSize_M))
    // cusparseDcsrsv2_bufferSize(handle, trans_L, m, nnz,
    //     descr_L, d_csrVal, d_csrRowPtr, d_csrColInd, info_L, &pBufferSize_L);
    // cusparseDcsrsv2_bufferSize(handle, trans_U, m, nnz,
    //     descr_U, d_csrVal, d_csrRowPtr, d_csrColInd, info_U, &pBufferSize_U);

    std::cout << std::endl << "queried buffer size" << std::endl;

    pBufferSize = pBufferSize_M;

    // pBuffer returned by cudaMalloc is automatically aligned to 128 bytes.
    CHECK_CUDA(cudaMalloc((void**)&pBuffer, pBufferSize))

    std::cout << std::endl << "allocated buffer" << std::endl;

    // step 4: perform analysis of incomplete Cholesky on M
    //         perform analysis of triangular solve on L
    //         perform analysis of triangular solve on U
    // The lower(upper) triangular part of M has the same sparsity pattern as L(U),
    // we can do analysis of csrilu0 and csrsv2 simultaneously.

    CHECK_CUSPARSE(cusparseScsrilu02_analysis(handle, m, nnz, descr_M,
        d_csrVal, d_csrRowPtr, d_csrColInd, info_M,
        policy_M, pBuffer))

    std::cout << std::endl << "analyzed" << std::endl;

    auto status = cusparseXcsrilu02_zeroPivot(handle, info_M, &structural_zero);
    if (CUSPARSE_STATUS_ZERO_PIVOT == status){
        printf("A(%d,%d) is missing\n", structural_zero, structural_zero);
    }

    // cusparseDcsrsv2_analysis(handle, trans_L, m, nnz, descr_L,
    //     d_csrVal, d_csrRowPtr, d_csrColInd,
    //     info_L, policy_L, pBuffer);

    // cusparseDcsrsv2_analysis(handle, trans_U, m, nnz, descr_U,
    //     d_csrVal, d_csrRowPtr, d_csrColInd,
    //     info_U, policy_U, pBuffer);

    // step 5: M = L * U
    CHECK_CUSPARSE(cusparseScsrilu02(handle, m, nnz, descr_M,
        d_csrVal, d_csrRowPtr, d_csrColInd, info_M, policy_M, pBuffer))

    std::cout << std::endl << "decomposed" << std::endl;
    
    // status = cusparseXcsrilu02_zeroPivot(handle, info_M, &numerical_zero);
    // if (CUSPARSE_STATUS_ZERO_PIVOT == status){
    // printf("U(%d,%d) is zero\n", numerical_zero, numerical_zero);
    // }

    // // step 6: solve L*z = x
    // cusparseDcsrsv2_solve(handle, trans_L, m, nnz, &alpha, descr_L,
    // d_csrVal, d_csrRowPtr, d_csrColInd, info_L,
    // d_x, d_z, policy_L, pBuffer);

    // // step 7: solve U*y = z
    // cusparseDcsrsv2_solve(handle, trans_U, m, nnz, &alpha, descr_U,
    // d_csrVal, d_csrRowPtr, d_csrColInd, info_U,
    // d_z, d_y, policy_U, pBuffer);

    // step 6: free resources
    CHECK_CUDA(cudaFree(pBuffer))
    std::cout << std::endl << "freed buffer" << std::endl << std::endl;
    CHECK_CUSPARSE(cusparseDestroyMatDescr(descr_M))
    // cusparseDestroyMatDescr(descr_L);
    // cusparseDestroyMatDescr(descr_U);
    CHECK_CUSPARSE(cusparseDestroyCsrilu02Info(info_M))
    // cusparseDestroyCsrsv2Info(info_L);
    // cusparseDestroyCsrsv2Info(info_U);

    // Debug: print the LU matrix (should be identity in total for 1. step)
    val_T tmpVals[nnz];
    int tmpRow [m+1];
    int tmpCol [nnz];
    cudaMemcpy(tmpVals, d_csrVal, nnz * sizeof(val_T), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpRow, d_csrRowPtr, (m+1) * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpCol, d_csrColInd, nnz * sizeof(int), cudaMemcpyDeviceToHost);
    int rowI = 0;
    std::cout << std::endl << "LU = " << std::endl;
    for (unsigned i = 0; i < nnz; i++) {
        std::cout << "[" << rowI << "," << tmpCol[i] << "]=" << tmpVals[i] << "; ";
        if (tmpRow[rowI+1] == i+1) {
            ++rowI;
        }
    }
    std::cout << std::endl << std::endl;

    LU decomp;

    cudaFree(decomp.csrColInd);
    cudaFree(decomp.csrRowPtr);
    cudaFree(decomp.csrVal);
    cudaMalloc(&decomp.csrColInd, nnz * sizeof(int));
    cudaMalloc(&decomp.csrRowPtr, (m+1) * sizeof(int));
    cudaMalloc(&decomp.csrVal, nnz * sizeof(val_T));

    cudaMemcpy(decomp.csrRowPtr, d_csrRowPtr, (m+1) * sizeof(int), cudaMemcpyDeviceToDevice);
    cudaMemcpy(decomp.csrColInd, d_csrColInd, nnz * sizeof(int), cudaMemcpyDeviceToDevice);
    cudaMemcpy(decomp.csrVal, d_csrVal, nnz * sizeof(val_T), cudaMemcpyDeviceToDevice);

    // Fill types for L an U:
    cusparseFillMode_t fillModeL = CUSPARSE_FILL_MODE_LOWER;
    cusparseFillMode_t fillModeR = CUSPARSE_FILL_MODE_UPPER;
    // Diag types of L and U:
    cusparseDiagType_t diagTypeL = CUSPARSE_DIAG_TYPE_UNIT;
    cusparseDiagType_t diagTypeR = CUSPARSE_DIAG_TYPE_NON_UNIT;

    decomp.descr_L = 0;
    CHECK_CUSPARSE(cusparseCreateCsr(&(decomp.descr_L), m, m, nnz, (void *)decomp.csrRowPtr, (void *)decomp.csrColInd, (void *)decomp.csrVal, CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I, CUSPARSE_INDEX_BASE_ZERO, CUDA_R_32F))
    PRINT_PREFIX;
    std::cout << "Created L descriptor" << std::endl;
    CHECK_CUSPARSE(cusparseSpMatSetAttribute(decomp.descr_L, CUSPARSE_SPMAT_FILL_MODE, &fillModeL, sizeof(fillModeL)))
    PRINT_PREFIX;
    std::cout << "L FillType set" << std::endl;
    CHECK_CUSPARSE(cusparseSpMatSetAttribute(decomp.descr_L, CUSPARSE_SPMAT_DIAG_TYPE, &diagTypeL, sizeof(diagTypeL)))
    PRINT_PREFIX;
    std::cout << "L Matrix generated" << std::endl;


    // Matrix descriptor for U:
    decomp.descr_U = 0;
    CHECK_CUSPARSE(cusparseCreateCsr(&(decomp.descr_U), m, m, nnz, (void *)decomp.csrRowPtr, (void *)decomp.csrColInd, (void *)decomp.csrVal, CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I, CUSPARSE_INDEX_BASE_ZERO, CUDA_R_32F))
    CHECK_CUSPARSE(cusparseSpMatSetAttribute(decomp.descr_U, CUSPARSE_SPMAT_FILL_MODE, &fillModeR, sizeof(fillModeR)))
    CHECK_CUSPARSE(cusparseSpMatSetAttribute(decomp.descr_U, CUSPARSE_SPMAT_DIAG_TYPE, &diagTypeR, sizeof(diagTypeR)))
    PRINT_PREFIX;
    std::cout << "U Matrix generated" << std::endl;

    val_T rhsVals[3] = {3, 13, 4};
    DenseVector rhs(3, rhsVals);

    DenseVector inter(3);
    DenseVector res(3);

    decomp.solve(&handle, false, rhs, inter, res);

    val_T* resV;
    cusparseDnVecGetValues(res.DnDescr, (void **)&resV);

    // DEBUG -------------------------------------------------
		std::cout << "testing res vector:" << std::endl;
		uint64_t testResSize = res.size;
		float testResVals[testResSize];
		std::cout << "res has now size: " << testResSize << std::endl;
		for (unsigned int i=0; i<testResSize; ++i) {testResVals[i] = 0.0f;}
		cudaMemcpy(testResVals, resV, testResSize*sizeof(val_T), cudaMemcpyDeviceToHost);
		// Debug: print all the values 
		std::cout << " res values are: " << std::endl;
		for (unsigned int i = 0; i < testResSize; ++i) {
			std::cout << testResVals[i] << ", ";
		}
		std::cout << "Adress of res vals: " << resV << std::endl;
		std::cout << std::endl;
	// DEBUG END ---------------------------------------------

    CHECK_CUSPARSE(cusparseDestroy(handle))
    CHECK_CUDA(cudaFree(d_csrRowPtr))
    CHECK_CUDA(cudaFree(d_csrColInd))
    CHECK_CUDA(cudaFree(d_csrVal))
}

int main (int argc, char** argv) {

    expLU();

    val_T vals[9] = {1, 2, 4,
                     3, 8, 14,
                     2, 6, 13};
    
    int rowP[4] = {0, 3, 6, 9};

    int colI[9] = {0, 1, 2, 0, 1, 2, 0, 1, 2};

    SparseMatrix B(3, 3, 9, rowP, colI, vals);

    cusparseHandle_t handle = NULL;
    cusparseCreate(&handle);

    LU decomp;

    decomp.performLU(&handle, &B);

    // Debug: print the LU matrix (should be identity in total for 1. step)
    val_T tmpVals[decomp.nnz];
    int tmpRow [decomp.m+1];
    int tmpCol [decomp.nnz];
    cudaMemcpy(tmpVals, decomp.csrVal, decomp.nnz * sizeof(val_T), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpRow, decomp.csrRowPtr, (decomp.m+1) * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(tmpCol, decomp.csrColInd, decomp.nnz * sizeof(int), cudaMemcpyDeviceToHost);
    int rowI = 0;
    std::cout << std::endl << "LU = " << std::endl;
    for (unsigned i = 0; i < decomp.nnz; i++) {
        std::cout << "[" << rowI << "," << tmpCol[i] << "]=" << tmpVals[i] << "; ";
        if (tmpRow[rowI+1] == i+1) {
            ++rowI;
        }
    }
    std::cout << std::endl << std::endl;

    val_T rhsVals[3] = {3, 13, 4};
    DenseVector rhs(3, rhsVals);

    DenseVector inter(3);
    DenseVector res(3);

    decomp.solve(&handle, false, rhs, inter, res);

    val_T* resV;
    cusparseDnVecGetValues(res.DnDescr, (void **)&resV);

    // DEBUG -------------------------------------------------
		std::cout << "testing res vector:" << std::endl;
		uint64_t testResSize = res.size;
		float testResVals[testResSize];
		std::cout << "res has now size: " << testResSize << std::endl;
		for (unsigned int i=0; i<testResSize; ++i) {testResVals[i] = 0.0f;}
		cudaMemcpy(testResVals, resV, testResSize*sizeof(val_T), cudaMemcpyDeviceToHost);
		// Debug: print all the values 
		std::cout << " res values are: " << std::endl;
		for (unsigned int i = 0; i < testResSize; ++i) {
			std::cout << testResVals[i] << ", ";
		}
		std::cout << "Adress of res vals: " << resV << std::endl;
		std::cout << std::endl;
	// DEBUG END ---------------------------------------------



    return 0;
}