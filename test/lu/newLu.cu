#include "simplex.cuh"

void luSolveT(cusolverSpHandle_t& solvHandle, cusparseHandle_t& handle, const SparseMatrix& B, const DenseVector& b, const bool transpose, DenseVector& sol) {

    std::cout << "Started lu" << std::endl;

    // Get values of matrix
    val_T* B_vals;
    int* B_rowP;
    int* B_colI;
    int64_t m;
    int64_t nnz;
    cusparseIndexType_t dummyRIT;
    cusparseIndexType_t dummyCIT;
    cusparseIndexBase_t dummyIB;
    cudaDataType dummyDT;
    cusparseCsrGet(B.spDescr, &m, &m, &nnz, (void **)&B_rowP, (void **)&B_colI, (void **)&B_vals, &dummyRIT, &dummyCIT, &dummyIB, &dummyDT);

    std::cout << "Got Matrix" << std::endl;

    // Transpose if necessary
    if (transpose) {
        val_T* B_vals_T;
        int* B_rowP_T;
        int* B_colI_T;
        size_t bufferSize;
        void* buffer;
        cudaMalloc((void **)&B_vals_T, nnz * sizeof(val_T));
        cudaMalloc((void **)&B_rowP_T, (m+1) * sizeof(int));
        cudaMalloc((void **)&B_colI_T, nnz * sizeof(int));
        cusparseCsr2cscEx2_bufferSize(handle, m, m, nnz, B_vals, B_rowP, B_colI, B_vals_T, B_rowP_T, B_colI_T, dummyDT, CUSPARSE_ACTION_NUMERIC, dummyIB, CUSPARSE_CSR2CSC_ALG1, &bufferSize);
        std::cout << "Queried workspace for transpose" << std::endl;
        cudaMalloc((void **)&buffer, bufferSize);
        cusparseCsr2cscEx2(handle, m, m, nnz, B_vals, B_rowP, B_colI, B_vals_T, B_rowP_T, B_colI_T, dummyDT, CUSPARSE_ACTION_NUMERIC, dummyIB, CUSPARSE_CSR2CSC_ALG1, buffer);
        std::cout << "Transposed Matrix" << std::endl;

        B_vals = B_vals_T;
        B_rowP = B_rowP_T;
        B_colI = B_colI_T;
    }

    // Get values of vector
    val_T* b_vals;
    cusparseDnVecGetValues(b.DnDescr, (void **)&b_vals);

    std::cout << "Got vector" << std::endl;

    // LU
    val_T* sol_vals;
    cudaMalloc((void **)&sol_vals, m * sizeof(val_T));

    std::cout << "Malloced sol" << std::endl;

    val_T B_vals_h[nnz];
    int B_rowP_h[m+1];
    int B_colI_h[nnz];
    cudaMemcpy(B_vals_h, B_vals, nnz * sizeof(val_T), cudaMemcpyDeviceToHost);
    cudaMemcpy(B_rowP_h, B_rowP, (m+1) * sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(B_colI_h, B_colI, nnz * sizeof(int), cudaMemcpyDeviceToHost);
    val_T b_vals_h[m];
    cudaMemcpy(b_vals_h, b_vals, m * sizeof(val_T), cudaMemcpyDeviceToHost);
    val_T sol_vals_h[m];

    int singularity;
    cusolverStatus_t status = cusolverSpScsrlsvluHost(solvHandle, m, nnz, B.descr, B_vals_h, B_rowP_h, B_colI_h, b_vals_h, TOL, 0, sol_vals_h, &singularity);

    std::cout << status << std::endl;

    // Copy solution in DenseVector
    cudaMemcpy(sol_vals, sol_vals_h, m * sizeof(val_T), cudaMemcpyHostToDevice);
    cusparseDnVecSetValues(sol.DnDescr, (void *)sol_vals);

    if (transpose) {
        cudaFree(B_vals);
        cudaFree(B_colI);
        cudaFree(B_rowP);
    }

}

int main() {

    cusolverSpHandle_t solvHandle;
    cusolverSpCreate(&solvHandle);

    cusparseHandle_t handle;
    cusparseCreate(&handle);

    val_T vals[9] = {1, 2, 4,
                     3, 8, 14,
                     2, 6, 13};
    
    int rowP[4] = {0, 3, 6, 9};

    int colI[9] = {0, 1, 2, 0, 1, 2, 0, 1, 2};

    val_T vals_T[9] = {1, 3, 2,
                       2, 8, 6,
                       4, 14, 13};

    int rowP_T[4] = {0, 3, 6, 9};

    int colI_T[9] = {0, 1, 2, 0, 1, 2, 0, 1, 2};

    val_T bVals[3] = {3, 13, 4};

    SparseMatrix B(3, 3, 9, rowP, colI, vals);

    SparseMatrix B_T(3, 3, 9, rowP_T, colI_T, vals_T);

    DenseVector b(3, bVals);

    DenseVector sol(3);

    DenseVector sol_T(3);

    luSolveT(solvHandle, handle, B, b, false, sol);

    luSolveT(solvHandle, handle, B_T, b, true, sol_T);

    cusolverSpDestroy(solvHandle);

    printVector(&sol, "Solution");

    printVector(&sol_T, "Transpose Solution");


    return 0;
}
