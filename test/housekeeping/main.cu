#include "simplex.cuh"
#include <thrust/sort.h>

void dense_vector_print(const DenseVector c);

int main (int argc, char** argv) {
    // TEST print
    {
        BasisVector b{12,12};
        PRINT_FUNC_PREFIX;
        std::cout <<"cuda err="<<cudaGetLastError();
        b.print_vecs();

        b.print();
    }
    PRINT_PREFIX;
    std::cout<<"destroyed... b ";
    std::cout <<"cuda err="<<cudaGetLastError();
    //thrust::device_vector<val_T> thr_ds_y{host_ds_y};

    // TEST WriteVectorAtIndices and UpdateBasis
    /*{
        val_T c_values[]={0,6,9,2.5,6};
        val_T d_values[]={-1,-1,-1,-1,-1};
        BasisVector b{sizeof(c_values)/sizeof(c_values[0])};

        DenseVector *c = new DenseVector(sizeof(c_values)/sizeof(c_values[0]), c_values);
        DenseVector *d = new DenseVector(sizeof(d_values)/sizeof(d_values[0]), d_values);
        PRINT_PREFIX;
        printVector(c, "c");
        PRINT_PREFIX;
        printVector(d, "d");

        PRINT_PREFIX;
        std::cout<<"starting cusparse"<<std::endl;
	    cusparseHandle_t handle = NULL;
        cusparseCreate(&handle);


        PRINT_PREFIX;
        std::cout<<"before WriteVectorAtIndexes"<<std::endl;
        WriteVectorAtIndexes(&handle,b,d,c);
        PRINT_PREFIX;
        std::cout<<"after WriteVectorAtIndexes"<<std::endl;

        PRINT_PREFIX;
        printVector(c, "c");
        PRINT_PREFIX;
        printVector(d, "d");

        val_T A_vals[9] = {4, 9, 15,
                         2, 3, 5,
                         3, 7, 14};
        val_T B_vals[9] = {3, 8, 14,
                         1, 2, 4,
                         2, 6, 13};
        int rowP[4] = {0, 3, 6, 9};
        int colI[9] = {0, 1, 2, 0, 1, 2, 0, 1, 2};
        PRINT_PREFIX;
        std::cout<<"creating SparseMatrix A"<<std::endl;
        SparseMatrix A(3, 3, 9, rowP, colI, A_vals);
        std::cout<<"creating SparseMatrix B"<<std::endl;
        SparseMatrix B(3, 3, 9, rowP, colI, B_vals);

        UpdateBasis(&handle, b, (unsigned int) 0, (unsigned int) 4, A, B);

        delete d;
        delete c;
    }*/

    // TEST GetCol
    {
        val_T c_values[]={-1,-1,-1,-1,-1};
        BasisVector b{sizeof(c_values)/sizeof(c_values[0]),4};

        DenseVector *c = new DenseVector(sizeof(c_values)/sizeof(c_values[0]), c_values);
        PRINT_PREFIX;
        printVector(c, "c");

        PRINT_PREFIX;
        std::cout<<"starting cusparse"<<std::endl;
	    cusparseHandle_t handle = NULL;
        cusparseCreate(&handle);


        val_T A_vals[9] = {4, 9, 15,
                         2, 3, 5,
                         3, 7, 14};
        int rowP[4] = {0, 3, 6, 9};
        int colI[9] = {0, 1, 2, 0, 1, 2, 0, 1, 2};
        PRINT_PREFIX;
        std::cout<<"creating SparseMatrix A"<<std::endl;
        SparseMatrix *A=new SparseMatrix{3, 3, 9, rowP, colI, A_vals};

        for (unsigned int i:{0,2,1}) {
            GetCol(&handle,A,c,i);
        
            PRINT_PREFIX;
            std::cout<<"Column "<<i<<" of matrix A:"<<std::endl;
            printVector(c, "c");
        }

        delete c;
    }

}
