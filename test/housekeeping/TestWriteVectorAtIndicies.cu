#include "simplex.cuh"
// #include <__clang_cuda_runtime_wrapper.h>
#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <string>
#include <cassert>
#include <cstdlib>
#include <random>

#define useThreadsPerBlock 1024


template <typename T>
bool testCSRRowPtr(const T* rowPtr, const int rows, const int nnz) {
    for (int i=1; i<rows+1; ++i) {
        if (rowPtr[i-1] > rowPtr[i]){
            std::cout << "rowPtr " << i << " is smaller than rowPtr " << i-1 << "  (" << rowPtr[i] << "<" << rowPtr[i-1] << ")" << std::endl;
            return false;
        }
    }

    if (rowPtr[0] != 0) {
        std::cout << "rowPtr[0]=" << rowPtr[0] << "  should be 0" << std::endl;
        return false;
    }

    if (rowPtr[rows] != nnz) {
        std::cout << "rowPtr[rows]=" << rowPtr[rows] << "  should be nnz=" << nnz << std::endl; 
        return false;
    }

    return true;
}

template <typename T>
bool testCSRColInd(const T* rowPtr, const T* colInd, const int rows, const int cols, const int nnz) {
    for (int i=1; i<rows+1; ++i) {
        int lastCol = -1;
        for (int j=rowPtr[i-1]; j<rowPtr[i]; ++j) {
            if (colInd[j] < 0 || colInd[j] >= cols) {
                std::cout << "colInd out of bounds at " << j << " value is=" << colInd[j] << std::endl;
                return false;
            }
            if (lastCol >= colInd[j]) {
                std::cout << "colInd ordering wrong at " << j << " value is=" << colInd[j] << std::endl;
                return false;
            }
            lastCol = colInd[j];
        }
    }

    return true;
}

template <typename T>
void printSpMatAsCSC(cusparseHandle_t* handle, T* M, std::string name="") {
    int64_t h_n, h_m, h_nnz;
    int* d_csrRowPtr;
    int* d_csrColInd;
    val_T* d_csrVal;

    cusparseIndexType_t h_rowPtrType, h_colIndType;
    cusparseIndexBase_t h_idxBase;
    cudaDataType h_valueType;

    CHECK_CUSPARSE(cusparseCsrGet(M->spDescr,
                                &h_m,
                                &h_n,
                                &h_nnz,
                                (void**)&d_csrRowPtr,
                                (void**)&d_csrColInd,
                                (void**)&d_csrVal,
                                &h_rowPtrType,
                                &h_colIndType,
                                &h_idxBase,
                                &h_valueType))
    
        //transform B into CSC
    int* d_cscColPtr;
    int* d_cscRowInd;
    val_T* d_cscVal;

    CHECK_CUDA(cudaMalloc((void**)&d_cscColPtr, (h_n + 1) * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_cscRowInd, h_nnz     * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_cscVal,    h_nnz     * sizeof(val_T)))
    
    // Buffer info
    size_t pBufferSize;
    void* pBuffer = NULL;

    cusparseCsr2CscAlg_t algo{CUSPARSE_CSR2CSC_ALG1};
    
    CHECK_CUSPARSE(cusparseCsr2cscEx2_bufferSize(*handle,
                                                 h_m,
                                                 h_n,
                                                 h_nnz,
                                                 d_csrVal,
                                                 d_csrRowPtr,
                                                 d_csrColInd,
                                                 d_cscVal,
                                                 d_cscColPtr,
                                                 d_cscRowInd,
                                                 h_valueType,
                                                 CUSPARSE_ACTION_NUMERIC,
                                                 h_idxBase,
                                                 algo,
                                                 &pBufferSize))
    
    // Allocate pBuffer
    CHECK_CUDA(cudaMalloc((void**)&pBuffer, pBufferSize))
    
    CHECK_CUSPARSE(cusparseCsr2cscEx2(*handle,
                                    h_m,
                                    h_n,
                                    h_nnz,
                                    d_csrVal,
                                    d_csrRowPtr,
                                    d_csrColInd,
                                    d_cscVal,
                                    d_cscColPtr,
                                    d_cscRowInd,
                                    h_valueType,
                                    CUSPARSE_ACTION_NUMERIC,
                                    h_idxBase,
                                    algo,
                                    pBuffer))

    CHECK_CUDA(cudaDeviceSynchronize())

    CHECK_CUDA(cudaFree(pBuffer))

    std::cout << "printing Sparse Matrix (CSC)" << name << "\n" << \
                "m (rows) = " << h_m << "\n" << \
                "n (cols) = " << h_n << "\n" << \
                "nnz      = " << h_nnz << std::endl;
    
    val_T h_vals[h_nnz];
    CHECK_CUDA(cudaMemcpy(h_vals, d_cscVal, h_nnz * sizeof(val_T), cudaMemcpyDeviceToHost));

    int h_rowInd[h_nnz];
    CHECK_CUDA(cudaMemcpy(h_rowInd, d_cscRowInd, h_nnz * sizeof(int), cudaMemcpyDeviceToHost));

    int h_colPtr[h_n + 1];
    CHECK_CUDA(cudaMemcpy(h_colPtr, d_cscColPtr, (h_n + 1) * sizeof(int), cudaMemcpyDeviceToHost));

    if (!testCSRRowPtr(h_colPtr, h_n, h_nnz) ||
        !testCSRColInd(h_colPtr, h_rowInd, h_n, h_m, h_nnz)) {
        std::cout << "numRows=" << h_m << " numCols=" << h_n << " nnz=" << h_nnz << std::endl;
        printArr(h_colPtr, h_n+1, "colPtr");
        printArr(h_rowInd, h_nnz, "rowInd");
        printArr(h_vals, h_nnz, "val");

        throw "wrong";
    }

    std::cout << "colPtr=" << std::endl;
    for (unsigned int i=0;  i<h_n + 1; ++i) {
        std::cout << h_colPtr[i] << ", ";
    }   std::cout << std::endl;

    std::cout << "rowInd=" << std::endl;
    for (unsigned int i=0; i<h_nnz; ++i) {
        std::cout << h_rowInd[i] << ", ";
    }   std::cout << std::endl;

    std::cout << "vals=" << std::endl;
    for (unsigned int i=0; i<h_nnz; ++i) {
        std::cout << h_vals[i] << ", ";
    }   std::cout << std::endl;
    
    CHECK_CUDA(cudaFree(d_cscColPtr))
    CHECK_CUDA(cudaFree(d_cscRowInd))
    CHECK_CUDA(cudaFree(d_cscVal))
}


int randScalar(const int min=2, const int max=8) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(min, max); // define the range

    return distr(gen);
}

void randVals(val_T* vals, unsigned int size) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(25, 63); // define the range

    for (unsigned int i=0; i<size; ++i) {
        vals[i] = distr(gen);
    }
}

void randValsInt(int* vals, unsigned int size, int max) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(0, max); // define the range

    for (unsigned int i=0; i<size; ++i) {
        vals[i] = distr(gen);
    }
}

void randRowPtr(int* rowPtr, int size, int max=2) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::exponential_distribution<> d(1);

    rowPtr[0] = 0;
    
    for (int i=1; i<size+1; ++i) {
        rowPtr[i] = rowPtr[i-1] + std::max(std::min((int)d(gen), max), 1);
    }
}

void randColInd_Val(const int* rowPtr, int* colInd, val_T* val, int numRows, int numCols) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr_ind(0, numCols-1); // define the range
    std::uniform_int_distribution<> distr_val(1, 10);

    for (int i=0; i<numRows; ++i) {
        int min_col = 0;
        for (int j=rowPtr[i]; j < rowPtr[i+1]; ++j) {
            int v = distr_ind(gen);
            int max = (numCols - min_col) - (rowPtr[i+1] - j);
            int dcol = std::min(v, max);

            colInd[j] = min_col + dcol;

            // std::cout << "max=" << max << " ,remaining=" << rowPtr[i+1]-j << " ,sum(should be numCol-1)=" << min_col+max << std::endl;

            // assert(max < numCols);
            assert(dcol >= 0 && dcol < numCols);
            // assert(min_col+max==numCols-1);

            val[j] = distr_val(gen);
            min_col = min_col + dcol + 1;
        }
    }

    int nnz = rowPtr[numRows];

    if (!testCSRRowPtr(rowPtr, numRows, nnz) ||
        !testCSRColInd(rowPtr, colInd, numRows, numCols, nnz)) {
        std::cout << "numRows=" << numRows << " numCols=" << numCols << " nnz=" << nnz << std::endl;
        printArr(rowPtr, numRows+1, "rowPtr");
        printArr(colInd, nnz, "colInd");
        printArr(val, nnz, "val");

        throw "wrong";
    }
}

bool compDenseVector(const DenseVector* a, const DenseVector* b, const float tol=10e-4) {
    if (a->size != b->size)
        return false;

    int size = a->size;

    val_T *d_a_vals, *d_b_vals;
    CHECK_CUSPARSE(cusparseDnVecGetValues(a->DnDescr, (void**)&d_a_vals))
    CHECK_CUSPARSE(cusparseDnVecGetValues(b->DnDescr, (void**)&d_b_vals))

    val_T h_a_vals[size];
    val_T h_b_vals[size];

    CHECK_CUDA(cudaMemcpy(h_a_vals, d_a_vals, size * sizeof(val_T), cudaMemcpyDeviceToHost))
    CHECK_CUDA(cudaMemcpy(h_b_vals, d_b_vals, size * sizeof(val_T), cudaMemcpyDeviceToHost))

    for (int i=0; i<size; ++i) {
        if (std::abs( h_a_vals[i] - h_b_vals[i] ) > tol)
            return false;
    }
    
    return true;
}


// ----------------------------------------------------------
//              NEW write vector at indicies
// ----------------------------------------------------------


// overwrites dest[i] with src[j] if i exists in idxs
// size is size of the idxs array
__global__ void TESToverwriteAtIdxs_Kernel(val_T* dest, const val_T* src, const int* idxs, const int size, const int numCols) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < size) {
        int j = idxs[i];
        if (j < numCols)
            dest[i] = src[j];
        else
            dest[i] = 0.0;
    }
}

// overwrites ds_Y with values from ds_A based on basis idxs (if idx does not exist in basis value at that idx is not overwritten)
void TESTwriteVectorAtIndices(cusparseHandle_t *handle, const BasisVector* basis,
                              DenseVector* cB, const DenseVector* c) {

    int64_t             size;
    int64_t             nnz;
    int*                indices;
    int*                values;
    cusparseIndexType_t idxType;
    cusparseIndexBase_t idxBase;
    cudaDataType        valueType;

    CHECK_CUSPARSE(cusparseSpVecGet(basis->descr,
                        &size,
                        &nnz,
                        (void**)&indices,
                        (void**)&values,
                        &idxType,
                        &idxBase,
                        &valueType))

    int gridDim = 1;
    int blockDim = cB->size;
    if (blockDim > useThreadsPerBlock) {
        gridDim = std::ceil(((float)blockDim) / useThreadsPerBlock);
        blockDim = useThreadsPerBlock;
    }
    std::cout << values << std::endl;
    TESToverwriteAtIdxs_Kernel<<<gridDim, blockDim>>>(cB->values, c->values, values, cB->size, c->size);
    CHECK_CUDA(cudaDeviceSynchronize())
}



// ----------------------------------------------------------
//              END write vector at indicies
// ----------------------------------------------------------



void testWriteVecAtIndicies(cusparseHandle_t* handle, const int numIter, const int min=2, const int max=10) {
    for (int i=0; i<numIter; ++i) {
        
        unsigned int n = randScalar(min, max);
        unsigned int m = randScalar(min, max);
        unsigned int basis_sz = randScalar(0, n);

        val_T c_vals[n];
        randVals(c_vals, n);

        val_T cB_vals[m];
        randVals(cB_vals, m);

        int basVals[m];
        randValsInt(basVals, m, m+n);
        int* basVals_d;
        cudaMalloc((void **)&basVals_d, m * sizeof(int));
        cudaMemcpy(basVals_d, basVals, m*sizeof(int), cudaMemcpyHostToDevice);

        DenseVector* c = new DenseVector(n, c_vals);
        DenseVector* cB = new DenseVector(m, cB_vals);

        BasisVector* basis = new BasisVector(m, n);
        cusparseSpVecSetValues(basis->descr, (void *)basVals_d);

        basis->print();

        std::cout << "Start test" << std::endl;
        TESTwriteVectorAtIndices(handle, basis,
                                 cB, c);

        printVector(c, "c", true);
        printVector(cB, "cB", true);
        std::cout << std::endl << std::endl;

        // bool passed = true;
        // DenseVector* col = new DenseVector(numRows);
        // for (unsigned int j=0; j<numCols; ++j) {
        //     getCol(handle, A, col, j);

        //     if (!compCol(A, col, j)) {
        //         if (passed) {
        //             std::cout << "\n\n\n --- ITERATION : " << i << " ----\n" << std::endl;
        //             std::cout << "rows=" << numRows << "  ,  cols=" << numCols << std::endl;
        //         }
        //         std::cout << "column " << j << std::endl;
        //         printVectorAsSpVec(col, std::to_string(j));
        //         passed = false;
        //     }
        // }

        // if (!passed) {
        //     printSpMat(A, " A");
        // } else {
        //     std::cout << "passed iter: " << i << std::endl;
        // }

        delete c;
        delete cB;
        delete basis;
    }
}



int main (int argc, char** argv) {

    cusparseHandle_t handle = NULL;
    cusparseCreate(&handle);

    int numIter = 100;
    if (argc > 1) {
        numIter = atoi(argv[1]);
        if (numIter <= 0) {
            std::cout << "nonvalid input for num iterations. Should be Integer bigger than 0" << std::endl;
            return 0;
        }
    }

    int min = 2;
    if (argc > 2) {
        min = atoi(argv[2]);
        if (min <= 0) {
            std::cout << "nonvalid input for minimum Dimension. Should be Integer bigger than 0" << std::endl;
            return 0;
        }
    }

    int max = 10;
    if (argc > 3) {
        max = atoi(argv[3]);
        if (max < min) {
            std::cout << "nonvalid input for maximum Dimension. Should be Integer bigger than Minimum Dimension (" << min << ")" << std::endl;
            return 0;
        }
    }

    int nDevices;

    cudaGetDeviceCount(&nDevices);
    for (int i = 0; i < nDevices; i++) {
        cudaDeviceProp prop;
        cudaGetDeviceProperties(&prop, i);
        printf("Device Number: %d\n", i);
        printf("  Device name: %s\n", prop.name);
        printf("  Memory Clock Rate (KHz): %d\n",
            prop.memoryClockRate);
        printf("  Memory Bus Width (bits): %d\n",
            prop.memoryBusWidth);
        printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
            2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
        printf("  max Threads per Block: %d\n",
            prop.maxThreadsPerBlock);
    }
    
    std::cout << "START test Write Vector At Indicies\n\n" << std::endl;
    testWriteVecAtIndicies(&handle, numIter, min, max);
    std::cout << "\n\nFINISH test Write Vector At Indiicies" << std::endl;

    return 0;
}