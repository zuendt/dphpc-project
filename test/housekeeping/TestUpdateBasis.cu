#include "simplex.cuh"
// #include <__clang_cuda_runtime_wrapper.h>
#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <string>
#include <cassert>
#include <cstdlib>
#include <random>


template <typename T>
bool testCSRRowPtr(const T* rowPtr, const int rows, const int nnz) {
    for (int i=1; i<rows+1; ++i) {
        if (rowPtr[i-1] > rowPtr[i]){
            std::cout << "rowPtr " << i << " is smaller than rowPtr " << i-1 << "  (" << rowPtr[i] << "<" << rowPtr[i-1] << ")" << std::endl;
            return false;
        }
    }

    if (rowPtr[0] != 0) {
        std::cout << "rowPtr[0]=" << rowPtr[0] << "  should be 0" << std::endl;
        return false;
    }

    if (rowPtr[rows] != nnz) {
        std::cout << "rowPtr[rows]=" << rowPtr[rows] << "  should be nnz=" << nnz << std::endl; 
        return false;
    }

    return true;
}

template <typename T>
bool testCSRColInd(const T* rowPtr, const T* colInd, const int rows, const int cols, const int nnz) {
    for (int i=1; i<rows+1; ++i) {
        int lastCol = -1;
        for (int j=rowPtr[i-1]; j<rowPtr[i]; ++j) {
            if (colInd[j] < 0 || colInd[j] >= cols) {
                std::cout << "colInd out of bounds at " << j << " value is=" << colInd[j] << std::endl;
                return false;
            }
            if (lastCol >= colInd[j]) {
                std::cout << "colInd ordering wrong at " << j << " value is=" << colInd[j] << std::endl;
                return false;
            }
            lastCol = colInd[j];
        }
    }

    return true;
}

template <typename T>
void printSpMatAsCSC(cusparseHandle_t* handle, T* M, std::string name="") {
    int64_t h_n, h_m, h_nnz;
    int* d_csrRowPtr;
    int* d_csrColInd;
    val_T* d_csrVal;

    cusparseIndexType_t h_rowPtrType, h_colIndType;
    cusparseIndexBase_t h_idxBase;
    cudaDataType h_valueType;

    CHECK_CUSPARSE(cusparseCsrGet(M->spDescr,
                                &h_m,
                                &h_n,
                                &h_nnz,
                                (void**)&d_csrRowPtr,
                                (void**)&d_csrColInd,
                                (void**)&d_csrVal,
                                &h_rowPtrType,
                                &h_colIndType,
                                &h_idxBase,
                                &h_valueType))
    
        //transform B into CSC
    int* d_cscColPtr;
    int* d_cscRowInd;
    val_T* d_cscVal;

    CHECK_CUDA(cudaMalloc((void**)&d_cscColPtr, (h_n + 1) * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_cscRowInd, h_nnz     * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_cscVal,    h_nnz     * sizeof(val_T)))
    
    // Buffer info
    size_t pBufferSize;
    void* pBuffer = NULL;

    cusparseCsr2CscAlg_t algo{CUSPARSE_CSR2CSC_ALG1};
    
    CHECK_CUSPARSE(cusparseCsr2cscEx2_bufferSize(*handle,
                                                 h_m,
                                                 h_n,
                                                 h_nnz,
                                                 d_csrVal,
                                                 d_csrRowPtr,
                                                 d_csrColInd,
                                                 d_cscVal,
                                                 d_cscColPtr,
                                                 d_cscRowInd,
                                                 h_valueType,
                                                 CUSPARSE_ACTION_NUMERIC,
                                                 h_idxBase,
                                                 algo,
                                                 &pBufferSize))
    
    // Allocate pBuffer
    CHECK_CUDA(cudaMalloc((void**)&pBuffer, pBufferSize))
    
    CHECK_CUSPARSE(cusparseCsr2cscEx2(*handle,
                                    h_m,
                                    h_n,
                                    h_nnz,
                                    d_csrVal,
                                    d_csrRowPtr,
                                    d_csrColInd,
                                    d_cscVal,
                                    d_cscColPtr,
                                    d_cscRowInd,
                                    h_valueType,
                                    CUSPARSE_ACTION_NUMERIC,
                                    h_idxBase,
                                    algo,
                                    pBuffer))

    CHECK_CUDA(cudaDeviceSynchronize())

    CHECK_CUDA(cudaFree(pBuffer))

    std::cout << "printing Sparse Matrix (CSC)" << name << "\n" << \
                "m (rows) = " << h_m << "\n" << \
                "n (cols) = " << h_n << "\n" << \
                "nnz      = " << h_nnz << std::endl;
    
    val_T h_vals[h_nnz];
    CHECK_CUDA(cudaMemcpy(h_vals, d_cscVal, h_nnz * sizeof(val_T), cudaMemcpyDeviceToHost));

    int h_rowInd[h_nnz];
    CHECK_CUDA(cudaMemcpy(h_rowInd, d_cscRowInd, h_nnz * sizeof(int), cudaMemcpyDeviceToHost));

    int h_colPtr[h_n + 1];
    CHECK_CUDA(cudaMemcpy(h_colPtr, d_cscColPtr, (h_n + 1) * sizeof(int), cudaMemcpyDeviceToHost));

    if (!testCSRRowPtr(h_colPtr, h_n, h_nnz) ||
        !testCSRColInd(h_colPtr, h_rowInd, h_n, h_m, h_nnz)) {
        std::cout << "numRows=" << h_m << " numCols=" << h_n << " nnz=" << h_nnz << std::endl;
        printArr(h_colPtr, h_n+1, "colPtr");
        printArr(h_rowInd, h_nnz, "rowInd");
        printArr(h_vals, h_nnz, "val");

        throw "wrong";
    }

    std::cout << "colPtr=" << std::endl;
    for (unsigned int i=0;  i<h_n + 1; ++i) {
        std::cout << h_colPtr[i] << ", ";
    }   std::cout << std::endl;

    std::cout << "rowInd=" << std::endl;
    for (unsigned int i=0; i<h_nnz; ++i) {
        std::cout << h_rowInd[i] << ", ";
    }   std::cout << std::endl;

    std::cout << "vals=" << std::endl;
    for (unsigned int i=0; i<h_nnz; ++i) {
        std::cout << h_vals[i] << ", ";
    }   std::cout << std::endl;
    
    CHECK_CUDA(cudaFree(d_cscColPtr))
    CHECK_CUDA(cudaFree(d_cscRowInd))
    CHECK_CUDA(cudaFree(d_cscVal))
}


int randScalar(const int min=2, const int max=8) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(min, max); // define the range

    return distr(gen);
}

void randVals(val_T* vals, unsigned int size) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(25, 63); // define the range

    for (unsigned int i=0; i<size; ++i) {
        vals[i] = distr(gen);
    }
}

void randRowPtr(int* rowPtr, int size, int max=2) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::exponential_distribution<> d(1);

    rowPtr[0] = 0;
    
    for (int i=1; i<size+1; ++i) {
        rowPtr[i] = rowPtr[i-1] + std::max(std::min((int)d(gen), max), 1);
    }
}

void randColInd_Val(const int* rowPtr, int* colInd, val_T* val, int numRows, int numCols) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr_ind(0, numCols-1); // define the range
    std::uniform_int_distribution<> distr_val(1, 10);

    for (int i=0; i<numRows; ++i) {
        int min_col = 0;
        for (int j=rowPtr[i]; j < rowPtr[i+1]; ++j) {
            int v = distr_ind(gen);
            int max = (numCols - min_col) - (rowPtr[i+1] - j);
            int dcol = std::min(v, max);

            colInd[j] = min_col + dcol;

            // std::cout << "max=" << max << " ,remaining=" << rowPtr[i+1]-j << " ,sum(should be numCol-1)=" << min_col+max << std::endl;

            // assert(max < numCols);
            assert(dcol >= 0 && dcol < numCols);
            // assert(min_col+max==numCols-1);

            val[j] = distr_val(gen);
            min_col = min_col + dcol + 1;
        }
    }

    int nnz = rowPtr[numRows];

    if (!testCSRRowPtr(rowPtr, numRows, nnz) ||
        !testCSRColInd(rowPtr, colInd, numRows, numCols, nnz)) {
        std::cout << "numRows=" << numRows << " numCols=" << numCols << " nnz=" << nnz << std::endl;
        printArr(rowPtr, numRows+1, "rowPtr");
        printArr(colInd, nnz, "colInd");
        printArr(val, nnz, "val");

        throw "wrong";
    }
}

void sparseRandVals(val_T* vals, unsigned int size) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distr(-20, 10);

    for (unsigned int i=0; i<size; ++i) {
        int v = distr(gen);
        if (v > 0)
            vals[i] = v;
        else
            vals[i] = 0.f;
    }
}

// gets number of nnz in a densematrix that is safed in a array of size 
void nnzOfDenseMatrix(const val_T* data, const unsigned int size,
                   const int numRows, const int numCols,
                   int* nnz) {
    int tmpNnz = 0;
    for (int i=0; i<numRows; ++i) {
        for (int j=0; j<numCols; ++j) {
            if (std::abs(data[i*numCols + j]) > (val_T)10e-4) {
                ++tmpNnz;
            }
        }
    }
    *nnz = tmpNnz;
}

// converst a dense matrix that is in for of one long array of size into a sparseMatrix fo numRows x numCols
// PRE: size = numRows * numCols
void denseToSparse(const val_T* data, const unsigned int size,
                   const int numRows, const int numCols,
                   int* rowPtr, int* colInd, val_T* vals, int nnz) {
    if (size != numRows * numCols) {
        std::cout << "62: there is a size mismatch" << std::endl;
        throw "size mismatch";
    }

    rowPtr[0] = 0;
    int tmpNnz = 0;
    for (int i=0; i<numRows; ++i) {
        for (int j=0; j<numCols; ++j) {
            if (std::abs(data[i*numCols + j]) > (val_T)10e-4) {
                colInd[tmpNnz] = j;
                vals[tmpNnz] = data[i*numCols + j];
                if (vals[tmpNnz] > 1e4) {
                    std::cout << "value bigger than possilbe " << vals[tmpNnz] << " at idx=" << i*numCols + j << std::endl;
                    std::cout << "numRows=" << numRows << " , numCols=" << numCols << " , nnz=" << nnz << std::endl;
                    printArr(data, numRows * numCols, "data");
                    throw "wrong";
                }
                ++tmpNnz;
            }
        }
        rowPtr[i+1] = tmpNnz;
    }

    rowPtr[numRows] = tmpNnz;

    if(nnz != tmpNnz) {
        std::cout << "nnz is wrong" << std::endl;
        std::cout << "numRows=" << numRows << " , numCols=" << numCols << " , nnz=" << nnz << " but currently is=" << tmpNnz << std::endl;
        printArr(data, numRows * numCols, "data");
        printArr(rowPtr, numRows+1, "rowPtr");
        printArr(colInd, nnz, "colInd");
        printArr(vals, nnz, "vals");
        throw "f";
    }

    if (!testCSRRowPtr(rowPtr, numRows, nnz)){
        std::cout << "rowPtr is wrong" << std::endl;
        std::cout << "numRows=" << numRows << " , numCols=" << numCols << " , nnz=" << nnz << std::endl;
        printArr(data, numRows * numCols, "data");
        printArr(rowPtr, numRows+1, "rowPtr");
        printArr(colInd, nnz, "colInd");
        printArr(vals, nnz, "vals");
        throw "wrong";
    }

    if (!testCSRColInd(rowPtr, colInd, numRows, numCols, nnz)) {
        std::cout << "colInd is wrong" << std::endl;
        std::cout << "numRows=" << numRows << " , numCols=" << numCols << " , nnz=" << nnz << std::endl;
        printArr(rowPtr, numRows+1, "rowPtr");
        printArr(colInd, nnz, "colInd");
        printArr(vals, nnz, "vals");
        throw "wrong";
    }
}

bool compDenseVector(const DenseVector* a, const DenseVector* b, const float tol=10e-4) {
    if (a->size != b->size)
        return false;

    int size = a->size;

    val_T *d_a_vals, *d_b_vals;
    CHECK_CUSPARSE(cusparseDnVecGetValues(a->DnDescr, (void**)&d_a_vals))
    CHECK_CUSPARSE(cusparseDnVecGetValues(b->DnDescr, (void**)&d_b_vals))

    val_T h_a_vals[size];
    val_T h_b_vals[size];

    CHECK_CUDA(cudaMemcpy(h_a_vals, d_a_vals, size * sizeof(val_T), cudaMemcpyDeviceToHost))
    CHECK_CUDA(cudaMemcpy(h_b_vals, d_b_vals, size * sizeof(val_T), cudaMemcpyDeviceToHost))

    for (int i=0; i<size; ++i) {
        if (std::abs( h_a_vals[i] - h_b_vals[i] ) > tol)
            return false;
    }
    
    return true;
}

bool compCol(const SparseMatrix* A, const DenseVector* col, const int colIdx, const float tol=10e-4) {

    int rows = A->m;
    int cols = A->n;
    int nnz = A->nnz;

    int *d_A_rowPtr = A->csrRowPtr;
    int *d_A_colInd = A->csrColInd;
    val_T *d_A_vals = A->csrVal;

    val_T *d_col_vals = col->values;

    int h_A_rowPtr[rows+1];
    int h_A_colInd[nnz];
    val_T h_A_vals[nnz];

    val_T h_col_vals[rows];

    CHECK_CUDA(cudaMemcpy(h_A_rowPtr, d_A_rowPtr, (rows+1) * sizeof(int), cudaMemcpyDeviceToHost))
    CHECK_CUDA(cudaMemcpy(h_A_colInd, d_A_colInd, nnz * sizeof(int), cudaMemcpyDeviceToHost))
    CHECK_CUDA(cudaMemcpy(h_A_vals, d_A_vals, nnz * sizeof(val_T), cudaMemcpyDeviceToHost))

    CHECK_CUDA(cudaMemcpy(h_col_vals, d_col_vals, rows * sizeof(val_T), cudaMemcpyDeviceToHost))

    for (int i=0; i<rows; ++i) {
        for (int j=h_A_rowPtr[i]; j<h_A_rowPtr[i+1]; ++j) {
            if (h_A_colInd[j] == colIdx)
                if (std::abs( h_A_vals[j] - h_col_vals[i] ) > tol) {
                    std::cout << "A value=" << h_A_vals[j] << " A idx=" << j << "  col value=" << h_col_vals[i] << " col idx=" << i << std::endl;
                    return false;
                }
        }
    }
    
    return true;
}

void testWriteVecAtIndicies(cusparseHandle_t* handle, const int numIter, const int min=2, const int max=10) {
    for (int i=0; i<numIter; ++i) {
        
        unsigned int n = randScalar(min, max);
        unsigned int basis_sz = randScalar(0, n);

        val_T* arr[n];
        randVals(arr, n);

        val_T* basis_sz[]

        writeVectorAtIndices(handle, BasisVector* basis,
        DenseVector* ds_Y, const DenseVector* ds_A);

        bool passed = true;
        DenseVector* col = new DenseVector(numRows);
        for (unsigned int j=0; j<numCols; ++j) {
            getCol(handle, A, col, j);

            if (!compCol(A, col, j)) {
                if (passed) {
                    std::cout << "\n\n\n --- ITERATION : " << i << " ----\n" << std::endl;
                    std::cout << "rows=" << numRows << "  ,  cols=" << numCols << std::endl;
                }
                std::cout << "column " << j << std::endl;
                printVectorAsSpVec(col, std::to_string(j));
                passed = false;
            }
        }

        if (!passed) {
            printSpMat(A, " A");
        } else {
            std::cout << "passed iter: " << i << std::endl;
        }

        delete A;
        delete col;
    }
}


void testGetCol(cusparseHandle_t* handle, const int numIter, const int min=2, const int max=10) {
    for (int i=0; i<numIter; ++i) {
        
        // unsigned int n = 3;
        unsigned int numRows = randScalar(min, max);
        unsigned int numCols = randScalar(min, max);
        // unsigned int size = numRows * numCols;

        // creating Matrix A;
        // val_T denseMatData[size];
        // int nnz = 0;
        // while (nnz < 1) {
        //     nnz = 0;
        //     sparseRandVals(denseMatData, size);
        //     nnzOfDenseMatrix(denseMatData, size, numRows, numCols, &nnz);
        // }


        int rowPtr[numRows+1];
        randRowPtr(rowPtr, numRows);

        int nnz = rowPtr[numRows];
        int colInd[nnz];
        val_T vals[nnz];

        randColInd_Val(rowPtr, colInd, vals, numRows, numCols);

        // int colInd[nnz];
        // val_T spVals[nnz];

        // denseToSparse(denseMatData, size, numRows, numCols, rowPtr, colInd, spVals, nnz);

        SparseMatrix* A = new SparseMatrix(numRows, numCols, nnz, rowPtr, colInd, vals);

        bool passed = true;
        DenseVector* col = new DenseVector(numRows);
        for (unsigned int j=0; j<numCols; ++j) {
            getCol(handle, A, col, j);

            if (!compCol(A, col, j)) {
                if (passed) {
                    std::cout << "\n\n\n --- ITERATION : " << i << " ----\n" << std::endl;
                    std::cout << "rows=" << numRows << "  ,  cols=" << numCols << std::endl;
                }
                std::cout << "column " << j << std::endl;
                printVectorAsSpVec(col, std::to_string(j));
                passed = false;
            }
        }

        if (!passed) {
            printSpMat(A, " A");
        } else {
            std::cout << "passed iter: " << i << std::endl;
        }

        delete A;
        delete col;
    }
}



void testUpdateBasis(cusparseHandle_t* handle, const int numIter, const int min=2, const int max=10, const bool IdB=false) {
    for (int i=0; i<numIter; ++i) {
        
        // unsigned int n = 3;
        unsigned int numRows = randScalar(min, max);
        unsigned int numCols = randScalar(min, max);
        // unsigned int size = numRows * numCols;

        // creating Matrix A;
        // val_T denseMatData[size];
        // int nnz = 0;
        // while (nnz < 1) {
        //     nnz = 0;
        //     sparseRandVals(denseMatData, size);
        //     nnzOfDenseMatrix(denseMatData, size, numRows, numCols, &nnz);
        // }

        // int rowPtr[numRows+1];
        // int colInd[nnz];
        // val_T spVals[nnz];

        // denseToSparse(denseMatData, size, numRows, numCols, rowPtr, colInd, spVals, nnz);

        int rowPtr[numRows+1];
        randRowPtr(rowPtr, numRows);

        int nnz = rowPtr[numRows];
        int colInd[nnz];
        val_T vals[nnz];

        randColInd_Val(rowPtr, colInd, vals, numRows, numCols);

        SparseMatrix* A = new SparseMatrix(numRows, numCols, nnz, rowPtr, colInd, vals);

        // creating Matrix B
        SparseMatrix* B;
        SparseMatrix* B_ref;
        int B_nnz = 0;
        if (IdB) {
            B = new SparseMatrix(numRows);
            B_ref = new SparseMatrix(numRows);
            B_nnz = numRows;
        } else {
            // int B_size = numRows * numRows;
            // val_T B_denseMatData[B_size];
            // B_nnz = 0;

            // while (B_nnz < 1) {
            //     B_nnz = 0;
            //     sparseRandVals(B_denseMatData, B_size);
            //     nnzOfDenseMatrix(B_denseMatData, B_size, numRows, numRows, &B_nnz);
            // }

            // int B_rowPtr[numRows+1];
            // int B_colInd[B_nnz];
            // val_T B_spVals[B_nnz];

            // denseToSparse(B_denseMatData, B_size, numRows, numRows, B_rowPtr, B_colInd, B_spVals, B_nnz);

            int B_rowPtr[numRows+1];
            randRowPtr(B_rowPtr, numRows);

            B_nnz = B_rowPtr[numRows];
            int B_colInd[B_nnz];
            val_T B_vals[B_nnz];

            randColInd_Val(B_rowPtr, B_colInd, B_vals, numRows, numCols);

            B = new SparseMatrix(numRows, numRows, B_nnz, B_rowPtr, B_colInd, B_vals);
            B_ref = new SparseMatrix(numRows, numRows, B_nnz, B_rowPtr, B_colInd, B_vals);
        }

        // creating basis vector
        BasisVector* basis = new BasisVector(numRows, numCols);

        // set entering and leaving variables
        unsigned int colIdx_Out = randScalar(0, numRows - 1);
        unsigned int colIdx_In = randScalar(0, numCols + numRows - 1);

        if (B_nnz != 0 && nnz != 0)
            updateBasis(handle, basis, colIdx_Out,
                        colIdx_In, A, B, false);

        bool passed = true;
        DenseVector* refCol = new DenseVector(numRows);
        DenseVector* newCol = new DenseVector(numRows);
        for (unsigned int j=0; j<numRows; ++j) {
            getCol(handle, B, newCol, j);

            if (j==colIdx_Out) {
                if (colIdx_In < numCols)
                    getCol(handle, A, refCol, colIdx_In);
                else
                    unitJvector(refCol, colIdx_In - numCols);
            }
            else
                getCol(handle, B_ref, refCol, j);

            if (!compDenseVector(newCol, refCol)) {
                if (passed) {
                    std::cout << "\n\n\n --- ITERATION : " << i << " ----\n" << std::endl;
                    std::cout << "rows=" << numRows << "  ,  cols=" << numCols << std::endl;
                    passed = false;
                }

                printVector(newCol, "newCol");
                printVector(refCol, "refCol");
            }
        }

        if (passed && numIter == 1) {       // allways prints
            std::cout << "\n\n\n --- ITERATION : " << i << " ----\n" << std::endl;
            std::cout << "rows=" << numRows << "  ,  cols=" << numCols << std::endl;
            std::cout << "nnz of A=" << nnz << "  nnz of B=" << B_nnz << std::endl;
            std::cout << "\nentering variable=" << colIdx_In << "   leaving variable=" << colIdx_Out << std::endl;
            printSpMatAsDenseMat(A, "A");
            printSpMatAsDenseMat(B_ref, "BEFORE B");
            printSpMatAsDenseMat(B, "AFTER B");
            basis->print();
        }

        if (!passed) {
            std::cout << "\nentering variable=" << colIdx_In << "   leaving variable=" << colIdx_Out << std::endl;
            if (numCols < 10 && numRows < 10) {
                printSpMatAsDenseMat(A, "A");
                printSpMatAsDenseMat(B_ref, "BEFORE B");
                printSpMatAsDenseMat(B, "AFTER B");
            } else if (numCols < 20 && numRows < 20) {
                printSpMat(A, "A");
                printSpMat(B_ref, "BEFORE B");
                printSpMat(B, "AFTER B");
            }
            basis->print();
        } else {
            std::cout << "passed iter: " << i << std::endl;
        }

        delete A;
        delete B;
        delete B_ref;
        delete basis;
        delete refCol;
        delete newCol;
    }
}

int main (int argc, char** argv) {

    cusparseHandle_t handle = NULL;
    cusparseCreate(&handle);

    int numIter = 100;
    if (argc > 1) {
        numIter = atoi(argv[1]);
        if (numIter <= 0) {
            std::cout << "nonvalid input for num iterations. Should be Integer bigger than 0" << std::endl;
            return 0;
        }
    }

    int min = 2;
    if (argc > 2) {
        min = atoi(argv[2]);
        if (min <= 0) {
            std::cout << "nonvalid input for minimum Dimension. Should be Integer bigger than 0" << std::endl;
            return 0;
        }
    }

    int max = 10;
    if (argc > 3) {
        max = atoi(argv[3]);
        if (max < min) {
            std::cout << "nonvalid input for maximum Dimension. Should be Integer bigger than Minimum Dimension (" << min << ")" << std::endl;
            return 0;
        }
    }

    bool idB = false;
    if (argc > 4) {
        std::string action(argv[4]);
        if (action == "true" || action == "t") {
            idB = true;
        } else if (action == "false" || action == "f") {
            idB = false;
        } else {
            std::cout << "nonvalid input for use Identity B. Should be true or t to enable / false or n to disable. It is disabled by default" << std::endl;
            return 0;
        }
    }

    int nDevices;

    cudaGetDeviceCount(&nDevices);
    for (int i = 0; i < nDevices; i++) {
        cudaDeviceProp prop;
        cudaGetDeviceProperties(&prop, i);
        printf("Device Number: %d\n", i);
        printf("  Device name: %s\n", prop.name);
        printf("  Memory Clock Rate (KHz): %d\n",
            prop.memoryClockRate);
        printf("  Memory Bus Width (bits): %d\n",
            prop.memoryBusWidth);
        printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
            2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
        printf("  max Threads per Block: %d\n",
            prop.maxThreadsPerBlock);
    }
    
    std::cout << "START test Update Basis\n\n" << std::endl;
    testUpdateBasis(&handle, numIter, min, max, idB);
    std::cout << "\n\nFINISH test Update Basis" << std::endl;

    // std::cout << "START test Get Col\n\n" << std::endl;
    // testGetCol(&handle, numIter, min, max);
    // std::cout << "\n\nFINISH test Get Col" << std::endl;

    // for (unsigned int i=0; i<numIter; ++i) {
    //     int numRows = randScalar(min, max);
    //     int numCols = randScalar(min, max);

    //     int rowPtr[numRows+1];
    //     randRowPtr(rowPtr, numRows);

    //     int nnz = rowPtr[numRows];
    //     int colInd[nnz];
    //     val_T vals[nnz];

    //     randColInd_Val(rowPtr, colInd, vals, numRows, numCols);

    //     if (!testCSRRowPtr(rowPtr, numRows, nnz) ||
    //         !testCSRColInd(rowPtr, colInd, numRows, numCols, nnz)) {
    //             std::cout << "numRows=" << numRows << " numCols=" << numCols << " nnz=" << nnz << std::endl;
    //             printArr(rowPtr, numRows+1, "rowPtr");
    //             printArr(colInd, nnz, "colInd");
    //             printArr(vals, nnz, "vals");
    //         }
        
    //     SparseMatrix* A = new SparseMatrix(numRows, numCols, nnz,
    //                                        rowPtr, colInd, vals);
        
    //     printSpMatAsDenseMat(A, "A dense");

    //     std::cout << "\n\nground truth:" << std::endl;

    //     printArr(rowPtr, numRows+1, "rowPtr");
    //     printArr(colInd, nnz, "colInd");
    //     printArr(vals, nnz, "vals");
    // }

    return 0;
}