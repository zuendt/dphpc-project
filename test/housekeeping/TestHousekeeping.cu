

#include "simplex.cuh"
#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <string>
#include <cassert>
#include <random>
#include <iomanip>

// generates an array of random values
// maybe would be worth it to do it based on a random seed
template <typename T>
void randVals(T* vals, const int size) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(-40, 63); // define the range

    for (int i=0; i<size; ++i) {
        vals[i] = distr(gen);
    }
}

template <typename T>
void printArr(const T* h_arr, const int size, std::string arrName) {
    std::cout << arrName << "=" << std::endl;

    for (unsigned int i=0; i<size; ++i) {
        std::cout << h_arr[i] << ", ";
    }
    
    std::cout << std::endl;
}

/*void printSpMatAsDenseMat(const SparseMatrix* M, std::string spName) {
    int64_t h_n, h_m, h_nnz;
    int* d_rowPtr;
    int* d_colInd;
    val_T* d_vals;

    cusparseIndexType_t h_rowPtrType, h_colIndType;
    cusparseIndexBase_t h_idxBase;
    cudaDataType h_valueType;

    CHECK_CUSPARSE(cusparseCsrGet(M->spDescr,
                                  &h_m,
                                  &h_n,
                                  &h_nnz,
                                  (void**)&d_rowPtr,
                                  (void**)&d_colInd,
                                  (void**)&d_vals,
                                  &h_rowPtrType,
                                  &h_colIndType,
                                  &h_idxBase,
                                  &h_valueType))

    std::cout << "\nprinting Dense Matrix " << spName << "\n" << \
                "m (rows) = " << h_m << "\n" << \
                "n (cols) = " << h_n << "\n" << \
                "nnz      = " << h_nnz << std::endl;
    
    val_T h_vals[h_nnz];
    CHECK_CUDA(cudaMemcpy(h_vals, d_vals, h_nnz * sizeof(val_T), cudaMemcpyDeviceToHost));

    int h_colInd[h_nnz];
    CHECK_CUDA(cudaMemcpy(h_colInd, d_colInd, h_nnz * sizeof(int), cudaMemcpyDeviceToHost));

    int h_rowPtr[h_m + 1];
    CHECK_CUDA(cudaMemcpy(h_rowPtr, d_rowPtr, (h_m + 1) * sizeof(int), cudaMemcpyDeviceToHost))

    
    std::cout << "cols     :  ";
    for (unsigned int i=0; i<h_m; ++i) {
        std::cout << std::setw(5) << i << "  ";
    }

    unsigned int nextNNZ_off = 0;
    for (unsigned int i=0; i<h_m; ++i) {
        std::cout << "\nrow=" << std::setw(4) << i << " :   ";

        unsigned int nextColVal = h_colInd[nextNNZ_off];
        for (unsigned int j=0; j < h_m; ++j) {
            if (j == nextColVal && nextNNZ_off < h_rowPtr[i+1]) {
                std::cout << std::setw(5) << h_vals[nextNNZ_off] << ", ";
                ++nextNNZ_off;
                nextColVal = h_colInd[nextNNZ_off];
            } else {
                std::cout << std::setw(5) << 0.0 << ", ";
            }
        }
    }

    std::cout << std::endl;
}

void printSpMatAsDenseMatFromArr(const int* colPtrs, const int* rowInds, const val_T* vals, int rows, int cols, std::string name) {
    std::cout << "rows     :  ";
    for (unsigned int i=0; i<rows; ++i) {
        std::cout << std::setw(5) << i << "  ";
    }

    unsigned int nextNNZ_off = 0;
    unsigned int nextRowVal = rowInds[nextNNZ_off];

    for (unsigned int i=0; i<cols; ++i) {
        std::cout << "\ncol=" << std::setw(4) << i << " :   ";

        for (unsigned int j=0; j < rows; ++j) {
            if (j == nextRowVal && nextNNZ_off < colPtrs[i+1]) {
                std::cout << std::setw(5) << vals[nextNNZ_off] << ", ";
                ++nextNNZ_off;
                nextRowVal = rowInds[nextNNZ_off];
            } else {
                std::cout << std::setw(5) << 0.0 << ", ";
            }
        }
    }

    std::cout << "\n nnz=" << colPtrs[cols] << "   printed nnz=" << nextNNZ_off-1 << std::endl;
}



// Goes through a Csr matrix and gets the colIdx column
// saves the result into extractCol
// Csr matrix is defined by rowPtr, colInd, values and number of rows
__global__ void GetColKernel(val_T* extractedCol, const int* rowPtr, const int* colInd, const val_T* values, const int rows, const int colIdx) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < rows) {
        int start = rowPtr[i];
        int end = rowPtr[i+1];

        extractedCol[i] = 0.f;

        for (unsigned int j=start; j<end; ++j) {
            if (colInd[j] == colIdx) {
                extractedCol[i] = values[j];
                break;
            } 
        }
    } 
}

// Gets the i-th column of A and overwrites the output vector with this column
void TESTGetCol(cusparseHandle_t *handle, const SparseMatrix* A,
    DenseVector* output,const unsigned int i) {

    // take column i of A and write into ds_output->
    PRINT_FUNC_PREFIX; std::cout << "Taking column i=" << i << std::endl;

    if (A->m != output->size) {
        throw std::logic_error("[housekeeping::GetCol] Dimension mismatch");
    }
    if (A->n <= i) {
        throw std::logic_error("[housekeeping::GetCol] index i="+
                std::to_string(i)+" out of range");
    }

    // Get the csr data
    int64_t h_n, h_m, h_nnz;
    int* d_rowPtr;
    int* d_colInd;
    val_T* d_vals;

    cusparseIndexType_t h_rowPtrType, h_colIndType;
    cusparseIndexBase_t h_idxBase;
    cudaDataType h_valueType;

    CHECK_CUSPARSE(cusparseCsrGet(A->spDescr,
                                  &h_m,
                                  &h_n,
                                  &h_nnz,
                                  (void**)&d_rowPtr,
                                  (void**)&d_colInd,
                                  (void**)&d_vals,
                                  &h_rowPtrType,
                                  &h_colIndType,
                                  &h_idxBase,
                                  &h_valueType))
    
    val_T* d_outputVals;                // empty array in which the values are copied
    CHECK_CUDA(cudaMalloc((void**)&d_outputVals, h_m * sizeof(val_T)))

    GetColKernel<<<1, h_m>>>(d_outputVals, d_rowPtr, d_colInd, d_vals, h_m, i);
    CHECK_CUDA(cudaDeviceSynchronize())

    //update the vector
    CHECK_CUSPARSE(cusparseDnVecSetValues(output->DnDescr, (void *)d_outputVals))
}

// overwrites a[i] with b[i] if i exists in idxs
// size is size of the idxs array
__global__ void overwriteAtIdxs(val_T* a, const val_T* b, const int* idxs, const int size) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < size) {
        int j = idxs[i];
        a[j] = b[j];
    }
}

// overwrites ds_Y with values from ds_A based on basis idxs (if idx does not exist in basis value at that idx is not overwritten)
void TESTWriteVectorAtIndexes(cusparseHandle_t *handle, BasisVector* basis,
        DenseVector* ds_Y, const DenseVector* ds_A) {

    // get Sp vector data
    int64_t             size;
    int64_t             nnz;
    int*                indices;
    int*                values;
    cusparseIndexType_t idxType;
    cusparseIndexBase_t idxBase;
    cudaDataType        valueType;

    CHECK_CUSPARSE(cusparseSpVecGet(basis->descr,
                        &size,
                        &nnz,
                        (void**)&indices,
                        (void**)&values,
                        &idxType,
                        &idxBase,
                        &valueType))

    val_T* ds_YVal;             // values of ds_Y
    CHECK_CUSPARSE(cusparseDnVecGetValues(ds_Y->DnDescr, (void **)&ds_YVal))

    val_T* ds_AVal;             // values of ds_A
    CHECK_CUSPARSE(cusparseDnVecGetValues(ds_A->DnDescr, (void **)&ds_AVal))

    overwriteAtIdxs<<<1, size>>>(ds_YVal, ds_AVal, indices, size);
    CHECK_CUDA(cudaDeviceSynchronize())

    //update the vector
    CHECK_CUSPARSE(cusparseDnVecSetValues(ds_Y->DnDescr, (void *)ds_YVal))
}

void removeAndInsertColCPU(const int* d_cscColPtr, const int* d_cscRowInd, const val_T* d_cscVal,
                                   const int* d_val_idxs_In, const val_T* d_val_In,
                                   int* d_cscRowInd_new, val_T* d_cscVal_new,
                                   const int colIdx_Out, const int colIdx_In, const int nnz_new,
                                   const int size_Out, const int size_In) {
    PRINT_FUNC_PREFIX; std::cout << "removing idx=" << colIdx_Out << "   inserting idx=" << colIdx_In << std::endl;
    PRINT_FUNC_PREFIX; std::cout << "remobing size=" << size_Out << "    inserting size=" << size_In << std::endl;

    // new arrays
    int cscRowInd_new[nnz_new];
    val_T cscVal_new[nnz_new];

    int nnzIdx_In;
    int nnzIdx_Out;
    CHECK_CUDA(cudaMemcpy(&nnzIdx_In, d_cscColPtr + colIdx_In, sizeof(int), cudaMemcpyDeviceToHost))
    CHECK_CUDA(cudaMemcpy(&nnzIdx_Out, d_cscColPtr + colIdx_Out, sizeof(int), cudaMemcpyDeviceToHost))

    int nnz_old = nnz_new - size_In + size_Out;
    int cscRowInd[nnz_old];
    val_T cscVal[nnz_old];

    CHECK_CUDA(cudaMemcpy(cscRowInd, d_cscRowInd, nnz_old * sizeof(int), cudaMemcpyDeviceToHost))
    CHECK_CUDA(cudaMemcpy(cscVal, d_cscVal, nnz_old * sizeof(val_T), cudaMemcpyDeviceToHost))

    int val_idxs_In[size_In];
    val_T val_In[size_In];

    CHECK_CUDA(cudaMemcpy(val_idxs_In, d_val_idxs_In, size_In * sizeof(int), cudaMemcpyDeviceToHost))
    CHECK_CUDA(cudaMemcpy(val_In, d_val_In, size_In * sizeof(val_T), cudaMemcpyDeviceToHost))

    for (unsigned int i=0; i<nnz_new; ++i) {
            if (i < nnz_new) {

            if (i < nnzIdx_In || i >= nnzIdx_In + size_In) {
            
                int off = 0;
                if (i >= nnzIdx_Out && i < nnzIdx_In) {
                    off = size_Out;
                } else if (i > nnzIdx_In && i <= nnzIdx_Out) {
                    off = -size_In;
                } else if (i > nnzIdx_Out || i > nnzIdx_In) {
                    off = size_Out - size_In;
                }
                
                cscRowInd_new[i] = cscRowInd[i + off];
                cscVal_new[i] = cscVal[i + off];

                std::cout << "idx=" << i << " off=" << off << "     val is=" << cscVal_new[i] << std::endl;
            } else {
                cscRowInd_new[i] = val_idxs_In[i - colIdx_In];
                cscVal_new[i] = val_In[i - colIdx_In];
                std::cout << "inserting at=" << i << "       val is=" << cscVal_new[i] <<std::endl;
            }
        }
    }

    printArr(cscRowInd, nnz_old, "cscRowInd");
    printArr(cscRowInd_new, nnz_new, "cscRowInd_new");

    std::cout << std::endl;

    printArr(cscVal, nnz_old, "cscVal");
    printArr(cscVal_new, nnz_new, "cscVal_new");

    std::cout << "\n\n\nKERNEL CPU 1 HAS FINISHED\n\n" << std::endl;
}

__global__ void removeAndInsertCol(const int* cscColInd, const int* cscRowInd, const val_T* cscVal,
                                   const int* val_idxs_In, const val_T* val_In,
                                   int* cscRowInd_new, val_T* cscVal_new,
                                   const int colIdx_Out, const int colIdx_In, const int nnz_new,
                                   const int size_Out, const int size_In) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < nnz_new) {
        int nnzIdx_In = cscColInd[colIdx_In];
        int nnzIdx_Out = cscColInd[colIdx_Out];

        if (i < nnzIdx_In || i >= nnzIdx_In + size_In) {
        
            int off = 0;
            if (i >= nnzIdx_Out && i < nnzIdx_In) {
                off = size_Out;
            } else if (i > nnzIdx_In && i <= nnzIdx_Out) {
                off = -size_In;
            } else if (i > nnzIdx_Out || i > nnzIdx_In) {
                off = size_Out - size_In;
            }
            
            cscRowInd_new[i] = cscRowInd[i + off];
            cscVal_new[i] = cscVal[i + off];

        } else {
            cscRowInd_new[i] = val_idxs_In[i - nnzIdx_In];
            cscVal_new[i] = val_In[i - nnzIdx_In];
        }
    }
}

void adjustCscColPtrCPU(const int* d_cscColPtr, int* d_cscColPtr_new,
                                const int colIdx_Out, const int colIdx_In, const int size,
                                const int size_Out, const int size_In) {

    int cscColPtr[size+1];
    int cscColPtr_new[size+1];

    CHECK_CUDA(cudaMemcpy(cscColPtr, d_cscColPtr, (size + 1) * sizeof(int), cudaMemcpyDeviceToHost))

    for (unsigned int i=0; i < size + 1; ++i) {
        if (i < size + 1) {
            int off = 0;
            int roff = 0;
            if (i >= colIdx_Out + 1 && i < colIdx_In + 1) {
                off = -size_Out;
                roff = 1;
            } else if (i >= colIdx_In + 1 && i < colIdx_Out + 1) {
                off = size_In;
                roff = -1;
            } else if (i >= colIdx_Out + 1 && i >= colIdx_In + 1) {
                off = -size_Out + size_In;
            }
            
            cscColPtr_new[i] = cscColPtr[i + roff] + off;

            if (i == colIdx_Out) std::cout << "out  ";
            else if (i == colIdx_In) std::cout << "in  ";
            std::cout << "idx=" << i << " off=" << off << "     col offset is=" << cscColPtr_new[i] << std::endl;
        }
    }

    printArr(cscColPtr, size+1, "cscColPtr");
    printArr(cscColPtr_new, size+1, "cscColPtr_new");

    std::cout << "\n\n\nKERNEL CPU 2 HAS FINISHED\n\n" << std::endl;
}

__global__ void adjustCscColPtr(const int* cscColPtr, int* cscColPtr_new,
                                const int colIdx_Out, const int colIdx_In, const int size,
                                const int size_Out, const int size_In) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < size) {
        int off = 0;
        int roff = 0;
        if (i >= colIdx_Out + 1 && i < colIdx_In + 1) {
            off = -size_Out;
            roff = 1;
        } else if (i >= colIdx_In + 1 && i < colIdx_Out + 1) {
            off = size_In;
            roff = -1;
        } else if (i >= colIdx_Out + 1 && i >= colIdx_In + 1) {
            off = -size_Out + size_In;
        }
        
        cscColPtr_new[i] = cscColPtr[i + roff] + off;
    }
}

__global__ void adjustBasisVec(const int* basisVal, int* basisVal_new,
                               const int colIdx_Out, const int colIdx_In, const int size) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < size) {
        int off = 0;
        int idx = basisVal[i];

        if ((i==0 && colIdx_In < idx) ||
            (colIdx_In < idx && colIdx_In > basisVal[i-1])) {
            basisVal_new[i] = colIdx_In;
        } else {
            if (idx >= colIdx_Out && idx < colIdx_In) {
                off = 1;
            } else if (i > colIdx_In && i <= colIdx_Out) {
                off = -1;
            }

            basisVal_new[i] = basisVal[i + off];
        }
    }
}


void denseVecToSparseVec(const val_T* d_denseVals, val_T* h_spVals, int* h_spIdx, int h_denseSize, int *h_spSize) {
    val_T h_TOL = (val_T)10e-3;

    val_T h_denseVals[h_denseSize];
    CHECK_CUDA(cudaMemcpy(h_denseVals, d_denseVals, h_denseSize * sizeof(val_T), cudaMemcpyDeviceToHost))

    int c = 0;
    for (unsigned int i=0; i < h_denseSize; ++i) {
        if (h_denseVals[i] >= h_TOL || h_denseVals[i] <= -h_TOL) {
            h_spVals[c] = h_denseVals[i];
            h_spIdx[c] = i;
            ++c;
        }
    }
    *h_spSize = c;
}


// converts the CSR B into CSC Format to then remove column i and copy in Column j from the Matrix A
// also removes i from the basis and enters j
void TESTUpdateBasis(cusparseHandle_t *handle, BasisVector* basis, unsigned const int colIdx_Out,
        unsigned const int colIdx_In, SparseMatrix* A, SparseMatrix* B) {

    // Get basis vector data
    int* d_basisVal;

    CHECK_CUSPARSE(cusparseSpVecGetValues(basis->descr,
                                          (void **)&d_basisVal))
    
    int* d_basisVal_new;
    CHECK_CUDA(cudaMalloc((void **)&d_basisVal_new, basis->m_ * sizeof(int)))
    adjustBasisVec<<<1, basis->m_>>>(d_basisVal, d_basisVal_new,
                                     colIdx_Out, colIdx_In, basis->m_);
    
    // Get the csr data from B
    int64_t h_n, h_m, h_nnz;
    int* d_csrRowPtr;
    int* d_csrColInd;
    val_T* d_csrVal;

    cusparseIndexType_t h_rowPtrType, h_colIndType;
    cusparseIndexBase_t h_idxBase;
    cudaDataType h_valueType;

    CHECK_CUSPARSE(cusparseCsrGet(B->spDescr,
                                  &h_m,
                                  &h_n,
                                  &h_nnz,
                                  (void**)&d_csrRowPtr,
                                  (void**)&d_csrColInd,
                                  (void**)&d_csrVal,
                                  &h_rowPtrType,
                                  &h_colIndType,
                                  &h_idxBase,
                                  &h_valueType))

    //CSC data
    int* d_cscColPtr;
    int* d_cscRowInd;
    val_T* d_cscVal;

    CHECK_CUDA(cudaMalloc((void**)&d_cscColPtr, (h_n + 1) * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_cscRowInd, h_nnz     * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_cscVal,    h_nnz     * sizeof(val_T)))
    std::cout << "\n Allocated for CSC:\n" << 
                            "cscColPtr=" << (h_n + 1) * sizeof(int) << 
                            "cscRowInd=" << h_nnz     * sizeof(int) << 
                            "cscVal="    << h_nnz     * sizeof(val_T) << "\n" << std::endl;
    
    // Buffer info
    size_t pBufferSize;
    void* pBuffer = NULL;

    cusparseCsr2CscAlg_t algo{CUSPARSE_CSR2CSC_ALG1};
    
    CHECK_CUSPARSE(cusparseCsr2cscEx2_bufferSize(*handle,
                                                 h_m,
                                                 h_n,
                                                 h_nnz,
                                                 d_csrVal,
                                                 d_csrRowPtr,
                                                 d_csrColInd,
                                                 d_cscVal,
                                                 d_cscColPtr,
                                                 d_cscRowInd,
                                                 h_valueType,
                                                 CUSPARSE_ACTION_NUMERIC,
                                                 h_idxBase,
                                                 algo,
                                                 &pBufferSize))
    
    // Allocate pBuffer
    CHECK_CUDA(cudaMalloc((void**)&pBuffer, pBufferSize))
    PRINT_FUNC_PREFIX; std::cout << "\n pBufferSize: " << pBufferSize << std::endl;
    
    CHECK_CUSPARSE(cusparseCsr2cscEx2(*handle,
                                    h_m,
                                    h_n,
                                    h_nnz,
                                    d_csrVal,
                                    d_csrRowPtr,
                                    d_csrColInd,
                                    d_cscVal,
                                    d_cscColPtr,
                                    d_cscRowInd,
                                    h_valueType,
                                    CUSPARSE_ACTION_NUMERIC,
                                    h_idxBase,
                                    algo,
                                    pBuffer))

    CHECK_CUDA(cudaFree(pBuffer))

    val_T vals[h_m] = {0.f};
    DenseVector output(h_m, vals);
    TESTGetCol(handle, A, &output, colIdx_In);

    // std::cout << "ERROR HERE? 298" << std::endl;

    val_T* d_denseVals;
    CHECK_CUSPARSE(cusparseDnVecGetValues(output.DnDescr, (void **)&d_denseVals))

    // std::cout << "ERROR HERE? 303" << std::endl;

    val_T h_spVals[(int)h_m];    
    int h_spIdx[(int)h_m];    

    int h_size_In;
    denseVecToSparseVec(d_denseVals, h_spVals, h_spIdx, h_m, &h_size_In);

    val_T* d_spVals;    
    int* d_spIdx; 
    CHECK_CUDA(cudaMalloc((void**)&d_spVals, h_size_In * sizeof(val_T)))
    CHECK_CUDA(cudaMalloc((void**)&d_spIdx, h_size_In * sizeof(int)))

    CHECK_CUDA(cudaMemcpy(d_spVals, h_spVals,  h_size_In * sizeof(val_T), cudaMemcpyHostToDevice))
    CHECK_CUDA(cudaMemcpy(d_spIdx, h_spIdx,  h_size_In * sizeof(int), cudaMemcpyHostToDevice))

    // std::cout << "ERROR HERE? 308" << std::endl;

    int h_size_arr[2];
    CHECK_CUDA(cudaMemcpy(h_size_arr, d_cscColPtr + colIdx_Out, 2 * sizeof(int), cudaMemcpyDeviceToHost))
    int h_size_Out = h_size_arr[1] - h_size_arr[0];

    // int h_size_wholeArr[(int)h_n];
    // CHECK_CUDA(cudaMemcpy(h_size_wholeArr, d_cscColPtr, (int)h_n * sizeof(int), cudaMemcpyDeviceToHost))
    // int h_size_wOut = h_size_wholeArr[colIdx_Out+1] - h_size_wholeArr[colIdx_Out];

    // std::cout << "WHAT WAS COPIED   w:" << h_size_wOut << "  p:" << h_size_Out << std::endl;

    int h_nnz_new = h_nnz + h_size_In - h_size_Out;

    int* d_cscColPtr_new;
    int* d_cscRowInd_new;
    val_T* d_cscVal_new;
    CHECK_CUDA(cudaMalloc((void**)&d_cscColPtr_new, (h_n + 1)  * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_cscRowInd_new, h_nnz_new * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_cscVal_new, h_nnz_new * sizeof(val_T)))

    // std::cout << "REACHED FIRST KERNEL" << std::endl;
    // std::cout << "out size: " << h_size_Out << "   in size: " << h_size_In << "    nnz old: " << h_nnz << "   nnz new: " << h_nnz_new << std::endl;

    int h_cscColPtr[h_n + 1];
    int h_cscRowInd[h_nnz_new];
    val_T h_cscVal[h_nnz_new];

    removeAndInsertColCPU(d_cscColPtr, d_cscRowInd, d_cscVal,
                            d_spIdx, d_spVals,
                            d_cscRowInd_new, d_cscVal_new,
                            colIdx_Out, colIdx_In, h_nnz_new,
                            h_size_Out, h_size_In);


    removeAndInsertCol<<<1, h_nnz_new>>>(d_cscColPtr, d_cscRowInd, d_cscVal,
                                        d_spIdx, d_spVals,
                                        d_cscRowInd_new, d_cscVal_new,
                                        colIdx_Out, colIdx_In, h_nnz_new,
                                        h_size_Out, h_size_In);
    
    CHECK_CUDA(cudaDeviceSynchronize())

    CHECK_CUDA(cudaMemcpy(h_cscRowInd, d_cscRowInd_new, h_nnz_new * sizeof(int), cudaMemcpyDeviceToHost))
    CHECK_CUDA(cudaMemcpy(h_cscVal, d_cscVal_new, h_nnz_new * sizeof(val_T), cudaMemcpyDeviceToHost))

    printArr(h_cscRowInd, h_nnz_new, "new cscRowInd after Kernel 1");
    printArr(h_cscVal, h_nnz_new, "new cscVal after Kernel 1");
    std::cout << std::endl;
    
    // std::cout << "LOL how did it pass the first kernel??" << std::endl;

    adjustCscColPtrCPU(d_cscColPtr, d_cscColPtr_new,
                                    colIdx_Out, colIdx_In, h_n,
                                    h_size_Out, h_size_In);

    adjustCscColPtr<<<1, h_n + 1>>>(d_cscColPtr, d_cscColPtr_new,
                                    colIdx_Out, colIdx_In, h_n + 1,
                                    h_size_Out, h_size_In);

    CHECK_CUDA(cudaDeviceSynchronize())

    CHECK_CUDA(cudaMemcpy(h_cscColPtr, d_cscColPtr_new, (h_n + 1) * sizeof(int), cudaMemcpyDeviceToHost))

    printArr(h_cscColPtr, h_n + 1, "new cscColPtr after Kernel 2");
    std::cout << std::endl;

    printSpMatAsDenseMatFromArr(h_cscColPtr, h_cscRowInd, h_cscVal, h_n, h_m, "TEST MATRIX");
    std::cout << std::endl;

    //CSR new data
    int* d_csrRowPtr_new;
    int* d_csrColInd_new;
    val_T* d_csrVal_new;

    CHECK_CUDA(cudaMalloc((void**)&d_csrRowPtr_new, (h_m + 1) * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_csrColInd_new, h_nnz_new     * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_csrVal_new,    h_nnz_new     * sizeof(val_T)))

    // Buffer info
    pBufferSize = 0;
    pBuffer = NULL;
    
    CHECK_CUSPARSE(cusparseCsr2cscEx2_bufferSize(*handle,
                                                 h_n,
                                                 h_m,
                                                 h_nnz_new,
                                                 d_cscVal_new,
                                                 d_cscColPtr_new,
                                                 d_cscRowInd_new,
                                                 d_csrVal_new,
                                                 d_csrRowPtr_new,
                                                 d_csrColInd_new,
                                                 h_valueType,
                                                 CUSPARSE_ACTION_NUMERIC,
                                                 h_idxBase,
                                                 algo,
                                                 &pBufferSize))
    
    // Allocate pBuffer
    CHECK_CUDA(cudaMalloc((void**)&pBuffer, pBufferSize))
    PRINT_FUNC_PREFIX; std::cout << "\n pBufferSize: " << pBufferSize << std::endl;
    
    CHECK_CUSPARSE(cusparseCsr2cscEx2(*handle,
                                        h_n,
                                        h_m,
                                        h_nnz_new,
                                        d_cscVal_new,
                                        d_cscColPtr_new,
                                        d_cscRowInd_new,
                                        d_csrVal_new,
                                        d_csrRowPtr_new,
                                        d_csrColInd_new,
                                        h_valueType,
                                        CUSPARSE_ACTION_NUMERIC,
                                        h_idxBase,
                                        algo,
                                        pBuffer))
    
    CHECK_CUDA(cudaFree(pBuffer))

    CHECK_CUSPARSE(cusparseDestroySpMat(B->spDescr))
    CHECK_CUSPARSE(cusparseCreateCsr(&B->spDescr, h_m, h_n, h_nnz_new, 
                                    d_csrRowPtr_new, d_csrColInd_new, d_csrVal_new,
                                    CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I,
                                    CUSPARSE_INDEX_BASE_ZERO, CUDA_R_32F))

    CHECK_CUSPARSE(cusparseSpVecSetValues(basis->descr,
                                          d_basisVal_new))
    
    B->nnz = h_nnz_new;
}   


void TESTUpdateBasis_triplet(cusparseHandle_t *handle, BasisVector* basis, unsigned const int i,
unsigned const int j, SparseMatrix* A, SparseMatrix* B) {

    int64_t h_n, h_m, h_nnz;
    int* d_csrRowPtr;
    int* d_csrColInd;
    val_T* d_csrVal;

    cusparseIndexType_t h_rowPtrType, h_colIndType;
    cusparseIndexBase_t h_idxBase;
    cudaDataType h_valueType;

    CHECK_CUSPARSE(cusparseCsrGet(B->spDescr,
                                  &h_m,
                                  &h_n,
                                  &h_nnz,
                                  (void**)&d_csrRowPtr,
                                  (void**)&d_csrColInd,
                                  (void**)&d_csrVal,
                                  &h_rowPtrType,
                                  &h_colIndType,
                                  &h_idxBase,
                                  &h_valueType))

    val_T * h_csrVal = (val_T *) malloc((h_nnz) * sizeof(val_T ));
    int* h_csrRowPtr = (int*) malloc ((h_m + 1) * sizeof(int));
    int* h_csrColInd = (int*) malloc (h_nnz * sizeof(int));

    CHECK_CUDA(cudaMemcpy(h_csrVal, d_csrVal, h_nnz*sizeof(val_T), cudaMemcpyDeviceToHost))
    CHECK_CUDA(cudaMemcpy(h_csrRowPtr, d_csrRowPtr, (h_m+1) * sizeof(int), cudaMemcpyDeviceToHost))
    CHECK_CUDA(cudaMemcpy(h_csrColInd, d_csrColInd, h_nnz * sizeof(int), cudaMemcpyDeviceToHost))

    val_T vals[h_m] = {0.f};
    DenseVector output(h_m, vals);
    TESTGetCol(handle, A, &output, i);

    CHECK_CUDA(cudaDeviceSynchronize())


    val_T* d_output_Vals;
    CHECK_CUSPARSE(cusparseDnVecGetValues(output.DnDescr, (void **)&d_output_Vals))
    val_T * h_output_Vals = (val_T*) malloc((h_nnz) * sizeof(val_T ));


    std::vector<csr_triple> B_triplets{};
    val_T tol = (float)1e-3;

    for(unsigned int r = 0; r < B->n; r++){
        int start = h_csrRowPtr[r + 1];
        int end = h_csrRowPtr[r + 1];
        bool not_zero = h_output_Vals[r] > tol || h_output_Vals[r] < -tol;

        for(unsigned int c = start; c < end; c++){
            int col = h_csrColInd[c];
            if(col != i && col != j){
                if(col < i && col > j){
                    col++;
                }else if(col > i && col < j){
                    col--;
                }
                csr_triple t(r, col, h_csrVal[c]);
                B_triplets.push_back(t);
            }

            if(not_zero && col <= j && (col + 1 == end || h_csrColInd[c + 1] > j)){
                csr_triple t(r,j,h_output_Vals[r]);
                B_triplets.push_back(t);
            }

        }
    }

    int64_t h_new_nnz = (int64_t)B_triplets.size();
    int* h_new_csrRowPtr = (int*) malloc((B->m + 1)* sizeof(int));
    int* h_new_csrColInd = (int*) malloc(h_new_nnz* sizeof(int));
    val_T* h_new_csrVal = (val_T*) malloc(h_new_nnz* sizeof(val_T));
    int row_idx = 0;
    h_new_csrRowPtr[0] = 0;
    int t = 0;

    for(csr_triple tripl:B_triplets){
        h_new_csrColInd[t] = thrust::get<1>(tripl);;
        h_new_csrVal[t] = thrust::get<2>(tripl);;
        if(row_idx != thrust::get<0>(tripl)){
            h_new_csrRowPtr[row_idx + 1] = t;
            row_idx++;
        }
        t++;
    }

     int* d_new_csrRowPtr = nullptr;
     int* d_new_csrColInd = nullptr;
    val_T* d_new_csrVal = nullptr;

    CHECK_CUDA(cudaMalloc((void**) &d_new_csrColInd, sizeof(int)*h_new_nnz))
    CHECK_CUDA(cudaMalloc((void**) &d_new_csrRowPtr, sizeof(int)*(B->m + 1)))
    CHECK_CUDA(cudaMalloc((void**) &d_new_csrVal, sizeof(val_T)*h_new_nnz))

    CHECK_CUDA(cudaMemcpy(d_new_csrColInd, h_new_csrColInd,sizeof(int)*h_new_nnz, cudaMemcpyHostToDevice))
    CHECK_CUDA(cudaMemcpy(d_new_csrRowPtr, h_new_csrRowPtr,sizeof(int)*(B->m + 1), cudaMemcpyHostToDevice))
    CHECK_CUDA(cudaMemcpy(d_new_csrVal, h_new_csrVal,sizeof(val_T)*h_new_nnz, cudaMemcpyHostToDevice))

    CHECK_CUSPARSE(cusparseCsrSetPointers(B->spDescr, d_new_csrRowPtr, d_new_csrColInd, d_new_csrVal))

    B->nnz = h_new_nnz;

    free(h_csrVal);
    free(h_csrRowPtr);
    free(h_csrColInd);
    free(h_output_Vals);
    free(h_new_csrRowPtr);
    free(h_new_csrColInd);
    free(h_new_csrVal);

}


// TEST FROM HERE ON


void TestBasisPrint() {
    {
    BasisVector b(5, 12);
    PRINT_FUNC_PREFIX;
    std::cout << "cuda err=" << cudaGetLastError() << std::endl;
    b.print_vecs();

    b.print();
    }
    PRINT_PREFIX;
    std::cout << "destroyed... b " << std::endl;
    std::cout << "cuda err="<<cudaGetLastError() << std::endl;
}
*/

void TestWriteVecAtInd(cusparseHandle_t* handle) {
    val_T c_values[]={0,6,9,2.5,6};
    val_T d_values[]={-1,-1,-1,-1,-1};
    BasisVector b{3, sizeof(c_values)/sizeof(c_values[0])};

    DenseVector *c = new DenseVector(sizeof(c_values)/sizeof(c_values[0]), c_values);
    DenseVector *d = new DenseVector(sizeof(d_values)/sizeof(d_values[0]), d_values);
    PRINT_PREFIX;
    printVector(c, "c");
    PRINT_PREFIX;
    printVector(d, "d");

    TESTWriteVectorAtIndexes(handle, &b, d, c);

    PRINT_PREFIX;
    printVector(c, "c");
    PRINT_PREFIX;
    printVector(d, "d");

    val_T A_vals[9] = {4, 9, 15,
                    2, 3, 5,
                    3, 7, 14};
    val_T B_vals[9] = {3, 8, 14,
                    1, 2, 4,
                    2, 6, 13};
    int rowP[4] = {0, 3, 6, 9};
    int colI[9] = {0, 1, 2, 0, 1, 2, 0, 1, 2};
    PRINT_PREFIX;
    std::cout<<"creating SparseMatrix A"<<std::endl;
    SparseMatrix A(3, 3, 9, rowP, colI, A_vals);
    std::cout<<"creating SparseMatrix B"<<std::endl;
    SparseMatrix B(3, 3, 9, rowP, colI, B_vals);

    // TESTUpdateBasis(handle, &b, (unsigned int) 0, (unsigned int) 4, &A, &B);

    // printSpMat(&A);

    // printSpMat(&B);

    delete d;
    delete c;
}

void TestUpdateBasis(cusparseHandle_t* handle) {
    unsigned int n = 3;

    unsigned int nnz = 9;
    // int rowPtr[] = {0,   4,  10,  17,  18,  22,  27,  34,  37,  45,  51,  57,  59,
    //     70,  77,  81,  86,  90,  93,  96, 100};

    // int colInd[] = {9, 15, 17, 18,  2,  3,  7,  8, 16, 17,  0,  1,  5,  6,  8, 10, 11,
    //     9,  3,  4, 10, 14,  0,  1,  5, 14, 19,  0,  7, 11, 13, 14, 16, 17,
    //     0,  7, 11,  0,  2,  3,  5,  9, 15, 16, 18,  2, 10, 12, 14, 18, 19,
    //     4,  6,  8, 14, 15, 17,  4, 17,  0,  4,  6,  7,  8,  9, 10, 12, 13,
    //    14, 15,  1,  3,  4, 11, 13, 15, 19,  8, 13, 17, 19,  8, 11, 13, 15,
    //    19,  4,  5,  6, 18,  4,  6,  9,  6, 14, 17,  0,  6,  8, 10};

    // val_T spVals[] = {30., 34., 31., 28., 31., 35., 36., 29., 36., 32., 35., 36., 35.,
    //    26., 28., 41., 37., 39., 44., 33., 33., 28., 33., 31., 35., 35.,
    //    44., 35., 36., 42., 34., 25., 35., 37., 32., 28., 36., 31., 40.,
    //    28., 35., 33., 33., 29., 36., 39., 35., 47., 32., 37., 29., 29.,
    //    45., 44., 36., 33., 41., 38., 31., 24., 35., 39., 29., 44., 36.,
    //    32., 32., 38., 25., 33., 39., 28., 32., 36., 35., 37., 31., 33.,
    //    31., 30., 33., 35., 28., 35., 40., 28., 37., 33., 30., 35., 39.,
    //    39., 35., 37., 34., 29., 35., 30., 36., 35.};

    val_T spVals[9] = {1, 2, 4,
                     3, 8, 14,
                     2, 6, 13};
    
    int rowPtr[4] = {0, 3, 6, 9};

    int colInd[9] = {0, 1, 2, 0, 1, 2, 0, 1, 2};

    SparseMatrix A(n, n, nnz, rowPtr, colInd, spVals);
    SparseMatrix B(n);
    BasisVector b(n, n);

    std::cout << "before update basis" << std::endl;
    printSpMatAsDenseMat(&A, "A");
    printSpMatAsDenseMat(&B, "B");
    b.print();

    // std::cout << "TESTING   TRIPLET   UPDATE" << std::endl;
    // TESTUpdateBasis_triplet(handle, &b, 0, 1, &A, &B);
    // std::cout << "FINISH TESTING   TRIPLET   UPDATE" << std::endl;

    std::cout << "TESTING   CSC   UPDATE" << std::endl;
    updateBasis(handle, &b, 0, 1, &A, &B);
    std::cout << "FINISH   CSC   UPDATE" << std::endl;

    std::cout << "after update basis" << std::endl;
    printSpMatAsDenseMat(&B, "B");
    b.print();
}

void TestGetCol(cusparseHandle_t* handle) {
    unsigned int n = 20;
    val_T outputVlas[n] = {0.f};

  unsigned int nnz = 100;
    int rowPtr[] = {0,   4,  10,  17,  18,  22,  27,  34,  37,  45,  51,  57,  59,
        70,  77,  81,  86,  90,  93,  96, 100};

    int colInd[] = {9, 15, 17, 18,  2,  3,  7,  8, 16, 17,  0,  1,  5,  6,  8, 10, 11,
        9,  3,  4, 10, 14,  0,  1,  5, 14, 19,  0,  7, 11, 13, 14, 16, 17,
        0,  7, 11,  0,  2,  3,  5,  9, 15, 16, 18,  2, 10, 12, 14, 18, 19,
        4,  6,  8, 14, 15, 17,  4, 17,  0,  4,  6,  7,  8,  9, 10, 12, 13,
       14, 15,  1,  3,  4, 11, 13, 15, 19,  8, 13, 17, 19,  8, 11, 13, 15,
       19,  4,  5,  6, 18,  4,  6,  9,  6, 14, 17,  0,  6,  8, 10};

    val_T spVals[] = {30., 34., 31., 28., 31., 35., 36., 29., 36., 32., 35., 36., 35.,
       26., 28., 41., 37., 39., 44., 33., 33., 28., 33., 31., 35., 35.,
       44., 35., 36., 42., 34., 25., 35., 37., 32., 28., 36., 31., 40.,
       28., 35., 33., 33., 29., 36., 39., 35., 47., 32., 37., 29., 29.,
       45., 44., 36., 33., 41., 38., 31., 24., 35., 39., 29., 44., 36.,
       32., 32., 38., 25., 33., 39., 28., 32., 36., 35., 37., 31., 33.,
       31., 30., 33., 35., 28., 35., 40., 28., 37., 33., 30., 35., 39.,
       39., 35., 37., 34., 29., 35., 30., 36., 35.};

    DenseVector* output = new DenseVector(n, outputVlas);
    
    SparseMatrix* A = new SparseMatrix(n, n, nnz, rowPtr, colInd, spVals);

    printSpMat(A);

    printVector(output, "before");

    TESTGetCol(handle, A, output, 2);

    printVector(output, "after");
}

void TestDenseToSparseVec() {
    int n=10;

    val_T vals[n];
    randVals(vals, n);
    DenseVector v(n, vals);

    printVector(&v, "v");

    int spSize;
    int spIdx[n];
    val_T spVal[n];

    val_T* d_denseVals;
    CHECK_CUSPARSE(cusparseDnVecGetValues(v.DnDescr, (void **)&d_denseVals))

    denseVecToSparseVec(d_denseVals, spVal, spIdx, n, &spSize);

    std::cout << "spVec idexes are: \n";
    for (unsigned int i=0; i<spSize; ++i) {
        std::cout << spIdx[i] << ", ";
    }   std::cout << std::endl;

    std::cout << "spVec vlaues are: \n";
    for (unsigned int i=0; i<spSize; ++i) {
        std::cout << spVal[i] << ", ";
    }   std::cout << std::endl;
}



int main (int argc, char** argv) {
    PRINT_PREFIX;
    std::cout << "starting cusparse" << std::endl;
    cusparseHandle_t handle = NULL;
    cusparseCreate(&handle);

    TestDenseToSparseVec();

    // TestBasisPrint();

    // TestWriteVecAtInd(&handle);

    // TestGetCol(&handle);

    TestUpdateBasis(&handle);
}

