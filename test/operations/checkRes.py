import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse import random
from scipy import stats
import sys
np.set_printoptions(threshold=sys.maxsize)

# Set the size of the matrix and vectors
n = 6
v1 = np.random.randint(0, 20, n)
v2 = np.random.randint(0, 20, n)
c = np.random.randint(0, 20, n)

rng = np.random.default_rng()
rvs = stats.poisson(25, loc=10).rvs
M = random(n, n, density=0.25, random_state=rng, data_rvs=rvs, format='csr')
M.toarray()

# Perform matrix multiplication and subtraction
result = M @ v1 - c

# Print vectors and matrix
print("Vector v1:")
print(repr(v1))
print("Vector v2:")
print(repr(v2))
print("\nVector c:")
print(repr(c))
print("\nnnz")
print(repr(M.nnz))
print("\nrowPtr:")
print(repr(M.indptr))
print("\ncolInd:")
print(repr(M.indices))
print("\nvals:")
print(repr(M.data))
print("\nResult (M @ v1 - c):")
print(repr(result))
