#include "simplex.cuh"
#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <string>
#include <cassert>
#include <random>

int randScalar(const int min=1, const int max=8) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(min, max); // define the range

    return distr(gen);
}

void randVals(val_T* vals, unsigned int size) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(25, 63); // define the range

    for (unsigned int i=0; i<size; ++i) {
        vals[i] = distr(gen);
    }
}

void sparseRandVals(val_T* vals, unsigned int size) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distr(-20, 10);

    for (unsigned int i=0; i<size; ++i) {
        int v = distr(gen);
        if (v > 0)
            vals[i] = v;
        else
            vals[i] = 0.f;
    }
}

// gets number of nnz in a densematrix that is safed in a array of size 
void nnzOfDenseMatrix(const val_T* data, const unsigned int size,
                   const int numRows, const int numCols,
                   int* nnz) {
    int tmpNnz = 0;
    for (int i=0; i<numRows; ++i) {
        for (int j=0; j<numCols; ++j) {
            if (std::abs(data[i*numRows + j]) > (val_T)10e-4) {
                ++tmpNnz;
            }
        }
    }
    *nnz = tmpNnz;
}

// converst a dense matrix that is in for of one long array of size into a sparseMatrix fo numRows x numCols
// PRE: size = numRows * numCols
void denseToSparse(const val_T* data, const unsigned int size,
                   const int numRows, const int numCols,
                   int* rowPtr, int* colInd, val_T* vals, int nnz) {
    if (size != numRows * numCols) {
        std::cout << "62: there is a size mismatch" << std::endl;
        throw "size mismatch";
    }

    rowPtr[0] = 0;
    int tmpNnz = 0;
    for (int i=0; i<numRows; ++i) {
        for (int j=0; j<numCols; ++j) {
            if (std::abs(data[i*numRows + j]) > (val_T)10e-4) {
                colInd[tmpNnz] = j;
                vals[tmpNnz] = data[i*numRows + j];
                ++tmpNnz;
            }
        }
        rowPtr[i+1] = tmpNnz;
    }
}

bool compDenseVector(const DenseVector* a, const DenseVector* b, const float tol=10e-4) {
    if (a->size != b->size)
        return false;

    int size = a->size;

    val_T *d_a_vals, *d_b_vals;
    CHECK_CUSPARSE(cusparseDnVecGetValues(a->DnDescr, (void**)&d_a_vals))
    CHECK_CUSPARSE(cusparseDnVecGetValues(b->DnDescr, (void**)&d_b_vals))

    val_T h_a_vals[size];
    val_T h_b_vals[size];

    CHECK_CUDA(cudaMemcpy(h_a_vals, d_a_vals, size * sizeof(val_T), cudaMemcpyDeviceToHost))
    CHECK_CUDA(cudaMemcpy(h_b_vals, d_b_vals, size * sizeof(val_T), cudaMemcpyDeviceToHost))

    for (int i=0; i<size; ++i) {
        if (std::abs( h_a_vals[i] - h_b_vals[i] ) > tol)
            return false;
    }
    
    return true;
}

__global__ void spmv_csr_scalar_kernel ( const int num_rows ,
                                        const int * ptr ,
                                        const int * indices ,
                                        const float * data ,
                                        const float * x ,
                                        const float * y,
                                        float * z) {
    int row = blockDim.x * blockIdx.x + threadIdx.x ;

    if( row < num_rows ){
        float dot = 0;

        int row_start = ptr [ row ];
        int row_end = ptr [ row +1];

        for (int jj = row_start ; jj < row_end ; jj ++)
            dot += data [ jj ] * x[ indices [ jj ]];

        z[ row ] = dot - y[ row ];
    }
}

void TESTSPMV(cusparseHandle_t* handle, const SparseMatrix* A, DenseVector* v1, DenseVector* c, DenseVector* v2) {
    // Get the csr data from A
    int64_t h_n, h_m, h_nnz;
    int* d_csrRowPtr;
    int* d_csrColInd;
    val_T* d_csrVal;

    cusparseIndexType_t h_rowPtrType, h_colIndType;
    cusparseIndexBase_t h_idxBase;
    cudaDataType h_valueType;

    CHECK_CUSPARSE(cusparseCsrGet(A->spDescr,
                                  &h_m,
                                  &h_n,
                                  &h_nnz,
                                  (void**)&d_csrRowPtr,
                                  (void**)&d_csrColInd,
                                  (void**)&d_csrVal,
                                  &h_rowPtrType,
                                  &h_colIndType,
                                  &h_idxBase,
                                  &h_valueType))

    //transform A into CSC
    int* d_cscColPtr;
    int* d_cscRowInd;
    val_T* d_cscVal;

    CHECK_CUDA(cudaMalloc((void**)&d_cscColPtr, (h_n + 1) * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_cscRowInd, h_nnz     * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_cscVal,    h_nnz     * sizeof(val_T)))
    std::cout << "\n Allocated for CSC:\n" << 
                            "cscColPtr=" << (h_n + 1) * sizeof(int) << 
                            "cscRowInd=" << h_nnz     * sizeof(int) << 
                            "cscVal="    << h_nnz     * sizeof(val_T) << "\n" << std::endl;
    
    // Buffer info
    size_t pBufferSize;
    void* pBuffer = NULL;

    cusparseCsr2CscAlg_t algo{CUSPARSE_CSR2CSC_ALG1};
    
    CHECK_CUSPARSE(cusparseCsr2cscEx2_bufferSize(*handle,
                                                 h_m,
                                                 h_n,
                                                 h_nnz,
                                                 d_csrVal,
                                                 d_csrRowPtr,
                                                 d_csrColInd,
                                                 d_cscVal,
                                                 d_cscColPtr,
                                                 d_cscRowInd,
                                                 h_valueType,
                                                 CUSPARSE_ACTION_NUMERIC,
                                                 h_idxBase,
                                                 algo,
                                                 &pBufferSize))
    
    // Allocate pBuffer
    CHECK_CUDA(cudaMalloc((void**)&pBuffer, pBufferSize))
    PRINT_FUNC_PREFIX; std::cout << "\n pBufferSize: " << pBufferSize << std::endl;
    
    CHECK_CUSPARSE(cusparseCsr2cscEx2(*handle,
                                    h_m,
                                    h_n,
                                    h_nnz,
                                    d_csrVal,
                                    d_csrRowPtr,
                                    d_csrColInd,
                                    d_cscVal,
                                    d_cscColPtr,
                                    d_cscRowInd,
                                    h_valueType,
                                    CUSPARSE_ACTION_NUMERIC,
                                    h_idxBase,
                                    algo,
                                    pBuffer))

    CHECK_CUDA(cudaFree(pBuffer))

    val_T* Cvals;
    CHECK_CUSPARSE(cusparseDnVecGetValues(c->DnDescr, (void**)&Cvals))

    val_T* V1vals;
    CHECK_CUSPARSE(cusparseDnVecGetValues(v1->DnDescr, (void**)&V1vals))

    val_T* V2vals;
    CHECK_CUDA(cudaMalloc((void**)&V2vals, h_n * sizeof(val_T)))

    spmv_csr_scalar_kernel<<<1, h_n>>>(h_n,
                                       d_cscColPtr,
                                       d_cscRowInd,
                                       d_cscVal,
                                       V1vals,
                                       Cvals,
                                       V2vals);
    
    CHECK_CUDA(cudaDeviceSynchronize())

    CHECK_CUDA(cudaFree(d_cscColPtr))
    CHECK_CUDA(cudaFree(d_cscRowInd))
    CHECK_CUDA(cudaFree(d_cscVal))

    CHECK_CUSPARSE(cusparseDnVecSetValues(v2->DnDescr, V2vals))

    return;
}

void testUnitJ() {
    unsigned int n=8;
    val_T unitVals[n];

    DenseVector* unitVec = new DenseVector(n, unitVals);

    printVector(unitVec, "initial unit vec");

    for (unsigned int i=0; i<n; ++i) {
        unitJvector(unitVec, 2);

        printVector(unitVec, "unit vec moved to j=2");
    }

}

void testDot() {
    unsigned int n=8;
    val_T aVal[n];
    val_T bVal[n];
    val_T* res;

    randVals(aVal, n);
    randVals(bVal, n);

    DenseVector* a = new DenseVector(n, aVal);
    DenseVector* b = new DenseVector(n, bVal);

    *res = 0.f;
    dot(a, b, res);

    printVector(a, "a");
    printVector(b, "b");
    
    std::cout << "cpu result is=" << *res << std::endl;

    CHECK_CUDA(cudaDeviceSynchronize());

    // *res = 0.f;
    // dotGPU(a, b, res);

    // std::cout << "gpu result is=" << *res << std::endl;
}

void testCalcV2(cusparseHandle_t* handle) {
    unsigned int n = 6;
    val_T v1Vals[] = {8, 15, 16, 15,  3,  9};

    val_T v2Vals[] = {14,  0, 18,  6, 13, 14};

    val_T cVals[] = {9, 13,  2, 17,  1, 15};

    unsigned int nnz = 9;
    int rowPtr[] = {0, 3, 5, 6, 6, 8, 9};

    int colInd[] = {0, 1, 4, 3, 5, 2, 2, 5, 2};

    val_T spVals[] = {31., 35., 25., 32., 28., 31., 28., 40., 30.};

    DenseVector* v1 = new DenseVector(n, v1Vals);
    DenseVector* v2 = new DenseVector(n, v2Vals);
    DenseVector* c = new DenseVector(n, cVals);

    SparseMatrix* A = new SparseMatrix(n, n, nnz, rowPtr, colInd, spVals);

    // calculateV2(handle, A, v1, c, v2);
    TESTSPMV(handle, A, v1, c, v2);

    printVector(v2, "v2");
}

void testCalcV2_random(cusparseHandle_t* handle, const int numIter) {
    for (int i=0; i<numIter; ++i) {
        
        // unsigned int n = 3;
        unsigned int numRows = randScalar();
        unsigned int numCols = randScalar();
        unsigned int size = numRows * numCols;

        val_T v1Vals[numRows];
        randVals(v1Vals, numRows);

        val_T v2Vals[numCols];
        randVals(v2Vals, numCols);
        
        val_T cVals[numCols];
        randVals(cVals, numCols);

        val_T denseMatData[size];
        sparseRandVals(denseMatData, size);

        int nnz;
        nnzOfDenseMatrix(denseMatData, size, numRows, numCols, &nnz);

        int rowPtr[numRows+1];
        int colInd[nnz];
        val_T spVals[nnz];

        denseToSparse(denseMatData, size, numRows, numCols, rowPtr, colInd, spVals, nnz);

        DenseVector* v1 = new DenseVector(numRows, v1Vals);
        DenseVector* v2_cusparse = new DenseVector(numCols, v2Vals);
        DenseVector* v2_kernel = new DenseVector(numCols, v2Vals);
        DenseVector* c = new DenseVector(numCols, cVals);

        SparseMatrix* A = new SparseMatrix(numRows, numCols, nnz, rowPtr, colInd, spVals);

        calculateV2(handle, A, v1, c, v2_cusparse, false);
        TESTSPMV(handle, A, v1, c, v2_kernel);


        if (!compDenseVector(v2_cusparse, v2_kernel)) {
            printVector(v1, "v1");
            printArr(v2Vals, numRows, "v2");
            printVector(c, "c");
            printArr(denseMatData, size, "denseMatData");

            printSpMatAsDenseMat(A, "A");

            std::cout << "\n\n\n cusparse spmv results in:\n" << std::endl;
            printVector(v2_cusparse, "v2 cusparse");

            std::cout << "\n\n\n kernel results in:\n" << std::endl;
            printVector(v2_kernel, "v2 kernel");
        }
    }
}



void testCalcV2_2(cusparseHandle_t* handle) {
    unsigned int n = 3;
    val_T v1Vals[] = {1.66667,  0, 0};

    val_T v2Vals[] = {0, 0, 0};

    val_T cVals[] = {3, 5, 4};

    unsigned int nnz = 7;
    int rowPtr[] = {0, 2, 4, 7};

    int colInd[] = {0, 1, 1, 2, 0, 1, 2};

    val_T spVals[] = {2, 3, 2, 5, 3, 2, 4};

    DenseVector* v1 = new DenseVector(n, v1Vals);
    DenseVector* v2 = new DenseVector(n, v2Vals);
    DenseVector* c = new DenseVector(n, cVals);
	
    SparseMatrix* A = new SparseMatrix(n, n, nnz, rowPtr, colInd, spVals);

    calculateV2(handle, A, v1, c, v2);

    printVector(v2, "v2");
}

void testCalcV2_markshare_ohnemps(cusparseHandle_t* handle) {
     /*if (argc<2) {
        std::cout << "error please provide a filename"<<std::endl;
        return;
    }*/
	//get filename from the first input parameter
	//char* filename = argv[1];
	//std::string outputFile = argv[1];

	// Manipulations for name of output file
	//int startTest = outputFile.find("test");
	//outputFile = outputFile.replace(startTest, 4, "sol");
	//std::string testName = outputFile.erase(startTest-1, 4);
	int n = 34;
	SparseMatrix *A = new SparseMatrix(n);
	//DenseVector *b = nullptr;
	
	
	PRINT_FUNC_PREFIX; std::cout << "Initialized Matrix and Vector: A,b,c" << std::endl;
	
	//read A b and c from file
	//mps_reader(filename, &A, &b, &c);
	
	//PRINT_FUNC_PREFIX; std::cout << "MPS_read done" << std::endl;
	

	PRINT_FUNC_PREFIX; std::cout << "size of DenseVector c" << n << std::endl;
	
	DenseVector::value_t v1Vals[n];
	for (unsigned int k = 0; k < n; ++k) {
		v1Vals[k] = 0;
	}
	
	PRINT_FUNC_PREFIX; std::cout << "v1Vals initialized" << std::endl;
	
	//val_T *cVals_d;
	val_T cVals_h[n];
	*(cVals_h) = 786;
	*(cVals_h + 1) = 759;
	*(cVals_h + 2) = 888;
	*(cVals_h + 3) = 649;
	for (unsigned int k = 4; k < n; ++k) {
		cVals_h[k] = 1;
	}
	DenseVector *c = new DenseVector(n, cVals_h);
	//PRINT_FUNC_PREFIX; std::cout << "cVals Vector initialized" << std::endl;
	//CHECK_CUSPARSE(cusparseDnVecGetValues(c->DnDescr, (void **)&cVals_d))
	//PRINT_FUNC_PREFIX; std::cout << "cusparseDnVecGetValues done" << std::endl;
	//CHECK_CUDA(cudaMemcpy(cVals_h, cVals_d, n*sizeof(val_T), cudaMemcpyDeviceToHost))
	//PRINT_FUNC_PREFIX; std::cout << "cudaMemcpy done" << std::endl;
	val_T v2Vals[n];
	
	std::cout << "initialized Array of length n v2Vals" << std::endl;
	for (unsigned int k = 0; k < n; ++k) {
		v2Vals[k] = -cVals_h[k];
	}
	std::cout << "v2Vals initialized" << std::endl;
	
	
	
	
    DenseVector* v1 = new DenseVector(n, v1Vals);
    DenseVector* v2 = new DenseVector(n, v2Vals);
    
	std::cout << "Initialized DenseVectors v1 and v2" << std::endl;
    calculateV2(handle, A, v1, c, v2);
	std::cout << "calculated V2" << std::endl;
    printVector(v2, "v2");
    
    
    delete v1;
    delete v2;
    
    v1 = nullptr;
    v2 = nullptr;
}

void testCalcV2_markshare_mitmps(cusparseHandle_t* handle, int argc, char** argv) {
     if (argc<2) {
        std::cout << "error please provide a filename"<<std::endl;
        return;
    }
	//get filename from the first input parameter
	char* filename = argv[1];
	//std::string outputFile = argv[1];

	// Manipulations for name of output file
	//int startTest = outputFile.find("test");
	//outputFile = outputFile.replace(startTest, 4, "sol");
	//std::string testName = outputFile.erase(startTest-1, 4);
	int n = 34;
	SparseMatrix *A = new SparseMatrix(n);
	DenseVector *b = nullptr;
	DenseVector *c = nullptr;
	
	PRINT_FUNC_PREFIX; std::cout << "Initialized Matrix and Vector: A,b,c" << std::endl;
	
	//read A b and c from file
	mps_reader(filename, &A, &b, &c);
	
	PRINT_FUNC_PREFIX; std::cout << "MPS_read done" << std::endl;
	

	PRINT_FUNC_PREFIX; std::cout << "size of DenseVector c" << n << std::endl;
	
	DenseVector::value_t v1Vals[n];
	for (unsigned int k = 0; k < n; ++k) {
		v1Vals[k] = 0;
	}
	
	PRINT_FUNC_PREFIX; std::cout << "v1Vals initialized" << std::endl;
	
	val_T *cVals_d;
	val_T cVals_h[n];
	*(cVals_h) = 786;
	*(cVals_h + 1) = 759;
	*(cVals_h + 2) = 888;
	*(cVals_h + 3) = 649;
	for (unsigned int k = 4; k < n; ++k) {
		cVals_h[k] = 1;
	}
	
	PRINT_FUNC_PREFIX; std::cout << "cVals Vector initialized" << std::endl;
	CHECK_CUSPARSE(cusparseDnVecGetValues(c->DnDescr, (void **)&cVals_d))
	PRINT_FUNC_PREFIX; std::cout << "cusparseDnVecGetValues done" << std::endl;
	CHECK_CUDA(cudaMemcpy(cVals_h, cVals_d, n*sizeof(val_T), cudaMemcpyDeviceToHost))
	//PRINT_FUNC_PREFIX; std::cout << "cudaMemcpy done" << std::endl;
	val_T v2Vals[n];
	
	std::cout << "initialized Array of length n v2Vals" << std::endl;
	for (unsigned int k = 0; k < n; ++k) {
		v2Vals[k] = -cVals_h[k];
	}
	std::cout << "v2Vals initialized" << std::endl;
	
	
	
	
    DenseVector* v1 = new DenseVector(n, v1Vals);
    DenseVector* v2 = new DenseVector(n, v2Vals);
    
	std::cout << "Initialized DenseVectors v1 and v2" << std::endl;
    calculateV2(handle, A, v1, c, v2);
	std::cout << "calculated V2" << std::endl;
    printVector(v2, "v2");
    
    
    delete v1;
    delete v2;
    
    v1 = nullptr;
    v2 = nullptr;
}


int main (int argc, char** argv) {

    cusparseHandle_t handle = NULL;
    cusparseCreate(&handle);

    std::cout << "start test on on calc V2" << std::endl;
    testCalcV2_random(&handle, 10);
    std::cout << "finisehd test on calc V2\n\n" << std::endl;


    // std::cout << "start test on unitJvec" << std::endl;
    // testUnitJ();
    // std::cout << "finish test on unitJvec\n\n" << std::endl;

    // std::cout << "start test on dot" << std::endl;
    // testDot();
    // std::cout << "finsih test on dot" << std::endl;
	std::cout << "start test on on calc V2 with markshare input without mps" << std::endl;
    testCalcV2_markshare_ohnemps(&handle);
    std::cout << "finisehd test on calc V2 with markshare input without mps\n\n" << std::endl;
    
    std::cout << "start test on on calc V2 with markshare input with mps " << std::endl;
    testCalcV2_markshare_mitmps(&handle, argc, argv);
    std::cout << "finisehd test on calc V2 with markshare input with mps \n\n" << std::endl;

    return 0;
}
