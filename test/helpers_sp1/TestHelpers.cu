#include "simplex.cuh"
// #include <__clang_cuda_runtime_wrapper.h>
#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <string>
#include <cassert>
#include <cstdlib>
#include <random>





template <typename T>
bool testCSRRowPtr(const T* rowPtr, const int rows, const int nnz) {
    for (int i=1; i<rows+1; ++i) {
        if (rowPtr[i-1] > rowPtr[i]){
            std::cout << "rowPtr " << i << " is smaller than rowPtr " << i-1 << "  (" << rowPtr[i] << "<" << rowPtr[i-1] << ")" << std::endl;
            return false;
        }
    }

    if (rowPtr[0] != 0) {
        std::cout << "rowPtr[0]=" << rowPtr[0] << "  should be 0" << std::endl;
        return false;
    }

    if (rowPtr[rows] != nnz) {
        std::cout << "rowPtr[rows]=" << rowPtr[rows] << "  should be nnz=" << nnz << std::endl; 
        return false;
    }

    return true;
}

template <typename T>
bool testCSRColInd(const T* rowPtr, const T* colInd, const int rows, const int cols, const int nnz) {
    for (int i=1; i<rows+1; ++i) {
        int lastCol = 0;
        for (int j=rowPtr[i-1]; j<rowPtr[i]; ++j) {
            if (colInd[j] < 0 || colInd[j] >= cols) {
                std::cout << "colInd out of bounds at " << j << " value is=" << colInd[j] << std::endl;
                return false;
            }
            if (lastCol >= colInd[j]) {
                std::cout << "colInd ordering wrong at " << j << " value is=" << colInd[j] << std::endl;
                return false;
            }
            lastCol = colInd[j];
        }
    }

    return true;
}

int randScalar(const int min=2, const int max=8) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(min, max); // define the range

    return distr(gen);
}

void randRowPtr(int* rowPtr, int size, int max=2) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::exponential_distribution<> d(1);

    rowPtr[0] = 0;
    
    for (int i=1; i<size+1; ++i) {
        rowPtr[i] = rowPtr[i-1] + std::max(std::min((int)d(gen), max), 1);
    }
}

void randColInd_Val(const int* rowPtr, int* colInd, val_T* val, int numRows, int numCols) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr_ind(0, numCols-1); // define the range
    std::uniform_int_distribution<> distr_val(1, 10);

    for (int i=0; i<numRows; ++i) {
        int last_col = 0;
        for (int j=rowPtr[i]; j<rowPtr[i+1]; ++j) {
            int v = distr_ind(gen);
            int max = (numCols - 1 - last_col) - (rowPtr[i+1]-j);
            int dcol = std::max(std::min(v, max), 1);

            colInd[j] = last_col + dcol;
            last_col = last_col + dcol;

            assert(max < numCols - 1);
            assert(dcol > 0 && dcol < numCols);
            val[j] = distr_val(gen);
        }
    }

    int nnz = rowPtr[numRows];

    if (!testCSRRowPtr(rowPtr, numRows, nnz) ||
        !testCSRColInd(rowPtr, colInd, numRows, numCols, nnz)) {
        std::cout << "numRows=" << numRows << " numCols=" << numCols << " nnz=" << nnz << std::endl;
        printArr(rowPtr, numRows+1, "rowPtr");
        printArr(colInd, nnz, "colInd");
        printArr(val, nnz, "val");

        throw "wrong";
    }
}



void TcreateA_aux(const SparseMatrix* A, SparseMatrix** A_aux) {
	//A_aux.n = A->n + 1;
	//A_aux.nnz = A->nnz + A->m;
	int cols = A->n + 1;
	int rows = A->m;
	int nnz = A->nnz + A->m;

	int* rowPtr = new int[rows+1];
	int* colInd = new int[nnz];
	SparseMatrix::value_t* val = new SparseMatrix::value_t[nnz];

	int ArowPtr[A->m+1];
	int AcolInd[A->nnz];
	SparseMatrix::value_t Aval[A->nnz];

	CHECK_CUDA(cudaMemcpy(ArowPtr, A->csrRowPtr, sizeof(int)*(A->m + 1), cudaMemcpyDeviceToHost))
	CHECK_CUDA(cudaMemcpy(AcolInd, A->csrColInd, sizeof(int)*(A->nnz), cudaMemcpyDeviceToHost))
	CHECK_CUDA(cudaMemcpy(Aval, A->csrVal, sizeof(SparseMatrix::value_t)*(A->nnz), cudaMemcpyDeviceToHost))
	

	for (int i=0; i < rows+1; ++i) {
		rowPtr[i] = ArowPtr[i] + i;		// as we add another column that has an entry in each case we need to insert that column into colInd
	}								    // to respect that the rowPtr also has to point to the new correct location -> each row has now an extra entry

	int lastColIdx = -1;
	int offset = 0;
	for (int i=0; i<A->nnz; ++i) {
		if (AcolInd[i] < lastColIdx) {		// means that we are now in an other row
			colInd[i+offset] = cols-1;		// we need to add the last column in before we start adding the vals from the next row
			val[i+offset] = -1;
			++offset;
		}

		lastColIdx = AcolInd[i];
		colInd[i+offset] = lastColIdx;
		val[i+offset] = Aval[i];
	}

    // set the last one also to -1
    colInd[nnz-1] = cols-1;
    val[nnz-1] = -1;

    // ++offset;
	// assert(offset == A->m);
	// otherwise means that something went wrong

	*A_aux = new SparseMatrix(rows, cols, nnz, rowPtr, colInd, val);
}

void testAux(int numIter, int min, int max) {
	for (int i=0; i<numIter; ++i) {

		unsigned int numRows = randScalar(min, max);
		unsigned int numCols = randScalar(min, max);

		int rowPtr[numRows+1];
		randRowPtr(rowPtr, numRows);

		int nnz = rowPtr[numRows];
		int colInd[nnz];
		val_T vals[nnz];

		randColInd_Val(rowPtr, colInd, vals, numRows, numCols);

		SparseMatrix* A = new SparseMatrix(numRows, numCols, nnz, rowPtr, colInd, vals);

		printSpMatAsDenseMat(A, "A");
		SparseMatrix* A_aux;
		TcreateA_aux(A, &A_aux);
		printSpMatAsDenseMat(A_aux, "A_aux");
	}
}

int main (int argc, char** argv) {
	int numIter = 100;
    if (argc > 1) {
        numIter = atoi(argv[1]);
        if (numIter <= 0) {
            std::cout << "nonvalid input for num iterations. Should be Integer bigger than 0" << std::endl;
            return 0;
        }
    }

    int min = 2;
    if (argc > 2) {
        min = atoi(argv[2]);
        if (min <= 0) {
            std::cout << "nonvalid input for minimum Dimension. Should be Integer bigger than 0" << std::endl;
            return 0;
        }
    }

    int max = 10;
    if (argc > 3) {
        max = atoi(argv[3]);
        if (max < min) {
            std::cout << "nonvalid input for maximum Dimension. Should be Integer bigger than Minimum Dimension (" << min << ")" << std::endl;
            return 0;
        }
    }

    std::cout << "STARTING: testing creation of AAux" << std::endl;
    testAux(numIter, min, max);
    std::cout << "ENDING: testing creation of AAux" << std::endl;
    
    return 0;
}