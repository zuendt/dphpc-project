#include "simplex.cuh"

// profiling variable right now only for simplex phase 2
// 0 don't profile
// 1 whole loop
// 2 WriteVectorAtIndicies
// 3 luSolve
// 4 calculateV2
// 5 selectEntering
// 6 getCol
// 7 selectLeaving
// 8 updateBasis


bool simplexPhase1(cusparseHandle_t* handle, cusolverSpHandle_t* solvHandle, const SparseMatrix* A,
				   SparseMatrix* B, const DenseVector*  b, const DenseVector* c, DenseVector* cB,
				   BasisVector* basis, DenseVector* xb, int maxNumIterations, int* status, const char profile) {
	// Check if the problem is already feasible (if all b entries are positive)
    PRINT_FUNC_PREFIX;
	std::cout << "selectingEnteringVar" << std::endl << std::endl;

	int i;
	
	bool feasible = !selectEnteringVar(b, &i, b, false);
	if (feasible) {
		// Taking 5 as feasible here
		std::cout <<"The problem was feasible form the start" << std::endl;
		*status = 5;
		return true;
	}

	//Create Objective vector cAux for the auxilary problem (Size A.n + 1 and all zero except last entry which is -1)
	val_T hlpr[A->n + 1];
	for (unsigned int k = 0; k < A->n; ++k) {
		hlpr[k] = 0;
	}
	hlpr[A->n] = -1.0;
    PRINT_PREFIX;
	std::cout << "creating cAux:" << std::endl;
	DenseVector cAux(A->n + 1, hlpr);

	// Create a new matrix A_aux which is a with an additional column of -1's
    PRINT_PREFIX;
	std::cout << "creating A_aux via creatA_aux(A)" << std::endl;
	SparseMatrix* A_aux;
	createA_aux(A, &A_aux);

	// Pivot in auxilary column
	int j = A->n-1;
	// What are the indices of basis here at the beginning? The slack variable columns are not part of A
    PRINT_PREFIX;
	std::cout << "UpdatingBasis" << std::endl;
	updateBasis(handle, basis, i, j, A_aux, B);

    PRINT_PREFIX;
	std::cout << "WritingVectorAtIndices" << std::endl;
	writeVectorAtIndices(handle, basis, cB, &cAux);

	// Solve the auxilary problem:
    PRINT_PREFIX;
	std::cout << "Using simplexPhase2 to get into feasible region" << std::endl;
	bool feasible1 = simplexPhase2(handle, solvHandle, A_aux, B, b, &cAux, cB, basis, xb, maxNumIterations, status, profile);

	// Check that auxilary variable is not in basis
	int numBlocks = basis->m_;

	bool* feasible2_d;
	bool feasible2 = true;
	CHECK_CUDA(cudaMalloc((void **)&feasible2_d, sizeof(bool)))
	CHECK_CUDA(cudaMemcpy(feasible2_d, &feasible2, sizeof(bool), cudaMemcpyHostToDevice))

	int* idx_d;
	int idx = cAux.size-1;
	CHECK_CUDA(cudaMalloc((void **)&idx_d, sizeof(int)))
	CHECK_CUDA(cudaMemcpy(idx_d, &idx, sizeof(int), cudaMemcpyHostToDevice))
	
	unsigned int* basisValues;
	CHECK_CUSPARSE(cusparseSpVecGetValues((basis->descr), (void **)&basisValues))

	checkMinimality<<<numBlocks, 1>>>(basisValues, idx_d, feasible2_d);
	CHECK_CUDA(cudaMemcpy(&feasible2, feasible2_d, sizeof(bool), cudaMemcpyDeviceToHost))

	CHECK_CUDA(cudaFree(feasible2_d))
	CHECK_CUDA(cudaFree(idx_d))

    PRINT_PREFIX;
	std::cout << "Give cB the correct values for phase 2" << std::endl;
	writeVectorAtIndices(handle, basis, cB, c);

	// Free A_aux (Not covered by destructor since its just a pointer)
	// A_aux->~SparseMatrix();
	delete A_aux;
	A_aux = nullptr;

	if (feasible1 && feasible2) {
		// Taking 5 as feasible here
		*status = 5;
		return true;
	} else {
		if (!feasible2 && feasible1) {
			*status = 0;
		} else {
			// Unknown might be termination through max iterations
			*status = 4;
		}
		return false;
	}

}


void createA_aux(const SparseMatrix* A, SparseMatrix** A_aux) {
	int cols = A->n + 1;
	int rows = A->m;
	int nnz = A->nnz + A->m;

	int* rowPtr = new int[rows+1];
	int* colInd = new int[nnz];
	SparseMatrix::value_t* val = new SparseMatrix::value_t[nnz];

	int ArowPtr[A->m+1];
	int AcolInd[A->nnz];
	SparseMatrix::value_t Aval[A->nnz];

	CHECK_CUDA(cudaMemcpy(ArowPtr, A->csrRowPtr, sizeof(int)*(A->m + 1), cudaMemcpyDeviceToHost))
	CHECK_CUDA(cudaMemcpy(AcolInd, A->csrColInd, sizeof(int)*(A->nnz), cudaMemcpyDeviceToHost))
	CHECK_CUDA(cudaMemcpy(Aval, A->csrVal, sizeof(SparseMatrix::value_t)*(A->nnz), cudaMemcpyDeviceToHost))
	

	for (int i=0; i < rows+1; ++i) {
		rowPtr[i] = ArowPtr[i] + i;		// as we add another column that has an entry in each case we need to insert that column into colInd
	}								    // to respect that the rowPtr also has to point to the new correct location -> each row has now an extra entry

	int lastColIdx = 0;
	int offset = 0;
	for (int i=0; i<A->nnz; ++i) {
		if (AcolInd[i] < lastColIdx) {		// means that we are now in an other row
			colInd[i+offset] = cols-1;		// we need to add the last column in before we start adding the vals from the next row
			val[i+offset] = -1;
			++offset;
		}

		lastColIdx = AcolInd[i];
		colInd[i+offset] = lastColIdx;
		val[i+offset] = Aval[i];
	}

    // set the last one also to -1
    colInd[nnz-1] = cols-1;
    val[nnz-1] = -1;

    // ++offset;
	// assert(offset == A->m);
	// otherwise means that something went wrong

	*A_aux = new SparseMatrix(rows, cols, nnz, rowPtr, colInd, val);
}


__global__ void checkMinimality(unsigned int* basisValues, int* idx, bool* feasible2_d) {

	if (basisValues[blockIdx.x*blockDim.x + threadIdx.x] == *idx) {
		*feasible2_d = false;
	}

}

// A: Constraint matrix of problem in standard form
// b: RHS vector
// c: Cost vector of problem in standard formim
// xb: Solution Vector of basic variables
// status: 0 optiaml, 1 unbounded, 2 not finished
bool simplexPhase2(cusparseHandle_t* handle, cusolverSpHandle_t* solvHandle, const SparseMatrix* A,
				   SparseMatrix* B, const DenseVector* b, const DenseVector* c, DenseVector* cB,
				   BasisVector* basis, DenseVector* xB, int maxNumIterations, int* status, const char profile) {
    // get A, c in canonical form (without slack variable entries)

	bool optimal = false;
	bool success = false;
	*status = 4;

	// v1
	DenseVector v1(A->m);	// #rows

	// v2
	DenseVector v2(A->n);	// #cols

	// // xB
	// DenseVector xB(A->m);	// #constraints

	// BinvA
	DenseVector BinvA(A->m);	// #constraints

	// AColJ (j-th column of A)
	DenseVector AColJ(A->m);

	// Helper for solving with LU
	DenseVector intermediate(A->m);

	// xB indices (0 to xb.size-1) (This vector is float for now since val_T is fixed. Int would make more sense tho)
	val_T* tmp = (val_T *)malloc(xB->size*sizeof(val_T));
	for (int l = 0; l < xB->size; ++l) {
		tmp[l] = l;
	}
	DenseVector xBInd(xB->size, tmp);
	delete tmp;
	tmp = nullptr;


	std::cout << "-------- AT START OF LOOP -------" << std::endl;
	std::cout << "constant data" << std::endl;
	printSpMatAsDenseMat(A, "A");
	printVector(c, "c");
	printVector(b, "b");

	std::cout << "\n non const inital data" << std::endl;
	printSpMatAsDenseMat(B, "B");
	printVector(cB, "cB");
	

	std::cout << "Simplex Phase 2, finished initialization of helpers\n" << std::endl;

	int k;

	// Loop until break or for some max number of iterations
			if (profile == '1') cudaProfilerStart();
	for (k=0; k<maxNumIterations; ++k) {

        // cB <- c at indices specified by basis
				if (profile == '2') cudaProfilerStart();
        writeVectorAtIndices(handle, basis, cB ,c);
				if (profile == '2') cudaProfilerStop();

		// v1 = cB * B^⁻1
				if (profile == '3') cudaProfilerStart();
		luSolve(*solvHandle, *handle, *B, *cB, true, v1);
				if (profile == '3') cudaProfilerStop();
		
		// v2 = cB * B^⁻1 * A - c
				if (profile == '4') cudaProfilerStart();
		calculateV2(handle, A, &v1, c, &v2);
				if (profile == '4') cudaProfilerStop();

		// xB = B^-1 * b
				if (profile == '3') cudaProfilerStart();
		luSolve(*solvHandle, *handle, *B, *b, false, *xB);
				if (profile == '3') cudaProfilerStop();
		
		int j; 
		int i; 

		// check if an optimal solution has been reached
				if (profile == '5') cudaProfilerStart();
		optimal = !selectEnteringVar(&v1, &j, &v2, false);	// if no Pivot is found solution is optimal
				if (profile == '5') cudaProfilerStop();

		if (optimal) { 
			*status = 1;
			break;
		}


		// If j is non-slack: Search in j-th col of A; if j is slack: Search in j-th col of B.
				if (profile == '6') cudaProfilerStart();
		if (j < A->n) {

			getCol(handle, A, &AColJ, j);

		} else if (j < A->n + A->m) {

			getCol(handle, B, &AColJ, j-(A->n));
		
		} else {
			std::cout << "j = " << j << "is dog shit" << std::endl;
		}
				if (profile == '6') cudaProfilerStop();

		// Col j of B^-1 * A
				if (profile == '3') cudaProfilerStart();
		luSolve(*solvHandle, *handle, *B, AColJ, false, BinvA);
				if (profile == '3') cudaProfilerStop();

				if (profile == '7') cudaProfilerStart();
		success = selectLeavingVar(&BinvA, xB, &i, &xBInd); // Tanja
				if (profile == '7') cudaProfilerStop();

		if (!success) {
			*status = 3;
			return false; // all entries negative -> unbounded
		}

		// Update basis
        // A: nonbasic cols
        // B: basic cols
        // reorganizes the old state to the new state, where i is leaving basis col index
        // and j is entering basis col index.
        // do we need to update cB: no it's done in WriteVectorAtIndices at loop start

				if (profile == '8') cudaProfilerStart();
		updateBasis(handle, basis, (unsigned int) i, (unsigned int) j, A, B);
				if (profile == '8') cudaProfilerStop();
		// PRINT_PREFIX; std::cout << "iter=" << k << "  finished UpdateBasis\n" << std::endl;
		CHECK_CUDA(cudaDeviceSynchronize());

		if (11 < k && k < 15) {
			std::cout << "\n\n" << std::endl;
			PRINT_FUNC_PREFIX; std::cout << "	ITERATION:  " << std::setw(6) << k << "\n" << std::endl;
			printVector(cB, "cB");
			printVector(&v1, "v1");
			printVector(&v2, "v2");
			printVector(xB, "xB");
			printVector(&AColJ, "AColJ");
			printVector(&BinvA, "BinvA");

			printSpMat(B, "B");

			std::cout << "\nleaving=" << i << "   entering=" << j << std::endl;		
		}
	}
			if (profile == '1') cudaProfilerStop();
	

	PRINT_FUNC_PREFIX; std::cout << "\nfinished on iteration " << k << std::endl;

    // false if maximum iteration count has been reached but solution has not been found
	return optimal;
}

