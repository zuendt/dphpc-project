#include "simplex.cuh"

template <typename T>
__global__ void copyArray(const T* src, T* dest, int size) {
    int i = blockIdx.x*blockDim.x + threadIdx.x;

    if (i < size) {
        dest[i] = src[i];
    }
}

// Vector v2 = v1*A_nonBasic-c_nonBasic; // v2 = cB * B^⁻1 * A - c, Timo
void calculateV2(cusparseHandle_t* handle, const SparseMatrix* A, const DenseVector* v1, const DenseVector* c, DenseVector* v2, bool debug) {

    int gridDim = 1;
    int blockDim = v2->size;
    if (blockDim > 1024) {
        gridDim = std::ceil(blockDim / 1024);
        blockDim = 1024;
    }
    copyArray<<<gridDim, blockDim>>>(c->values, v2->values, v2->size);
    CHECK_CUDA(cudaDeviceSynchronize())

            if (debug) {
                PRINT_FUNC_PREFIX; std::cout << "perfoming v2 = cB * B^⁻1 * A - c" << std::endl;
                std::cout << "v2: " << v2->size << ", v1: " << v1->size << ", c:" << c->size << std::endl;
                std::cout << "A:  m=" << A->m << ", n=" << A->n << std::endl;

                //printSpMatAsDenseMat(A, "A");
                printVector(v2, "v2");
                printVector(c, "c");
                printVector(v1, "v1");
            }

    // multipliers for SpMB:   v2 = alpha * op(A) * v1 + beta * v2
    const val_T alpha = 1.0f;
    const val_T beta = -1.0f;

    // Buffer info
    size_t pBufferSize = 0;
    void* pBuffer = NULL;
	
    // Query buffer size for the Matrix Vector muliplication
    CHECK_CUSPARSE(cusparseSpMV_bufferSize(*handle,
                        CUSPARSE_OPERATION_TRANSPOSE,
                        &alpha,
                        A->spDescr,
                        v1->DnDescr,
                        &beta,
                        v2->DnDescr,
                        CUDA_R_32F,
                        CUSPARSE_SPMV_ALG_DEFAULT,
                        &pBufferSize))
	//std::cout << "\n pBufferSize before Malloc: " << pBufferSize << std::endl << std::endl;
    // Allocate pBuffer
    //cudaDeviceSynchronize();
    CHECK_CUDA(cudaMalloc(&pBuffer, pBufferSize));

            if (debug)
                std::cout << "\n pBufferSize: " << pBufferSize << std::endl << std::endl;
	
    // calculate v2 = alpha * op(A) * v1 + beta * v2
    CHECK_CUSPARSE(cusparseSpMV(*handle,
                        CUSPARSE_OPERATION_TRANSPOSE,
                        &alpha,
                        A->spDescr,
                        v1->DnDescr,
                        &beta,
                        v2->DnDescr,
                        CUDA_R_32F,
                        CUSPARSE_SPMV_ALG_DEFAULT,
                        pBuffer))

    CHECK_CUDA(cudaDeviceSynchronize())
    CHECK_CUDA(cudaFree(pBuffer))

            if (debug) {
                PRINT_PREFIX; std::cout <<"freeing pBuffer="<<pBuffer<<std::endl;

                PRINT_PREFIX; std::cout << "successfully computed v2" << std::endl;
                printVector(v2, "v2");
            }

    return;
}



void unitJvector(DenseVector* unitVec, const int j) {
    unsigned int sz = unitVec->size;

    if (j >= sz) {
        std::cout << "wrong size:   vector size is=" << sz << "  but is j=" << j << std::endl;
        throw "idx out of bounds";
    }
    
    val_T arr[sz] = {(val_T)0.0};
    arr[j] = (val_T)1.0;
    
    val_T* Garr;
    CHECK_CUDA(cudaMalloc((void**)&Garr, sz*sizeof(val_T)));
    CHECK_CUDA(cudaMemcpy(Garr, arr, sz*sizeof(val_T), cudaMemcpyHostToDevice))

    CHECK_CUSPARSE(cusparseDnVecSetValues(unitVec->DnDescr, (void*)Garr))

    CHECK_CUDA(cudaFree(unitVec->values))
    unitVec->values = Garr;
}


void dot(const DenseVector* a, const DenseVector* b, val_T* res) {

    val_T h_aVals[a->size];
    CHECK_CUDA(cudaMemcpy(h_aVals, a->values, a->size*sizeof(val_T), cudaMemcpyDeviceToHost))

    val_T h_bVals[b->size];
    CHECK_CUDA(cudaMemcpy(h_bVals, b->values, b->size*sizeof(val_T), cudaMemcpyDeviceToHost))

    if (a->size != b->size) {
    	std::cout << "sizes of the vectors do not match" << std::endl;
        return;
    }

    val_T tmp = 0.0f;
    for (unsigned int i=0; i<a->size; ++i) {
        tmp += h_aVals[i] * h_bVals[i];
    }

    *res = tmp;
}



// Helper for dot
__global__ void doCalac(const val_T* aVal, const val_T* bVal, val_T* res) {

    (*res) += (aVal[blockDim.x*blockIdx.x + threadIdx.x] * bVal[blockDim.x*blockIdx.x + threadIdx.x]);

}

void dotGPU(const DenseVector* a, const DenseVector* b, val_T* res) {

    int numThreads = a->size;
    val_T* res_d;
    CHECK_CUDA(cudaMalloc((void**)&res_d, sizeof(val_T)))

    doCalac<<<1,numThreads>>>(a->values, b->values, res_d);

    CHECK_CUDA(cudaDeviceSynchronize());
    
    CHECK_CUDA(cudaMemcpy(res, res_d, sizeof(val_T), cudaMemcpyDeviceToHost))
}

