#ifndef SELECTENTERLEAVE_CU
#define SELECTENTERLEAVE_CU
#include <thrust/find.h> 
#include <thrust/reduce.h>
#include <thrust/functional.h>
#include "simplex.cuh"
#include <climits>
#include <cassert>
#include <iostream>
#include <iomanip>
#include "lib/cuPrintf.cuh"
#include <thrust/execution_policy.h>

#define MAX_RATIO 1e20f

struct negative{
	__host__ __device__
 	bool operator()(int x)
 	{
 		return x < 0;
 	}
};



bool selectEnteringVar(const DenseVector* v1, int* j, const DenseVector* v2, const bool blandsrule) {
    int size1 = v1->size;
    int size2 = v2->size;
	
	//get values
	
    val_T* vals1_d;
    CHECK_CUSPARSE(cusparseDnVecGetValues(v1->DnDescr, (void **)&vals1_d))
 
	val_T* vals2_d;
    CHECK_CUSPARSE(cusparseDnVecGetValues(v2->DnDescr, (void **)&vals2_d))
	
	
	
	
	if(blandsrule){	
		val_T* start_d = vals2_d;
		val_T* end_d = start_d + size2;
		
		//pointer to the first element which is negative executed on GPU, if there is no such negative element-> it == end		
		val_T *it = thrust::find_if(thrust::device, start_d, end_d, negative());

		if (it == end_d){
			end_d =  vals1_d + size1;
			start_d = vals1_d;
			it = thrust::find_if(thrust::device, start_d, end_d, negative());
			if(it == end_d){
				return false;
			}
			else{
				*j = it - start_d + size2;
				return true;
			}
		}
 		else{
			
 			*j = it - start_d;
 			return true;
 		}
	}
	else{
		//find min element
    	val_T* min_ele1_d = thrust::min_element(thrust::device, vals1_d, vals1_d + size1);
    	val_T* min_ele2_d = thrust::min_element(thrust::device, vals2_d, vals2_d + size2);
    	
    	
		val_T min_ele1_h[1];
    	CHECK_CUDA(cudaMemcpy(min_ele1_h, min_ele1_d, sizeof(val_T), cudaMemcpyDeviceToHost))

    	val_T min_ele2_h[1];
    	CHECK_CUDA(cudaMemcpy(min_ele2_h, min_ele2_d, sizeof(val_T), cudaMemcpyDeviceToHost))
    	val_T first = *min_ele1_h;
    	val_T second = *min_ele2_h;

    	if (first <= second){
    	    PRINT_FUNC_PREFIX; std::cout << "Pivot is: " << first << std::endl;
    	    if (TOL + first >= 0.f || first >= 0.f + TOL) return false;
    	    *j = min_ele1_d - vals1_d + size2;
    	}
    	else{
    	    PRINT_FUNC_PREFIX; std::cout << "Pivot is: " << second << std::endl;
    	    if ( TOL + second >= 0.f || second >= 0.f + TOL ) return false;
    	    *j = min_ele2_d - vals2_d;
    	}

		PRINT_FUNC_PREFIX; std::cout << "Pivot index: " << *j << std::endl;

		return true;
	}   
}




__global__ void divKernel(val_T* c, const val_T* a, const val_T* b, int size, val_T maxRatio) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < size) {
        if (b[i] > TOL /*&& a[i] > 0*/){
            c[i] = a[i] / b[i];
        }
        else{
            c[i] = maxRatio;
        }
    } 
}

bool selectLeavingVar(const DenseVector* B_invA, const DenseVector* xB, int* i, const DenseVector* index) {
    int size = xB->size;

    val_T *B_invAVals_d, *xBVals_d, *ratios_d;
    CHECK_CUSPARSE(cusparseDnVecGetValues(B_invA->DnDescr, (void **)&B_invAVals_d))
    CHECK_CUSPARSE(cusparseDnVecGetValues(xB->DnDescr, (void **)&xBVals_d))
    
    CHECK_CUDA(cudaMalloc((void**)&ratios_d, size*sizeof(val_T)))
    
    // get the ratios
	int gridDim = 1;
    int blockDim = size;
    if (blockDim > 1024) {
        gridDim = std::ceil(blockDim / 1024);
        blockDim = 1024;
    }
    divKernel<<<gridDim, blockDim>>>(ratios_d, xBVals_d, B_invAVals_d, size, MAX_RATIO);
    CHECK_CUDA(cudaDeviceSynchronize())

    val_T* min_ratio_ele_d = thrust::min_element(thrust::device, ratios_d, ratios_d + size); 
	*i = min_ratio_ele_d - ratios_d;     // gets the index of the minimum ratio

    val_T min_ratio_ele_h[1];
    CHECK_CUDA(cudaMemcpy(min_ratio_ele_h, min_ratio_ele_d, sizeof(val_T), cudaMemcpyDeviceToHost))
    PRINT_FUNC_PREFIX; std::cout << "min ratio element is=" << *min_ratio_ele_h << std::endl;
    
    PRINT_FUNC_PREFIX; std::cout << "index is=" << *i << std::endl;
	
    CHECK_CUDA(cudaFree(ratios_d))
	if(min_ratio_elle_h == MAX_RATIO) return false;
	if(min_ratio_ele_d == ratios_d + size) return false;
	else return true;
}

#endif
