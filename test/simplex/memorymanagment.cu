#include "simplex.cuh"
#include <thrust/device_ptr.h>
#include <thrust/host_vector.h>

#define DEBUGGING_MEM false


// -------------------------------------------------
//                  DENSE VECTOR
// -------------------------------------------------

DenseVector::DenseVector() : size(0), valueType(CUDA_R_32F) {
    PRINT_FUNC_PREFIX; std::cout << "warning, initializing 0-size vector" << std::endl;
}

DenseVector::DenseVector(unsigned int n) : size(n), valueType(CUDA_R_32F){
    CHECK_CUDA(cudaMalloc(&values, size*sizeof(val_T)))
    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout << "malloced values, size=" << size*sizeof(val_T) << std::endl;
    }

    CHECK_CUSPARSE(cusparseCreateDnVec(&DnDescr, size, values, valueType));
    if (DEBUGGING_MEM) {
        PRINT_PREFIX; std::cout << "initialized DnVec descr" << std::endl;
    }
}

DenseVector::DenseVector(unsigned int n, val_T* val) : size(n), valueType(CUDA_R_32F){   
            if (DEBUGGING_MEM) {
                PRINT_FUNC_PREFIX; std::cout << "DenseVec init n, val    n=" << n << "  size=" << size << std::endl; }
    
    CHECK_CUDA(cudaMalloc((void**)&values, n*sizeof(val_T)));
    //std::cout << "malloced values, size=" << size*sizeof(t) << " with s ele_size=" << sizeof(t) << " vs devize size=" << size*sizeof(CUDA_R_32F) << " with single ele_size=" << sizeof(CUDA_R_32F) << std::endl;
            if (DEBUGGING_MEM) {
                PRINT_PREFIX; std::cout << "malloced values, size=" << size*sizeof(val_T) << std::endl; }

    CHECK_CUDA(cudaMemcpy(values, val, size*sizeof(val_T), cudaMemcpyHostToDevice));
            if (DEBUGGING_MEM) {
                PRINT_PREFIX; std::cout << "copied memory to device" << std::endl; }

    CHECK_CUSPARSE(cusparseCreateDnVec(&DnDescr, size, values, valueType));
            if (DEBUGGING_MEM) {
                PRINT_PREFIX; std::cout << "initialized DnVec descr" << std::endl; }
}

// Helper for the DenseVector copy constructer
__global__ void copyData(const val_T* src, val_T* dest, const int size) {
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    
    if (i < size) {
        dest[i] = src[i];
    }
}

DenseVector::DenseVector(const DenseVector& b) : size(b.size), valueType(b.valueType) {
    val_T *b_values;
    CHECK_CUSPARSE(cusparseDnVecGetValues(b.DnDescr,(void **)&b_values));

    CHECK_CUDA(cudaMalloc((void**)&values, size*sizeof(val_T)));
            if (DEBUGGING_MEM) {
                PRINT_FUNC_PREFIX; std::cout << "malloced values, size=" << size*sizeof(val_T) << std::endl; }

    int gridDim = 1;
    int blockDim = size;
    if (blockDim > 1024) {
        gridDim = std::ceil(blockDim / 1024);
        blockDim = 1024;
    }
    copyData<<<gridDim, blockDim>>>((val_T*)b_values, (val_T*)values, size);
            if (DEBUGGING_MEM) {
                PRINT_PREFIX; std::cout << "copied other vector data" << std::endl; }

    CHECK_CUSPARSE(cusparseCreateDnVec(&DnDescr, size, values, valueType));
            if (DEBUGGING_MEM) {
                PRINT_PREFIX; std::cout << "initialized DnVec descr" << std::endl; }
}

DenseVector::~DenseVector() {
    CHECK_CUSPARSE(cusparseDestroyDnVec(DnDescr));
    PRINT_FUNC_PREFIX; std::cout << "descriptor-destroyed Dense Vector" << std::endl;

    CHECK_CUDA(cudaFree(values))
    values = nullptr;
}



// -------------------------------------------------
//                  SPARSE MATRIX
// -------------------------------------------------


SparseMatrix::SparseMatrix() : m(0), n(0), nnz(0){
    csrVal = nullptr;
    csrRowPtr = nullptr;
    csrColInd = nullptr;
}

SparseMatrix::SparseMatrix(unsigned int dim) : m(dim), n(dim), nnz(dim) {
    int* rowPtr = new int[n+1];
    int* colInd = new int[n];
    val_T* val = new val_T[n];

    for (int i=0; i<n; i++) {
        rowPtr[i] = i;
        colInd[i] = i;
        val[i] = 1.0;
    }
    rowPtr[n] = n;

    CHECK_CUSPARSE(cusparseCreateMatDescr(&descr));
    CHECK_CUSPARSE(cusparseSetMatIndexBase(descr, CUSPARSE_INDEX_BASE_ZERO))
    CHECK_CUSPARSE(cusparseSetMatType(descr, CUSPARSE_MATRIX_TYPE_GENERAL))
    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout << "Initialized Sparse Matrix" << std::endl;
    }

    CHECK_CUDA(cudaMalloc((void**)&csrVal, nnz*sizeof(val_T)))
    CHECK_CUDA(cudaMalloc((void**)&csrRowPtr, (m+1) * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&csrColInd, nnz * sizeof(int)))
    if (DEBUGGING_MEM) {
        PRINT_PREFIX;
        std::cout << "malloced memory for vals=" << nnz*sizeof(val_T);
        std::cout << " rowPtrs=" << (m+1) * sizeof(int) << " and colInd=" << nnz * sizeof(int) << std::endl;
    }

    CHECK_CUDA(cudaMemcpy(csrVal, val, nnz*sizeof(val_T), cudaMemcpyHostToDevice))
    CHECK_CUDA(cudaMemcpy(csrRowPtr, rowPtr, (m+1) * sizeof(int), cudaMemcpyHostToDevice))
    CHECK_CUDA(cudaMemcpy(csrColInd, colInd, nnz * sizeof(int), cudaMemcpyHostToDevice))
    if (DEBUGGING_MEM) {
        PRINT_PREFIX; std::cout << "copied memory form host to device" << std::endl;
    }

    cusparseCreateCsr(&spDescr, m, n, nnz, csrRowPtr, csrColInd, csrVal, 
                      CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I,
                      CUSPARSE_INDEX_BASE_ZERO, CUDA_R_32F);
}

SparseMatrix::SparseMatrix(unsigned int rows, unsigned int cols, unsigned int nonZeros, int* rowPtr, int* colInd, val_T* val) : m(rows), n(cols), nnz(nonZeros) {
    cusparseCreateMatDescr(&descr);
    CHECK_CUSPARSE(cusparseSetMatIndexBase(descr, CUSPARSE_INDEX_BASE_ZERO))
    CHECK_CUSPARSE(cusparseSetMatType(descr, CUSPARSE_MATRIX_TYPE_GENERAL))

    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout << "Initialized Sparse Matrix" << std::endl;
    }
    csrVal = nullptr;
    csrRowPtr = nullptr;
    csrColInd = nullptr;

    CHECK_CUDA(cudaMalloc((void**)&csrVal, nnz*sizeof(val_T)))
    CHECK_CUDA(cudaMalloc((void**)&csrRowPtr, (m+1) * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&csrColInd, nnz * sizeof(int)))
    if (DEBUGGING_MEM) {
        PRINT_PREFIX; std::cout << "malloced memory for vals=" << nnz*sizeof(val_T) << " rowPtrs=" << (m+1) * sizeof(int) << " and colInd=" << nnz * sizeof(int) << std::endl;
    }

    CHECK_CUDA(cudaMemcpy(csrVal, val, nnz*sizeof(val_T), cudaMemcpyHostToDevice))
    CHECK_CUDA(cudaMemcpy(csrRowPtr, rowPtr, (m+1) * sizeof(int), cudaMemcpyHostToDevice))
    CHECK_CUDA(cudaMemcpy(csrColInd, colInd, nnz * sizeof(int), cudaMemcpyHostToDevice))
    if (DEBUGGING_MEM) {
        PRINT_PREFIX; std::cout << "copyed memory form host to device" << std::endl;
    }

    cusparseCreateCsr(&spDescr, m, n, nnz, csrRowPtr, csrColInd, csrVal,
                      CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I,
                      CUSPARSE_INDEX_BASE_ZERO, CUDA_R_32F);
}

SparseMatrix::~SparseMatrix() {
    CHECK_CUDA(cudaFree(csrVal))
    CHECK_CUDA(cudaFree(csrRowPtr))
    CHECK_CUDA(cudaFree(csrColInd))
    csrVal = nullptr;
    csrRowPtr = nullptr;
    csrColInd = nullptr;

    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout << "Entered Matrix destructor" << std::endl;
    }

    CHECK_CUSPARSE(cusparseDestroyMatDescr(descr))
    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout << "Destroyed MatDescr" << std::endl;
    }

    CHECK_CUSPARSE(cusparseDestroySpMat(spDescr))

    PRINT_FUNC_PREFIX; std::cout << "freed and destroyed SparseMatrix" << std::endl;
}

