#ifndef SIMPLEX_CUH
#define SIMPLEX_CUH

typedef float val_T;

#include <thrust/extrema.h>
#include <thrust/execution_policy.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

// conversions between cuda and thrust need raw_pointer_cast
#include <thrust/device_ptr.h>
#include <thrust/tuple.h>
#include <cusparse.h>
#include <cusolverSp.h>
#include <string>
#include <cassert>

// DEBUGING
#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <iomanip>

// PROFILING
#include <cuda_profiler_api.h>


// Tolerance for floating point zero checks
#define TOL 1e-10f
#define DEBUGGING true

// -------------------------------------------------------------
//                      ERROR DETECTION
// -------------------------------------------------------------


#define CHECK_CUDA(func)                                                        \
{                                                                               \
    cudaError_t status = (func);                                                \
    if (status != cudaSuccess) {                                                \
        PRINT_RED_PREFIX;                                                       \
        std::cout<<"CUDA API failed with error: ";                              \
        std::cout<<cudaGetErrorString(status)<<" ("<<status<<")"<<std::endl;    \
    }                                                                           \
}

#define CHECK_CUSPARSE(func)                                                    \
{                                                                               \
    cusparseStatus_t status = (func);                                           \
    if (status != CUSPARSE_STATUS_SUCCESS) {                                    \
        PRINT_RED_PREFIX;                                                       \
        std::cout<<"CUSPARSE API failed with error:" ;                          \
        std::cout<<cusparseGetErrorString(status)<<" ("<<status<<")"<<std::endl;\
    }                                                                           \
}


// -------------------------------------------------------------
//                      PRINT PREFIXES
// -------------------------------------------------------------


#define PRINT_RED_PREFIX { std::cout << "\033[31m["<< __FILE__ <<"("<< __LINE__ <<")] \033[0m"; }

#define PRINT_PREFIX { std::cout << "\033[32m["<< __FILE__ <<"("<< __LINE__ <<")] \033[0m"; }

#define PRINT_FUNC_PREFIX { std::cout << "\033[32m["<< __FILE__ <<"("<< \
    __LINE__ << ":" << __FUNCTION__ <<")] \033[0m"; }



// --------------------------------------------------------------------------
//                          PRINTING FUNCTIONS
// --------------------------------------------------------------------------

// For Debuging only
// First argument is a DenseVector
// second argument is a string to identify the vector when debugging
template <typename T, typename S>
void printVector(T* v, S name, bool debug=DEBUGGING)  {
    if (debug) {
        uint64_t vSize = v->size;

        val_T* vals;
        // CHECK_CUDA(cudaMalloc((void**)&vals, vSize * sizeof(val_T)))
        CHECK_CUSPARSE(cusparseDnVecGetValues(v->DnDescr, (void**)&vals))

        float vVals[vSize];
        // std::cout << name << " has now size: " << vSize << std::endl;
        // for (unsigned int i=0; i<vSize; ++i) {vVals[i] = 0.0f;}
        cudaMemcpy(vVals, vals, vSize * sizeof(val_T), cudaMemcpyDeviceToHost);
        // Debug: print all the values 
        std::cout <<"[device_ptr="<<vals<<",host="<<vVals<<"] "<<name<<" values are: ";
        unsigned int c=0;
        for (unsigned int i = 0; i < vSize; ++i) {
            if (c % 15 == 0)
                std::cout << std::endl;
            std::cout << vVals[i] << ", ";
            ++c;
        }
        std::cout << std::endl;
    }
}

// First argument is SparseMatrix
template <typename T>
void printSpMat(T* M, bool debug=DEBUGGING) {
    if (debug) {
        int64_t h_n, h_m, h_nnz;
        int* d_rowPtr;
        int* d_colInd;
        val_T* d_vals;

        cusparseIndexType_t h_rowPtrType, h_colIndType;
        cusparseIndexBase_t h_idxBase;
        cudaDataType h_valueType;

        CHECK_CUSPARSE(cusparseCsrGet(M->spDescr,
                                    &h_m,
                                    &h_n,
                                    &h_nnz,
                                    (void**)&d_rowPtr,
                                    (void**)&d_colInd,
                                    (void**)&d_vals,
                                    &h_rowPtrType,
                                    &h_colIndType,
                                    &h_idxBase,
                                    &h_valueType))

        std::cout << "printing Sparse Matrix\n" << \
                    "m (rows) = " << h_m << "\n" << \
                    "n (cols) = " << h_n << "\n" << \
                    "nnz      = " << h_nnz << std::endl;
        
        val_T h_vals[h_nnz];
        CHECK_CUDA(cudaMemcpy(h_vals, d_vals, h_nnz * sizeof(val_T), cudaMemcpyDeviceToHost));

        int h_colInd[h_nnz];
        CHECK_CUDA(cudaMemcpy(h_colInd, d_colInd, h_nnz * sizeof(int), cudaMemcpyDeviceToHost));

        int h_rowPtr[h_m + 1];
        CHECK_CUDA(cudaMemcpy(h_rowPtr, d_rowPtr, (h_m + 1) * sizeof(int), cudaMemcpyDeviceToHost))

        std::cout << "rowPtr=" << std::endl;
        for (unsigned int i=0;  i<h_m + 1; ++i) {
            std::cout << h_rowPtr[i] << ", ";
        }   std::cout << std::endl;

        std::cout << "colInd=" << std::endl;
        for (unsigned int i=0; i<h_nnz; ++i) {
            std::cout << h_colInd[i] << ", ";
        }   std::cout << std::endl;

        std::cout << "vals=" << std::endl;
        for (unsigned int i=0; i<h_nnz; ++i) {
            std::cout << h_vals[i] << ", ";
        }   std::cout << std::endl;
    }
}

template <typename T>
void printArr(const T* h_arr, const int size, std::string arrName, bool debug=DEBUGGING)  {
    if (debug) {
        std::cout << arrName << "=" << std::endl;

        for (unsigned int i=0; i<size; ++i) {
            std::cout << h_arr[i] << ", ";
        }
        
        std::cout << std::endl;
    }
}


template <typename T>
void printArrDevice(const T* d_arr, const int size, std::string arrName, bool debug=DEBUGGING)  {
    if (debug) {
        std::cout << arrName << "=" << std::endl;

        val_T h_vals[size];
        CHECK_CUDA(cudaMemcpy((void *)h_vals, (void *)d_arr, size * sizeof(val_T), cudaMemcpyDeviceToHost));

        for (unsigned int i=0; i<size; ++i) {
            std::cout << h_vals[i] << ", ";
        }
        
        std::cout << std::endl;
    }
}


// print a CSR SparseMatrix M and its name spName
// the matrix is printed in dense format
template <typename T>
void printSpMatAsDenseMat(const T* M, std::string spName, bool debug=DEBUGGING)  {
    if (debug) {
        int64_t h_n, h_m, h_nnz;
        int* d_rowPtr;
        int* d_colInd;
        val_T* d_vals;

        cusparseIndexType_t h_rowPtrType, h_colIndType;
        cusparseIndexBase_t h_idxBase;
        cudaDataType h_valueType;

        CHECK_CUSPARSE(cusparseCsrGet(M->spDescr,
                                    &h_m,
                                    &h_n,
                                    &h_nnz,
                                    (void**)&d_rowPtr,
                                    (void**)&d_colInd,
                                    (void**)&d_vals,
                                    &h_rowPtrType,
                                    &h_colIndType,
                                    &h_idxBase,
                                    &h_valueType))

        std::cout << "\nprinting Dense Matrix " << spName << "\n" << \
                    "m (rows) = " << h_m << "\n" << \
                    "n (cols) = " << h_n << "\n" << \
                    "nnz      = " << h_nnz << std::endl;
        
        val_T h_vals[h_nnz];
        CHECK_CUDA(cudaMemcpy(h_vals, d_vals, h_nnz * sizeof(val_T), cudaMemcpyDeviceToHost));

        int h_colInd[h_nnz];
        CHECK_CUDA(cudaMemcpy(h_colInd, d_colInd, h_nnz * sizeof(int), cudaMemcpyDeviceToHost));

        int h_rowPtr[h_m + 1];
        CHECK_CUDA(cudaMemcpy(h_rowPtr, d_rowPtr, (h_m + 1) * sizeof(int), cudaMemcpyDeviceToHost))

        
        std::cout << "cols     :  ";
        for (unsigned int i=0; i<h_n; ++i) {
            std::cout << std::setw(5) << i << "  ";
        }

        unsigned int nextNNZ_off = 0;
        for (unsigned int i=0; i<h_m; ++i) {
            std::cout << "\nrow=" << std::setw(4) << i << " :   ";

            unsigned int nextColVal = h_colInd[nextNNZ_off];
            for (unsigned int j=0; j < h_n; ++j) {
                if (j == nextColVal && nextNNZ_off < h_rowPtr[i+1]) {
                    std::cout << std::setw(5) << h_vals[nextNNZ_off] << ", ";
                    ++nextNNZ_off;
                    nextColVal = h_colInd[nextNNZ_off];
                } else {
                    std::cout << std::setw(5) << 0.0 << ", ";
                }
            }
        }

        std::cout << std::endl;
    }
}



// -------------------------------------------------------------
//                      ABSTRACT CLASSES
// -------------------------------------------------------------

// Abstract format for a csr matrix (usable in HOST context)
struct SparseMatrix {
    SparseMatrix();
    SparseMatrix(unsigned int dim); // Initailizes identity matrix of size dim
    SparseMatrix(unsigned int rows, unsigned int cols, unsigned int nonZeros, int* rowPtr, int* colInd, val_T* val);

    ~SparseMatrix();

    int64_t rows;                   // Number of rows
    int64_t cols;                   // Number of cols
    int64_t nnz;                    // Number of non-zero entries

    int* csrRowPtr;                 // csr pointers
    int* csrColInd;
    val_T* csrVal;
    
    cusparseMatDescr_t descr;       // Descriptor
    cusparseSpMatDescr_t spDescr;   // sp Descriptor
};

// Abstract format for a dense vector
struct DenseVector {
    typedef val_T value_t;

    DenseVector();
    DenseVector(unsigned int n);
    DenseVector(unsigned int n, value_t* val);
    // Copy Constructor:
    DenseVector(const DenseVector& b);
    ~DenseVector();

    uint64_t size;                  // Size

    value_t* values;

    cusparseDnVecDescr_t DnDescr;   // Descriptor (new)

    cudaDataType valueType;         // Cuda equivalent to value_t value type (CUDA_R_32F for val_T)
};


// -------------------------------------------------------------
//                          FUNCTION DECLARATIONS
// -------------------------------------------------------------


// MAIN FUNCTIONS ---------------------

// Performs first phase of the simplex algorithm
// PRE: A is the constraint matrix, B is an identity matrix, b is the right hand side vector, c are the objective function coefficients, 
//      cB is all zeros (same size as b), basis contains the indices corresponding to the slack variable columns and xb is equal to b.
//      (A, b and c are from a problem in canonical form)
// POST: B contains the cols of A corresponding to basic variables, cB contains the entries of c corresponding to basic variables,
//       basiscontains the indices of the columns of A in B (in order) and xB contains the values of the basic variables.
//       If the function returns true, then all the outputs correspond to a basic feasible solution of the canonical problem (A, b, c).
//       If it returns false (the problem is infeasible) the outputs are arbitrary.
//       The termination conditions are returned through status.
bool simplexPhase1(cusparseHandle_t* handle, cusolverSpHandle_t* solvHandle, const SparseMatrix* A, SparseMatrix* B, const DenseVector* b, const DenseVector* c, DenseVector* cB, int* b_idx, DenseVector* xb, int maxNumIterations, int* status, const char profile);

// Performs second phase of simplex algorithm
bool simplexPhase2(cusparseHandle_t* handle, cusolverSpHandle_t* solvHandle, const SparseMatrix* A, SparseMatrix* B, const DenseVector* b, const DenseVector* c, DenseVector* cB, int* b_idx, DenseVector* xB, int maxNumIterations, int* status, const char profile);


// BASISFUNCTIONS ---------------------

// take column i of A and write into ds_output.
void getCol(cusparseHandle_t *handle, const SparseMatrix* A,
    DenseVector* output, const unsigned int i);

// Overwrite a vector ds_Y with only the entries of a given vector ds_A, which
// are specified as indices in a seperate vector of indices: basis.
// uses its own little helper sp_helper
void writeVectorAtIndices(cusparseHandle_t *handle, int* b_idx,
        DenseVector* ds_Y, const DenseVector* ds_A, const bool debug=false);


// Update basis
    // A: nonbasic cols
    // B: basic cols
    // reorganizes the old state to the new state, where i is leaving basis col index
    // and j is entering basis col index.
    // do we need to update cB: no it's done in WriteVectorAtIndices at loop start

// converts the CSR B into CSC Format to then remove column i and copy in Column j from the Matrix A
// also removes i from the basis and enters j
void updateBasis(cusparseHandle_t *handle, int* b_idx, unsigned const int colIdx_Out,
        unsigned const int colIdx_In, const SparseMatrix* A, SparseMatrix* B, const bool debug=false);


// OPERATIONS ------------------------

// Perform v2 = A * v1 - c
void calculateV2(cusparseHandle_t* handle, const SparseMatrix* A, const DenseVector* v1, const DenseVector* c, DenseVector* v2, bool debug=false);

// Returns a dense Vector with all zeros, except at the j-th entry whrer the value is 1.0
// Lets replace this by an Identity matrix in the main program of which we will reference the j-th column/row when the vector is needed
void unitJvector(DenseVector* unitVec, const int j);

// Compute dot product of 2 DenseVectors a and b
void dot(const DenseVector* a, const DenseVector* b, val_T* res);
void dotGPU(const DenseVector* a, const DenseVector* b, val_T* res);


// ENTERING/LEAVING SELECTION --------

// Selects col of the next pivot entry <-> the basis entering variabel
bool selectEnteringVar(const DenseVector* v1, int* j, const DenseVector* v2, const bool blandsrule = false);

bool selectLeavingVar(const DenseVector* B_invA, const DenseVector* xB, int* i, const DenseVector* index);


// READER ----------------------------

//reads mps format from filename
void mps_reader(char *filename, SparseMatrix** A, DenseVector** b, DenseVector** c);

// Output result to a file
void write_sol(int status, val_T opt_val, val_T* solution, const int numVars, const int numConst);


// HELPER ----------------------------

// Directly solves linear system with LU
void luSolve(cusolverSpHandle_t& solvHandle, cusparseHandle_t& handle, const SparseMatrix& B, const DenseVector& b, const bool transpose, DenseVector& sol, const bool debug=false);

// Add the -1 column to A for the auxilary problem
void createA_aux(const SparseMatrix* A, SparseMatrix** A_aux);

// Gather all solution values from both slack and decision variables
void gatherSolution(const DenseVector* xb, const int* b_idx, val_T** solution, const int size);


// HELPER KERNEL ---------------------

// Helper for gatherSolution
__global__ void setBasicVals(const val_T* xbVals, const int* basisVals, val_T* solution);

// Check that index idx is not in basis
// PRE: Assumes feasible2_d to be true at the beginning
__global__ void checkMinimality(unsigned int* basisValues, int* idx, bool* feasible2_d);

// Helper for removeSlacksFrom_xb
__global__ void makeZeros(val_T* newVals);



// DEBUGGING --------------------------
// generates an array of random values
// mabey would be worth it to do it based on a random seed
template <typename T>
void randVals(T* vals, const int size) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(-40, 63); // define the range

    for (int i=0; i<size; ++i) {
        vals[i] = distr(gen);
    }
}

#endif