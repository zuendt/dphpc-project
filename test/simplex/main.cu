
#include "simplex.cuh"
#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <string>
#include <cassert>

// Dont know if it makes sens to fix this here, but would work
#define MAX_NUM_ITERATIONS 1e4

int main (int argc, char** argv) {
    if (argc<2) {
        std::cout << "error please provide a filename"<<std::endl;
        return 1;
    }

	// get filename from the first input parameter
	char* filename = argv[1];

	// get profiler information
	const char* profile;
	if (argc >= 3)
		profile = argv[2];
	else
	 	profile = "0";

	// initialize handle before doing ANYTHING with cusparse
	cusparseHandle_t handle = NULL;
    cusparseCreate(&handle);

	// Same for cusolver handle
	cusolverSpHandle_t solvHandle = NULL;
    cusolverSpCreate(&solvHandle);
	
	SparseMatrix *A = nullptr;
	DenseVector *b = nullptr;
	DenseVector *c = nullptr;
	
	//read A b and c from file
	mps_reader(filename, &A, &b, &c);
    PRINT_FUNC_PREFIX; std::cout << "read succesfully  ->  A, b, c initialzed" << std::endl;

	// Get rows and columns of A
	int A_rows = A->rows;
	int A_cols = A->cols;
	
	//create B Matrix as Identity
	SparseMatrix B(A_rows);

	// Create basic and nonbasic idx arrays
	int b_idx[A_rows];
	int n_idx[A_cols];

	// Create cb_B vector
	val_T zeros[A_rows];
	for (unsigned i = 0; i < A_rows; i++) {
		zeros[i] = 0.0;
	}
	DenseVector cb_B(A_rows, zeros);

	// Creare Binv_b vector
	DenseVector Binv_b(*b);

	int status;

	// Do phase 1
    bool feasible = simplexPhase1(&handle, &solvHandle, A, &B, b, c, &cb_B, &b_idx, &Binv_b, MAX_NUM_ITERATIONS, &status, *profile);
	
	PRINT_PREFIX; std::cout << "phase1 finished" << std::endl;

	// If problem is feasible, do phase 2
	bool result;
	val_T opt_val = 0;
	val_T* solution;
	if (feasible) {
		std::cout << "\n\n\n Starting phase2 simplex on feasible problem:\n" << std::endl;
		result = simplexPhase2(&handle, &solvHandle, A, &B, b, c, &cb_B, &b_idx, &Binv_b, MAX_NUM_ITERATIONS, &status, *profile);
	} else {
		// Fill vector with values of original variables
		std::cout << "\n\n\n Starting problem was not possible to convert to feasible -> no solution found:\n" << std::endl;
		gatherSolution(&Binv_b, &b_idx, &solution, A_cols + A_rows);
		write_sol(status, opt_val, solution, A_cols, A_rows);
		return 0;
	}
	
	if (result) {
		// Compute optimal value
		dot(&cb_B, &Binv_b, &opt_val);
		std::cout << "Computed optimal value=" << opt_val << std::endl;
	}

	// Fill vector with values of original variables
	std::cout << "removing Slack variables form xb" << std::endl;
	gatherSolution(&xb, &basis, &solution, A_cols + A_rows);

	std::cout << "writing solution file" << std::endl;
	write_sol(status, opt_val, solution, A_cols, A_rows);
	std::cout << "Delete" << std::endl;
	std::cout << "Deleting dynamically allocated objects" << std::endl;
	


	delete solution; solution = nullptr;
	std::cout << "Deleted solution" << std::endl;
	
	delete c; c = nullptr;
	std::cout << "Deleted c" << std::endl;
	
	delete b; b = nullptr;
	std::cout << "Deleted b" << std::endl;

	delete A; A = nullptr;
	std::cout << "Deleted A" << std::endl;
	
	cusolverSpDestroy(solvHandle);
	cusparseDestroy(handle);

    return 0;
}
