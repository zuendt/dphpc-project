#include <glpk.h>

#include <iostream>
#include <string>
#include <cassert>
#include <cfloat>

void TEST_mps_reader(char *filename){
    //read MPS file into glpk format
    glp_prob *lp = glp_create_prob();
    bool fail = glp_read_mps(lp, GLP_MPS_FILE, NULL, filename);
	if (fail){
		std::cout << "glp_read_mps was unsuccessful, break process" << std::endl;
		 return;
	} 
	std::cout << "glp_read_mps finished successfully" << std::endl;
    //get values about
    int m = glp_get_num_rows(lp);
    int n = glp_get_num_cols(lp);
    int nnz = glp_get_num_nz(lp);
	std::cout << "get values for number of rows, cols and nnz" << std::endl;

    double c_values[2*m + 2*n];
    int row_idx[m];

    //Debug
    std::cout << m + 2*n << ", ";

    int cnt = 0;
    int idx = 0;
    for (int i = 0; i < m ; i++) {

        switch(glp_get_row_type(lp,i + 1)){
            case GLP_FX:
                c_values[idx] = glp_get_row_ub(lp, i + 1);
                row_idx[i] = idx;
                idx++;
                c_values[idx] = -1.0*glp_get_row_lb(lp, i + 1);
                idx++;
                break;
            case GLP_UP:
                c_values[idx] = glp_get_row_ub(lp, i + 1);
                row_idx[i] = idx;
                idx++;
                break;
            case GLP_LO:
                c_values[idx] = -1.0*glp_get_row_lb(lp, i + 1);
                row_idx[i] = idx;
                idx++;
                break;
            case GLP_FR:
                if(cnt == 0) cnt++;
                else std::cout << "Should not have more then one free row!" << std::endl;
                break;
            default:
                std::cout << "Should not happen!" << std::endl;
        }

    }

    m = idx;
    std::cout << "New m after c= " << m << std::endl;

    double cscVals[nnz+2*n];
    int cscColPtr[n+1];
    int cscRowInd[nnz+2*n];

    int indices[m];
    double values[m];
    cscColPtr[0] = 0;
    int extra_rows = 0;
    int cost_tmp = 0;
    // for glpk i + 1 (indices one-based)

    for (unsigned int i = 0; i < n;i++) {
        int numberValues = glp_get_mat_col(lp, i + 1, indices, values);
        int k = 0;

        std::cout << "last idx=" << indices[numberValues] << std::endl;
        std::cout << "first idx=" << indices[0] << std::endl;

        for (unsigned int j = 0; j < numberValues; j++) {
            
            std::cout << "j+1=" << j+1 << std::endl;
            std::cout << "indices=" << indices[j+ 1] << std::endl;

            switch(glp_get_row_type(lp, indices[j + 1])){
                case GLP_FX:
                    cscRowInd[cscColPtr[i] + k] = row_idx[indices[j + 1] - 1];
                    cscVals[cscColPtr[i] + k] = values[j + 1];
                    k++;
                    cscRowInd[cscColPtr[i] + k] = row_idx[indices[j + 1] - 1] + 1;
                    cscVals[cscColPtr[i] + k] = -1.0*values[j + 1];
                    nnz++;
                    k++;
                    break;
                case GLP_UP:
                    cscRowInd[cscColPtr[i] + k] = row_idx[indices[j + 1] - 1];
                    cscVals[cscColPtr[i] + k] = values[j + 1];
                    k++;
                    break;
                case GLP_LO:
                    cscRowInd[cscColPtr[i] + k] = row_idx[indices[j + 1] - 1];
                    cscVals[cscColPtr[i] + k] = -1.0*values[j + 1];
                    k++;
                    break;
                case GLP_FR:
                    break;
                default:
                    std::cout << "Should not happen!" << std::endl;
        if(values == nullptr){
    	; std::cout << "malloc values unsuccessfull" << std::endl;
    	return;
    }    }
        }
        cscColPtr[i + 1] = cscColPtr[i] + k;

        //add aditional constraint from bounds
        double ub = glp_get_col_ub(lp, i + 1);
        if (ub < DBL_MAX) {
            cscVals[cscColPtr[i + 1]] = 1;
            cscRowInd[cscColPtr[i + 1]] = extra_rows + m;
            cscColPtr[i + 1]++;
            c_values[extra_rows + m] = ub;
            extra_rows++;
        }
        double lb = glp_get_col_lb(lp, i + 1);
        if (lb > 0) {
            cscVals[cscColPtr[i + 1]] = -1;
            cscRowInd[cscColPtr[i + 1]] = extra_rows + m;
            cscColPtr[i + 1]++;
            c_values[extra_rows + m] = -1.0 * lb;
            extra_rows++;
        }

    }

    //new rows
    m += extra_rows;
    nnz += extra_rows;

    // transform from double to float
    std::cout << "m=" << m << "   n=" << n << std::endl;

    float transformedC_values[m];
    float tC_v[m];

    for (int i=0; i<m; ++i) {
        tC_v[i] = (float)c_values[i];
    }

    //get values for b (c in primal) if primal minimization multiply with -1 such that dual is maximise
    int b_size = n;
    float b_value[b_size];
    float tol = 1e-3;

    if(glp_get_obj_dir(lp) == GLP_MAX){
        for (int i = 0; i < b_size; i++) {
            b_value[i] = -1.0f*(float)glp_get_obj_coef(lp, i + 1);
            if(b_value[i] > tol || b_value[i] < -tol) --nnz;
        }
    }
    else{
        for (int i = 0; i < b_size; i++) {
            b_value[i] = (float)glp_get_obj_coef(lp, i + 1);
            if(b_value[i] > tol || b_value[i] < -tol) --nnz;
        }
    }

    // transform from double to float
    float transformedCscVals[nnz];
    for (int i=0; i<nnz; ++i) {
        transformedCscVals[i] = (float)cscVals[i];
    }

    std::cout << "NNZ of A in reader: " << nnz << std::endl;

    // *A = new SparseMatrix(n,m,nnz,cscColPtr, cscRowInd, transformedCscVals);

    // printSpMatAsDenseMat(*A, "A");
    // printSpMat(*A);

    // *b = new DenseVector(b_size, b_value);

    glp_delete_prob(lp);
    glp_free_env();
    lp = nullptr;

}



int main (int argc, char** argv) {
    char* filename = argv[1];

    TEST_mps_reader(filename);

    return 0;
}
