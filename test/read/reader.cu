#include <glpk.h>
#include <iostream>
#include <cfloat>
#include "reader.cuh"

void mps_reader(char *filename){
    /*
    //set Deafult values to Descriptor of A
    cusparseCreateMatDescr(&A->descr);
    cusparseSetMatType(A->descr,CUSPARSE_MATRIX_TYPE_GENERAL);
    cusparseSetMatFillMode(A->descr,CUSPARSE_FILL_MODE_UPPER);
    cusparseSetMatDiagType(A->descr,CUSPARSE_DIAG_TYPE_NON_UNIT);
    cusparseSetMatIndexBase(A->descr, CUSPARSE_INDEX_BASE_ZERO);
    */

    //read MPS file into glpk format
    glp_prob *lp = glp_create_prob();
    bool fail = glp_read_mps(lp, GLP_MPS_FILE, NULL, filename);
	if (fail){
		//PRINT_FUNC_PREFIX;
		std::cout << "glp_read_mps was unsuccessful, break process" << std::endl;
		 return;
	} 
	//PRINT_FUNC_PREFIX; 
	std::cout << "glp_read_mps finished successfully" << std::endl;
    //get values about
    int rows = glp_get_num_rows(lp);
    int cols = glp_get_num_cols(lp);
    int nnz = glp_get_num_nz(lp);
	//PRINT_FUNC_PREFIX; 
	std::cout << "get values for number of rows, cols and nnz" << std::endl;
	
    //double* c_values = (double*) malloc((2*rows + 2*cols)*sizeof(double));
    //int* row_idx = (int*) malloc((rows)* sizeof(int));
    double c_values[2*rows + 2*cols];
    int row_idx[rows];
    /*
    if(c_values == nullptr){
    	PRINT_FUNC_PREFIX; std::cout << "malloc c_values unsuccessfull" << std::endl;
    	return;
    }
    if(row_idx == nullptr){
    	PRINT_FUNC_PREFIX; std::cout << "malloc row_idx unsuccessfull" << std::endl;
    }
     */
    //Debug
    std::cout << rows + 2*cols << ", ";

    int cnt = 0;
    int idx = 0;
    for (int i = 0; i < rows ; i++) {

        switch(glp_get_row_type(lp,i + 1)){
            case GLP_FX:
                c_values[idx] = glp_get_row_ub(lp, i + 1);
                row_idx[i] = idx;
                idx++;
                c_values[idx] = -1.0*glp_get_row_lb(lp, i + 1);
                idx++;
                break;
            case GLP_UP:
                c_values[idx] = glp_get_row_ub(lp, i + 1);
                row_idx[i] = idx;
                idx++;
                break;
            case GLP_LO:
                c_values[idx] = -1.0*glp_get_row_lb(lp, i + 1);
                row_idx[i] = idx;
                idx++;
                break;
            case GLP_FR:
                if(cnt == 0) cnt++;
                else std::cout << "Should not have more then one free row!" << std::endl;
                break;
            default:
                std::cout << "Should not happen!" << std::endl;
        }

    }

    std::cout << "First loop passed!" << std::endl;
    rows = idx;

    //double* cscVals = (double*) malloc((nnz + 2*cols) * sizeof(double));
    //int* cscColPtr = (int*) malloc ((cols + 1) * sizeof(int));
    //int* cscRowInd = (int*) malloc ((nnz + 2*cols) * sizeof(int));
    double cscVals[nnz+2*cols];
    int cscColPtr[cols+1];
    int cscRowInd[nnz+2*cols];

    /*
	if(cscVals == nullptr){
    	PRINT_FUNC_PREFIX; std::cout << "malloc cscValsunsuccessfull" << std::endl;
    	return;
    }
    if(cscColPtr == nullptr){
    	PRINT_FUNC_PREFIX; std::cout << "malloc cscColPtr unsuccessfull" << std::endl;
    	return;
    }

    if(cscRowInd == nullptr){
    	PRINT_FUNC_PREFIX; std::cout << "malloc cscRowInd unsuccessfull" << std::endl;
    	return;
    }
    */

    //put values of A in csc format for easy transposition
    //int *indices = (int *) malloc( cols* sizeof(int));

    /*
    if(indices == nullptr){
    	PRINT_FUNC_PREFIX; std::cout << "malloc indices unsuccessfull" << std::endl;
    	return;
    }
    */
    //double *values = (double *) malloc(cols* sizeof(double));

    /*
    if(values == nullptr){
    	PRINT_FUNC_PREFIX; std::cout << "malloc values unsuccessfull" << std::endl;
    	return;
    }
    */
    cscColPtr[0] = 0;
    int extra_rows = 0;
    int cost_tmp = 0;
    // for glpk i + 1 (indices one-based)
    for (unsigned int i = 0; i < cols;i++) {
        double values[rows];
        int indices[rows];
        int numberValues = glp_get_mat_col(lp, i + 1, indices, values);
        int k = 0;
        std::cout << "last idx=" << indices[numberValues] << std::endl;
        std::cout << "first idx=" << indices[0] << std::endl;
		//DEBUG-START------------
		for(unsigned int jdebug = 0; jdebug < numberValues; jdebug++){
			std::cout << "Vals at :" << jdebug + 1 << " is : " << values[jdebug + 1] << std::endl;
            std::cout << "Indices at :" << jdebug + 1 << " is : " << indices[jdebug + 1] << std::endl;
		}
		//DEBUG-END--------------
        for (unsigned int j = 0; j < numberValues; j++) {
            std::cout << "j+1=" << j+1 << std::endl;
            std::cout << "indicies=" << indices[j+ 1] << std::endl;
            switch(glp_get_row_type(lp,indices[j + 1])){
                case GLP_FX:
                    cscRowInd[cscColPtr[i] + k] = row_idx[indices[j + 1] - 1];
                    cscVals[cscColPtr[i] + k] = values[j + 1];
                    k++;
                    cscRowInd[cscColPtr[i] + k] = row_idx[indices[j + 1] - 1] + 1;
                    cscVals[cscColPtr[i] + k] = -1.0*values[j + 1];
                    nnz++;
                    k++;
                    break;
                case GLP_UP:
                    cscRowInd[cscColPtr[i] + k] = row_idx[indices[j + 1] - 1];
                    cscVals[cscColPtr[i] + k] = values[j + 1];
                    k++;
                    break;
                case GLP_LO:
                    cscRowInd[cscColPtr[i] + k] = row_idx[indices[j + 1] - 1];
                    cscVals[cscColPtr[i] + k] = -1.0*values[j + 1];
                    k++;
                    break;
                case GLP_FR:
                    break;
                default:
                    std::cout << "Should not happen!" << std::endl;
        }
     }
        cscColPtr[i + 1] = cscColPtr[i] + k;

        //add aditional constraint from bounds
        double ub = glp_get_col_ub(lp, i + 1);
        if (ub < DBL_MAX) {
            cscVals[cscColPtr[i + 1]] = 1;
            cscRowInd[cscColPtr[i + 1]] = extra_rows + rows;
            cscColPtr[i + 1]++;
            c_values[extra_rows + rows] = ub;
            extra_rows++;
        }
        double lb = glp_get_col_lb(lp, i + 1);
        if (lb > 0) {
            cscVals[cscColPtr[i + 1]] = -1;
            cscRowInd[cscColPtr[i + 1]] = extra_rows + rows;
            cscColPtr[i + 1]++;
            c_values[extra_rows + rows] = -1.0 * lb;
            extra_rows++;
        }

    }

    std::cout << "\n before free 161 inside the reader" << std::endl;
    /*
	free(row_idx);
    row_idx = nullptr;
    */
    std::cout << "\n after free 161 inside the reader" << std::endl;

    //new rows
    rows += extra_rows;
    nnz += extra_rows;


    /*
    // Debug
    std::cout << "rows=" <<  rows << " cols=" << cols << ", Matrix thinks rows=" << (*A)->rows << " cols=" << (*A)->cols << std::endl;
    std::cout << "\nA looks like this in reader:" << std::endl;
    for(int k = 0; k < (*A)->rows; k++){
        for(int j = cscColPtr[k]; j < cscColPtr[k + 1]; j++){
            std::cout << "Row: " << k << " Col: " << cscRowInd[j] << " Val: " << cscVals[j] << "  |  ";
        }
        std::cout << std::endl;
    }

    float testA_csrRowPtr[(*A)->rows + 1];
    CHECK_CUDA(cudaMemcpy(testA_csrRowPtr, (*A)->csrRowPtr, ((*A)->rows + 1)*sizeof(unsigned int), cudaMemcpyDeviceToHost))

    float testA_csrColInd[(*A)->nnz];
    CHECK_CUDA(cudaMemcpy(testA_csrColInd, (*A)->csrColInd, ((*A)->nnz)*sizeof(unsigned int), cudaMemcpyDeviceToHost))

    float testA_csrVals[(*A)->nnz];
    CHECK_CUDA(cudaMemcpy(testA_csrVals, (*A)->csrVal, ((*A)->nnz)*sizeof(float), cudaMemcpyDeviceToHost))

    std::cout << "testing if A, b, c is still correct:" << std::endl;

    std::cout << "\nA looks like this from reader:" << std::endl;
    for(int k = 0; k < (*A)->rows; k++){
        for(int j = testA_csrRowPtr[k]; j < testA_csrRowPtr[k + 1]; j++){
            std::cout << "[" << k << "," << testA_csrColInd[j] << "] : " << testA_csrVals[j] << "  |  ";
        }
        std::cout << std::endl;

    }
    std::cout << std::endl;
    std::cout << std::endl;
     */

    // transform from double to float
    std::cout << "rows=" << rows << "   cols=" << cols << std::endl;

    //float* transformedC_values = (float*) malloc(rows* sizeof(float));
    float transformedC_values[rows];
    float tC_v[rows];

    std::cout << "thrash" << std::endl;
/*
    if(transformedC_values == nullptr){
    	PRINT_FUNC_PREFIX; std::cout << "malloc transformedC_values unsuccessfull" << std::endl;
    	return;
    }
    */
    std::cout << "mmmmmmmmmmmmmmmmmm" << std::endl;

    for (int i=0; i<rows; ++i) {
        // transformedC_values[i] = (float)c_values[i];
        tC_v[i] = (float)c_values[i];
    }

    std::cout << "absolut shit to debug like this" << std::endl;

    // *c = new DenseVector(rows, transformedC_values);
    //*c = new DenseVector(rows, tC_v);

    /*
    std::cout << "\nc looks like this in reader:" << std::endl;
    for(int k = 0; k < (*c)->size; k++) { std::cout << transformedC_values[k] << ", "; }
    std::cout << std::endl;
    */

    std::cout << "\n before free 224 inside the reader" << std::endl;

	

    std::cout << "\n after free 224 inside the reader" << std::endl;

    //get values for b (c in primal) if primal minimization multiply with -1 such that dual is maximise
    int b_size = cols;
    //float* b_value = (float*) malloc(b_size * sizeof(float));
    float b_value[b_size];
    /*
	if(b_value == nullptr){
    	PRINT_FUNC_PREFIX; std::cout << "malloc b_value unsuccessfull" << std::endl;
    	return;
    }
    */
    float tol = 1e-3;

    if(glp_get_obj_dir(lp) == GLP_MAX){
        for (int i = 0; i < b_size; i++) {
            b_value[i] = -1.0f*(float)glp_get_obj_coef(lp, i + 1);
            if(b_value[i] > tol || b_value[i] < -tol) --nnz;
        }
    }
    else{
        for (int i = 0; i < b_size; i++) {
            b_value[i] = (float)glp_get_obj_coef(lp, i + 1);
            if(b_value[i] > tol || b_value[i] < -tol) --nnz;
        }
    }

    // transform from double to float
    //float* transformedCscVals = (float*) malloc(nnz*sizeof(float));
    float transformedCscVals[nnz];
    /*
    if(transformedCscVals == nullptr){
    	PRINT_FUNC_PREFIX; std::cout << "malloc transformedCscVAls unsuccessfull" << std::endl;
    	return;
    }
    */
    for (int i=0; i<nnz; ++i) {
        transformedCscVals[i] = (float)cscVals[i];
    }

    std::cout << "NNZ of A in reader: " << nnz << std::endl;
   // std::cout << "ColPtr:" << std::endl;

    //*A = new SparseMatrix(cols,rows,nnz,cscColPtr, cscRowInd, transformedCscVals);

    //printSpMatAsDenseMat(*A, "A");
    //printSpMat(*A);

    //*b = new DenseVector(b_size, b_value);

    std::cout << "\n before free 273 inside the reader" << std::endl;

    /*
    std::cout << "\nb looks like this in reader:" << std::endl;
    for(int k = 0; k < (*b)->size; k++) { std::cout << b_value[k] << ", "; }
    std::cout << std::endl;

    free(transformedCscVals);
    transformedCscVals = nullptr;
    
    free(b_value);
    b_value = nullptr;
    
    free(transformedC_values);
	transformedC_values = nullptr;
	
	free(values);
	values = nullptr;
	
	free(indices);
    indices = nullptr;
    
     
    free(cscRowInd);
    cscRowInd = nullptr;
    
    free(cscColPtr);
    cscColPtr = nullptr;
    
    free(cscVals);
    cscVals = nullptr;
    
    
   	free(row_idx);
   	row_idx = nullptr;
   	
    
	free(c_values);
	c_values = nullptr;
	*/
    
   
    
    
 

    std::cout << "\n after free 273 inside the reader" << std::endl;
   
    

    glp_delete_prob(lp);
    // glp_free_env();
    // lp = nullptr;

}

