
#include "simplex.cuh"
#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <string>
#include <cassert>


// Dont know if it makes sense to fix this here, but would work
#define MAX_NUM_ITERATIONS 1e9

int main (int argc, char** argv) {
    if (argc < 2) {
        std::cout << "error please provide a filename" << std::endl;
        return 1;
    }
    //get filename from the first input parameter
    char *filename = argv[1];

    // initalize handle before doing ANYTHING with cusparse
    cusparseHandle_t handle = NULL;
    cusparseCreate(&handle);

    //construct SparseMatrix A, DenseVector b, DenseVector c
    SparseMatrix *A = nullptr;
    DenseVector *b = nullptr;
    DenseVector *c = nullptr;

    //read A b and c from file
    bool fail = glp_read_mps(lp, GLP_MPS_FILE, NULL, filename);
	if (fail){
		PRINT_FUNC_PREFIX; std::cout << "glp_read_mps was unsuccessful, break process" << std::endl;
		 return;
	} 
    PRINT_FUNC_PREFIX;
    std::cout << "read succesfully  ->  A, b, c initialzed" << std::endl;

    unsigned int testA_csrRowPtr[A->m + 1];
    CHECK_CUDA(cudaMemcpy(testA_csrRowPtr, A->csrRowPtr, (A->m + 1)*sizeof(int), cudaMemcpyDeviceToHost))

    unsigned int testA_csrColInd[A->nnz];
    CHECK_CUDA(cudaMemcpy(testA_csrColInd, A->csrColInd, (A->nnz)*sizeof(int), cudaMemcpyDeviceToHost))

    float testA_csrVals[A->nnz];
    CHECK_CUDA(cudaMemcpy(testA_csrVals, A->csrVal, (A->nnz)*sizeof(float), cudaMemcpyDeviceToHost))

    std::cout << "testing if A, b, c is still correct:" << std::endl;

    std::cout << "\nA looks like this from reader:" << std::endl;
    for(int k = 0; k < A->m; k++){
        for(int j = testA_csrRowPtr[k]; j < testA_csrRowPtr[k + 1]; j++){
            std::cout << "[" << k << "," << testA_csrColInd[j] << "] : " << testA_csrVals[j] << "  |  ";
        }
        std::cout << std::endl;
    }

    float testC[c->size];
    val_T* testC_d;
    CHECK_CUSPARSE(cusparseDnVecGetValues(c->DnDescr, (void **)&testC_d));
    CHECK_CUDA(cudaMemcpy(testC, testC_d, (c->size)*sizeof(float), cudaMemcpyDeviceToHost))

    std::cout << "\nc looks like this from reader:" << std::endl;
    for(int k = 0; k < c->size; k++) { std::cout << testC[k] << ", "; }
    std::cout << std::endl;

    float testB[b->size];
    val_T* testB_d;
    CHECK_CUSPARSE(cusparseDnVecGetValues(b->DnDescr, (void **)&testB_d));
    CHECK_CUDA(cudaMemcpy(testB, testB_d, (b->size)*sizeof(float), cudaMemcpyDeviceToHost))

    std::cout << "\nb looks like this from reader:" << std::endl;
    for(int k = 0; k < b->size; k++) { std::cout << testB[k] << ", "; }
    std::cout << std::endl;

    return 0;
}
