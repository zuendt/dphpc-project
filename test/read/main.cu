#include <fstream>
#include <string>
#include <cassert>
#include <iostream>
#include <reader.cuh>

int main (int argc, char** argv) {
    if (argc<2) {
        std::cout << "error please provide a filename"<<std::endl;
        return 1;
    }
	//get filename from the first input parameter
	char* filename = argv[1];
	mps_reader(filename);
}
