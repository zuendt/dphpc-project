#include "simplex.cuh"
#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <string>
#include <cassert>
#include <thrust/execution_policy.h>
#include <iomanip>
#include <algorithm>
#include <cstdlib>
#include "simplex.cuh"
#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <string>
#include <cassert>
#include <cstdlib>
#include <random>

void randVals(val_T* vals, unsigned int size) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_real_distribution<> distr(-30, 300); // define the range

    for (unsigned int i=0; i<size; ++i){
    	vals[i] = distr(gen);
    }
}
void randVals_positive(val_T* vals, unsigned int size) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_real_distribution<> distr(-300, 300); // define the range

    for (unsigned int i=0; i<size; ++i) {
  	float x = distr(gen);
    	if(x < 0) vals[i] = 0;
        else vals[i] = x;
    }
}
void testEntering(bool blandsRule, int size) {
	int size1 = size; //2000; //std::rand();
	int size2 = size;//2000 //std::rand();
	DenseVector::value_t v_vals[size1];
	DenseVector::value_t v2_vals[size2];
	randVals(v_vals, size1);
	randVals(v2_vals,size2);
	DenseVector v(size1, v_vals);
	DenseVector v2(size2, v2_vals);
    //printVector(&v, "v");
	auto negative = [](DenseVector::value_t p){ return p < 0;};
    
    int j;
    int correct;
    bool success;
    DenseVector::value_t *start2 = v2_vals;
    DenseVector::value_t *end2 = v2_vals + size2;
    DenseVector::value_t *start1 = v_vals;
    DenseVector::value_t *end1 = v_vals + size1;
    
    if(blandsRule){
    	
    	DenseVector::value_t *it2 = std::find_if(start2, end2, negative);
    	
    	if(it2 != end2){
    		correct = it2 - start2;
    		success = true;
    	}
    	else{
    		
    		DenseVector::value_t *it1 = std::find_if(start1,end1, negative);
    		if(it1 != end1){
    			correct = it1 - start1 + size2;
    			success = true;
    		}
    		else{
    			success = false;
    		}
    	}
    }
    else{
    	DenseVector::value_t *min1 = std::min_element(start1,end1);
    	DenseVector::value_t *min2 = std::min_element(start2,end2);
    	
    	if(*min1 < *min2){
    		correct = min1 - start1 + size2;
    		success = (*min1 < 0);
       	}
    	else{
    		correct = min2 - start2;
    		success = (*min2 < 0);
    	}
    }
    
    bool success_comp = selectEnteringVar(&v, &j, &v2, blandsRule);
    if(success){
		if(success_comp == success && correct == j) {
			std::cout << "passed" << std::endl;
		
		} 
		else {
			std::cout << "computed success" << std::setw(5) << success_comp <<  "actual success" << std::setw(5) << success << std::endl;
			std::cout << "computed index" << std::setw(5) << j << "actual index" << std::setw(5) << correct << std::endl;
			std::cout << "size1:" << size1 << std::setw(5) << "size2:" << size2 << std::endl;
		    
		}
    }
    else{
    	if(success != success_comp){
    		std::cout << "computed success" << std::setw(5) << success_comp <<  "actual success" << std::setw(5) << success << std::endl;
    		std::cout << "size1:" << size1 << std::setw(5) << "size2:" << size2 << std::endl;
    	}
    	else{
    		std::cout << "passed" << std::endl;
    	}
    } 
    
}
bool testEntering(DenseVector *v, int correct, DenseVector *v2 ,bool success, bool blandsRule) {

    //printVector(&v, "v");

    int j;
    bool success_comp = selectEnteringVar(v, &j, v2, blandsRule);
    if(success){
		if(success_comp == success && correct == j) {
			return true;
		  
		} 
		else {
			std::cout << "success:" << std::setw(5) << ", index" << j << std::endl;
		    return false;
		}
    }
    else return (success_comp == success);
}
void testLeaving(int sizein) {
   
    //printVector(&xB, "xB");
    //printVector(&B_invA, "B_invA");
    int size =  sizein; //2000; //std::rand();
	DenseVector::value_t B_invA_vals[size];
	DenseVector::value_t xB_vals[size];
	randVals(B_invA_vals, size);
	randVals_positive(xB_vals,size);
	DenseVector B_invA(size, B_invA_vals);
	DenseVector xB(size, xB_vals);
	DenseVector::value_t basis[size];
	
	DenseVector::value_t *basisbegin = basis;
	DenseVector::value_t *basisend = basis + size;
	
	std::iota(basisbegin, basisend, 0);
    //printVector(&v, "v");
	auto minratio = [&B_invA_vals, &xB_vals](int i, int j){ 
		if(B_invA_vals[i] > 0){
			DenseVector::value_t ratioi = xB_vals[i]/B_invA_vals[i];
			if(B_invA_vals[j] > 0){
				DenseVector::value_t ratioj = xB_vals[j]/B_invA_vals[j];
				if(ratioi < ratioj) return true;
				return false;
			}
			return true;
		}
		else return false;
	};
    
    int i;
    
    bool success_comp = selectLeavingVar(&B_invA, &xB, &i);
    
    DenseVector::value_t *it = std::min_element(basisbegin, basisend, minratio);
    bool success = ( it != basisend);
    int correct = it-basisbegin;
    if(success){
		if(success_comp == success && correct == i) {
			std::cout << "passed" << std::endl;
		
		} 
		else {
			std::cout << "computed success" << std::setw(5) << success_comp << std::setw(5) << "actual success:" << std::setw(5) << success << std::endl;
			std::cout << "computed index" << std::setw(5) << i << std::setw(5) << "actual index" << std::setw(5) << correct << std::endl;
			std::cout << "minratio" << std::setw(5) << xB_vals[i]/B_invA_vals[i] << std::setw(5) << "minratio" << std::setw(5) << xB_vals[correct]/B_invA_vals[correct] << std::endl;
			std::cout << "size1" << std::setw(5) << size << std::endl;
			
		    
		}
    }
    else{
    	if(success != success_comp){
    		std::cout << "computed success" << std::setw(5) << success_comp <<  "actual success" << std::setw(5) << success << std::endl;
    		std::cout << "size:" << size << std::endl;
    	}
    	else{
    		std::cout << "passed" << std::endl;
    	}
    } 
}
bool testLeaving(DenseVector *B_invA, DenseVector *xB, int correct, bool success) {
   
    //printVector(&xB, "xB");
    //printVector(&B_invA, "B_invA");
    int i;
    bool success_comp = selectLeavingVar(B_invA, xB, &i);
    if(success){
    
		if(success_comp == success && i == correct) {
			return true;
		} 
		else {
		    return false;
		}
	}
	else return (success_comp == success); 

}

int main (int argc, char** argv) {
	/*val_T v1_val[14] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	val_T v2_val[14] = {-2,-3,0,0,0,0,-4, -5, -6, -8,-10,-12,-15,-20};
	val_T v3_val[14] = {4,5,7,8,1,0,0,0,0,0,5,6,7,3};
	val_T v4_val[14] = {-3.05176e-05, 3.05176e-05, 1.52588e-05, -1.52588e-05, 0.540097, -2.98023e-07, 1.21499, -0.865113, 0, 3.57628e-07, 0, 2.54455, 0, -1};
	
	DenseVector v1(14, v1_val);
	DenseVector v2(14, v2_val);
	DenseVector v3(14, v3_val);
	DenseVector v4(14, v4_val);*/
	
    std::cout << "START: testing entering" << std::endl;
    int numEnt = 4;
    int size1 = 200; 
    if(argc > 1){
    	size1 = atoi(argv[1]);
    }
    if (argc > 2) {
    	numEnt = atoi(argv[2]);
    }
    std::cout << "set numEnt" << std::endl;
    for(unsigned int i = 0; i < numEnt; ++i){
    	testEntering(false, size1);
    }
    //std::cout << "passed" << testEntering(&v1, 1, &v1, false, true) << std::endl;
    //std::cout << "passed" << testEntering(&v1, 1, &v1, false, false) << std::endl;
    //std::cout << "passed" << testEntering(&v2, 0, &v2, true, true) << std::endl;
    //std::cout << "passed" << testEntering(&v2, 13, &v2, true, false) << std::endl;
    //std::cout << "passed" << testEntering(&v3, 13, &v3, false, true) << std::endl;
    //std::cout << "passed" << testEntering(&v3, 13, &v3, false, false) << std::endl;
    //std::cout << "passed" << testEntering(&v4, 7, &v4, true, true) << std::endl;
    //std::cout << "passed" << testEntering(&v4, 13, &v4, true, false) << std::endl;
    
    //std::cout << "passed" << testEntering(&v2, 27, &v3, true, false) << std::endl;
    //std::cout << "passed" << testEntering(&v2, 14, &v3, true, true) << std::endl;
    //std::cout << "passed" << testEntering(&v3, 13, &v2, true, false) << std::endl;
    //std::cout << "passed" << testEntering(&v3, 0, &v2, true, true) << std::endl;
    //std::cout << "ENDING: testing entering" << std::endl;


    std::cout << "START: testing leaving" << std::endl;
    int numLeav = 4; 
    int size2 = 200;
    if(argc > 3){
    	size2= atoi(argv[3]);
    }
    if(argc > 4){
    	numLeav = atoi(argv[4]);
    }
    for(unsigned int i = 0; i < numLeav ; ++i){
    	testLeaving(size2);
    }
    //std::cout << "passed" << testLeaving(&v2,&v3,0,false) << std::endl;
    //std::cout << "passed" << testLeaving(&v1,&v3,0,false) << std::endl;
    //std::cout << "passed" << testLeaving(&v3,&v3,0,true) << std::endl;
    //std::cout << "passed" << testLeaving(&v4,&v3,6,true) << std::endl;
    std::cout << "ENDING: testing leaving" << std::endl;
    
    return 0;
}
