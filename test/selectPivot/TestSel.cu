#include "simplex.cuh"
#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <string>
#include <cassert>
#include <random>
#include <thrust/execution_policy.h>

void randVals(val_T* vals, unsigned int size) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(-40, 63); // define the range

    for (unsigned int i=0; i<size; ++i) {
        vals[i] = distr(gen);
    }
}

// negative
struct negative
{
  __host__ __device__
  bool operator()(val_T x)
  {
    return x < -1e-10;
  }
};

bool TESTselectEnteringVar(const DenseVector* v1, const DenseVector* v2, int* j, const bool bland=false) {
    int v1_size = v1->size;
    val_T* v1_values = v1->values;

    int v2_size = v2->size;
    val_T* v2_values = v2->values;

    int pivot;

    if (bland) {
        val_T* d_min_ele_v2 = thrust::find_if(thrust::device, v2_values, v2_values + v2_size, negative());

        if (d_min_ele_v2 < v2_values + v2_size) {
            val_T h_min_ele_v2[1];                                                                          // DEBUGING
            CHECK_CUDA(cudaMemcpy(h_min_ele_v2, d_min_ele_v2, sizeof(val_T), cudaMemcpyDeviceToHost))       // DEBUGING
            pivot = *d_min_ele_v2;                                                                          // DEBUGING
            
            *j = v1_values - d_min_ele_v2;
        } else {
            val_T* d_min_ele_v1 = thrust::find_if(thrust::device, v1_values, v1_values + v1_size, negative());

            if (d_min_ele_v1 < v1_values + v1_size) {
                val_T h_min_ele_v1[1];                                                                      // DEBUGING
                CHECK_CUDA(cudaMemcpy(h_min_ele_v1, d_min_ele_v1, sizeof(val_T), cudaMemcpyDeviceToHost))   // DEBUGING
                pivot = *d_min_ele_v1;                                                                      // DEBUGING
                
                *j = v2_values - d_min_ele_v2;
            } else {
                return false;
            }
        }
    }

    else {
        val_T* d_min_ele_v2 = thrust::min_element(thrust::device, v2_values, v2_values + v2_size);
        val_T* d_min_ele_v1 = thrust::min_element(thrust::device, v1_values, v1_values + v1_size);

        val_T h_min_ele_v2[1];
        CHECK_CUDA(cudaMemcpy(h_min_ele_v2, d_min_ele_v2, sizeof(val_T), cudaMemcpyDeviceToHost))

        val_T h_min_ele_v1[1];
        CHECK_CUDA(cudaMemcpy(h_min_ele_v1, d_min_ele_v1, sizeof(val_T), cudaMemcpyDeviceToHost))

        if (h_min_ele_v2 < v2_values + v2_size && h_min_ele_v2 < 0.f && h_min_ele_v2 <= h_min_ele_v1) {
            pivot = *h_min_ele_v2;
            *j = h_min_ele_v2 - v2_values;
        } else if (h_min_ele_v1 < v1_values + v1_size && h_min_ele_v1 < 0.f) {
            pivot = *h_min_ele_v1;
            *j = h_min_ele_v1 - v1_values + v2_size;
        } else {
            return false;
        }
    }

    std::cout << "Pivot is: " << pivot << std::endl;
    std::cout << "Pivot index: " << *j << std::endl;
    return true;
}



__global__ void divKernel(val_T* c, const val_T* a, const val_T* b, int size, val_T maxRatio) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < size) {
        if (b[i] > 0)
            c[i] = a[i] / b[i];
        else
            c[i] = maxRatio;
    } 
}

bool TESTselectLeavingVar(const DenseVector* B_invA, const DenseVector* xB, int* i, const DenseVector* index) {
    int size = xB->size;
    int blockCount = 1;
    int threadsPerBlockCount = size;

    val_T *d_B_invAVals = B_invA->values,
    val_T *d_xBVals = xB->values;

    val_T*d_ratios;
    CHECK_CUDA(cudaMalloc((void**)&d_ratios, size*sizeof(val_T)))

    // get the ratios
    divKernel<<<blockCount, threadsPerBlockCount>>>(d_ratios, d_xBVals, d_B_invAVals, size, 1000.f);
    CHECK_CUDA(cudaDeviceSynchronize())
    	
	val_T* d_min_ratio_ele = thrust::min_element(thrust::device, d_ratios, d_ratios + size); 
	*i = d_min_ratio_ele - d_ratios;     // gets the index of the minimum ratio

    val_T h_min_ratio_ele[1];
    CHECK_CUDA(cudaMemcpy(h_min_ratio_ele, d_min_ratio_ele, sizeof(val_T), cudaMemcpyDeviceToHost))
    std::cout << "min ratio element is=" << *h_min_ratio_ele << std::endl;
    
    std::cout << "index is=" << *i << std::endl;

	if(d_min_ratio_ele == d_ratios + size) return false;
	else return true;
}








void testEntering() {
    unsigned int n=8;
    val_T vVals[n];

    randVals(vVals, n);

    DenseVector v(n, vVals);

    printVector(&v, "v");

    int j;
    bool succes = TESTselectEnteringVar(&v, &j);
    if(succes) {
        std::cout << "Select EnteringVar was found" << std::endl;
    } else {
        std::cout << "No EnteringVar was found" << std::endl;
    }
}

void testLeaving() {
    unsigned int n=8;
    val_T xBVals[n];
    val_T B_invAVals[n];
    val_T indexVals[n];

    randVals(xBVals, n);
    randVals(B_invAVals, n);
    for (unsigned int i=0; i<n; ++i) {
        indexVals[i] = i;
    }
    
    DenseVector xB(n, xBVals);
    DenseVector B_invA(n, B_invAVals);
    DenseVector index(n, indexVals);

    printVector(&xB, "xB");
    printVector(&B_invA, "B_invA");
    printVector(&index, "index");

    int i;
    bool succes = TESTselectLeavingVar(&B_invA, &xB, &i, &index);
    if(succes) {
        std::cout << "Select EnteringVar was found" << std::endl;
    } else {
        std::cout << "No EnteringVar was found" << std::endl;
    }

}

int main (int argc, char** argv) {
    std::cout << "STARTING: testing entering" << std::endl;
    testEntering();
    std::cout << "ENDING: testing entering" << std::endl;


    std::cout << "STARTING: testing leaving" << std::endl;
    testLeaving();
    std::cout << "ENDING: testing leaving" << std::endl;
    
    return 0;
}