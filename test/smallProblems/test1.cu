#include "simplex.cuh"
#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <string>
#include <cassert>
#include <iomanip>

// This simple test should terminate in 4 iterations
#define MAX_NUM_ITERATIONS 5

int main(int argc, char** argv) {
	PRINT_FUNC_PREFIX;
	std::cout << "This is SimpleTest1!"<< std::endl;
	const char* profile;
	if (argc >= 3)
		profile = argv[2];
	else
	 	profile = "0";
	 	
	// initialize handle before doing ANYTHING with cusparse
	cusparseHandle_t handle = NULL;
    cusparseCreate(&handle);

	// Same for cusolver handle
	cusolverSpHandle_t solvHandle = NULL;
    cusolverSpCreate(&solvHandle);

	//construct SparseMatrix A, DenseVector b, DenseVector c
    //this is a simple problem (A,b,c) for testing purpose, usually the three values are set to nullptr and mps_read is used
	//the problem is taken from https://cbom.atozmath.com/example/CBOM/Simplex.aspx?q=sm
	DenseVector::value_t master_val = 765/41;
	//this is the solution to the primal, TODO: calculate solution for DUAL 
	DenseVector::value_t mastersolution_vals[] = {89/41, 50/41, 62/41};
	int size = 3;
	DenseVector mastersolution(size, mastersolution_vals);
    unsigned int rows = 3;
    unsigned int cols = 4;
    unsigned int nonZeros = 9;

    val_T valuesA[] = {2,-2,3,2,5,3,-3,2,4};
    int colInd[] = {0,1,2,2,3,0,1,2,3};
    int rowPtr[] = {0,3,5,9};

    val_T valuesb[] = {8,10,15};
    val_T valuesc[] = {3,-3, 5,4};
    SparseMatrix Ar =  SparseMatrix(rows, cols, nonZeros, rowPtr, colInd, valuesA);//nullptr;
	DenseVector br =  DenseVector(rows, valuesb); //nullptr;
	DenseVector cr =  DenseVector(cols, valuesc);//nullptr;
	
	SparseMatrix *A = &Ar;
	DenseVector *b = &br;
	DenseVector *c = &cr;
	
    PRINT_FUNC_PREFIX;
	std::cout << "A, b, c initialzed" << std::endl;

	std::cout << "testing if A, b, c is still correct:" << std::endl; 
	
    printVector(b, "b");
	printVector(c, "c");
	printSpMatAsDenseMat(A, "A");
	//create Identity Matrix
	int n = A->m;
    PRINT_PREFIX;
	std::cout << "Identity matrix dimension=" << n << "   A->m:" << A->m << ", A->n:" << A->n << std::endl;
	SparseMatrix B(n);

    PRINT_PREFIX;
	std::cout << "B sparsMat inizialized" << std::endl;

	// Create cB vector
	DenseVector::value_t zeros [n];
	for (unsigned i = 0; i < n; i++) {
		zeros[i] = 0.0;
	}
	DenseVector cB(n, zeros);
    PRINT_PREFIX;
	std::cout << "cB vec inizialized" << std::endl;

	// Create basis vector
	BasisVector basis(A->m,A->n);

    PRINT_PREFIX;
	std::cout << "basis initialized" << std::endl;

	// Creare xb vector
	DenseVector xb(*b);

    PRINT_PREFIX;
	std::cout << "xb dense vector initialized" << std::endl;

	int status;

	// Do phase 1
    bool feasible = simplexPhase1(&handle, &solvHandle, A, &B, b, c, &cB, &basis, &xb, MAX_NUM_ITERATIONS, &status, *profile);
	printVector(&cB, "cB after simplexPhase1");
    PRINT_PREFIX;
	std::cout << "phase1 finished" << std::endl;

	// DEBUG -------------------------------------------------
	
	std::cout << "testing b vector (shouldn't have any changes):" << std::endl;
	uint64_t testbSize = b->size;
    printVector(b, "b");
	// DEBUG END ---------------------------------------------

	// If problem is feasible, do phase 2
	bool result;
	DenseVector::value_t opt_val = 0;
	DenseVector::value_t* solution;
	if (feasible) {
		std::cout << "\n\n\n Starting phase2 simplex on feasible problem:\n" << std::endl;
		result = simplexPhase2(&handle, &solvHandle, A, &B, b, c, &cB, &basis, &xb, (int)MAX_NUM_ITERATIONS, &status, *profile);
	} else {
		// Fill vector with values of original variables
		std::cout << "\n\n\n Starting problem was not possible to convert to feasible -> no solution found:\n" << std::endl;
		gatherSolution(&xb, &basis, &solution, A->n+A->m);
		write_sol(status, opt_val, solution, A->n, A->m);
		return 0;
	}
	
	if (result) {
		// Compute optimal value
		dot(&cB, &xb, &opt_val);
		std::cout << "Computed optimal value=" << opt_val << std::endl;
	}

	// Fill vector with values of original variables
	std::cout << "removing Slack variables form xb" << std::endl;
	gatherSolution(&xb, &basis, &solution, A->n+A->m);

	std::cout << "writing solution file" << std::endl;
	write_sol(status, opt_val, solution, A->n, A->m);


	delete solution;
	solution = nullptr;
	cusolverSpDestroy(solvHandle);
	cusparseDestroy(handle);
	
	bool passed = 0;
	//DenseVector computed_solution(solution.size(), solution);
	//printVector(computed_solution, "computed solution");
	
	//printVector(mastersolution, "expected solution");
	//if(solution == mastersolution_vals && opt_val = master_val){
	//	passed = 1;
	// }

	//std::cout << "passed status:" << std::setw(5) << passed << std::endl;

    return 0;
}
