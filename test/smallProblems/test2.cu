
#include "simplex.cuh"
#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <string>
#include <cassert>

// Don't know if it makes sense to fix this here, but would work
#define MAX_NUM_ITERATIONS 1e9

int main(int argc, char** argv) {
    /*DenseVector::value_t master_val = 765/41;
	DenseVector::value_t* mastersolution;
	
	// initialize handle before doing ANYTHING with cusparse
	cusparseHandle_t handle = NULL;
    cusparseCreate(&handle);

	// Manipulations for name of output file
	//int startTest = outputFile.find("test");
	//outputFile = outputFile.replace(startTest, 4, "sol");
	//std::string testName = outputFile.erase(startTest-1, 4);
	
	//construct SparseMatrix A, DenseVector b, DenseVector c
    //this is a simple problem (A,b,c) for testing purpose
	//the problem is taken from https://cbom.atozmath.com/example/CBOM/Simplex.aspx?q=sm
    unsigned int rows = 3;
    unsigned int cols = 3;
    unsigned int nonZeros = 7;

    val_T valuesA[] = {2,3,3,2,2,5,4};
    int colInd[] = {0,2,0,1,2,1,2};
    int rowPtr[] = {0,2,5};

    val_T valuesc[] = {8,10,15};
    val_T valuesb[] = {-3,-5,-4};
    SparseMatrix Ar =  SparseMatrix(rows, cols, nonZeros, rowPtr, colInd, valuesA);//nullptr;
	DenseVector br =  DenseVector(cols, valuesb); //nullptr;
	DenseVector cr =  DenseVector(rows, valuesc);//nullptr;
	
	SparseMatrix *A = &Ar;
	DenseVector *b = &br;
	DenseVector *c = &cr;
	
	//read A b and c from file
    PRINT_FUNC_PREFIX;
	std::cout << "read succesfully  ->  A, b, c initialzed" << std::endl;

	std::cout << "testing if A, b, c is still correct:" << std::endl; 
	

	std::cout << "testing b vector:" << std::endl;
    printVector(b, "b");
	
	//create Identity Matrix
	int n = A->m;
    PRINT_PREFIX;
	std::cout << "Identity matrix dimension=" << n << "   A->m:" << A->m << ", A->n:" << A->n << std::endl;
	SparseMatrix B(n);

    PRINT_PREFIX;
	std::cout << "B sparsMat inizialized" << std::endl;

	// Create cB vector
	DenseVector::value_t zeros [n];
	for (unsigned i = 0; i < n; i++) {
		zeros[i] = 0.0;
	}
	DenseVector cB(n, zeros);
    PRINT_PREFIX;
	std::cout << "cB vec inizialized" << std::endl;

	// Create basis vector
	BasisVector basis(A->m,A->n);

    PRINT_PREFIX;
	std::cout << "basis initialized" << std::endl;

	// Creare xb vector
	DenseVector xb(*b);

    PRINT_PREFIX;
	std::cout << "xb dense vector initialized" << std::endl;

	int status;

	// Do phase 1
    bool feasible = simplexPhase1(&handle, A, &B, b, c, &cB, &basis, &xb, MAX_NUM_ITERATIONS, &status);

    PRINT_PREFIX;
	std::cout << "phase1 finished" << std::endl;

	// DEBUG -------------------------------------------------
	std::cout << "testing b vector (shouldn't have any changes):" << std::endl;
	uint64_t testbSize = b->size;
    printVector(b, "b");
	// DEBUG END ---------------------------------------------

	// If problem is feasible, do phase 2
	bool result;
	DenseVector::value_t opt_val = 0;
	DenseVector::value_t* solution;
	if (feasible) {
		std::cout << "\n\n\n Starting phase2 simplex on feasible problem:\n" << std::endl;
		result = simplexPhase2(&handle, A, &B, b, c, &cB, &basis, &xb, MAX_NUM_ITERATIONS, &status);
	} else {
		// Fill vector with values of original variables
		std::cout << "\n\n\n Starting problem was not possible to convert to feasible -> no solution found:\n" << std::endl;
		gatherSolution(&xb, &basis, &solution, A->n+A->m);
		write_sol(status, opt_val, solution, A->n, A->m);
		return 0;
	}
	
	if (result) {
		// Compute optimal value
		dot(&cB, &xb, &opt_val);
		std::cout << "Computed optimal value=" << opt_val << std::endl;
	}

	// Fill vector with values of original variables
	std::cout << "removing Slack variables form xb" << std::endl;
	gatherSolution(&xb, &basis, &solution, A->n+A->m);

	std::cout << "writing solution file" << std::endl;
	write_sol(status, opt_val, solution, A->n, A->m);
	bool passed = 0;
	if(solution == mastersolution && opt_val = master_val) passed = 1;

	delete solution;

    std::cout << "passed" << passed << std::endl;
    */
}
