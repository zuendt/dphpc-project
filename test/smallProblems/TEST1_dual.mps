NAME          TEST1
ROWS
 N  COST
 L  c1
 L  c2
 L  c3
COLUMNS
    x1        COST                 8
    x1        c1                   2
    x1        c2                   3
    x2        COST                 10
    x2        c2                   2
    x2        c3                   5
    x3        COST                 15
    x3        c1                   3
    x3        c2                   2
    x3        c3                   4
RHS
    RHS         c1                   3
    RHS         c2                   5
    RHS         c3                   4
ENDATA
