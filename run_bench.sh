#!/bin/bash

if [ "$1" = '' ]||[ "$2" = '' ];then
    echo 'Usage:'
    echo "  '$0 {solver} {test_suite} all': run all 40 problems"
    echo "  '$0 {solver} {test_suite} short': run the short (<10s) problems"
    echo "Test suites:"
    echo "  small_benchmark : miplib 40 small problems"
    echo "  simple_solvable : handcrafted"
    echo "  simple_unsolvable : handcrafted"
    echo "'short' mode has no effect in test suites other than small_benchmark."
    echo "Solvers:"
    echo "  project: dphpc-project"
    echo "  scip: miplib-located binary solver, is fast"
    echo "  scipy: python solver, uses highs solver"
    echo "  LOOP i: run simple_solvable suite in a loop for all 3 solvers, writing output to results/ starting count at i"
    exit 0
fi

bench_workdir='benchmark/miplib'
set -u
#do NOT set -e, we want to continue on a benchmark crash

hostname="$(cat /etc/hostname)"
if [ "${hostname}" != 'davinci' ];then
    solvers='scip project scipy'
else
    solvers='scip project scipy'
fi
if [ "$1" = 'LOOP' ];then
    test_suite='simple_solvable'
    for i in $(seq -w "${2}" 100);do
        echo $i
        for solver in ${solvers};do
            out_fn="results/run${i}_${hostname}_${solver}_${test_suite}"
            time ./run_bench.sh "${solver}" "${test_suite}" all 2>&1|tee "${out_fn}"
        done
    done
    exit 0
fi

while read line;do
    if ! grep -P 'enlight_hard.mps.gz|sp150x300d.mps.gz|exp-1-500-5-5.mps.gz|200x1188c.mps.gz|neos-787933.mps.gz|nu25-pr12.mps.gz|neos17.mps.gz|neos-827175.mps.gz|neos8.mps.gz' <<< "${line}" >/dev/null;then
        if [ "$3" = 'short' ];then
            continue #skip this non-short problem
        fi
    fi
    echo "solver=${1}"
    echo "startproblem=${bench_workdir}/${line}"
    date  +"exactstartdate=%s.%N"
    case "${1}" in
    'project')
        timeout 3600 ./run.sh ./project "${bench_workdir}/${line}"
        ;;
    'scipy')
        timeout 3600 "benchmark/miplib/bin/scipy" "${bench_workdir}/${line}"
        ;;
    'scip')
        scip_path="${HOME}/dphpc-project/benchmark/miplib/bin/scip"
        timeout 3600 "${scip_path}" -f "${bench_workdir}/${line}"
        ;;
    esac
    echo "exitstatus=$?"
    date  +"exactstopdate=%s.%N"
done < "${bench_workdir}/testsets/${2}.test"
