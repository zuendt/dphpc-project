#!/usr/bin/python3

import numpy as np
import mip
import scipy
import sys

def dims_of_problem(filename:str) :
    """
    Reads a .mps and output n, m dimensions and nnz nonzero entries
    """
    mdl = mip.Model(solver_name="CBC")
    mdl.verbose=0
    mdl.read(filename)

    # model parameters
    num_vars = len(mdl.vars)
    num_cons = len(mdl.constrs)


    nnz=0
    # constraint coefficient matrix
    for i, con in enumerate(mdl.constrs):
        expr=con.expr.expr
        if con.expr.sense == "=":
            #load sparse data into Aeq_*
            for jx,var in enumerate(mdl.vars) :
                if var in expr :
                    nnz+=2
        else :
            #load sparse data into A_*
            for jx,var in enumerate(mdl.vars) :
                if var in expr :
                    nnz+=1

    return num_vars,num_cons,nnz

def show_dims(it) :
    a,b,nnz=it
    print(it)
    print('sparsity pcent',nnz/(a*b)*100)

if len(sys.argv)>2 :
    for i in sys.argv[1:] :
        show_dims(dims_of_problem(i))
else :
    show_dims(dims_of_problem(sys.argv[1]))
