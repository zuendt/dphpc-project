#!/usr/bin/python3

import sys
import glob


def r(i) :
    return round(i,9)

def extract_runbench(input_glob_files) :
    input_files=[j for i in input_glob_files for j in glob.glob(i)]

    exactdates={}
    current_prob='UNKNOWN'

    for fln in input_files :
        with open(fln,'r') as f :
            while l:=f.readline() :
                if l.startswith('exactstartdate=') :
                    exactdates[current_prob][-1].append(float(l.split('exactstartdate=',1)[1].strip()))
                elif l.startswith('exactstopdate=') :
                    exactdates[current_prob][-1].append(float(l.split('exactstopdate=',1)[1].strip()))
                elif l.startswith('startproblem=') :
                    current_prob=l.split('startproblem=',1)[1]
                    if current_prob not in exactdates :
                        exactdates[current_prob]=[]
                    exactdates[current_prob].append([])

    for k,ls in exactdates.items() :
        name=k.split(' ')[0].split('/')[-1].strip()
        rundata=[]
        try :
            for stt,endt in ls :
                rundata.append(r(endt-stt))
        except ValueError :
            pass
        yield name,rundata

def extract_out(input_glob_files) :
    input_files=[j for i in input_glob_files for j in glob.glob(i)]

    exactdates={}
    current_prob='UNKNOWN'

    for fln in input_files :
        with open(fln,'r') as f :
            while l:=f.readline() :
                if l.startswith('exactdate=') :
                    exactdates[current_prob][-1].append(float(l.split('exactdate=',1)[1].strip()))
                elif l.startswith('@01 ') :
                    current_prob=l.split('@01 ',1)[1]
                    if current_prob not in exactdates :
                        exactdates[current_prob]=[]
                    exactdates[current_prob].append([])

    for k,ls in exactdates.items() :
        name=k.split(' ')[0].split('/')[-1]
        rundata=[]
        for stt,endt in ls :
            rundata.append(r(endt-stt))
        yield name,rundata

if __name__=='__main__' :
    if len(sys.argv)==1 :
        print('Usage: {mode} {result .out file} will print the time taken by using exactdate=* lines')
        exit(0)
    if sys.argv[1]=='out' :
        extract=extract_out
    elif sys.argv[1]=='runbench' :
        extract=extract_runbench
    for name,rundata in extract(sys.argv[2:]) :
        print(name.strip(),*rundata,sep=',')
