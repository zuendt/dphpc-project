from mip import *

m = Model(name='FIRST')

# 10 deciding vars
n = 10
y = [ m.add_var() for i in range(n) ]

# RHS
b = [3.3, 5, 27, -4, 11, -4.5, -9, 1, -2.2, 10]

# 10 constraints
c1 = [1, 2, 3, 3, -4, 9, 2, 0, 1.4, 0]
m += xsum(c1[i]*y[i] for i in range(n)) <= b[0]

c2 = [0, -2.3, 30, -17, 0, 0, 4, -2, 0.3, -10.2]
m += xsum(c2[i]*y[i] for i in range(n)) <= b[1]

c3 = [0, 0, 0, 0, 0, 1, 1, 0, 0, -1]
m += xsum(c3[i]*y[i] for i in range(n)) == b[2]

c4 = [-14, -20.4, 0, 3, 33, 4.4, 0, 0, 7, -7.7]
m += xsum(c4[i]*y[i] for i in range(n)) >= b[3]

c5_6 = [2, 3, 9.5, -8, 20, 1.4, 0.33, 5, -24, 12]
m += xsum(c5_6[i]*y[i] for i in range(n) if i%2 == 0) <= b[4]
m += xsum(c5_6[i]*y[i] for i in range(n) if i%2 != 0) == b[5]

m += 3*y[3] + 2*y[8] >= b[6]

m += 7.3*y[1] + -5*y[7] <= b[7]

c9 = [-1, 1, 0, 0, -4, 0, 9, 23, 0, -8.3]
m += xsum(c9[i]*y[i] for i in range(n)) <= b[8]

c10 = [0, 0, 15, -15, 3, 3, 0, 4, -7, 6]
m += xsum(c10[i]*y[i] for i in range(n)) <= b[9]

# objective
obj = [6.6, -3, 4, 0, 1, -5, 0, 7.1, 0, -2.2]
m.objective = xsum(obj[i]*y[i] for i in range(n))

# Create mps
m.write('FIRST.mps')

# printing the solution
status = m.optimize(max_seconds=300)
if status == OptimizationStatus.OPTIMAL:
    print('optimal solution cost {} found'.format(m.objective_value))
elif status == OptimizationStatus.FEASIBLE:
    print('sol.cost {} found, best possible: {}'.format(m.objective_value, m.objective_bound))
elif status == OptimizationStatus.NO_SOLUTION_FOUND:
    print('no feasible solution found, lower bound is: {}'.format(m.objective_bound))
if status == OptimizationStatus.OPTIMAL or status == OptimizationStatus.FEASIBLE:
    print('solution:')
    for v in m.vars:
       if abs(v.x) > 1e-6: # only printing non-zeros
          print('{} : {}'.format(v.name, v.x))
