NAME          EXAMPLE4
ROWS
 N  OBJ
 G  ROW01
 G  ROW02
 G  ROW03
COLUMNS
    COL01     OBJ                3.0   ROW01            110.0
    COL01     ROW02              4.0   ROW03              2.0
    COL02     ROW01            205.0   ROW02             32.0
    COL02     OBJ               24.0   ROW03             12.0
    COL03     ROW01            160.0   ROW02             13.0
    COL03     OBJ               13.0   ROW03             54.0
    COL04     ROW01            160.0   ROW02              8.0
    COL04     ROW03            285.0   OBJ                9.0
    COL05     OBJ               20.0   ROW01            420.0
    COL05     ROW02              4.0   ROW03             22.0
    COL06     ROW01            260.0   OBJ               19.0
    COL06     ROW02             14.0   ROW03             80.0
RHS
    RHS1      ROW01           2000.0
    RHS1      ROW02             55.0
    RHS1      ROW03            800.0
BOUNDS
 UP BND1      COL01              4.0
 UP BND1      COL02              3.0
 UP BND1      COL03              2.0
 UP BND1      COL04              8.0
 UP BND1      COL05              2.0
 UP BND1      COL06              2.0
ENDATA