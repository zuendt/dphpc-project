NAME          EXAMPLE3
ROWS
 N  OBJ
 E  ROW01
 E  ROW02
 E  ROW03
 E  ROW04
COLUMNS
    COL01     OBJ               -1.0   ROW01             -2.0
    COL01     ROW02             -1.0   ROW03             -1.0
    COL01     ROW04              1.0
    COL02     ROW01              1.0   ROW02              1.0
    COL02     OBJ               -2.0   ROW03              2.0
    COL03     ROW01              1.0
    COL04     ROW01              1.0
    COL05     ROW01              1.0
    COL06     ROW02              1.0
    COL07     ROW03              1.0
RHS
    RHS1      ROW01              2.0
    RHS1      ROW02              2.0
    RHS1      ROW03              4.0
    RHS1      ROW04              4.0
ENDATA