NAME          EXAMPLE2
ROWS
 N  OBJ
 L  ROW01
 L  ROW02
 L  ROW03
COLUMNS
    COL01     OBJ               -2.0   ROW01              1.0
    COL01     ROW02              1.0
    COL02     ROW01              1.0   ROW02              2.0
    COL02     OBJ               -3.0
    COL03     ROW01             -1.0   ROW03              1.0
    COL03     OBJ               -4.0
    COL04     ROW01              1.0   OBJ               -5.0
    COL04     ROW03              1.0
    COL05     ROW01              1.0
    COL06     ROW02              1.0
    COL07     ROW03              1.0
RHS
    RHS1      ROW01             10.0
    RHS1      ROW02              8.0
    RHS1      ROW03             20.0
ENDATA