NAME          BLANK     FREE
ROWS
 N  OBJROW
 G  constr(0)
 G  constr(1)
 G  constr(2)
 G  constr(3)
 L  constr(4)
 L  constr(5)
 L  constr(6)
 L  constr(7)
 L  constr(8)
 L  constr(9)
 L  constr(10)
 L  constr(11)
 L  constr(12)
 L  constr(13)
 G  constr(14)
 G  constr(15)
 G  constr(16)
 G  constr(17)
 G  constr(18)
 G  constr(19)
 G  constr(20)
 G  constr(21)
 G  constr(22)
COLUMNS
    x[0,0] constr(0) 1.           constr(4) 187.        
    x[0,1] constr(0) 1.           constr(5) 187.        
    x[0,2] constr(0) 1.           constr(6) 187.        
    x[0,3] constr(0) 1.           constr(7) 187.        
    x[0,4] constr(0) 1.           constr(8) 187.        
    x[0,5] constr(0) 1.           constr(9) 187.        
    x[0,6] constr(0) 1.           constr(10) 187.        
    x[0,7] constr(0) 1.           constr(11) 187.        
    x[0,8] constr(0) 1.           constr(12) 187.        
    x[0,9] constr(0) 1.           constr(13) 187.        
    x[1,0] constr(1) 1.           constr(4) 119.        
    x[1,1] constr(1) 1.           constr(5) 119.        
    x[1,2] constr(1) 1.           constr(6) 119.        
    x[1,3] constr(1) 1.           constr(7) 119.        
    x[1,4] constr(1) 1.           constr(8) 119.        
    x[1,5] constr(1) 1.           constr(9) 119.        
    x[1,6] constr(1) 1.           constr(10) 119.        
    x[1,7] constr(1) 1.           constr(11) 119.        
    x[1,8] constr(1) 1.           constr(12) 119.        
    x[1,9] constr(1) 1.           constr(13) 119.        
    x[2,0] constr(2) 1.           constr(4) 74.         
    x[2,1] constr(2) 1.           constr(5) 74.         
    x[2,2] constr(2) 1.           constr(6) 74.         
    x[2,3] constr(2) 1.           constr(7) 74.         
    x[2,4] constr(2) 1.           constr(8) 74.         
    x[2,5] constr(2) 1.           constr(9) 74.         
    x[2,6] constr(2) 1.           constr(10) 74.         
    x[2,7] constr(2) 1.           constr(11) 74.         
    x[2,8] constr(2) 1.           constr(12) 74.         
    x[2,9] constr(2) 1.           constr(13) 74.         
    x[3,0] constr(3) 1.           constr(4) 90.         
    x[3,1] constr(3) 1.           constr(5) 90.         
    x[3,2] constr(3) 1.           constr(6) 90.         
    x[3,3] constr(3) 1.           constr(7) 90.         
    x[3,4] constr(3) 1.           constr(8) 90.         
    x[3,5] constr(3) 1.           constr(9) 90.         
    x[3,6] constr(3) 1.           constr(10) 90.         
    x[3,7] constr(3) 1.           constr(11) 90.         
    x[3,8] constr(3) 1.           constr(12) 90.         
    x[3,9] constr(3) 1.           constr(13) 90.         
    y[0] OBJROW 1.           constr(4)  -250.      
    y[0] constr(14) 1.          
    y[1] OBJROW 1.           constr(5)  -250.      
    y[1] constr(14)  -1.         constr(15) 1.          
    y[2] OBJROW 1.           constr(6)  -250.      
    y[2] constr(15)  -1.         constr(16) 1.          
    y[3] OBJROW 1.           constr(7)  -250.      
    y[3] constr(16)  -1.         constr(17) 1.          
    y[4] OBJROW 1.           constr(8)  -250.      
    y[4] constr(17)  -1.         constr(18) 1.          
    y[5] OBJROW 1.           constr(9)  -250.      
    y[5] constr(18)  -1.         constr(19) 1.          
    y[6] OBJROW 1.           constr(10)  -250.      
    y[6] constr(19)  -1.         constr(20) 1.          
    y[7] OBJROW 1.           constr(11)  -250.      
    y[7] constr(20)  -1.         constr(21) 1.          
    y[8] OBJROW 1.           constr(12)  -250.      
    y[8] constr(21)  -1.         constr(22) 1.          
    y[9] OBJROW 1.           constr(13)  -250.      
    y[9] constr(22)  -1.        
RHS
    RHS constr(0) 1.           constr(1) 2.          
    RHS constr(2) 2.           constr(3) 1.          
BOUNDS
 UI BOUND x[0,0]        1e+30
 UI BOUND x[0,1]        1e+30
 UI BOUND x[0,2]        1e+30
 UI BOUND x[0,3]        1e+30
 UI BOUND x[0,4]        1e+30
 UI BOUND x[0,5]        1e+30
 UI BOUND x[0,6]        1e+30
 UI BOUND x[0,7]        1e+30
 UI BOUND x[0,8]        1e+30
 UI BOUND x[0,9]        1e+30
 UI BOUND x[1,0]        1e+30
 UI BOUND x[1,1]        1e+30
 UI BOUND x[1,2]        1e+30
 UI BOUND x[1,3]        1e+30
 UI BOUND x[1,4]        1e+30
 UI BOUND x[1,5]        1e+30
 UI BOUND x[1,6]        1e+30
 UI BOUND x[1,7]        1e+30
 UI BOUND x[1,8]        1e+30
 UI BOUND x[1,9]        1e+30
 UI BOUND x[2,0]        1e+30
 UI BOUND x[2,1]        1e+30
 UI BOUND x[2,2]        1e+30
 UI BOUND x[2,3]        1e+30
 UI BOUND x[2,4]        1e+30
 UI BOUND x[2,5]        1e+30
 UI BOUND x[2,6]        1e+30
 UI BOUND x[2,7]        1e+30
 UI BOUND x[2,8]        1e+30
 UI BOUND x[2,9]        1e+30
 UI BOUND x[3,0]        1e+30
 UI BOUND x[3,1]        1e+30
 UI BOUND x[3,2]        1e+30
 UI BOUND x[3,3]        1e+30
 UI BOUND x[3,4]        1e+30
 UI BOUND x[3,5]        1e+30
 UI BOUND x[3,6]        1e+30
 UI BOUND x[3,7]        1e+30
 UI BOUND x[3,8]        1e+30
 UI BOUND x[3,9]        1e+30
 BV BOUND y[0] 1.          
 BV BOUND y[1] 1.          
 BV BOUND y[2] 1.          
 BV BOUND y[3] 1.          
 BV BOUND y[4] 1.          
 BV BOUND y[5] 1.          
 BV BOUND y[6] 1.          
 BV BOUND y[7] 1.          
 BV BOUND y[8] 1.          
 BV BOUND y[9] 1.          
ENDATA
