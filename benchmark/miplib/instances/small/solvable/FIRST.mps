NAME          FIRST
ROWS
 N  OBJROW
 L  constr(0)
 L  constr(1)
 E  constr(2)
 G  constr(3)
 L  constr(4)
 E  constr(5)
 G  constr(6)
 L  constr(7)
 L  constr(8)
 L  constr(9)
COLUMNS
    var(0) OBJROW      6.6        constr(0)     1.          
    var(0) constr(3)  -14.        constr(4)     2.          
    var(0) constr(8)  -1.        
    var(1) OBJROW     -3.         constr(0)     2.          
    var(1) constr(1)  -2.3        constr(3)    -20.4      
    var(1) constr(5)   3.         constr(7)     7.3         
    var(1) constr(8)   1.          
    var(2) OBJROW 4.              constr(0)     3.          
    var(2) constr(1)   30.        constr(4)     9.5         
    var(2) constr(9)   15.         
    var(3) constr(0)   3.         constr(1)    -17.       
    var(3) constr(3)   3.         constr(5)    -8.        
    var(3) constr(6)   3.         constr(9)    -15.       
    var(4) OBJROW      1.         constr(0)    -4.        
    var(4) constr(3)   33.        constr(4)     20.         
    var(4) constr(8)  -4.         constr(9)     3.          
    var(5) OBJROW     -5.         constr(0)     9.          
    var(5) constr(2)   1.         constr(3)     4.4         
    var(5) constr(5)   1.4        constr(9)     3.          
    var(6) constr(0)   2.         constr(1)     4.          
    var(6) constr(2)   1.         constr(4)     0.33        
    var(6) constr(8)   9.          
    var(7) OBJROW      7.1        constr(1)    -2.        
    var(7) constr(5)   5.         constr(7)    -5.        
    var(7) constr(8)   23.        constr(9)     4.          
    var(8) constr(0)   1.4        constr(1)     0.3         
    var(8) constr(3)   7.         constr(4)    -24.       
    var(8) constr(6)   2.         constr(9)    -7.        
    var(9) OBJROW     -2.2        constr(1)    -10.2      
    var(9) constr(2)  -1.         constr(3)    -7.7       
    var(9) constr(5)   12.        constr(8)    -8.3       
    var(9) constr(9)   6.          
RHS
    RHS    constr(0)   3.3        constr(1)     5.          
    RHS    constr(2)   27.        constr(3)    -4.        
    RHS    constr(4)   11.        constr(5)    -4.5       
    RHS    constr(6)  -9.         constr(7)     1.          
    RHS    constr(8)  -2.2        constr(9)     10.         
ENDATA
