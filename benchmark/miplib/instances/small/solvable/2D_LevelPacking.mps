NAME          BLANK     FREE
ROWS
 N  OBJROW
 E  constr(0)
 E  constr(1)
 E  constr(2)
 E  constr(3)
 E  constr(4)
 E  constr(5)
 E  constr(6)
 E  constr(7)
 L  constr(8)
 L  constr(9)
 L  constr(10)
 L  constr(11)
 L  constr(12)
 L  constr(13)
 L  constr(14)
 L  constr(15)
COLUMNS
    var(0) OBJROW 2.           constr(0) 1.          
    var(0) constr(8)  -6.        
    var(1) constr(2) 1.           constr(8) 5.          
    var(2) constr(0) 1.           constr(9) 4.          
    var(3) OBJROW 4.           constr(1) 1.          
    var(3) constr(9)  -7.        
    var(4) constr(2) 1.           constr(9) 5.          
    var(5) constr(5) 1.           constr(9) 4.          
    var(6) constr(7) 1.           constr(9) 3.          
    var(7) OBJROW 1.           constr(2) 1.          
    var(7) constr(10)  -5.        
    var(8) constr(0) 1.           constr(11) 4.          
    var(9) constr(1) 1.           constr(11) 3.          
    var(10) constr(2) 1.           constr(11) 5.          
    var(11) OBJROW 5.           constr(3) 1.          
    var(11) constr(11)  -8.        
    var(12) constr(5) 1.           constr(11) 4.          
    var(13) constr(6) 1.           constr(11) 7.          
    var(14) constr(7) 1.           constr(11) 3.          
    var(15) constr(0) 1.           constr(12) 4.          
    var(16) constr(1) 1.           constr(12) 3.          
    var(17) constr(2) 1.           constr(12) 5.          
    var(18) constr(3) 1.           constr(12) 2.          
    var(19) OBJROW 6.           constr(4) 1.          
    var(19) constr(12)  -9.        
    var(20) constr(5) 1.           constr(12) 4.          
    var(21) constr(6) 1.           constr(12) 7.          
    var(22) constr(7) 1.           constr(12) 3.          
    var(23) constr(0) 1.           constr(13) 4.          
    var(24) constr(2) 1.           constr(13) 5.          
    var(25) OBJROW 3.           constr(5) 1.          
    var(25) constr(13)  -6.        
    var(26) constr(0) 1.           constr(14) 4.          
    var(27) constr(1) 1.           constr(14) 3.          
    var(28) constr(2) 1.           constr(14) 5.          
    var(29) constr(3) 1.           constr(14) 2.          
    var(30) constr(5) 1.           constr(14) 4.          
    var(31) OBJROW 5.           constr(6) 1.          
    var(31) constr(14)  -3.        
    var(32) constr(7) 1.           constr(14) 3.          
    var(33) constr(0) 1.           constr(15) 4.          
    var(34) constr(1) 1.           constr(15) 3.          
    var(35) constr(2) 1.           constr(15) 5.          
    var(36) constr(5) 1.           constr(15) 4.          
    var(37) OBJROW 4.           constr(7) 1.          
    var(37) constr(15)  -7.        
RHS
    RHS constr(0) 1.           constr(1) 1.          
    RHS constr(2) 1.           constr(3) 1.          
    RHS constr(4) 1.           constr(5) 1.          
    RHS constr(6) 1.           constr(7) 1.          
BOUNDS
 BV BOUND var(0) 1.          
 BV BOUND var(1) 1.          
 BV BOUND var(2) 1.          
 BV BOUND var(3) 1.          
 BV BOUND var(4) 1.          
 BV BOUND var(5) 1.          
 BV BOUND var(6) 1.          
 BV BOUND var(7) 1.          
 BV BOUND var(8) 1.          
 BV BOUND var(9) 1.          
 BV BOUND var(10) 1.          
 BV BOUND var(11) 1.          
 BV BOUND var(12) 1.          
 BV BOUND var(13) 1.          
 BV BOUND var(14) 1.          
 BV BOUND var(15) 1.          
 BV BOUND var(16) 1.          
 BV BOUND var(17) 1.          
 BV BOUND var(18) 1.          
 BV BOUND var(19) 1.          
 BV BOUND var(20) 1.          
 BV BOUND var(21) 1.          
 BV BOUND var(22) 1.          
 BV BOUND var(23) 1.          
 BV BOUND var(24) 1.          
 BV BOUND var(25) 1.          
 BV BOUND var(26) 1.          
 BV BOUND var(27) 1.          
 BV BOUND var(28) 1.          
 BV BOUND var(29) 1.          
 BV BOUND var(30) 1.          
 BV BOUND var(31) 1.          
 BV BOUND var(32) 1.          
 BV BOUND var(33) 1.          
 BV BOUND var(34) 1.          
 BV BOUND var(35) 1.          
 BV BOUND var(36) 1.          
 BV BOUND var(37) 1.          
ENDATA
