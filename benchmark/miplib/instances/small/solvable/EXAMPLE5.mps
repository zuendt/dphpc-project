NAME          EXAMPLE5
ROWS
 N  OBJ
 G  ROW01
 E  ROW02
 E  ROW03
 L  ROW04
COLUMNS
    COL01     OBJ                1.0   ROW01              1.0
    COL01     ROW02             -1.0
    COL02     ROW01              1.0   OBJ                8.0
    COL03     ROW01              1.0   ROW04              1.0
    COL03     OBJ                9.0
    COL04     ROW02              1.0   ROW03             -1.0
    COL04     OBJ                2.0
    COL05     OBJ                7.0   ROW02              1.0
    COL05     ROW04              1.0
    COL06     ROW03              1.0   OBJ                3.0
    COL06     ROW04              1.0
RHS
    RHS1      ROW01              1.0
    RHS1      ROW02              0.0
    RHS1      ROW03              0.0
    RHS1      ROW04              1.0
ENDATA