NAME          TEST1
ROWS
 N  COST
 E  c1
 E  c2
 E  c3
COLUMNS
    x1        COST      -3
    x1        c1        2
    x1        c3        3
    x2        COST      -5
    x2        c1        3
    x2        c2        2
    x2        c3        2
    x3        COST      -4
    x3        c2        5
    x3        c3        4
RHS
    B         c1        8
    B         c2        10
    B         c3        15
ENDATA
