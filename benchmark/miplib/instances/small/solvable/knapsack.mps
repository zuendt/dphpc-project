NAME          knapsack  FREE
ROWS
 N  OBJROW
 L  constr(0)
COLUMNS
    var(0) OBJROW 10.          constr(0) 11.         
    var(1) OBJROW 13.          constr(0) 15.         
    var(2) OBJROW 18.          constr(0) 20.         
    var(3) OBJROW 31.          constr(0) 35.         
    var(4) OBJROW 7.           constr(0) 10.         
    var(5) OBJROW 15.          constr(0) 33.         
RHS
    RHS constr(0) 47.         
BOUNDS
 BV BOUND var(0) 1.          
 BV BOUND var(1) 1.          
 BV BOUND var(2) 1.          
 BV BOUND var(3) 1.          
 BV BOUND var(4) 1.          
 BV BOUND var(5) 1.          
ENDATA
