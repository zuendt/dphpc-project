NAME          WHAT
ROWS
 N  OBJECTRW
 E  R0000001
 E  R0000002
 E  R0000003
 E  R0000004
COLUMNS
    C0000001  OBJECTRW  -0040.000000   R0000001      1.000000
    C0000001  R0000003      1.000000
    C0000002  OBJECTRW  -0050.000000   R0000001      1.000000
    C0000002  R0000004      1.000000
    C0000003  OBJECTRW  -0050.000000   R0000002      1.000000
    C0000003  R0000003      1.000000
    C0000004  OBJECTRW  -0040.000000   R0000002      1.000000
    C0000004  R0000004      1.000000
RHS
    RHS1      R0000001      1.000000   R0000002      1.000000
    RHS1      R0000003      1.000000   R0000004      1.000000
BOUNDS
 BV BOUND1    C0000001      1.000000
 BV BOUND1    C0000002      1.000000
 BV BOUND1    C0000003      1.000000
 BV BOUND1    C0000004      1.000000
ENDATA