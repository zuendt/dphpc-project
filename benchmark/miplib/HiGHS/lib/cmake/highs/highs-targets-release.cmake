#----------------------------------------------------------------
# Generated CMake target import file for configuration "RELEASE".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "highs::highs" for configuration "RELEASE"
set_property(TARGET highs::highs APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(highs::highs PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libhighs.so.1.6.0"
  IMPORTED_SONAME_RELEASE "libhighs.so.1.6"
  )

list(APPEND _IMPORT_CHECK_TARGETS highs::highs )
list(APPEND _IMPORT_CHECK_FILES_FOR_highs::highs "${_IMPORT_PREFIX}/lib/libhighs.so.1.6.0" )

# Import target "highs::highs-bin" for configuration "RELEASE"
set_property(TARGET highs::highs-bin APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(highs::highs-bin PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/highs"
  )

list(APPEND _IMPORT_CHECK_TARGETS highs::highs-bin )
list(APPEND _IMPORT_CHECK_FILES_FOR_highs::highs-bin "${_IMPORT_PREFIX}/bin/highs" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
