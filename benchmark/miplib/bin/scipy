#!/usr/bin/python3

import numpy as np
import mip
import scipy
import sys

#set to True if you want lots of text output
verbose=False

import code

def read_mps_for_u_e(filename:str) :
    """
    Reads a .mps and saves all the data of the MILP:

    min c^T * x

    s.t.        A*x <= b_u
                A*x == b_eq
          lb <=   x <= ub
                x_i integer or boolean specified in integrality
    """
    mdl = mip.Model(solver_name="CBC")
    mdl.verbose=int(verbose)
    mdl.read(filename)

    # model parameters
    num_vars = len(mdl.vars)
    num_cons = len(mdl.constrs)

    log_verbose("starting")

    # variable types and bounds
    x_lower_b = np.zeros(num_vars)
    x_upper_b = np.inf*np.ones(num_vars)
    integrality = np.zeros(num_vars)
    for i, var in enumerate(mdl.vars):
        x_lower_b[i] = var.lb
        x_upper_b[i] = var.ub
        if var.var_type == mip.CONTINUOUS :
            pass # 0 already in there
        elif var.var_type == mip.BINARY :
            integrality[i] = 3 #semi-integer, within upper bound=1
            #log_verbose('BINARY',var.ub) var.ub was already 1 before...
            x_upper_b[i]=1 # to be completely sure
        elif var.var_type == mip.INTEGER :
            integrality[i] = 1

    log_verbose("Loop1 finished")

    # objective
    c = np.zeros(num_vars)
    for i, var in enumerate(mdl.vars):
        if var in mdl.objective.expr:
            c[i] = mdl.objective.expr[var]
    if mdl.sense != "MIN":
        c *= -1.0

    log_verbose("Loop2 finished")

    # constraint coefficient matrix
    b_eq = np.zeros(num_cons)
    b_upper_b = np.zeros((num_cons))
    A_row_ixs,A_col_ixs,A_data=[],[],[]
    Aeq_row_ixs,Aeq_col_ixs,Aeq_data=[],[],[]
    for i, con in enumerate(mdl.constrs):
        expr=con.expr.expr
        if con.expr.sense == "=":
            b_eq[i]=con.rhs
            #load sparse data into Aeq_*
            for jx,var in enumerate(mdl.vars) :
                if var in expr :
                    Aeq_data.append(expr[var])
                    Aeq_row_ixs.append(i)
                    Aeq_col_ixs.append(jx)
        else :
            if con.expr.sense == "<":
                b_upper_b[i]=con.rhs
            elif con.expr.sense == ">":
                #invert to make "<"
                b_upper_b[i]=-con.rhs
            #load sparse data into A_*
            for jx,var in enumerate(mdl.vars) :
                if var in expr :
                    A_data.append(expr[var])
                    if con.expr.sense == ">":
                        #invert to make "<"
                        A_data[-1]*=-1
                    A_row_ixs.append(i)
                    A_col_ixs.append(jx)

    A = scipy.sparse.csr_matrix((A_data, (A_row_ixs, A_col_ixs)),
        shape=(num_cons, num_vars))
    Aeq = scipy.sparse.csr_matrix((Aeq_data, (Aeq_row_ixs, Aeq_col_ixs)),
        shape=(num_cons, num_vars))

    return mdl, c, A, b_upper_b, Aeq, b_eq, x_lower_b, x_upper_b, integrality

def log_verbose(*args) :
    if verbose :
        print(*args,file=sys.stderr)

def read_mps_for_u_l(filename: str):
    """
    Reads a .mps and saves all the data of the MILP:

    min c^T * x

    s.t. b_l <= A*x <= b_u
          lb <=   x <= ub
                x_i integer or boolean specified in integrality
    """
    mdl = mip.Model(solver_name="CBC")
    mdl.read(filename)

    # model parameters
    num_vars = len(mdl.vars)
    num_cons = len(mdl.constrs)

    log_verbose("starting")

    log_verbose(mdl.vars)
    # variable types and bounds
    lb = np.zeros(num_vars)
    ub = np.inf*np.ones(num_vars)
    integrality = np.zeros(num_vars)
    for i, var in enumerate(mdl.vars):
        lb[i] = var.lb
        ub[i] = var.ub
        if var.var_type == mip.CONTINUOUS :
            pass # 0 already in there
        elif var.var_type == mip.BINARY :
            integrality[i] = 3 #semi-integer, within upper bound=1
            #log_verbose('BINARY',var.ub) var.ub was already 1 before...
            ub[i]=1 # to be completely sure
        elif var.var_type == mip.INTEGER :
            integrality[i] = 1

    log_verbose("Loop1 finished")

    # objective
    c = np.zeros(num_vars)
    for i, var in enumerate(mdl.vars):
        if var in mdl.objective.expr:
            c[i] = mdl.objective.expr[var]
    if mdl.sense != "MIN":
        c *= -1.0

    log_verbose("Loop2 finished")

    # constraint coefficient matrix
    b_l = -np.inf*np.ones((num_cons))
    b_u = np.inf*np.ones((num_cons))
    row_ind = []
    col_ind = []
    data    = []
    for i, con in enumerate(mdl.constrs):
        if con.expr.sense == "=":
            b_l[i] = con.rhs
            b_u[i] = con.rhs
        elif con.expr.sense == "<":
            b_u[i] = con.rhs
        elif con.expr.sense == ">":
            b_l[i] = con.rhs
        for j, var in enumerate(mdl.vars):
            if var in (expr := con.expr.expr):
                coeff = expr[var]
                row_ind.append(i)
                col_ind.append(j)
                data.append(coeff)
        log_verbose(i, num_cons)
    A = scipy.sparse.csr_matrix((data, (row_ind, col_ind)), shape=(num_cons, num_vars))

    return mdl, c, b_l, A, b_u, lb, ub, integrality

def solve_with_mip(mdl:mip.Model) :
    log_verbose(mdl.optimize())
    log_verbose('optimal value:',mdl.objective_value)
    result=[]
    for v in mdl.vars:
        log_verbose(v.name,':', v.x)
        result.append(v.x)
    return result

def print_problem(**kwargs) :
    log_verbose("Problem Input:")
    for k,v in kwargs.items() :
        log_verbose(k,':',v)

#inFile = input("input file: ")
#outFile = input("output file: ")

#hardcoded
mode='linprog'
if mode=='milp' :
    mdl, c, b_l, A, b_u, lb, ub, integrality = read_mps_for_u_l(sys.argv[1])
    print_problem(c=c, b_l=b_l, A=A, b_u=b_u, lb=lb, ub=ub, integrality=integrality)
elif mode=='linprog' :
    mdl, c, A, b_u, Aeq, b_eq, lb, ub, integrality=read_mps_for_u_e(sys.argv[1])
    print_problem(c=c,A=A,b_u=b_u, Aeq=Aeq, b_eq=b_eq, lb=lb, ub=ub, integrality=integrality)

#solve_with_mip(mdl)


if mode=='milp' :
    bounds = scipy.optimize.Bounds(lb, ub)
    constraints = scipy.optimize.LinearConstraint(A.todense(), b_l, b_u)
    log_verbose("starting to solve...")
    res = scipy.optimize.milp(c=c, constraints=constraints, bounds=bounds,
            integrality=integrality, options={"time_limit": 60})
elif mode=='linprog' :
    bounds=list(zip(lb,ub))
    res = scipy.optimize.linprog(c=c,A_ub=A,b_ub=b_u,A_eq=Aeq,b_eq=b_eq,bounds=bounds,
        method='highs', #default
        #apparently unsupported:
        #callback=lambda part_result:print({k:getattr(part_result,k) for k in ('success',
        #    'x','fun','phase','status')}),
        integrality=integrality,
        options={"time_limit": 300}, # 5min
    )

log_verbose(res)
print('SOLFILESTART')
if res.fun==None :
    print('=unkn=')
    exit(0)
print('=opt=',res.fun)
#code.interact(local=locals())
for label,value in zip((i.name for i in mdl.vars),res.x) :
    print(label,value)
