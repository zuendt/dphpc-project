----------------------------+----------------+----------------+------+---------+-------+--------+---------
Name                        |   Dual Bound   |  Primal Bound  | Gap% |  Nodes  |  Time | Status | Solution 
----------------------------+----------------+----------------+------+---------+-------+--------+---------
air05                                   26374            26374    0.0       533      44       ok        ok
CMS750_4                                  252              252    0.0     25585     803       ok        ok
dano3_3                            576.344633       576.344633    0.0        20     113       ok        ok
dano3_5                            576.924916       576.924916    0.0       141     315       ok        ok
enlight_hard                               37               37    0.0         1       1       ok        ok
exp-1-500-5-5                           65887            65887    0.0         1       3       ok        ok
fast0507                                  174              174    0.0       845     330       ok        ok
fiball                                    138              138    0.0      2494     151       ok        ok
h80x6320d                          6382.09905       6382.09905    0.0         5     133       ok        ok
irp                                12159.4928       12159.4928    0.0         6      24       ok        ok
markshare_4_0                               1                1    0.0   2190888     199       ok        ok
mas74                              11801.1857       11801.1857    0.0   5226901    1570       ok        ok
mas76                              40005.0541       40005.0541    0.0    312773     118       ok        ok
mik-250-20-75-4                        -52301           -52301    0.0      9874      30       ok        ok
mzzv42z                                -20540           -20540    0.0      1206     289       ok        ok
n5-3                                     8105             8105    0.0      1054      44       ok        ok
neos-1122047                              161              161    0.0         1      12       ok        ok
neos-1171448                             -309             -309    0.0         8      20       ok        ok
neos-1445765                           -17783           -17783    0.0       263      44       ok        ok
neos-1582420                               91               91    0.0       278      29       ok        ok
neos-787933                                30               30    0.0         1       2       ok        ok
neos-827175                         112.00152        112.00152    0.0         1       8       ok        ok
neos-860300                              3201             3201    0.0         2      21       ok        ok
neos-911970                             54.76            54.76    0.0    164530     408       ok        ok
neos-950242                                 4                4    0.0       100     169       ok        ok
neos-957323                       -237.756681      -237.756681    0.0         2     109       ok        ok
neos-960392                              -238             -238    0.0        43     592       ok        ok
neos17                            0.150002577      0.150002577    0.0      3373       7       ok        ok
neos5                                      15               15    0.0   1451738     498       ok        ok
neos8                                   -3719            -3719    0.0         1       8       ok        ok
nu25-pr12                               53905            53905    0.0       129       6       ok        ok
nw04                                    16862            16862    0.0         6      28       ok        ok
p200x1188c                              15078            15078    0.0         1       3       ok        ok
pk1                                        11               11    0.0    316834     112       ok        ok
qap10                                     340              340    0.0         1      44       ok        ok
seymour1                           410.763701       410.763701    0.0      1111      60       ok        ok
sp150x300d                                 69               69    0.0        54       1       ok        ok
swath1                             379.071296       379.071296    0.0       375      14       ok        ok
swath3                             397.761344       397.761344    0.0    340716    1954       ok        ok
tr12-30                                130596           130596    0.0    245996     455       ok        ok
----------------------------+----------------+----------------+------+---------+-------+--------+---------

solved/stopped/failed: 40/0/0

@03 MIPLIB script version 1.0.3
@02 timelimit: 3600
@01 SCIP(8.0.4)(6.0.4)
