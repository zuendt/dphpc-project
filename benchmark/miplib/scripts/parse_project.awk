#!/usr/bin/awk -f
#* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
#*                                                                           *
#*            This file is part of the test engine for MIPLIB2017            *
#*                                                                           *
#* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


# set all solver specific data:
#  solver ["?"]
#  solverversion ["?"]
#  solverremark [""]
#  bbnodes [0]
#  db [-infty]
#  pb [+infty]
#  aborted [1]
#  timeout [0]

# solver name
BEGIN {
   solver = "DPHPC_project";
}
# solver version
/^SCIP version/ { solverversion = "1.0"; }
#
# branch and bound nodes
/^  nodes \(total\)    :/ { bbnodes = $4; }
# dual and primal bound
/^  Primal Bound     :/ {
   if( $4 == "infeasible" )
   {
      pb = +infty;
      db = +infty;
   }
   else if( $4 == "-" )
      pb = +infty;
   else
      pb = $4;
}
/^  Dual Bound       :/ {
   if( $4 != "-" )
      db = $4;
}
# solving status
/^SCIP Status        :/ { aborted = 0; }
/solving was interrupted/ { timeout = 1; }
/problem is solved/ { timeout = 0; }
