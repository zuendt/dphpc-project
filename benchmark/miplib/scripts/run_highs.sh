#* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
#*                                                                           *
#*       This file is  NOT part of the test engine for MIPLIB2017            *
#*                                                                           *
#* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

# executable options: https://ergo-code.github.io/HiGHS/dev/executable/#Executable

SOLVER=$1
BINNAME=$2
NAME=$3
TIMELIMIT=$4
MEMLIMIT=$5   # The memory limit (in MB)
SOLFILE=$6
THREADS=$7
MIPGAP=$8

TMPFILE=check.$SOLVER.tmp

echo "WARNING, ignoring threads amount"
echo "WARNING, ignoring memlimit"

if [ "$(cat /etc/hostname)" = 'davinci' ];then
    export LD_PRELOAD="${BINNAME%%bin/highs}/HiGHS/lib/libhighs.so.1.6"
else
    export LD_LIBRARY_PATH="./HiGHS/lib/" 
fi

"${BINNAME}" --model_file $NAME \
               --time_limit $TIMELIMIT \
               --presolve "off" \
               --solution_file "${SOLFILE}" \
               --parallel "on" > "${TMPFILE}"

cat "${SOLFILE}" >> "${TMPFILE}"
