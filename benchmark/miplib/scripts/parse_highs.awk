#!/usr/bin/awk -f
#* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
#*                                                                           *
#*            This file is part of the test engine for MIPLIB2017            *
#*                                                                           *
#* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


# set all solver specific data:
#  solver ["?"]
#  solverversion ["?"]
#  solverremark [""]
#  bbnodes [0]
#  db [-infty]
#  pb [+infty]
#  aborted [1]
#  timeout [0]

# solver name
BEGIN {
   solver = "HIGHS";
}
# solver version unknown
# LP solver name and version
lps = "none";
solverremark = lps;

# branch and bound nodes
/^  nodes \(total\)    :/ { bbnodes = $4; }
# dual and primal bound
/^c Status:/ {
   if( $3 == "INFEASIBLE" )
   {
      pb = +infty;
      db = +infty;
        knownsolstatus[$2] = "inf";
   }
   else if ($3 == "OPTIMAL") {
      pb = $3;
        knownsolstatus[$2] = "opt";
   }
}
/^c Objective:/ {
    if ($5 != "" )
        knownsolvalue[$2] = $5;
}
/^  Dual Bound       :/ {
   if( $4 != "-" )
      db = $4;
}
# solving status
/^e o f/ { aborted = 0; }
/Status\s+Time limit reached/ { timeout = 1; }
/^HiGHS run time/ { timeout = 0; }
