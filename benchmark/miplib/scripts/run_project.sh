
SOLVER=$1
BINNAME=$2
NAME=$3
TIMELIMIT=$4
MEMLIMIT=$5   # The memory limit (in MB)
SOLFILE=$6
THREADS=$7
MIPGAP=$8

TMPFILE=check.$SOLVER.tmp

echo > $TMPFILE
echo > $SOLFILE

# NOTHING to be done!!

    # set threads to given value
    # set mipgap to given value
    # set timing to wall-clock time and pass time limit
    # set the available memory
    # use deterministic mode (warning if not possible)


# resolve NAME to be an absolute path
if [ "${NAME##/}" = "${NAME}" ];then
    NAME="$(pwd)/${NAME}"
fi

echo $BINNAME $NAME
#run
$BINNAME $NAME > $SOLFILE

if test -e $SOLFILE
then
    #convert/remove useless stuff from output of project,
    # into solfile format
    #Prepare into tmpfile
    in_solfile_sgmt=0
    while read line;do
        if [ "${in_solfile_sgmt}" = '1' ];then
            echo "${line}" >> $TMPFILE
        fi
        if [ "${line}" = 'SOLFILESTART' ];then
            in_solfile_sgmt=1
        fi
    done < $SOLFILE

    # hardcoded, WRONG example
    #echo '=obj= 54' >> $TMPFILE
    #echo 'XONE 99' >> $TMPFILE
    #echo 'YTWO -99' >> $TMPFILE
    #echo 'ZTHREE 3' >> $TMPFILE

    #replace solfile
    mv $TMPFILE $SOLFILE
fi
