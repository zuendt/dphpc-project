#ifndef SIMPLEX_CUH
#define SIMPLEX_CUH

typedef float val_T;

#include <thrust/extrema.h>
#include <thrust/execution_policy.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

// conversions between cuda and thrust need raw_pointer_cast
#include <thrust/device_ptr.h>
#include <thrust/tuple.h>
#include <cusparse.h>
#include <cusolverSp.h>
#include <string>
#include <cassert>

// DEBUGING
#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <iomanip>

// PROFILING
#include <cuda_profiler_api.h>


// Tolerance for floating point zero checks
#define TOL 1e-4f

// DEBUGGING
#define DEBUGGING true

#define DEBUGGING_UpdateBasis false
#define DEBUGGING_WriteVecAtIndicies false

#define DEBUGGING_SelEntering false
#define DEBUGGING_SelLeaving false

#define DEBUGGING_Operations false

#define DEBUGGING_LU false

#define DEBUGGING_Reader false

#define DEBUGGING_Phase1 false

// -------------------------------------------------------------
//                      ERROR DETECTION
// -------------------------------------------------------------


#define CHECK_CUDA(func)                                                        \
{                                                                               \
    cudaError_t status = (func);                                                \
    if (status != cudaSuccess) {                                                \
        PRINT_RED_PREFIX;                                                       \
        std::cout<<"CUDA API failed with error: ";                              \
        std::cout<<cudaGetErrorString(status)<<" ("<<status<<")"<<std::endl;    \
    }                                                                           \
}

#define CHECK_CUSPARSE(func)                                                    \
{                                                                               \
    cusparseStatus_t status = (func);                                           \
    if (status != CUSPARSE_STATUS_SUCCESS) {                                    \
        PRINT_RED_PREFIX;                                                       \
        std::cout<<"CUSPARSE API failed with error:" ;                          \
        std::cout<<cusparseGetErrorString(status)<<" ("<<status<<")"<<std::endl;\
    }                                                                           \
}


// -------------------------------------------------------------
//                      PRINT PREFIXES
// -------------------------------------------------------------


#define PRINT_RED_PREFIX { std::cout << "\033[31m["<< __FILE__ <<"("<< __LINE__ <<")] \033[0m"; }

#define PRINT_PREFIX { std::cout << "\033[32m["<< __FILE__ <<"("<< __LINE__ <<")] \033[0m"; }

#define PRINT_FUNC_PREFIX { std::cout << "\033[32m["<< __FILE__ <<"("<< \
    __LINE__ << ":" << __FUNCTION__ <<")] \033[0m"; }



// --------------------------------------------------------------------------
//                          PRINTING FUNCTIONS
// --------------------------------------------------------------------------

// For Debuging only
// First argument is a DenseVector
// second argument is a string to identify the vector when debugging
template <typename T, typename S>
void printVector(T* v, S name, bool debug=DEBUGGING)  {
    if (debug) {
        uint64_t vSize = v->size;

        val_T* vals;
        // CHECK_CUDA(cudaMalloc((void**)&vals, vSize * sizeof(val_T)))
        CHECK_CUSPARSE(cusparseDnVecGetValues(v->DnDescr, (void**)&vals))

        float vVals[vSize];
        // std::cout << name << " has now size: " << vSize << std::endl;
        // for (unsigned int i=0; i<vSize; ++i) {vVals[i] = 0.0f;}
        cudaMemcpy(vVals, vals, vSize * sizeof(val_T), cudaMemcpyDeviceToHost);
        // Debug: print all the values 
        std::cout <<"[device_ptr="<<vals<<",host="<<vVals<<"] "<<name<<" values are: ";
        unsigned int c=0;
        for (unsigned int i = 0; i < vSize; ++i) {
            if (c % 15 == 0)
                std::cout << std::endl;
            std::cout << vVals[i] << ", ";
            ++c;
        }
        std::cout << std::endl;
    }
}

template <typename T, typename S>
void printVectorAsSpVec(T* v, S name, bool debug=DEBUGGING)  {
    if (debug) {
        uint64_t vSize = v->size;

        val_T* vals;
        CHECK_CUSPARSE(cusparseDnVecGetValues(v->DnDescr, (void**)&vals))

        float vVals[vSize];
        cudaMemcpy(vVals, vals, vSize * sizeof(val_T), cudaMemcpyDeviceToHost);

        float tol = 10e-5;

        std::cout << name << " indicies are: ";
        for (unsigned int i = 0; i < vSize; ++i) {
            if (std::abs(vVals[i]) > tol)
                std::cout << i << ", ";
        }
        std::cout << std::endl;

        std::cout << name << " values are: ";
        for (unsigned int i = 0; i < vSize; ++i) {
            if (std::abs(vVals[i]) > tol)
                std::cout << vVals[i] << ", ";
        }
        std::cout << std::endl;
    }
}

// First argument is SparseMatrix
template <typename T>
void printSpMat(T* M, std::string name="", bool debug=DEBUGGING) {
    if (debug) {
        int64_t h_n, h_m, h_nnz;
        int* d_rowPtr;
        int* d_colInd;
        val_T* d_vals;

        cusparseIndexType_t h_rowPtrType, h_colIndType;
        cusparseIndexBase_t h_idxBase;
        cudaDataType h_valueType;

        CHECK_CUSPARSE(cusparseCsrGet(M->spDescr,
                                    &h_m,
                                    &h_n,
                                    &h_nnz,
                                    (void**)&d_rowPtr,
                                    (void**)&d_colInd,
                                    (void**)&d_vals,
                                    &h_rowPtrType,
                                    &h_colIndType,
                                    &h_idxBase,
                                    &h_valueType))

        std::cout << "printing Sparse Matrix " << name << "\n" << \
                    "m (rows) = " << h_m << "\n" << \
                    "n (cols) = " << h_n << "\n" << \
                    "nnz      = " << h_nnz << std::endl;
        
        val_T h_vals[h_nnz];
        CHECK_CUDA(cudaMemcpy(h_vals, d_vals, h_nnz * sizeof(val_T), cudaMemcpyDeviceToHost));

        int h_colInd[h_nnz];
        CHECK_CUDA(cudaMemcpy(h_colInd, d_colInd, h_nnz * sizeof(int), cudaMemcpyDeviceToHost));

        int h_rowPtr[h_m + 1];
        CHECK_CUDA(cudaMemcpy(h_rowPtr, d_rowPtr, (h_m + 1) * sizeof(int), cudaMemcpyDeviceToHost))

        std::cout << "rowPtr=" << std::endl;
        for (unsigned int i=0;  i<h_m + 1; ++i) {
            std::cout << h_rowPtr[i] << ", ";
        }   std::cout << std::endl;

        std::cout << "colInd=" << std::endl;
        for (unsigned int i=0; i<h_nnz; ++i) {
            std::cout << h_colInd[i] << ", ";
        }   std::cout << std::endl;

        std::cout << "vals=" << std::endl;
        for (unsigned int i=0; i<h_nnz; ++i) {
            std::cout << h_vals[i] << ", ";
        }   std::cout << std::endl;
    }
}

template <typename T>
void printArr(const T* h_arr, const int size, std::string arrName, bool debug=DEBUGGING)  {
    if (debug) {
        std::cout << arrName << "=" << std::endl;

        for (unsigned int i=0; i<size; ++i) {
            std::cout << h_arr[i] << ", ";
        }
        
        std::cout << std::endl;
    }
}


template <typename T>
void printArrDevice(const T* d_arr, const int size, std::string arrName, bool debug=DEBUGGING)  {
    if (debug) {
        std::cout << arrName << "=" << std::endl;

        val_T h_vals[size];
        CHECK_CUDA(cudaMemcpy((void *)h_vals, (void *)d_arr, size * sizeof(val_T), cudaMemcpyDeviceToHost));

        for (unsigned int i=0; i<size; ++i) {
            std::cout << h_vals[i] << ", ";
        }
        
        std::cout << std::endl;
    }
}


// print a CSR SparseMatrix M and its name spName
// the matrix is printed in dense format
template <typename T>
void printSpMatAsDenseMat(const T* M, std::string spName, bool debug=DEBUGGING)  {
    if (debug) {
        int64_t h_n, h_m, h_nnz;
        int* d_rowPtr;
        int* d_colInd;
        val_T* d_vals;

        cusparseIndexType_t h_rowPtrType, h_colIndType;
        cusparseIndexBase_t h_idxBase;
        cudaDataType h_valueType;

        CHECK_CUSPARSE(cusparseCsrGet(M->spDescr,
                                    &h_m,
                                    &h_n,
                                    &h_nnz,
                                    (void**)&d_rowPtr,
                                    (void**)&d_colInd,
                                    (void**)&d_vals,
                                    &h_rowPtrType,
                                    &h_colIndType,
                                    &h_idxBase,
                                    &h_valueType))

        std::cout << "\nprinting Dense Matrix " << spName << "\n" << \
                    "m (rows) = " << h_m << "\n" << \
                    "n (cols) = " << h_n << "\n" << \
                    "nnz      = " << h_nnz << std::endl;
        
        val_T h_vals[h_nnz];
        CHECK_CUDA(cudaMemcpy(h_vals, d_vals, h_nnz * sizeof(val_T), cudaMemcpyDeviceToHost));

        int h_colInd[h_nnz];
        CHECK_CUDA(cudaMemcpy(h_colInd, d_colInd, h_nnz * sizeof(int), cudaMemcpyDeviceToHost));

        int h_rowPtr[h_m + 1];
        CHECK_CUDA(cudaMemcpy(h_rowPtr, d_rowPtr, (h_m + 1) * sizeof(int), cudaMemcpyDeviceToHost))

        
        std::cout << "cols     :  ";
        for (unsigned int i=0; i<h_n; ++i) {
            std::cout << std::setw(5) << i << "  ";
        }

        unsigned int nextNNZ_off = 0;
        for (unsigned int i=0; i<h_m; ++i) {
            std::cout << "\nrow=" << std::setw(4) << i << " :   ";

            unsigned int nextColVal = h_colInd[nextNNZ_off];
            for (unsigned int j=0; j < h_n; ++j) {
                if (j == nextColVal && nextNNZ_off < h_rowPtr[i+1]) {
                    std::cout << std::setw(5) << h_vals[nextNNZ_off] << ", ";
                    ++nextNNZ_off;
                    nextColVal = h_colInd[nextNNZ_off];
                } else {
                    std::cout << std::setw(5) << 0.0 << ", ";
                }
            }
        }

        std::cout << std::endl;
    }
}



// -------------------------------------------------------------
//                      ABSTRACT CLASSES
// -------------------------------------------------------------


// Abstract format for a csr matrix (usable in HOST context)
struct SparseMatrix {
    typedef val_T value_t;

    SparseMatrix();
    SparseMatrix(unsigned int dim); // Initailizes identity matrix of size dim
    SparseMatrix(unsigned int rows, unsigned int cols, unsigned int nonZeros, int* rowPtr, int* colInd, value_t* val);

    ~SparseMatrix();

    int64_t m;                      // Number of rows
    int64_t n;                      // Number of cols
    int64_t nnz;                    // Number of non-zero entries

    int* csrRowPtr;                 // csr pointers
    int* csrColInd;
    value_t* csrVal;
    
    cusparseMatDescr_t descr;       // Descriptor
    cusparseSpMatDescr_t spDescr;   // sp Descriptor
};

// Abstract format for a dense vector
struct DenseVector {
    typedef val_T value_t;

    DenseVector();
    DenseVector(unsigned int n);
    DenseVector(unsigned int n, value_t* val);
    // Copy Constructor:
    DenseVector(const DenseVector& b);
    ~DenseVector();

    uint64_t size;                  // Size

    value_t* values;

    cusparseDnVecDescr_t DnDescr;   // Descriptor (new)

    cudaDataType valueType;         // Cuda equivalent to value_t value type (CUDA_R_32F for val_T)
};

// Mabey we can replace BasisVector with this still
// struct SparseVector {
//     typedef val_T value_t;

//     SparseVector();
//     SparseVector(unsigned int rows, unsigned int cols);
//     SparseVector(unsigned int size);
//     SparseVector(unsigned int size, unsigned int* indicies, value_t* values);
//     SparseVector(const SparseVector* spVec)          // copy constructor
//     ~SparseVector();

//     uint64_t size;

//     unsigned int* indicies;
//     value_t* values;

//     cusparseSpVecDescr SpDescr;
// };


// only for storing basis vector
using csr_triple=thrust::tuple<int,int,val_T>;
struct BasisVector {
    BasisVector(unsigned int m, unsigned int n);
    ~BasisVector();

    void print_vecs();                                  // to stdout FOR DEBUGGING because expensive to copy data around
    void print();                                       // to stdout FOR DEBUGGING because expensive to copy data around
    void mat_one_extract_set_position(unsigned int row,unsigned int col);

    unsigned int m_;
    unsigned int n_;
    cusparseIndexType_t idx_t;
    cusparseIndexBase_t i_b_t;
    cudaDataType_t dat_t;

    cusparseSpVecDescr_t descr;                         // sparse vec is more convenient for extracting entries of other mat/vecs

    thrust::device_vector<unsigned int> values_vec;
    void *values_vptr;

    thrust::device_vector<unsigned int> indices_vec;
    void *indices_vptr;
};

// CAN BE DELETED ONLY HERE TO ENABLE COMPILATION AS OLD CODE STILL IN SRC
struct LU {
    typedef val_T value_t;

    // Fill types for L an U:
    cusparseFillMode_t fillModeL = CUSPARSE_FILL_MODE_LOWER;
    cusparseFillMode_t fillModeR = CUSPARSE_FILL_MODE_UPPER;
    // Diag types of L and U:
    cusparseDiagType_t diagTypeL = CUSPARSE_DIAG_TYPE_UNIT;
    cusparseDiagType_t diagTypeR = CUSPARSE_DIAG_TYPE_NON_UNIT;

    // Number of rows/cols:
    int64_t m;
    // Number of non-zero entries:
    int64_t nnz;
    // Descriptors:
    cusparseSpMatDescr_t descr_L;
    cusparseSpMatDescr_t descr_U;
    // Value array:
    value_t* csrVal;
    // Row pointer arry:
    int* csrRowPtr;
    // Col index array:
    int* csrColInd;
    // Cuda value type (CUDA_R_32F for val_T):
    cudaDataType valueType = CUDA_R_32F;
    // Cuda index type (CUSPARSE_INDEX_32I for int):
    cusparseIndexType_t indexType = CUSPARSE_INDEX_32I;

    LU();
    ~LU();

    // Performs incomplete LU of a given matrix A in CRS format
    void performLU(cusparseHandle_t* handle, const SparseMatrix* A);

    // Solve fundtion
    void solve(cusparseHandle_t* handle, bool transpose, const DenseVector& rhs, DenseVector& intermediate, DenseVector& x);
};



// -------------------------------------------------------------
//                          FUNCTION DECLARATIONS
// -------------------------------------------------------------


// MAIN FUNCTIONS ---------------------

// Performs first phase of the simplex algorithm
// PRE: A is the constraint matrix, B is an identity matrix, b is the right hand side vector, c are the objective function coefficients, 
//      cB is all zeros (same size as b), basis contains the indices corresponding to the slack variable columns and xb is equal to b.
//      (A, b and c are from a problem in canonical form)
// POST: B contains the cols of A corresponding to basic variables, cB contains the entries of c corresponding to basic variables,
//       basiscontains the indices of the columns of A in B (in order) and xB contains the values of the basic variables.
//       If the function returns true, then all the outputs correspond to a basic feasible solution of the canonical problem (A, b, c).
//       If it returns false (the problem is infeasible) the outputs are arbitrary.
//       The termination conditions are returned through status.
bool simplexPhase1(cusparseHandle_t* handle, cusolverSpHandle_t* solvHandle, const SparseMatrix* A, SparseMatrix* B, const DenseVector* b, const DenseVector* c, DenseVector* cB, BasisVector** basis, DenseVector* xb, int maxNumIterations, int* status, const char profile, const bool debug=DEBUGGING);

// Performs second phase of simplex algorithm
bool simplexPhase2(cusparseHandle_t* handle, cusolverSpHandle_t* solvHandle, const SparseMatrix* A, SparseMatrix* B, const DenseVector* b, const DenseVector* c, DenseVector* cB, BasisVector* basis, DenseVector* xB, int maxNumIterations, int* status, const char profile, const bool debug=DEBUGGING);


// BASISFUNCTIONS ---------------------

// take column i of A and write into ds_output.
void getCol(cusparseHandle_t *handle, const SparseMatrix* A,
    DenseVector* output, const unsigned int i);

// Overwrite a vector ds_Y with only the entries of a given vector ds_A, which
// are specified as indices in a seperate vector of indices: basis.
// uses its own little helper sp_helper
void writeVectorAtIndices(cusparseHandle_t *handle, const BasisVector* basis,
        DenseVector* cB, const DenseVector* c, const bool debug=DEBUGGING_WriteVecAtIndicies);

// converts the CSR B into CSC Format to then remove column i and copy in Column j from the Matrix A
// also removes i from the basis and enters j
void updateBasis(cusparseHandle_t *handle, BasisVector* basis, unsigned const int colIdx_Out,
        unsigned const int colIdx_In, const SparseMatrix* A, SparseMatrix* B, const bool debug=DEBUGGING_UpdateBasis);


// OPERATIONS ------------------------

// Perform v2 = A * v1 - c
void calculateV2(cusparseHandle_t* handle, const SparseMatrix* A, const DenseVector* v1, const DenseVector* c, DenseVector* v2, bool debug=DEBUGGING_Operations);

// Returns a dense Vector with all zeros, except at the j-th entry whrer the value is 1.0
// Lets replace this by an Identity matrix in the main program of which we will reference the j-th column/row when the vector is needed
void unitJvector(DenseVector* unitVec, const int j);

// Compute dot product of 2 DenseVectors a and b
void dot(const DenseVector* a, const DenseVector* b, DenseVector::value_t* res);
void dotGPU(const DenseVector* a, const DenseVector* b, DenseVector::value_t* res);


// ENTERING/LEAVING SELECTION --------

// Selects col of the next pivot entry <-> the basis entering variabel
bool enableBland(const DenseVector * xB);
bool selectEnteringVar(const DenseVector* v1, int* j, const DenseVector* v2, const bool blandsrule = true, bool activateBland = false, const bool debug=DEBUGGING_SelEntering);

bool selectLeavingVar(const DenseVector* B_invA, const DenseVector* xB, int* i, const bool debug=DEBUGGING_SelLeaving);


// READER ----------------------------

//reads mps format from filename
void mps_reader(char *filename, SparseMatrix** A, DenseVector** b, DenseVector** c, const bool debug=DEBUGGING_Reader);

// Output result to a file
void write_sol(int status, val_T opt_val, DenseVector::value_t* solution, const int numVars, const int numConst);


// HELPER ----------------------------

// Directly solves linear system with LU
void luSolve(cusolverSpHandle_t& solvHandle, cusparseHandle_t& handle, const SparseMatrix& B, const DenseVector& b, const bool transpose, DenseVector& sol, const bool debug=DEBUGGING_LU);

// Add the -1 column to A for the auxilary problem
void createA_aux(const SparseMatrix* A, SparseMatrix** A_aux);

// Gather all solution values from both slack and decision variables
void gatherSolution(const DenseVector* xb, const BasisVector* basis, DenseVector::value_t** solution, const int size);


// HELPER KERNEL ---------------------

// Helper for gatherSolution
__global__ void setBasicVals(const DenseVector::value_t* xbVals, const int* basisVals, DenseVector::value_t* solution);

// Check that index idx is not in basis
// PRE: Assumes feasible2_d to be true at the beginning
__global__ void checkMinimality(unsigned int* basisValues, int* idx, bool* feasible2_d);

// Helper for removeSlacksFrom_xb
__global__ void makeZeros(DenseVector::value_t* newVals);

// Helper for the DenseVector copy constructer
__global__ void copyData(const DenseVector::value_t* src, DenseVector::value_t* dest, const int size);

// DEBUGGING --------------------------
// generates an array of random values
// mabey would be worth it to do it based on a random seed
template <typename T>
void randVals(T* vals, const int size) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(-40, 63); // define the range

    for (int i=0; i<size; ++i) {
        vals[i] = distr(gen);
    }
}

#endif










/*
#ifndef SIMPLEX_CUH
#define SIMPLEX_CUH

#include "debugingUtils.cu"
#include <thrust/extrema.h>
#include <thrust/execution_policy.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
// conversions between cuda and thrust need raw_pointer_cast
#include <thrust/device_ptr.h>
#include <thrust/tuple.h>
#include <cusparse.h>
#include <cusolverSp.h>
#include <string>
#include <cassert>

typedef float val_T;

// Tolerance for floating point zero checks
#define TOL 1e-4f

#define CHECK_CUDA(func)                                                        \
{                                                                               \
    cudaError_t status = (func);                                                \
    if (status != cudaSuccess) {                                                \
        PRINT_RED_PREFIX;                                                       \
        std::cout<<"CUDA API failed with error: ";                              \
        std::cout<<cudaGetErrorString(status)<<" ("<<status<<")"<<std::endl;    \
    }                                                                           \
}

#define CHECK_CUSPARSE(func)                                                    \
{                                                                               \
    cusparseStatus_t status = (func);                                           \
    if (status != CUSPARSE_STATUS_SUCCESS) {                                    \
        PRINT_RED_PREFIX;                                                       \
        std::cout<<"CUSPARSE API failed with error:" ;                          \
        std::cout<<cusparseGetErrorString(status)<<" ("<<status<<")"<<std::endl;\
    }                                                                           \
}


#define PRINT_RED_PREFIX { std::cout << "\033[31m["<< __FILE__ <<"("<< __LINE__ <<")] \033[0m"; }

#define PRINT_PREFIX { std::cout << "\033[32m["<< __FILE__ <<"("<< __LINE__ <<")] \033[0m"; }

#define PRINT_FUNC_PREFIX { std::cout << "\033[32m["<< __FILE__ <<"("<< \
    __LINE__ << ":" << __FUNCTION__ <<")] \033[0m"; }

// For Debuging only
// First argument is a DenseVector
// second argument is a string to identify the vector when debugging
template <typename T, typename S>
void printVector(T* v, S name) {
    uint64_t vSize = v->size;

    val_T* vals;
    // CHECK_CUDA(cudaMalloc((void**)&vals, vSize * sizeof(val_T)))
    CHECK_CUSPARSE(cusparseDnVecGetValues(v->DnDescr, (void**)&vals))

    float vVals[vSize];
    // std::cout << name << " has now size: " << vSize << std::endl;
    // for (unsigned int i=0; i<vSize; ++i) {vVals[i] = 0.0f;}
    cudaMemcpy(vVals, vals, vSize * sizeof(val_T), cudaMemcpyDeviceToHost);
    // Debug: print all the values 
    std::cout <<"[device_ptr="<<vals<<",host="<<vVals<<"] "<<name<<" values are: ";
    unsigned int c=0;
    for (unsigned int i = 0; i < vSize; ++i) {
        if (c % 15 == 0)
            std::cout << std::endl;
        std::cout << vVals[i] << ", ";
        ++c;
    }
    std::cout << std::endl;
}

// First argument is SparseMatrix
template <typename T>
void printSpMat(const T* M) {
    int64_t h_n, h_m, h_nnz;
    int* d_rowPtr;
    int* d_colInd;
    val_T* d_vals;

    cusparseIndexType_t h_rowPtrType, h_colIndType;
    cusparseIndexBase_t h_idxBase;
    cudaDataType h_valueType;

    CHECK_CUSPARSE(cusparseCsrGet(M->spDescr,
                                  &h_m,
                                  &h_n,
                                  &h_nnz,
                                  (void**)&d_rowPtr,
                                  (void**)&d_colInd,
                                  (void**)&d_vals,
                                  &h_rowPtrType,
                                  &h_colIndType,
                                  &h_idxBase,
                                  &h_valueType))

    std::cout << "printing Sparse Matrix\n" << \
                "m (rows) = " << h_m << "\n" << \
                "n (cols) = " << h_n << "\n" << \
                "nnz      = " << h_nnz << std::endl;
    
    val_T h_vals[h_nnz];
    CHECK_CUDA(cudaMemcpy(h_vals, d_vals, h_nnz * sizeof(val_T), cudaMemcpyDeviceToHost));

    int h_colInd[h_nnz];
    CHECK_CUDA(cudaMemcpy(h_colInd, d_colInd, h_nnz * sizeof(int), cudaMemcpyDeviceToHost));

    int h_rowPtr[h_m + 1];
    CHECK_CUDA(cudaMemcpy(h_rowPtr, d_rowPtr, (h_m + 1) * sizeof(int), cudaMemcpyDeviceToHost))

    std::cout << "rowPtr=" << std::endl;
    for (unsigned int i=0;  i<h_m + 1; ++i) {
        std::cout << h_rowPtr[i] << ", ";
    }   std::cout << std::endl;

    std::cout << "colInd=" << std::endl;
    for (unsigned int i=0; i<h_nnz; ++i) {
        std::cout << h_colInd[i] << ", ";
    }   std::cout << std::endl;

    std::cout << "vals=" << std::endl;
    for (unsigned int i=0; i<h_nnz; ++i) {
        std::cout << h_vals[i] << ", ";
    }   std::cout << std::endl;
}

// Abstract format for a csr matrix (usable in HOST context)
struct SparseMatrix {
    typedef val_T value_t;

    SparseMatrix();
    SparseMatrix(unsigned int dim); // Initailizes identity matrix of size dim
    SparseMatrix(unsigned int rows, unsigned int cols, unsigned int nonZeros, int* rowPtr, int* colInd, value_t* val);

    // SparseMatrix(const SparseMatrix* SpMat);
    ~SparseMatrix();

    // Number of rows:
    int64_t m;
    // Number of cols:
    int64_t n;
    // Number of non-zero entries:
    int64_t nnz;
    // Descriptor:
    cusparseMatDescr_t descr;
    cusparseSpMatDescr_t spDescr;
    // Value array:
    value_t* csrVal;
    // Row pointer arry:
    int* csrRowPtr;
    // Col index array:
    int* csrColInd;
};

// Abstract format for a dense vector
struct DenseVector {
    typedef val_T value_t;     // should be float!

    DenseVector();
    DenseVector(unsigned int n);
    DenseVector(unsigned int n, value_t* val);
    // Copy Constructor:
    DenseVector(const DenseVector& b);
    ~DenseVector();

    // Size:
    uint64_t size;
    // Descriptor (new)
    cusparseDnVecDescr_t DnDescr;
    // Cuda equivalent to value_t value type (CUDA_R_32F for val_T):
    cudaDataType valueType;
};

// only for storing basis vector
using csr_triple=thrust::tuple<int,int,val_T>;
struct BasisVector {
    BasisVector(unsigned int m,unsigned int n);
    ~BasisVector();
    const unsigned int m_;
    const unsigned int n_;
    cusparseIndexType_t idx_t;
    cusparseIndexBase_t i_b_t;
    cudaDataType_t dat_t;
    //NOT NEEDED:
    //unsigned int* single_int_value;

    // sparse vec is more convenient for extracting entries of other mat/vecs
    cusparseSpVecDescr_t descr;

    thrust::device_vector<unsigned int> values_vec;
    void *values_vptr;

    //const thrust::device_vector<unsigned int> ones_vec;
    //const void *ones_vptr;

    thrust::device_vector<unsigned int> indices_vec;
    void *indices_vptr;

    //NOT NEEDED:
    // a big sparse matrix with only one 1 value, everything else 0
    //thrust::device_vector<val_T> mat_one_values;
    //thrust::device_vector<unsigned int> mat_one_row_ofs;
    //thrust::device_vector<unsigned int> mat_one_col_ind;
    //cusparseSpMatDescr_t mat_one_descr;

    void print_vecs(); // to stdout FOR DEBUGGING because expensive to copy data around
    void print(); // to stdout FOR DEBUGGING because expensive to copy data around
    void mat_one_extract_set_position(unsigned int row,unsigned int col);
};

//
struct LU {
    typedef val_T value_t;

    // Fill types for L an U:
    cusparseFillMode_t fillModeL = CUSPARSE_FILL_MODE_LOWER;
    cusparseFillMode_t fillModeR = CUSPARSE_FILL_MODE_UPPER;
    // Diag types of L and U:
    cusparseDiagType_t diagTypeL = CUSPARSE_DIAG_TYPE_UNIT;
    cusparseDiagType_t diagTypeR = CUSPARSE_DIAG_TYPE_NON_UNIT;

    // Number of rows/cols:
    int64_t m;
    // Number of non-zero entries:
    int64_t nnz;
    // Descriptors:
    cusparseSpMatDescr_t descr_L;
    cusparseSpMatDescr_t descr_U;
    // Value array:
    value_t* csrVal;
    // Row pointer arry:
    int* csrRowPtr;
    // Col index array:
    int* csrColInd;
    // Cuda value type (CUDA_R_32F for val_T):
    cudaDataType valueType = CUDA_R_32F;
    // Cuda index type (CUSPARSE_INDEX_32I for int):
    cusparseIndexType_t indexType = CUSPARSE_INDEX_32I;

    LU();
    ~LU();

    // Performs incomplete LU of a given matrix A in CRS format
    void performLU(cusparseHandle_t* handle, const SparseMatrix* A);

    // Solve fundtion
    void solve(cusparseHandle_t* handle, bool transpose, const DenseVector& rhs, DenseVector& intermediate, DenseVector& x);
};

// Type for communicating termination condition of the algorithm
// enum SimplexError {
//     maxIterations,
//     infeasible,
//     unbounded,
//     optimal,
//     feasible
// };

// Performs first phase of the simplex algorithm
// PRE: A is the constraint matrix, B is an identity matrix, b is the right hand side vector, c are the objective function coefficients, 
//      cB is all zeros (same size as b), basis contains the indices corresponding to the slack variable columns and xb is equal to b.
//      (A, b and c are from a problem in canonical form)
// POST: B contains the cols of A corresponding to basic variables, cB contains the entries of c corresponding to basic variables,
//       basiscontains the indices of the columns of A in B (in order) and xB contains the values of the basic variables.
//       If the function returns true, then all the outputs correspond to a basic feasible solution of the canonical problem (A, b, c).
//       If it returns false (the problem is infeasible) the outputs are arbitrary.
//       The termination conditions are returned through status.
bool simplexPhase1(cusparseHandle_t* handle, cusolverSpHandle_t* solvHandle, const SparseMatrix* A, SparseMatrix* B, const DenseVector* b, const DenseVector* c, DenseVector* cB, BasisVector* basis, DenseVector* xb, int maxNumIterations, int* status);

// Performs second phase of simplex algorithm
bool simplexPhase2(cusparseHandle_t* handle, cusolverSpHandle_t* solvHandle, const SparseMatrix* A, SparseMatrix* B, const DenseVector* b, const DenseVector* c, DenseVector* cB, BasisVector* basis, DenseVector* xb, int maxNumIterations, int* status);

// Returns a Matrix with only the columns of a given matrix A, which are specified as indices in a seperate vector cols
// see UpdateBasis which does this and some more
//SparseMatrix getMatrixCols();

// Overwrite a vector y with only the entries of a given vector a, which
//  are specified as indices in a seperate vector of indices: basis.
// uses its own little helper sp_helper
void WriteVectorAtIndices(cusparseHandle_t *h, BasisVector& basis,
        DenseVector *y, const DenseVector *a);
//DenseVector *getVectorEntries(const DenseVector *c, const DenseVector *basis);

void UpdateBasis(cusparseHandle_t *h, BasisVector basis, unsigned const int i,
        unsigned const int j, const SparseMatrix A, SparseMatrix B);

// Perform v2 = A * v1 - c
void calculateV2(cusparseHandle_t* handle, const SparseMatrix* A, const DenseVector* v1, const DenseVector* c, DenseVector* v2);

// Selects col of the next pivot entry <-> the basis entering variabel
bool selectEnteringVar(const DenseVector* v, int* j);

// Returns a dense Vector with all zeros, except at the j-th entry whrer the value is 1.0
// Lets replace this by an Identity matrix in the main program of which we will reference the j-th column/row when the vector is needed
void unitJvector(DenseVector* unitVec, const int j);


//reads mps format from filename
void mps_reader(char *filename, SparseMatrix** A, DenseVector** b, DenseVector** c);

// Add the -1 column to A for the auxilary problem
void createA_aux(const SparseMatrix* A, SparseMatrix** A_aux);

// Temporarily added this decleration so my tests compile
void createA_aux(const SparseMatrix* A, SparseMatrix** A_aux);

// Check that index idx is not in basis
// PRE: Assumes feasible2_d to be true at the beginning
__global__ void checkMinimality(unsigned int* basisValues, int* idx, bool* feasible2_d);

// Output result to a file
void write_sol(int status, val_T opt_val, DenseVector::value_t* solution, const int numVars, const int numConst);

// Compute dot product of 2 DenseVectors a and b
void dot(const DenseVector* a, const DenseVector* b, DenseVector::value_t* res);
void dotGPU(const DenseVector* a, const DenseVector* b, DenseVector::value_t* res);

// Helper for removeSlacksFrom_xb
__global__ void makeZeros(DenseVector::value_t* newVals);

// Helper for the DenseVector copy constructer
__global__ void copyData(const DenseVector::value_t* src, DenseVector::value_t* dest);

// take column i of A and write into ds_output.
void GetCol(cusparseHandle_t *handle, const SparseMatrix *sp_A, DenseVector *ds_output, unsigned int i);

// Gather all solution values from both slack and decision variables
void gatherSolution(const DenseVector* xb, const BasisVector* basis, DenseVector::value_t** solution, const int size);

// Helper for gatherSolution
__global__ void setBasicVals(const DenseVector::value_t* xbVals, const int* basisVals, DenseVector::value_t* solution);

//
bool selectLeavingVar(const DenseVector* B_invA, const DenseVector* xB, int* i, const DenseVector* index);

// Directly solves linear system with LU
void luSolve(cusolverSpHandle_t& solvHandle, cusparseHandle_t& handle, const SparseMatrix& B, const DenseVector& b, const bool transpose, DenseVector& sol);

// // Construct a solution vector that contains the solution values for all the original variables
// void removeSlacksFrom_xb(const DenseVector* xb, const BasisVector* basis, DenseVector::value_t** xb_noSlacks, const int size);

// // Helper for removeSlacksFrom_xb
// __global__ void fill(const DenseVector::value_t* xbVals, const int* basisVals, DenseVector::value_t* newVals, const int* size);

// // Selects row of the next pivot entry <-> the basis leaving variable. Returns false if problem is unbounded
// __device__ __host__ bool selectLeavingVar(const DenseVector B_invA, const DenseVector xB, int i, const DenseVector index);


#endif

*/
