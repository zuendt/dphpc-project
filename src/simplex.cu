#include "simplex.cuh"

// profiling variable right now only for simplex phase 2
// 0 don't profile
// 1 whole loop
// 2 WriteVectorAtIndicies
// 3 luSolve
// 4 calculateV2
// 5 selectEntering
// 6 getCol
// 7 selectLeaving
// 8 updateBasis

__global__ void reduceSlacks(unsigned int* basis_d, int numNonSlacks, int size) {
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	if (i < size) {
		if (basis_d[i] > numNonSlacks) {
			basis_d[i] -= 1;
		}
	}
}


bool simplexPhase1(cusparseHandle_t* handle, cusolverSpHandle_t* solvHandle, const SparseMatrix* A,
				   SparseMatrix* B, const DenseVector*  b, const DenseVector* c, DenseVector* cB,
				   BasisVector** basis, DenseVector* xb, int maxNumIterations, int* status, const char profile, const bool debug) {
	// Check if the problem is already feasible (if all b entries are positive)
    PRINT_FUNC_PREFIX;
    std::cout<<"maxNumIterations="<<maxNumIterations<<std::endl;
	//std::cout << "selectingEnteringVar" << std::endl << std::endl;

	bool dbP1 = DEBUGGING_Phase1;

	int i;

	printSpMatAsDenseMat(A, "A", dbP1);
	printSpMat(A, "A", dbP1);
	
	bool feasible = !selectEnteringVar(b, &i, b, false);
	if (feasible) {
		// Taking 5 as feasible here
		//std::cout <<"The problem was feasible form the start" << std::endl;
		*basis = new BasisVector(A->m, A->n);
		*status = 5;
		if (dbP1) {
			std::cout << "Problem feasible from start" <<std::endl;
		}
		return true;
	}

	//Create Objective vector cAux for the auxilary problem (Size A.n + 1 and all zero except last entry which is -1)
	DenseVector::value_t hlpr[A->n + 1];
	for (unsigned int k = 0; k < A->n; ++k) {
		hlpr[k] = 0;
	}
	hlpr[A->n] = -1.0;
    //PRINT_PREFIX;
	//std::cout << "creating cAux:" << std::endl;
	DenseVector cAux(A->n + 1, hlpr);


	printVector(&cAux, "cAux", dbP1);

	printVector(b, "b", dbP1);

	// Create a new matrix A_aux which is a with an additional column of -1's
    //PRINT_PREFIX;
	//std::cout << "creating A_aux via creatA_aux(A)" << std::endl;
	SparseMatrix* A_aux;
	createA_aux(A, &A_aux);
	printSpMat(A_aux, "A_aux", dbP1);
	printSpMatAsDenseMat(A_aux, "A_aux", dbP1);

	// Adjusted basis for A_aux
	// (*basis)->~BasisVector();
	// *basis = nullptr;
	*basis = new BasisVector(A_aux->m, A_aux->n);

	if (dbP1) {
		(*basis)->print();
	}
	std::cout << A->n << ", " << i << std::endl;
	
	// Pivot in auxilary column
	int j = A->n;
	// What are the indices of basis here at the beginning? The slack variable columns are not part of A
    //PRINT_PREFIX;
	//std::cout << "UpdatingBasis" << std::endl;
	updateBasis(handle, *basis, i, j, A_aux, B);

	if (dbP1) {
		(*basis)->print();
	}
    //PRINT_PREFIX;
	//std::cout << "WritingVectorAtIndices" << std::endl;
	writeVectorAtIndices(handle, *basis, cB, &cAux);

	printVector(cB, "cB", dbP1);

	printSpMatAsDenseMat(B, "B", dbP1);

	// Solve the auxilary problem:
    PRINT_PREFIX;
	//std::cout << "Using simplexPhase2 to get into feasible region" << std::endl;
	bool feasible1 = simplexPhase2(handle, solvHandle, A_aux, B, b, &cAux, cB, *basis, xb, maxNumIterations, status, profile, debug);

	if (dbP1) {
		(*basis)->print();
	}

	printSpMatAsDenseMat(B, "B", dbP1);

	printVector(cB, "cB", dbP1);

	printVector(xb, "xb", dbP1);

	// Check that auxilary variable is not in basis
	int numBlocks = (*basis)->m_;

	bool* feasible2_d;
	bool feasible2 = true;
	CHECK_CUDA(cudaMalloc((void **)&feasible2_d, sizeof(bool)))
	CHECK_CUDA(cudaMemcpy(feasible2_d, &feasible2, sizeof(bool), cudaMemcpyHostToDevice))

	int* idx_d;
	int idx = cAux.size-1;
	CHECK_CUDA(cudaMalloc((void **)&idx_d, sizeof(int)))
	CHECK_CUDA(cudaMemcpy(idx_d, &idx, sizeof(int), cudaMemcpyHostToDevice))
	
	unsigned int* basisValues;
	CHECK_CUSPARSE(cusparseSpVecGetValues(((*basis)->descr), (void **)&basisValues))

	checkMinimality<<<numBlocks, 1>>>(basisValues, idx_d, feasible2_d);
	CHECK_CUDA(cudaMemcpy(&feasible2, feasible2_d, sizeof(bool), cudaMemcpyDeviceToHost))

	CHECK_CUDA(cudaFree(feasible2_d))
	CHECK_CUDA(cudaFree(idx_d))

	// Reduce slack variable cols in basis by 1 in index again (removing of auxiliary var)
	int gridDim = 1;
    int blockDim = (*basis)->m_;
    if (blockDim > 1024) {
        gridDim = std::ceil(blockDim / 1024);
        blockDim = 1024;
    }
	reduceSlacks<<<gridDim, blockDim>>>(basisValues, A->n, (*basis)->m_);

	if (dbP1) {
		(*basis)->print();
	}

    PRINT_PREFIX;
	//std::cout << "Give cB the correct values for phase 2" << std::endl;
	writeVectorAtIndices(handle, *basis, cB, c);

	printVector(cB, "cB", dbP1);

	// Free A_aux (Not covered by destructor since its just a pointer)
	// A_aux->~SparseMatrix();
	delete A_aux;
	A_aux = nullptr;

	(*basis)->n_ = A->n;

	if (feasible1 && feasible2) {
		// Taking 5 as feasible here
		*status = 5;
		return true;
	} else {
		if (!feasible2 && feasible1) {
			*status = 0;
		} else {
			// Unknown might be termination through max iterations
			*status = 4;
		}
		return false;
	}

}


void createA_aux(const SparseMatrix* A, SparseMatrix** A_aux) {
	int cols = A->n + 1;
	int rows = A->m;
	int nnz = A->nnz + A->m;

	int* rowPtr = new int[rows+1];
	int* colInd = new int[nnz];
	SparseMatrix::value_t* val = new SparseMatrix::value_t[nnz];

	int ArowPtr[A->m+1];
	int AcolInd[A->nnz];
	SparseMatrix::value_t Aval[A->nnz];

	CHECK_CUDA(cudaMemcpy(ArowPtr, A->csrRowPtr, sizeof(int)*(A->m + 1), cudaMemcpyDeviceToHost))
	CHECK_CUDA(cudaMemcpy(AcolInd, A->csrColInd, sizeof(int)*(A->nnz), cudaMemcpyDeviceToHost))
	CHECK_CUDA(cudaMemcpy(Aval, A->csrVal, sizeof(SparseMatrix::value_t)*(A->nnz), cudaMemcpyDeviceToHost))
	

	for (int i=0; i < rows+1; ++i) {
		rowPtr[i] = ArowPtr[i] + i;		// as we add another column that has an entry in each case we need to insert that column into colInd
	}								    // to respect that the rowPtr also has to point to the new correct location -> each row has now an extra entry

	int lastColIdx = -1;
	int offset = 0;
	for (int i=0; i<A->nnz; ++i) {
		if (ArowPtr[offset+1] == i) {		// means that we are now in an other row
			colInd[i+offset] = cols-1;		// we need to add the last column in before we start adding the vals from the next row
			val[i+offset] = -1;
			++offset;
		}

		lastColIdx = AcolInd[i];
		colInd[i+offset] = lastColIdx;
		val[i+offset] = Aval[i];
	}

    // set the last one also to -1
    colInd[nnz-1] = cols-1;
    val[nnz-1] = -1;

    // ++offset;
	// assert(offset == A->m);
	// otherwise means that something went wrong

	*A_aux = new SparseMatrix(rows, cols, nnz, rowPtr, colInd, val);
}


__global__ void checkMinimality(unsigned int* basisValues, int* idx, bool* feasible2_d) {

	if (basisValues[blockIdx.x*blockDim.x + threadIdx.x] == *idx) {
		*feasible2_d = false;
	}

}

// A: Constraint matrix of problem in standard form
// b: RHS vector
// c: Cost vector of problem in standard formim
// xb: Solution Vector of basic variables
// status: 0 optiaml, 1 unbounded, 2 not finished
bool simplexPhase2(cusparseHandle_t* handle, cusolverSpHandle_t* solvHandle, const SparseMatrix* A,
				   SparseMatrix* B, const DenseVector* b, const DenseVector* c, DenseVector* cB,
				   BasisVector* basis, DenseVector* xB, int maxNumIterations, int* status, const char profile, const bool debug) {
    // get A, c in canonical form (without slack variable entries)
    PRINT_FUNC_PREFIX;
    std::cout<<"maxNumIterations="<<maxNumIterations<<std::endl;

	bool optimal = false;
	bool success = false;
	*status = 4;

	// Identity Matrix I of size mxm 
	//SparseMatrix I(A->m);


    //basis
    // 012345.. at start, up to unsigned int m
    // does a "cudaMalloc" (using thrust library)
    // BasisVector basis{A->m}; // count of variables


	// v1
	DenseVector v1(A->m);	// #rows

	// v2
	DenseVector v2(A->n);	// #cols

	// // xB
	// DenseVector xB(A->m);	// #constraints

	// BinvA
	DenseVector BinvA(A->m);	// #constraints

	// AColJ (j-th column of A)
	DenseVector AColJ(A->m);

	// Helper for solving with LU
	DenseVector intermediate(A->m);

	// xB indices (0 to xb.size-1) (This vector is float for now since DenseVector::value_t is fixed. Int would make more sense tho)
	DenseVector::value_t* tmp = (DenseVector::value_t *)malloc(xB->size*sizeof(DenseVector::value_t));
	for (int l = 0; l < xB->size; ++l) {
		tmp[l] = l;
	}
	DenseVector xBInd(xB->size, tmp);
	free(tmp);
	tmp = nullptr;


	if (debug) {
		std::cout << "-------- AT START OF LOOP -------" << std::endl;
		std::cout << "constant data" << std::endl;
		printSpMatAsDenseMat(A, "A");
		printVector(c, "c");
		printVector(b, "b");

		std::cout << "\n non const inital data" << std::endl;
		printSpMatAsDenseMat(B, "B");
		printVector(cB, "cB");
		

		// LU handler;
		// LU* B_decomp = new LU;

		std::cout << "Simplex Phase 2, finished initialization of helpers\n" << std::endl;
	}

	int k;
	int dummy;
	bool blandsrule = enableBland(xB);
	//selectEnteringVar(xB,&dummy,xB,false, blandsrule);
	
	// Loop until break or for some max number of iterations
			if (profile == '1') cudaProfilerStart();
	for (k=0; k<maxNumIterations; ++k) {
        //wöri imfall nid eso mache..., pro iteration wird nur 1 column
        //  geändert -> besser nicht alle column's neu ausrechnen
        // see updateBasis at the end
		//SparseMatrix B = BasicColsOfA(A, basis); // Felix
		// PRINT_PREFIX; std::cout << "iter=" << k << "  perfoming LU" << std::endl;
		// B_decomp->performLU(handle, B); // Florian
		// PRINT_PREFIX; std::cout << "finished LU\n" << std::endl;

		// DEBUG -------------------------------------------------
		// std::cout << "cB before write VectorAtIndices:" << std::endl;
		// printVector(cB, "cB before write VectorAtIndices");
		// DEBUG END ---------------------------------------------

        // update cB
		//DenseVector cB = BasicEntriesOfc(c, basis); // Felix
        // cB <- c at indices specified by basis
		// PRINT_PREFIX; std::cout << "iter=" << k << "  starting WriteVectorAtIndices" << std::endl;
				if (profile == '2') cudaProfilerStart();
        writeVectorAtIndices(handle, basis, cB ,c);
				if (profile == '2') cudaProfilerStop();
		// PRINT_PREFIX; std::cout << "iter=" << k << "  filled cB\n" << std::endl;

		// DEBUG -------------------------------------------------
		// std::cout << "cB vector after WriteVectorAtIndices:" << std::endl;
		// printVector(cB, "cB");
		// std::cout << "Adress of cB vals: " << cB->values << std::endl;
		// std::cout << std::endl;
		// DEBUG END ---------------------------------------------

		// DEBUG -------------------------------------------------
		// std::cout << "testing c vector (shouldn't have any changes):" << std::endl;
		// printVector(c, "c");
		// printVector(b, "b");
		
		//printSpMatAsDenseMat(A, "A");
		
		//printSpMatAsDenseMat(B, "B");
		// DEBUG END ---------------------------------------------
		// PRINT_PREFIX; std::cout << "iter=" << k << "  starting B_decomp.solve for cB" << std::endl;
		//B_decomp->solve(handle, true, *cB, intermediate, v1); // Should solve v1 = cB * B^⁻1, Florian
				if (profile == '3') cudaProfilerStart();
		luSolve(*solvHandle, *handle, *B, *cB, true, v1);
				if (profile == '3') cudaProfilerStop();
		// if (k == 13) {
		// 	b.print();
		// 	printSpMatAsDenseMat(A, "A");
		// 	printSpMatAsDenseMat(B, "B");
		// }
		// PRINT_PREFIX; printVector(&v1, "v1");
		// PRINT_PREFIX; std::cout << "iter=" << k << "  solved LU for cB\n" << std::endl;
		
		//PRINT_PREFIX; std::cout << "iter=" << k << "  calculateV2" << std::endl;
				if (profile == '4') cudaProfilerStart();
		calculateV2(handle, A, &v1, c, &v2); // v2 = cB * B^⁻1 * A - c, Timo
				if (profile == '4') cudaProfilerStop();
		// PRINT_PREFIX; std::cout << "iter=" << k << "  calculated v2\n" << std::endl;
		// printVector(&v2, "v2");
		// printVector(c, "c");
		//B_decomp->solve(handle, false, *b, intermediate, xB); // xB = B^-1 * b, Timo
				if (profile == '3') cudaProfilerStart();
		luSolve(*solvHandle, *handle, *B, *b, false, *xB);
				if (profile == '3') cudaProfilerStop();
		// PRINT_PREFIX; std::cout << "iter=" << k << "  solved LU for b\n" << std::endl;
		
		int j; 
		int i; 

		// check if an optimal solution has been reached
		// PRINT_PREFIX; std::cout << "iter=" << k << "  selectEnteringVar" << std::endl;
				// if( 11 < k && k < 15){
				// 	printVector(&v1, "v1");
				//  	printVector(&v2, "v2");
				// }
				if (profile == '5') cudaProfilerStart();
		optimal = !selectEnteringVar(&v1, &j, &v2,true); // blandsrule);	// if no Pivot is found solution is optimal
				if (profile == '5') cudaProfilerStop();
		// PRINT_PREFIX; std::cout << "iter=" << k << "  selected entering val\n" << std::endl;
		if (optimal) { // Tanja
			*status = 1;
			break;
		}


		// If j is non-slack: Search in j-th col of A; if j is slack: Search in j-th col of B.
				if (profile == '6') cudaProfilerStart();
		if (j < A->n) {
			// PRINT_PREFIX; std::cout << "iter=" << k << "  GetCol" << std::endl;
			getCol(handle, A, &AColJ, j);
			// PRINT_PREFIX; std::cout << "iter=" << k << "  Got jth col\n" << std::endl;
		} else if (j < A->n + A->m) {
			// PRINT_PREFIX; std::cout << "iter=" << k << "  GetCol" << std::endl;
			// getCol(handle, B, &AColJ, j-(A->n));
			unitJvector(&AColJ, j-(A->n));
			// PRINT_PREFIX; std::cout << "iter=" << k << "  Got jth col\n" << std::endl;
		} else {
			std::cout << "j = " << j << "is dog shit" << std::endl;
		}
				if (profile == '6') cudaProfilerStop();

		// PRINT_PREFIX; std::cout << "iter=" << k << "  B_decomp.solve for AColJ" << std::endl;
		//B_decomp->solve(handle, false, AColJ, intermediate, BinvA); // Col j of B^-1 * A
				// if( 11 < k && k < 15){
				// 	printSpMat(B, "B");
				// 	printSpMatAsDenseMat(B, "B");
				// }
				if (profile == '3') cudaProfilerStart();
		luSolve(*solvHandle, *handle, *B, AColJ, false, BinvA);
				if (profile == '3') cudaProfilerStop();
		// PRINT_PREFIX; std::cout << "iter=" << k << "  solved LU for AColJ\n" << std::endl;
		
		// PRINT_PREFIX; std::cout << "iter=" << k << "  selectLeavingVar" << std::endl;
				if (profile == '7') cudaProfilerStart();
		success = selectLeavingVar(&BinvA, xB, &i); // Tanja
				if (profile == '7') cudaProfilerStop();
		// PRINT_PREFIX; std::cout << "iter=" << k << "  finished selectLeavingVar\n" << std::endl;
		if (!success) {
			*status = 3;
			return false; // all entries negative -> unbounded
		}

		// Update basis
        // A: nonbasic cols
        // B: basic cols
        // reorganizes the old state to the new state, where i is leaving basis col index
        // and j is entering basis col index.
        // do we need to update cB: no it's done in WriteVectorAtIndices at loop start
		// PRINT_PREFIX; std::cout << "iter=" << k << "  UpdateBasis" << std::endl;
				if (profile == '8') cudaProfilerStart();
		updateBasis(handle, basis, (unsigned int) i, (unsigned int) j, A, B);
				if (profile == '8') cudaProfilerStop();
		// PRINT_PREFIX; std::cout << "iter=" << k << "  finished UpdateBasis\n" << std::endl;
		CHECK_CUDA(cudaDeviceSynchronize());

		if (debug) {
			std::cout << "\n\n" << std::endl;
			PRINT_FUNC_PREFIX; std::cout << "	ITERATION:  " << std::setw(6) << k << "\n" << std::endl;
			printVector(cB, "cB");
			printVector(&v1, "v1");
			printVector(&v2, "v2");
				
			printVector(xB, "xB");
			printVector(&AColJ, "AColJ");
			printVector(&BinvA, "BinvA");

			if (B->m < 10)
				printSpMatAsDenseMat(B, "B");
			else
			 	printSpMat(B, "B");
			std::cout << "\nleaving=" << i << "   entering=" << j << std::endl;
			basis->print();
		}
	}
			if (profile == '1') cudaProfilerStop();
	
	
	
	//in case you want to uncomment these: don't forget to add the lines &I = nullptr; etc.
	// delete &I;

	// delete &v1;
	// delete &v2;
	// delete &xB;
	// delete &BinvA;

    // does the "cudaFree"s
    // delete basis;

	PRINT_FUNC_PREFIX; std::cout << "\nfinished on iteration " << k << std::endl;

    // false if maximum iteration count has been reached but solution has not been found
	return optimal;
}







// // Helper for removeSlacksFrom_xb
// __global__ void fill(const DenseVector::value_t* xbVals, const int* basisVals, DenseVector::value_t* newVals, const int* size) {
	
// 	int idx = basisVals[blockIdx.x];
// 	if (idx < *size) {
// 		newVals[idx] = xbVals[blockIdx.x];
// 	}

// }

// void removeSlacksFrom_xb(const DenseVector* xb, const BasisVector* basis, DenseVector::value_t** xb_noSlacks, const int size) {

// 	int* basisVals;
// 	cusparseSpVecGetValues((basis->descr), (void **)&basisVals);
// 	DenseVector::value_t* newVals_d;
// 	cudaMalloc((void **)&newVals_d, size*sizeof(DenseVector::value_t));
// 	DenseVector::value_t *newVals = new DenseVector::value_t[size];
// 	for (unsigned i = 0; i < size; ++i) {
// 		newVals[i] = 0.0;
// 	}

// 	// Launch block for each basis vector entry
// 	int numBlocks = basis->m_;
// 	makeZeros<<<numBlocks, 1>>>(newVals_d);
// 	CHECK_CUDA(cudaDeviceSynchronize());

// 	int* size_d;
// 	cudaMalloc((void **)&size_d, sizeof(int));
// 	cudaMemcpy(size_d, &size, sizeof(int), cudaMemcpyHostToDevice);

// 	val_T* xbVals;
// 	CHECK_CUSPARSE(cusparseDnVecGetValues(xb->DnDescr, (void**)&xbVals))

// 	fill<<<numBlocks, 1>>>(xbVals, basisVals, newVals_d, size_d);
// 	CHECK_CUDA(cudaDeviceSynchronize());
// 	cudaMemcpy(newVals, newVals_d, size*sizeof(DenseVector::value_t), cudaMemcpyDeviceToHost);
// 	*xb_noSlacks = newVals;

// }
