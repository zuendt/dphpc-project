
#ifdef SHOW_FUNCTION_NAMES

#ifndef INSTRUMENT_FUCTIONS_CPP

// remove warnings of "redefined"
#ifndef _GNU_SOURCE
    #define _GNU_SOURCE
#endif

#include <dlfcn.h>

#include <stdlib.h>
#include <stdio.h>

#define TRACE_FD 2

void __cyg_profile_func_enter (void *, void *)
   __attribute__((no_instrument_function));

void __cyg_profile_func_enter (void *func,  void *caller)
{
  static FILE* trace = NULL;
  Dl_info info;

  if (trace == NULL) {
    trace = fdopen(TRACE_FD, "w");
    if (trace == NULL) abort();
    setbuf(trace, NULL);
  }
  if (dladdr(func, &info))
    if (info.dli_fname && info.dli_sname) {
        fprintf (trace, "[instrument_functions.cu] %p [%s] %s\n",
             func,
             info.dli_fname ? info.dli_fname : "?",
             info.dli_sname ? info.dli_sname : "?");
    }
}

#endif // ifndef INSTRUMENT_FUCTIONS_CPP

#define INSTRUMENT_FUCTIONS_CPP yes


#endif // SHOW_FUNCTION_NAMES
