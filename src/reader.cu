#include "simplex.cuh"
#include <glpk.h>
#include <cfloat>

template <typename T>
bool testCSCColPtr(const T* colPtr, const int cols, const int nnz) {
    for (int i=1; i<cols+1; ++i) {
        if (colPtr[i-1] > colPtr[i]){
            std::cout << "colPtr " << i << " is smaller than colPtr " << i-1 << "  (" << colPtr[i] << "<" << colPtr[i-1] << ")" << std::endl;
            return false;
        }
    }

    if (colPtr[0] != 0) {
        std::cout << "colPtr[0]=" << colPtr[0] << "  should be 0" << std::endl;
        return false;
    }

    if (colPtr[cols] != nnz) {
        std::cout << "colPtr[cols]=" << colPtr[cols] << "  should be nnz=" << nnz << std::endl; 
        return false;
    }

    return true;
}

template <typename T>
bool testCSCRowInd(const T* rowInd, const int cols, const int rows, const int nnz) {
    for (int i=0; i<nnz; ++i) {
        if (rowInd[i] < 0 || rowInd[i] >= rows) {
            std::cout << "rowInd out of bounds at " << i << " value is=" << rowInd[i] << std::endl;
            return false;
        }
    }

    return true;
}

void mps_reader(char *filename, SparseMatrix **A, DenseVector **b, DenseVector **c, const bool debug){

    //read MPS file into glpk format
    glp_prob *lp = glp_create_prob();
    bool fail = glp_read_mps(lp, GLP_MPS_FILE, NULL, filename);
    if (fail){
        PRINT_FUNC_PREFIX;
        std::cout << "glp_read_mps was unsuccessful, break process" << std::endl;
        throw "wrong";
    }
    PRINT_FUNC_PREFIX;
    std::cout << "glp_read_mps finished successfully" << std::endl;
    //get values about
    int rows = glp_get_num_rows(lp);
    int cols = glp_get_num_cols(lp);
    int nnz = glp_get_num_nz(lp);
    //PRINT_FUNC_PREFIX;
    std::cout << "get values for number of rows, cols and nnz" << std::endl;

    double c_values[2*rows + 2*cols];
    int row_idx[rows];

    //Debug
    std::cout << rows + 2*cols << ", ";

    int cnt = 0;
    int idx = 0;
    for (int i = 0; i < rows ; i++) {

        switch(glp_get_row_type(lp,i + 1)){
            case GLP_FX:
                c_values[idx] = glp_get_row_ub(lp, i + 1);
                row_idx[i] = idx;
                idx++;
                c_values[idx] = -1.0*glp_get_row_lb(lp, i + 1);
                idx++;
                break;
            case GLP_UP:
                c_values[idx] = -1.0*glp_get_row_ub(lp, i + 1);
                row_idx[i] = idx;
                idx++;
                break;
            case GLP_LO:
                c_values[idx] = glp_get_row_lb(lp, i + 1);
                row_idx[i] = idx;
                idx++;
                break;
            case GLP_FR:
                if(cnt == 0) cnt++;
                else std::cout << "Should not have more then one free row!" << std::endl;
                break;
            default:
                std::cout << "Should not happen!" << std::endl;
        }

    }



    rows = idx;

    double cscVals[2*nnz+2*cols];
    int cscColPtr[cols+1];
    int cscRowInd[2*nnz+2*cols];

    cscColPtr[0] = 0;
    int extra_rows = 0;
    //int cost_tmp = 0;
    // for glpk i + 1 (indices one-based)
    for (unsigned int i = 0; i < cols;i++) {
        double values[rows + 2] = {0.0};
        int indices[rows + 2] = {0};
        int numberValues = glp_get_mat_col(lp, i + 1, indices, values);
        int k = 0;

        for (unsigned int j = 0; j < numberValues; j++) {
            switch(glp_get_row_type(lp,indices[j + 1])){
                case GLP_FX:
                    cscRowInd[cscColPtr[i] + k] = row_idx[indices[j + 1] - 1];
                    cscVals[cscColPtr[i] + k] = values[j + 1];
                    k++;
                    cscRowInd[cscColPtr[i] + k] = row_idx[indices[j + 1] - 1] + 1;
                    cscVals[cscColPtr[i] + k] = -1.0*values[j + 1];
                    nnz++;
                    k++;
                    break;
                case GLP_UP:
                    cscRowInd[cscColPtr[i] + k] = row_idx[indices[j + 1] - 1];
                    cscVals[cscColPtr[i] + k] = -1.0*values[j + 1];
                    k++;
                    break;
                case GLP_LO:
                    cscRowInd[cscColPtr[i] + k] = row_idx[indices[j + 1] - 1];
                    cscVals[cscColPtr[i] + k] = values[j + 1];
                    k++;
                    break;
                case GLP_FR:
                    break;
                default:
                    std::cout << "Should not happen!" << std::endl;
            }
        }
        cscColPtr[i + 1] = cscColPtr[i] + k;



        //add aditional constraint from bounds
        double ub = glp_get_col_ub(lp, i + 1);
        if (ub < DBL_MAX) {
            cscVals[cscColPtr[i + 1]] = -1.0;
            cscRowInd[cscColPtr[i + 1]] = extra_rows + rows;
            cscColPtr[i + 1]++;
            c_values[extra_rows + rows] = -1.0 *ub;
            extra_rows++;
        }
        double lb = glp_get_col_lb(lp, i + 1);
        if (lb > DBL_MIN) {
            cscVals[cscColPtr[i + 1]] = 1.0;
            cscRowInd[cscColPtr[i + 1]] = extra_rows + rows;
            cscColPtr[i + 1]++;
            c_values[extra_rows + rows] = lb;
            extra_rows++;
        }
    }

    if (debug)
        printArr(c_values, idx, "C_values");

    //new rows
    rows += extra_rows;
    nnz += extra_rows;

    // transform from double to float
    std::cout << "rows=" << rows << "   cols=" << cols << std::endl;

    float tC_v[rows];

    for (int i=0; i<rows; ++i) {
        // transformedC_values[i] = (float)c_values[i];
        tC_v[i] = (float)c_values[i];
    }

    *c = new DenseVector(rows, tC_v);

    //get values for b (c in primal) if primal minimization multiply with -1 such that dual is maximise
    int b_size = cols;

    float b_value[b_size];

    float tol = 1e-9;

    if(glp_get_obj_dir(lp) == GLP_MAX){
        for (int i = 0; i < b_size; i++) {
            b_value[i] = -1.0f*(float)glp_get_obj_coef(lp, i + 1);
            if(b_value[i] > tol || b_value[i] < -tol) --nnz;
        }
    }
    else{
        for (int i = 0; i < b_size; i++) {
            b_value[i] = (float)glp_get_obj_coef(lp, i + 1);
            if(b_value[i] > tol || b_value[i] < -tol) --nnz;
        }
    }

    // transform from double to float
    float transformedCscVals[nnz];

    for (int i=0; i<nnz; ++i) {
        transformedCscVals[i] = (float)cscVals[i];
    }

    if (!testCSCColPtr(cscColPtr, cols, nnz)){
        std::cout << "colPtr is wrong" << std::endl;
        throw "wrong";
    }

    if (!testCSCRowInd(cscRowInd, cols, rows, nnz)) {
        std::cout << "rowInd is wrong" << std::endl;
        throw "wrong";
    }
        

    *A = new SparseMatrix(cols,rows,nnz,cscColPtr, cscRowInd, transformedCscVals);

    if (debug) {
        if ((*A)->m < 20 && (*A)->n < 10)
            printSpMatAsDenseMat(*A, "A");
        //printSpMat(*A);
    }

    *b = new DenseVector(b_size, b_value);

    glp_delete_prob(lp);

}
