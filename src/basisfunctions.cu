#ifndef BASISFUNCTIONS
#define BASISFUNCTIONS

#include "simplex.cuh"

#include <iostream>
#include <cusparse.h>
#include <fstream>
#include <iomanip>

#define useThreadsPerBlock 1024

// prints a CSC Matrix defined by the arrays and its name
// it prints this CSC Matrix in dense format
void printSpMatAsDenseMatFromArr(const int* colPtrs, const int* rowInds, const val_T* vals, int rows, int cols, std::string name) {
    std::cout << "rows     :  ";
    for (unsigned int i=0; i<rows; ++i) {
        std::cout << std::setw(5) << i << "  ";
    }

    unsigned int nextNNZ_off = 0;
    unsigned int nextRowVal = rowInds[nextNNZ_off];

    for (unsigned int i=0; i<cols; ++i) {
        std::cout << "\ncol=" << std::setw(4) << i << " :   ";

        for (unsigned int j=0; j < rows; ++j) {
            if (j == nextRowVal && nextNNZ_off < colPtrs[i+1]) {
                std::cout << std::setw(5) << vals[nextNNZ_off] << ", ";
                ++nextNNZ_off;
                nextRowVal = rowInds[nextNNZ_off];
            } else {
                std::cout << std::setw(5) << 0.0 << ", ";
            }
        }
    }

    std::cout << "\n nnz=" << colPtrs[cols] << "   printed nnz=" << nextNNZ_off-1 << std::endl;
}


// --------------------------------------------------------------------------
//                          GET COLUMN FUNCTIONS
// --------------------------------------------------------------------------


// Goes through a Csr matrix and gets the colIdx column
// saves the result into extractCol
// Csr matrix is defined by rowPtr, colInd, values and number of rows
__global__ void getCol_Kernel(val_T* extractedCol, const int* rowPtr, const int* colInd, const val_T* values, const int rows, const int colIdx) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < rows) {
        int start = rowPtr[i];
        int end = rowPtr[i+1];

        extractedCol[i] = 0.f;

        for (unsigned int j=start; j<end; ++j) {
            if (colInd[j] == colIdx) {
                extractedCol[i] = values[j];
                break;
            } 
        }
    } 
}

// Gets the i-th column of A and overwrites the output vector with this column
void getCol(cusparseHandle_t *handle, const SparseMatrix* A,
    DenseVector* output, const unsigned int i) {

    // take column i of A and write into ds_output->
    // PRINT_FUNC_PREFIX; std::cout << "Taking column i=" << i << std::endl;

    if (A->m != output->size) {
        throw std::logic_error("Dimension mismatch");
    }
    if (A->n <= i) {
        throw std::logic_error("index i="+
                std::to_string(i)+" out of range");
    }

    // Get the csr data
    int64_t h_n, h_m, h_nnz;
    int* d_rowPtr;
    int* d_colInd;
    val_T* d_vals;

    cusparseIndexType_t h_rowPtrType, h_colIndType;
    cusparseIndexBase_t h_idxBase;
    cudaDataType h_valueType;

    CHECK_CUSPARSE(cusparseCsrGet(A->spDescr,
                                  &h_m,
                                  &h_n,
                                  &h_nnz,
                                  (void**)&d_rowPtr,
                                  (void**)&d_colInd,
                                  (void**)&d_vals,
                                  &h_rowPtrType,
                                  &h_colIndType,
                                  &h_idxBase,
                                  &h_valueType))
    
    val_T* d_outputVals;                // empty array in which the values are copied
    CHECK_CUDA(cudaMalloc((void**)&d_outputVals, h_m * sizeof(val_T)))

    int gridDim = 1;
    int blockDim = h_m;
    if (blockDim > useThreadsPerBlock) {
        gridDim = std::ceil(((float)blockDim) / useThreadsPerBlock);
        blockDim = useThreadsPerBlock;
    }
    getCol_Kernel<<<gridDim, blockDim>>>(d_outputVals, d_rowPtr, d_colInd, d_vals, h_m, i);
    CHECK_CUDA(cudaDeviceSynchronize())

    //update the vector
    CHECK_CUSPARSE(cusparseDnVecSetValues(output->DnDescr, (void *)d_outputVals))

    CHECK_CUDA(cudaDeviceSynchronize())

    CHECK_CUDA(cudaFree(output->values))
    output->values = d_outputVals;
}



// --------------------------------------------------------------------------
//                      WRITE VECTOR AT INDICES	 FUNCTION
// --------------------------------------------------------------------------



// overwrites a[i] with b[i] if i exists in idxs
// size is size of the idxs array
// __global__ void overwriteAtIdxs_Kernel(val_T* a, const val_T* b, const int* idxs, const int* numCols) {
//     int i = blockIdx.x * blockDim.x + threadIdx.x;
//     // if (i < size) {
//     //     int j = idxs[i];
//     //     a[j] = b[j];
//     // }
//     int j = idxs[i];
//     if (j < *numCols) {
//         a[i] = b[j];
//     } else {
//         a[i] = 0;
//     }

// }

// // overwrites ds_Y with values from ds_A based on basis idxs (if idx does not exist in basis value at that idx is not overwritten)
// void writeVectorAtIndices(cusparseHandle_t *handle, BasisVector* basis,
//         DenseVector* ds_Y, const DenseVector* ds_A, const bool debug) {

//     if (debug)
//         basis->print();

//     // get Sp vector data
//     int64_t             size;
//     int64_t             nnz;
//     int*                indices;
//     int*                values;
//     cusparseIndexType_t idxType;
//     cusparseIndexBase_t idxBase;
//     cudaDataType        valueType;

//     CHECK_CUSPARSE(cusparseSpVecGet(basis->descr,
//                         &size,
//                         &nnz,
//                         (void**)&indices,
//                         (void**)&values,
//                         &idxType,
//                         &idxBase,
//                         &valueType))

//     // val_T* ds_YVal;             // values of ds_Y
//     // CHECK_CUDA(cudaMalloc(((void**)&ds_YVal), ds_Y->size * sizeof(DenseVector::value_t)))
//     // CHECK_CUSPARSE(cusparseDnVecGetValues(ds_Y->DnDescr, (void **)&ds_YVal))

//     // val_T* ds_AVal;             // values of ds_A
//     // CHECK_CUDA(cudaMalloc(((void**)&ds_AVal), ds_A->size * sizeof(DenseVector::value_t)))
//     // CHECK_CUSPARSE(cusparseDnVecGetValues(ds_A->DnDescr, (void **)&ds_AVal))

//     // Get the number of non-slack variables
//     int numCols = (int)ds_A->size;
//     int* numCols_d;
//     cudaMalloc((void **)&numCols_d, sizeof(int));
//     cudaMemcpy((void*)numCols_d, (void*)&numCols, sizeof(int), cudaMemcpyHostToDevice);
//     if (debug)
//         std::cout << "copied numCols" << std::endl;

//     // overwriteAtIdxs_Kernel<<<1, size>>>(ds_YVal, ds_AVal, values, numCols_d);
//     int gridDim = 1;
//     int blockDim = numCols;
//     if (blockDim > useThreadsPerBlock) {
//         gridDim = std::ceil(((float)blockDim) / useThreadsPerBlock);
//         blockDim = useThreadsPerBlock;
//     }
//     overwriteAtIdxs_Kernel<<<gridDim, blockDim>>>(ds_Y->values, ds_A->values, values, numCols_d);
//     CHECK_CUDA(cudaDeviceSynchronize())

//     //update the vector

//     // printArrDevice(ds_Y->values, ds_Y->size, "ds_Y values   BEFORE   cusparseDnVecSetValues");

//     // CHECK_CUSPARSE(cusparseDnVecSetValues(ds_Y->DnDescr, (void *)ds_YVal))

//     // printArrDevice(ds_Y->values, ds_Y->size, "ds_Y values   AFTER   cusparseDnVecSetValues");

//     // CHECK_CUDA(cudaFree(ds_Y->values))
//     // ds_Y->values = nullptr;

//     // ds_Y->values = ds_YVal;

//     CHECK_CUDA(cudaFree(numCols_d))
// }

__global__ void overwriteAtIdxs_Kernel(val_T* dest, const val_T* src, const int* idxs, const int size, const int numCols) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < size) {
        int j = idxs[i];
        if (j < numCols)
            dest[i] = src[j];
        else
            dest[i] = 0.0;
    }
}

void writeVectorAtIndices(cusparseHandle_t *handle, const BasisVector* basis,
                              DenseVector* cB, const DenseVector* c, const bool debug) {

    // if (debug)
    //     basis->print();

    int64_t             size;
    int64_t             nnz;
    int*                indices;
    int*                values;
    cusparseIndexType_t idxType;
    cusparseIndexBase_t idxBase;
    cudaDataType        valueType;

    CHECK_CUSPARSE(cusparseSpVecGet(basis->descr,
                        &size,
                        &nnz,
                        (void**)&indices,
                        (void**)&values,
                        &idxType,
                        &idxBase,
                        &valueType))

    if (debug)
        std::cout << "copied numCols" << std::endl;

    int gridDim = 1;
    int blockDim = cB->size;
    if (blockDim > useThreadsPerBlock) {
        gridDim = std::ceil(((float)blockDim) / useThreadsPerBlock);
        blockDim = useThreadsPerBlock;
    }
    
    overwriteAtIdxs_Kernel<<<gridDim, blockDim>>>(cB->values, c->values, values, cB->size, c->size);
    CHECK_CUDA(cudaDeviceSynchronize())
}



// --------------------------------------------------------------------------
//                          UPDATE BASIS FUNCTIONS
// --------------------------------------------------------------------------


//                      ------------ CPU ---------------


void removeAndInsertColCPU(const int* d_cscColPtr, const int* d_cscRowInd, const val_T* d_cscVal,
                                   const int* d_val_idxs_In, const val_T* d_val_In,
                                   int* d_cscRowInd_new, val_T* d_cscVal_new,
                                   const int colIdx_Out, const int colIdx_In, const int nnz_new,
                                   const int size_Out, const int size_In) {
    // PRINT_FUNC_PREFIX; std::cout << "removing column at idx=" << colIdx_Out << "inserting column at idx=" << colIdx_In << std::endl;
    // PRINT_FUNC_PREFIX; std::cout << "removing size=" << size_Out << "inserting size=" << size_In << std::endl;

    // new arrays
    int cscRowInd_new[nnz_new];
    val_T cscVal_new[nnz_new];

    // int nnzIdx_In;
    int nnzIdx_Out;
    // CHECK_CUDA(cudaMemcpy(&nnzIdx_In, d_cscColPtr + colIdx_In, sizeof(int), cudaMemcpyDeviceToHost))
    CHECK_CUDA(cudaMemcpy(&nnzIdx_Out, d_cscColPtr + colIdx_Out, sizeof(int), cudaMemcpyDeviceToHost))

    int nnz_old = nnz_new - size_In + size_Out;
    int cscRowInd[nnz_old];
    val_T cscVal[nnz_old];

    CHECK_CUDA(cudaMemcpy(cscRowInd, d_cscRowInd, nnz_old * sizeof(int), cudaMemcpyDeviceToHost))
    CHECK_CUDA(cudaMemcpy(cscVal, d_cscVal, nnz_old * sizeof(val_T), cudaMemcpyDeviceToHost))

    int val_idxs_In[size_In];
    val_T val_In[size_In];

    CHECK_CUDA(cudaMemcpy(val_idxs_In, d_val_idxs_In, size_In * sizeof(int), cudaMemcpyDeviceToHost))
    CHECK_CUDA(cudaMemcpy(val_In, d_val_In, size_In * sizeof(val_T), cudaMemcpyDeviceToHost))

    for (unsigned int i=0; i<nnz_new; ++i) {
        if (i < nnz_new) {

            if (i < nnzIdx_Out || i >= nnzIdx_Out + size_In) {
            
                int off = 0;
                // if (i >= nnzIdx_Out && i < nnzIdx_In) {
                //     off = size_Out;
                // } else if (i > nnzIdx_In && i <= nnzIdx_Out) {
                //     off = -size_In;
                // } else if (i > nnzIdx_Out || i > nnzIdx_In) {
                //     off = size_Out - size_In;
                // }
                if (i > nnzIdx_Out) {
                    off = size_Out - size_In;
                }
                
                cscRowInd_new[i] = cscRowInd[i + off];
                cscVal_new[i] = cscVal[i + off];

                // std::cout << "idx=" << i << " off=" << off << "     val is=" << cscVal_new[i] << std::endl;
            } else {
                cscRowInd_new[i] = val_idxs_In[i - nnzIdx_Out];
                cscVal_new[i] = val_In[i - nnzIdx_Out];
                // std::cout << "inserting at=" << i << "       val is=" << cscVal_new[i] <<std::endl;
            }
        }
    }

    // printArr(cscRowInd, nnz_old, "cscRowInd");
    // printArr(cscRowInd_new, nnz_new, "cscRowInd_new");

    // std::cout << std::endl;

    // printArr(cscVal, nnz_old, "cscVal");
    // printArr(cscVal_new, nnz_new, "cscVal_new");

    // std::cout << "\n\n\nKERNEL CPU 1 HAS FINISHED\n\n" << std::endl;
}

void adjustCscColPtrCPU(const int* d_cscColPtr, int* d_cscColPtr_new,
                                const int colIdx_Out, const int colIdx_In, const int size,
                                const int size_Out, const int size_In) {

    int cscColPtr[size+1];
    int cscColPtr_new[size+1];

    CHECK_CUDA(cudaMemcpy(cscColPtr, d_cscColPtr, (size + 1) * sizeof(int), cudaMemcpyDeviceToHost))

    for (unsigned int i=0; i < size + 1; ++i) {
        if (i < size + 1) {
            int off = 0;
            // int roff = 0;
            // if (i >= colIdx_Out + 1 && i < colIdx_In + 1) {
            //     off = -size_Out;
            //     roff = 1;
            // } else if (i >= colIdx_In + 1 && i < colIdx_Out + 1) {
            //     off = size_In;
            //     roff = -1;
            // } else if (i >= colIdx_Out + 1 && i >= colIdx_In + 1) {
            //     off = -size_Out + size_In;
            // }
            
            // cscColPtr_new[i] = cscColPtr[i + roff] + off;

            if (i >= colIdx_Out + 1) {
                off = -size_Out + size_In;
            }
            
            cscColPtr_new[i] = cscColPtr[i] + off;

            // if (i == colIdx_Out) std::cout << "out  ";
            // else if (i == colIdx_In) std::cout << "in  ";
            // std::cout << "idx=" << i << " off=" << off << "     col offset is=" << cscColPtr_new[i] << std::endl;
        }
    }

    // printArr(cscColPtr, size+1, "cscColPtr");
    // printArr(cscColPtr_new, size+1, "cscColPtr_new");

    // std::cout << "\n\n\nKERNEL CPU 2 HAS FINISHED\n\n" << std::endl;
}



//              ----------- GPU -----------


__global__ void removeAndInsertCol_Kernel(const int* cscColInd, const int* cscRowInd, const val_T* cscVal,
                                         const int* val_idxs_In, const val_T* val_In,
                                         int* cscRowInd_new, val_T* cscVal_new,
                                         const int colIdx_Out, const int colIdx_In, const int nnz_new,
                                         const int size_Out, const int size_In) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < nnz_new) {
        // int nnzIdx_In = cscColInd[colIdx_In];
        int nnzIdx_Out = cscColInd[colIdx_Out];

        if (i < nnzIdx_Out || i >= nnzIdx_Out + size_In) {
        
            int off = 0;
            // if (i >= nnzIdx_Out && i < nnzIdx_In) {
            //     off = size_Out;
            // } else if (i > nnzIdx_In && i <= nnzIdx_Out) {
            //     off = -size_In;
            // } else if (i > nnzIdx_Out || i > nnzIdx_In) {
            //     off = size_Out - size_In;
            // }
            if (i >= nnzIdx_Out) {
                off = size_Out - size_In;
            }
            
            cscRowInd_new[i] = cscRowInd[i + off];
            cscVal_new[i] = cscVal[i + off];

        } else {
            cscRowInd_new[i] = val_idxs_In[i - nnzIdx_Out];
            cscVal_new[i] = val_In[i - nnzIdx_Out];
        }
    }
}

__global__ void adjustCscColPtr_Kernel(const int* cscColPtr, int* cscColPtr_new,
                                       const int colIdx_Out, const int colIdx_In, const int size,
                                       const int size_Out, const int size_In) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < size) {
        int off = 0;
        // int roff = 0;
        // if (i >= colIdx_Out + 1 && i < colIdx_In + 1) {
        //     off = -size_Out;
        //     roff = 1;
        // } else if (i >= colIdx_In + 1 && i < colIdx_Out + 1) {
        //     off = size_In;
        //     roff = -1;
        // } else if (i >= colIdx_Out + 1 && i >= colIdx_In + 1) {
        //     off = -size_Out + size_In;
        // }
        
        // cscColPtr_new[i] = cscColPtr[i + roff] + off;

        if (i >= colIdx_Out + 1) {
            off = -size_Out + size_In;
        }

        cscColPtr_new[i] = cscColPtr[i] + off;
    }
}

__global__ void adjustBasisVec_Kernel(const int* basisVal, int* basisVal_new,
                                      const int colIdx_Out, const int colIdx_In, const int size) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < size) {
        int off = 0;
        int idx = basisVal[i];

        if ((i==0 && colIdx_In < idx) ||
            (colIdx_In < idx && colIdx_In > basisVal[i-1])) {
            basisVal_new[i] = colIdx_In;
        } else {
            if (idx >= colIdx_Out && idx < colIdx_In) {
                off = 1;
            } else if (i > colIdx_In && i <= colIdx_Out) {
                off = -1;
            }

            basisVal_new[i] = basisVal[i + off];
        }
    }
}

__global__ void adjustBasisVec_Kernel_new(int* basisVal_new, const int* basisVal,
                                      const int colIdx_Out, const int colIdx_In, const int size) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < size) {
        if (i == colIdx_Out) {
            basisVal_new[colIdx_Out] = colIdx_In;
        } else {
            basisVal_new[i] = basisVal[i];
        }
    }
}

void denseVecToSparseVec(const val_T* d_denseVals, val_T* h_spVals, int* h_spIdx, int h_denseSize, int *h_spSize) {
    val_T h_TOL = (val_T)10e-4;

    val_T h_denseVals[h_denseSize];
    CHECK_CUDA(cudaMemcpy(h_denseVals, d_denseVals, h_denseSize * sizeof(val_T), cudaMemcpyDeviceToHost))

    int c = 0;
    for (unsigned int i=0; i < h_denseSize; ++i) {
        if (h_denseVals[i] >= h_TOL || h_denseVals[i] <= -h_TOL) {
            h_spVals[c] = h_denseVals[i];
            h_spIdx[c] = i;
            ++c;
        }
    }
    *h_spSize = c;
}


// converts the CSR B into CSC Format to then remove column i and copy in Column j from the Matrix A
// also removes i from the basis and enters j
void updateBasis(cusparseHandle_t *handle, BasisVector* basis, unsigned const int colIdx_Out,
        unsigned const int colIdx_In, const SparseMatrix* A, SparseMatrix* B, const bool debug) {

    // Get basis vector data
    int* d_basisVal;

    CHECK_CUSPARSE(cusparseSpVecGetValues(basis->descr,
                                          (void **)&d_basisVal))
    
    // update basis vector
    int* d_basisVal_new;
    CHECK_CUDA(cudaMalloc((void **)&d_basisVal_new, basis->m_ * sizeof(int)))
    // adjustBasisVec_Kernel<<<1, basis->m_>>>(d_basisVal, d_basisVal_new,
    //                                                           colIdx_Out, colIdx_In, basis->m_);
    int gridDim = 1;
    int blockDim = basis->m_;
    if (blockDim > useThreadsPerBlock) {
        gridDim = std::ceil(((float)blockDim) / useThreadsPerBlock);
        blockDim = useThreadsPerBlock;
    }
    adjustBasisVec_Kernel_new<<<gridDim, blockDim>>>(d_basisVal_new, d_basisVal, colIdx_Out, colIdx_In, basis->m_);

    // Get the csr data from B
    int64_t h_n, h_m, h_nnz;
    int* d_csrRowPtr;
    int* d_csrColInd;
    val_T* d_csrVal;

    cusparseIndexType_t h_rowPtrType, h_colIndType;
    cusparseIndexBase_t h_idxBase;
    cudaDataType h_valueType;

    CHECK_CUSPARSE(cusparseCsrGet(B->spDescr,
                                  &h_m,
                                  &h_n,
                                  &h_nnz,
                                  (void**)&d_csrRowPtr,
                                  (void**)&d_csrColInd,
                                  (void**)&d_csrVal,
                                  &h_rowPtrType,
                                  &h_colIndType,
                                  &h_idxBase,
                                  &h_valueType))

    //transform B into CSC
    int* d_cscColPtr;
    int* d_cscRowInd;
    val_T* d_cscVal;

    CHECK_CUDA(cudaMalloc((void**)&d_cscColPtr, (h_n + 1) * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_cscRowInd, h_nnz     * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_cscVal,    h_nnz     * sizeof(val_T)))
    if (debug) {
        std::cout << "\n Allocated for CSC:\n" << 
                            "cscColPtr=" << (h_n + 1) * sizeof(int) << 
                            "cscRowInd=" << h_nnz     * sizeof(int) << 
                            "cscVal="    << h_nnz     * sizeof(val_T) << "\n" << std::endl; 
    }
    
    // Buffer info
    size_t pBufferSize;
    void* pBuffer = NULL;

    cusparseCsr2CscAlg_t algo{CUSPARSE_CSR2CSC_ALG1};
    
    CHECK_CUSPARSE(cusparseCsr2cscEx2_bufferSize(*handle,
                                                 h_m,
                                                 h_n,
                                                 h_nnz,
                                                 d_csrVal,
                                                 d_csrRowPtr,
                                                 d_csrColInd,
                                                 d_cscVal,
                                                 d_cscColPtr,
                                                 d_cscRowInd,
                                                 h_valueType,
                                                 CUSPARSE_ACTION_NUMERIC,
                                                 h_idxBase,
                                                 algo,
                                                 &pBufferSize))
    
    // Allocate pBuffer
    CHECK_CUDA(cudaMalloc((void**)&pBuffer, pBufferSize))
    if (debug) {
        PRINT_FUNC_PREFIX; std::cout << "\n pBufferSize: " << pBufferSize << std::endl;
    }
    
    CHECK_CUSPARSE(cusparseCsr2cscEx2(*handle,
                                    h_m,
                                    h_n,
                                    h_nnz,
                                    d_csrVal,
                                    d_csrRowPtr,
                                    d_csrColInd,
                                    d_cscVal,
                                    d_cscColPtr,
                                    d_cscRowInd,
                                    h_valueType,
                                    CUSPARSE_ACTION_NUMERIC,
                                    h_idxBase,
                                    algo,
                                    pBuffer))

    CHECK_CUDA(cudaDeviceSynchronize())

    CHECK_CUDA(cudaFree(pBuffer))

    // get the colIdx_In column from A and transform the column to a sparse vector
    val_T vals[h_m] = {0.f};
    DenseVector output(h_m, vals);
    if (colIdx_In < A->n) {
        getCol(handle, A, &output, colIdx_In);
    } else {
        unitJvector(&output, colIdx_In - A->n);
    }

    val_T* d_denseVals;
    CHECK_CUSPARSE(cusparseDnVecGetValues(output.DnDescr, (void **)&d_denseVals))

    val_T h_spVals[(int)h_m];    
    int h_spIdx[(int)h_m];    

    int h_size_In;
    denseVecToSparseVec(d_denseVals, h_spVals, h_spIdx, h_m, &h_size_In);

    // sadly the current implementation of denseVecToSparseVec is on host, so we have to copy to gpu
    val_T* d_spVals;    
    int* d_spIdx; 
    CHECK_CUDA(cudaMalloc((void**)&d_spVals, h_size_In * sizeof(val_T)))
    CHECK_CUDA(cudaMalloc((void**)&d_spIdx, h_size_In * sizeof(int)))

    CHECK_CUDA(cudaMemcpy(d_spVals, h_spVals,  h_size_In * sizeof(val_T), cudaMemcpyHostToDevice))
    CHECK_CUDA(cudaMemcpy(d_spIdx, h_spIdx,  h_size_In * sizeof(int), cudaMemcpyHostToDevice))

    int h_size_arr[2];
    CHECK_CUDA(cudaMemcpy(h_size_arr, d_cscColPtr + colIdx_Out, 2 * sizeof(int), cudaMemcpyDeviceToHost))
    int h_size_Out = h_size_arr[1] - h_size_arr[0];

    int h_nnz_new = h_nnz + h_size_In - h_size_Out;


    // create space for our new updated basis, not really memoryefficient :(
    int* d_cscColPtr_new;
    int* d_cscRowInd_new;
    val_T* d_cscVal_new;
    CHECK_CUDA(cudaMalloc((void**)&d_cscColPtr_new, (h_n + 1)  * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_cscRowInd_new, h_nnz_new * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_cscVal_new, h_nnz_new * sizeof(val_T)))

            // int h_cscColPtr[h_n + 1];
            // int h_cscRowInd[h_nnz_new];
            // val_T h_cscVal[h_nnz_new];

            // this is a cpu version used to validate that the kernel is correct, unfortunately this doesn't deliver the right result...
            // removeAndInsertColCPU(d_cscColPtr, d_cscRowInd, d_cscVal,
            //                         d_spIdx, d_spVals,
            //                         d_cscRowInd_new, d_cscVal_new,
            //                         colIdx_Out, colIdx_In, h_nnz_new,
            //                         h_size_Out, h_size_In);

    gridDim = 1;
    blockDim = h_nnz_new;
    if (blockDim > useThreadsPerBlock) {
        gridDim = std::ceil(((float)blockDim) / useThreadsPerBlock);
        blockDim = useThreadsPerBlock;
    }

    if (blockDim < 1 || blockDim > 1000 && gridDim < 1 || gridDim > 500) 
        std::cout << "removeAndInsertCol gridDim=" << gridDim << "  blockDim=" << blockDim << "   nnz_new=" << h_nnz_new << std::endl;

    // the actual gpu kernel
    if (blockDim>0 && gridDim>0) {
        removeAndInsertCol_Kernel<<<gridDim, blockDim>>>(d_cscColPtr, d_cscRowInd, d_cscVal,
                                            d_spIdx, d_spVals,
                                            d_cscRowInd_new, d_cscVal_new,
                                            colIdx_Out, colIdx_In, h_nnz_new,
                                            h_size_Out, h_size_In);
    }
    
            // // only when we debug, prints result of the gpu kernel
            // // this must be the same as the result obtained by the cpu version
            CHECK_CUDA(cudaDeviceSynchronize())

            // CHECK_CUDA(cudaMemcpy(h_cscRowInd, d_cscRowInd_new, h_nnz_new * sizeof(int), cudaMemcpyDeviceToHost))
            // CHECK_CUDA(cudaMemcpy(h_cscVal, d_cscVal_new, h_nnz_new * sizeof(val_T), cudaMemcpyDeviceToHost))

            // printArr(h_cscRowInd, h_nnz_new, "new cscRowInd after Kernel 1");
            // printArr(h_cscVal, h_nnz_new, "new cscVal after Kernel 1");
            // std::cout << std::endl;


            // // this is a cpu version used to validate that the kernel is correct
            // adjustCscColPtrCPU(d_cscColPtr, d_cscColPtr_new,
            //                                 colIdx_Out, colIdx_In, h_n,
            //                                 h_size_Out, h_size_In);
    
    gridDim = 1;
    blockDim = h_n + 1;
    if (blockDim > useThreadsPerBlock) {
        gridDim = std::ceil(((float)blockDim) / useThreadsPerBlock);
        blockDim = useThreadsPerBlock;
    }
    // the actual gpu kernel
    adjustCscColPtr_Kernel<<<gridDim, blockDim>>>(d_cscColPtr, d_cscColPtr_new,
                                    colIdx_Out, colIdx_In, h_n + 1,
                                    h_size_Out, h_size_In);

    CHECK_CUDA(cudaDeviceSynchronize())     // this is the ONLY synchronize that is necessary in this code!

            // // only when we debug, prints result of the gpu kernel
            // // this must be the same as the result obtained by the cpu version
            // CHECK_CUDA(cudaMemcpy(h_cscColPtr, d_cscColPtr_new, (h_n + 1) * sizeof(int), cudaMemcpyDeviceToHost))

            // printArr(h_cscColPtr, h_n + 1, "new cscColPtr after Kernel 2");
            // std::cout << std::endl;

            // printSpMatAsDenseMatFromArr(h_cscColPtr, h_cscRowInd, h_cscVal, h_n, h_m, "TEST MATRIX");
            // std::cout << std::endl;

    //CSR new data
    int* d_csrRowPtr_new;
    int* d_csrColInd_new;
    val_T* d_csrVal_new;

    CHECK_CUDA(cudaMalloc((void**)&d_csrRowPtr_new, (h_m + 1) * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_csrColInd_new, h_nnz_new     * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&d_csrVal_new,    h_nnz_new     * sizeof(val_T)))

    // Buffer info
    pBufferSize = 0;
    pBuffer = NULL;
    
    CHECK_CUSPARSE(cusparseCsr2cscEx2_bufferSize(*handle,
                                                 h_n,
                                                 h_m,
                                                 h_nnz_new,
                                                 d_cscVal_new,
                                                 d_cscColPtr_new,
                                                 d_cscRowInd_new,
                                                 d_csrVal_new,
                                                 d_csrRowPtr_new,
                                                 d_csrColInd_new,
                                                 h_valueType,
                                                 CUSPARSE_ACTION_NUMERIC,
                                                 h_idxBase,
                                                 algo,
                                                 &pBufferSize))
    
    // Allocate pBuffer
    CHECK_CUDA(cudaMalloc((void**)&pBuffer, pBufferSize))
            if (debug) {
                PRINT_FUNC_PREFIX; std::cout << "\n pBufferSize: " << pBufferSize << std::endl;
                // std::cout << "\n n: " << h_n << std::endl;
                // std::cout << "\n m: " << h_m << std::endl;
            }
    
    CHECK_CUSPARSE(cusparseCsr2cscEx2(*handle,
                                        h_n,
                                        h_m,
                                        h_nnz_new,
                                        d_cscVal_new,
                                        d_cscColPtr_new,
                                        d_cscRowInd_new,
                                        d_csrVal_new,
                                        d_csrRowPtr_new,
                                        d_csrColInd_new,
                                        h_valueType,
                                        CUSPARSE_ACTION_NUMERIC,
                                        h_idxBase,
                                        algo,
                                        pBuffer))
    
    CHECK_CUDA(cudaDeviceSynchronize())

    CHECK_CUDA(cudaFree(pBuffer))
    pBuffer = NULL;

    CHECK_CUDA(cudaFree(d_cscColPtr))
    CHECK_CUDA(cudaFree(d_cscRowInd))
    CHECK_CUDA(cudaFree(d_cscVal))
    d_cscColPtr = NULL;
    d_cscRowInd = NULL;
    d_cscVal = NULL;

    CHECK_CUDA(cudaFree(d_cscColPtr_new))
    CHECK_CUDA(cudaFree(d_cscRowInd_new))
    CHECK_CUDA(cudaFree(d_cscVal_new))
    d_cscColPtr_new = NULL;
    d_cscRowInd_new = NULL;
    d_cscVal_new = NULL;

    CHECK_CUDA(cudaFree(d_spVals))
    CHECK_CUDA(cudaFree(d_spIdx))
    d_spVals = NULL;
    d_spIdx = NULL;

    // set B to the result of our calculations
    // as there is no guarantee that the inserted vector has the same amount of nnz as the old one we need to create a new SpMat
    // mabey we could use a slightly to large sparseMatrix to circumvent this issue
    // For this we would need:
    //      - A check for if the dimension actually got larger would be necessary
    //      - Seperatly safe the true amount of nnz in our class as the one returned by
    //        cusparseSpMatGet would be wrong!
    CHECK_CUSPARSE(cusparseDestroySpMat(B->spDescr))
    B->spDescr = NULL;

    CHECK_CUDA(cudaFree(d_csrRowPtr))
    CHECK_CUDA(cudaFree(d_csrColInd))
    CHECK_CUDA(cudaFree(d_csrVal))
    
    d_csrRowPtr = NULL;
    d_csrColInd = NULL;
    d_csrVal = NULL;

    CHECK_CUSPARSE(cusparseCreateCsr(&B->spDescr, h_m, h_n, h_nnz_new, 
                                    d_csrRowPtr_new, d_csrColInd_new, d_csrVal_new,
                                    CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I,
                                    CUSPARSE_INDEX_BASE_ZERO, CUDA_R_32F))

    B->nnz = h_nnz_new;
    B->csrRowPtr = d_csrRowPtr_new;
    B->csrColInd = d_csrColInd_new;
    B->csrVal = d_csrVal_new;

    // set basis vector to the new values
    CHECK_CUSPARSE(cusparseSpVecSetValues(basis->descr,
                                          d_basisVal_new))
    
    CHECK_CUDA(cudaFree(d_basisVal))
    d_basisVal = NULL;
}


#endif
