
#ifndef FELIX_VERSION
#define FELIX_VERSION


#include "simplex.cuh"
#include <thrust/sort.h>

void dense_vector_print(const DenseVector c) {
    val_T *values;
    CHECK_CUSPARSE(cusparseDnVecGetValues(c.DnDescr,
                    (void **) &values));

    //copy from DEVICE to HOST
    const thrust::device_ptr<val_T> values_thr=thrust::device_pointer_cast(values);
    PRINT_PREFIX;
    std::cout<<"values="<<values<<" ";
    thrust::copy(values_thr,values_thr+c.size, std::ostream_iterator<val_T>(std::cout, ", "));
    std::cout << std::endl;
}


void BasisVector::print() {
    assert(this->values_vec.size() == this->indices_vec.size());
    unsigned int *indices;
    unsigned int *values;
    CHECK_CUSPARSE(cusparseSpVecGet(this->descr,
                    (int64_t *) &(this->m_),
                    (int64_t *) &(this->m_),
                    (void **) &indices, (void **) &values,
                    &this->idx_t,&this->i_b_t,&this->dat_t));

    //copy from DEVICE to HOST
    const thrust::device_ptr<unsigned int> indices_thr=thrust::device_pointer_cast(indices);
    PRINT_FUNC_PREFIX;
    std::cout<<"indices="<<indices<<" ";
    thrust::copy(indices_thr,indices_thr+this->m_, std::ostream_iterator<int>(std::cout, ", "));
    std::cout << std::endl;

    const thrust::device_ptr<unsigned int> values_thr=thrust::device_pointer_cast(values);
    PRINT_PREFIX;
    std::cout<<"values="<<values<<" ";
    thrust::copy(values_thr,values_thr+this->m_, std::ostream_iterator<int>(std::cout, ", "));
    std::cout << std::endl;
}

// inspired from https://github.com/NVIDIA/thrust/blob/master/examples/sparse_vector.cu
void BasisVector::print_vecs() {
    assert(this->values_vec.size() == this->indices_vec.size());

    PRINT_FUNC_PREFIX;
    std::cout<<"WARNING: it is possible cusparse is not updating the";
    std::cout<<" thrust internal pointers. Please check these values";
    std::cout<<" are actually changing after writes: == this.print()!"<<std::endl;

    for(size_t i = 0; i < this->indices_vec.size(); i++)
        std::cout << "(" << this->indices_vec[i] << "," << this->values_vec[i] << ") ";
    std::cout << std::endl;
}

void BasisVector::mat_one_extract_set_position(unsigned int row,unsigned int col) {
    /*// a big sparse matrix with only one 1 value, everything else 0
    thrust::device_vector<val_T> mat_one_values;
    thrust::device_vector<unsigned int> mat_one_row_ofs;
    thrust::device_vector<unsigned int> mat_one_col_ind;
    cusparseSpMatDescr_t mat_one;*/
    // set the row_ofs and col_ind pointers to "move" the 1 value to row and col indices.
    //helper_set_row_col_ptrs<<<1,this->m_>>>(
            //this->mat_one_row_ofs,this->mat_one_col_ind,
            //row,col,(val_T) this->m_);
}














// /*
// __device__ void helper_set_row_col_ptrs(val_T *mat_one_row_ofs,val_T *mat_one_col_ind,
//         unsigned int row,unsigned int col,val_T n) {
    
//     * On the mat_one sparse matrix (in CSR fmt), set col_ind and row_ofs
//     * pointers so that the single nnz 1.0 value ends up at position row,col.
    
//     mat_one_col_ind[0]=col;
//     unsigned int k=threadIdx.x+blockIdx.x*blockDim.x;
//     mat_one_row_ofs[k]=(k<=row)? 0.0:n;
// }

// void UpdateVectorFromBasis(cusparseHandle_t *handle, const BasisVector basis,
//         DenseVector ds_output, unsigned int i, unsigned int j) {
//     // swap over values...
// }

// struct copier {
//     const int m;
//     __device__ __host__ double operator()(double ix, double val) {
//         if (ix>=m) {
//             return val;
//         }
//     }


// };*/

// /*
// void WriteVectorAtIndices(cusparseHandle_t *handle, BasisVector& basis,
//         DenseVector* ds_Y, const DenseVector* ds_A) {
//     // take values where basis index is smaller n from vector ds_A and gather them in ds_Y

//     int *basis_idx;
//     int *basis_val;
//     // TODO: read from BasisVector the correct void pointers
//     CHECK_CUSPARSE(cusparseSpVecGet(
//                     basis.descr,
//                     (int64_t *) &basis.m_,
//                     (int64_t *) &basis.m_,
//                     (void**) &basis_idx,
//                     (void**) &basis_val,
//                     &basis.idx_t,&basis.i_b_t,&basis.dat_t))

//     thrust::device_ptr<int> basis_thr_idx = thrust::device_pointer_cast(basis_idx);
//     thrust::device_ptr<int> basis_thr_val = thrust::device_pointer_cast(basis_val);
//     void *ds_ptr_A;
//     void *ds_ptr_Y;
//     CHECK_CUSPARSE(cusparseDnVecGetValues(ds_A->DnDescr,(void **) &ds_ptr_A))
//     CHECK_CUSPARSE(cusparseDnVecGetValues(ds_Y->DnDescr,(void **) &ds_ptr_Y))
//     const thrust::device_ptr<val_T> ds_thr_A=thrust::device_pointer_cast(ds_ptr_A);
//     thrust::device_ptr<val_T> ds_thr_Y=thrust::device_pointer_cast(ds_ptr_Y);

//     PRINT_FUNC_PREFIX;
//     std::cout <<"copying from device to host! idx="<<basis_thr_idx<<",len="<<basis.m_<<std::endl;
//     /*
//     unsigned int j=0;
//     for (unsigned int i=0;i<basis.m_;i++) {
//         if (basis_thr_idx[i] >= basis.m_) {
//             ds_thr_Y[j++] = ds_thr_A[i];
//         }
//     }*/

//     // copy over INTO std::cout for printing!
//     //thrust::copy(basis_thr_idx,basis_thr_idx+basis.m_, std::ostream_iterator<int>(std::cout, ", "));
//     //copy over to host
//     int * host_idx=(int *)malloc(sizeof(int)*basis.m_);
//     thrust::copy(basis_thr_idx,basis_thr_idx+basis.m_,host_idx);
//     val_T * host_A_vals=(val_T *)malloc(sizeof(val_T)*ds_A->size);
//     thrust::copy(ds_thr_A,ds_thr_A+ds_A->size,host_A_vals);
    
//     PRINT_PREFIX;
//     std::cout <<"in host, updating"<<std::endl;

//     thrust::host_vector<val_T> host_ds_y;
//     for (unsigned int i=0;i<basis.m_;i++) {
//         std::cout<<"["<<i<<"]="<<host_idx[i]<<",";
//         if (host_idx[i]<basis.m_) {
//             host_ds_y.push_back(host_A_vals[i]);
//         }
//     }
//     std::cout<<std::endl<<"len="<<host_ds_y.size()<<std::endl;

//     val_T *host_ds_y_data=thrust::raw_pointer_cast(host_ds_y.data());
//     // memCopy instead of SetValues
//     CHECK_CUDA(cudaMemcpy(host_ds_y_data,ds_ptr_Y,host_ds_y.size(), cudaMemcpyHostToDevice));

//     // OTHER ALGORITHM, prototype level
//     // ds_B the basis vector. it has index entries. an index
//     // entry signifies that the concerned column is a basis column.

//     //void *as_indices;
//     //cusparseDnVecGetValues(db_B.descr,(void**)&as_indices);
//     //cusparseCreateSpVec(handle,sp_helper.descr,ds_A.size,ds_B.size,indices,ones,
//             //CUSPARSE_INDEX_32I,CUSPARSE_INDEX_BASE_ZERO,CUDA_R_64F);

//     // Gather X_v[i]<-Y[X_i[i]]
//     // Y=ds_A, X=basis
//     // write data into basis sparse vec
//     //cusparseGather(*handle, *ds_A.descr,basis.descr);

//     // Scatter basis into ds_Y
//     //cusparseScatter(*handle, basis.descr,*ds_Y.descr);

//     // return sparse basis vec to previous state with "ones"
//     //cusparseSpVecSetValues(basis.descr,basis.values_ones);
//     // return ds_Y;
// }

// /* WARNING: this is prototype not-working code, feel free to take the ideas though

// // exploit parallelizability: use numThreads==sparse_basis_vals.size(==n)
// __device__ void helper_swap_i_j(val_T *sparse_basis_vals,unsigned int *sparse_basis_idxs,
//         unsigned int i, unsigned int j,unsigned int n,unsigned int* single_int_value) {
//     unsigned int k=threadIdx.x+blockIdx.x*blockDim.x;
//      SHOULD DO:
//     if sparse_basis_vals[k]==i
//         sparse_basis_vals.pop(k)
//         sparse_basis_idxs.pop(k)
//     // sorted?
//     elif sparse_basis_vals[k]>=j
//         sparse_basis_vals.insert(k,1.0)
//         sparse_basis_idxs.insert(k,j)
//     // replace i<-> j inplace
//     // TODO WARNING: this puts the indices out of order. is that bad? YES!!
//     if (k>=n) return;
//     if (sparse_basis_vals[k]==i) {
//         sparse_basis_vals[k]=j;
//         *single_int_value=k;
//     }

// }

// __device__ void helper_copy_cols(unsigned int *single_int_value,unsigned int j,
//         val_T *sp_B,val_T *sp_A) {
//     // Search through ... not needed anymore?
//     unsigned int k=threadIdx.x+blockIdx.x*blockDim.x;
//     for (unsigned int i=0;i<n;i++) {
//         if (sp_B_indices[i][col_idx]!=k) return;
//         // instead, if we found one concerned value: overwrite with column form A
//         sp_B_values.at(col=*single_int_value,row=sp_B_curr_row)=A.at(col=j,row=sp_B_curr_row);
//     } 
// }

// //__device__ count_reallocate_cols_new_size(const val_T *sp_B_row_ofs,
//         //const val_T *sp_B_col_idx,const unsigned int i) {
//     //unsigned int k=threadIdx.x+blockIdx.x*blockDim.x;
// //
// //}
// */

// void GetCol(cusparseHandle_t *handle,const SparseMatrix* sp_A,
//         DenseVector* ds_output,unsigned int i) {
//     // take column i of A and write into ds_output.
//     PRINT_FUNC_PREFIX;
//     std::cout<<"Taking column i="<<i<<std::endl;

//     if (sp_A->m<ds_output->size) {
//         PRINT_RED_PREFIX;
//         std::cout<<"WARNING, proceeding with smaller vector than "
//             <<"what was given as argument"<<std::endl;
//     } else if (sp_A->m!=ds_output->size) {
//         throw std::logic_error("[housekeeping::GetCol] Dimension mismatch");
//     }
//     if (sp_A->n<=i) {
//         throw std::logic_error("[housekeeping::GetCol] index i="+
//                 std::to_string(i)+" out of range");
//     }
//     thrust::host_vector<val_T> host_out_thr(sp_A->m);
//     //useless, will get overwritten!
//     //thrust::copy(output_thr,output_thr+sp_A->m,host_out_thr.begin());

//     thrust::device_ptr<int> A_row_ptr=thrust::device_pointer_cast(sp_A->csrRowPtr);
//     thrust::host_vector<int> host_A_row_ptr(sp_A->m+1);
//     thrust::copy(A_row_ptr,A_row_ptr+host_A_row_ptr.size(),host_A_row_ptr.begin());

//     thrust::device_ptr<int> A_col_ind=thrust::device_pointer_cast(sp_A->csrColInd);
//     thrust::host_vector<int> host_A_col_ind(sp_A->nnz);
//     thrust::copy(A_col_ind,A_col_ind+host_A_col_ind.size(),host_A_col_ind.begin());

//     thrust::device_ptr<val_T> A_val=thrust::device_pointer_cast(sp_A->csrVal);
//     thrust::host_vector<val_T> host_A_val(sp_A->nnz);
//     thrust::copy(A_val,A_val+host_A_val.size(),host_A_val.begin());

//     // read one column of A
//     for (unsigned int row=0;row<sp_A->n;row++) {
//         //get row offset
//         unsigned int row_ofs=host_A_row_ptr[row];
//         if (row>1 and host_A_row_ptr[row-1]==row_ofs) {
//             // this is an empty row, skip to next one
//             //new_B_row_ofs[row]=new_B_row_ofs[row-1];
//             continue;
//         }
//         unsigned int val_idx=host_A_col_ind[row_ofs];
//         //TODO: thrust::replace? instead of copying A.csrVal and ds_output around...
//         // overwrite if col==i:
//         if (val_idx==i) {
//            host_out_thr[row]=host_A_val[val_idx];
//         }
//     }
//     // take the values pointer and reinterpret it as a thrust::device_vector.
//     // but not exaclty a device_vector, just a device_ptr. 
//     val_T *ds_output_values;
//     CHECK_CUSPARSE(cusparseDnVecGetValues(ds_output->DnDescr,
//                 (void **) &ds_output_values));
//     thrust::device_ptr<val_T> output_thr = thrust::device_pointer_cast(ds_output_values);

//     //write back
//     thrust::copy(host_out_thr.begin(),host_out_thr.end(),output_thr);
// }

// void UpdateBasis(cusparseHandle_t *handle, BasisVector basis, unsigned const int i,
//         unsigned const int j, const SparseMatrix sp_A, SparseMatrix sp_B) {
//     assert(i<sp_A.n);
//     assert(j>=sp_A.n);

//     int *basis_idx;
//     int *basis_val;
//     //TODO: get from BasisVector struct members
//     CHECK_CUSPARSE(cusparseSpVecGet(
//                     basis.descr,
//                     (int64_t *) &basis.m_,
//                     (int64_t *) &basis.m_,
//                     (void**) &basis_idx,
//                     (void**) &basis_val,
//                     &basis.idx_t,&basis.i_b_t,&basis.dat_t));
//     thrust::device_ptr<int> basis_thr_idx = thrust::device_pointer_cast(basis_idx);
//     thrust::device_ptr<int> basis_thr_val = thrust::device_pointer_cast(basis_val);
//     thrust::device_vector<int> new_basis_idx{};
//     thrust::device_vector<int> new_basis_val{};
//     unsigned int new_basis_val_idx=0;
//     bool has_inserted_j=false;

//     PRINT_FUNC_PREFIX;
//     std::cout <<"starting with i="<<i<<" j="<<j<<std::endl;

//     // figure out the i <-> j swap
//     unsigned int tgt_col;
//     for (unsigned int b_row=0;b_row<basis.m_;b_row++) {
//         basis_thr_idx[b_row];
//         if (basis_thr_val[b_row]>=j && ! has_inserted_j) {
//             has_inserted_j=true;
//             // insert j just before b_row element
//             new_basis_val.push_back(j);
//             new_basis_idx.push_back(new_basis_val_idx++); // and increment
//         }
//         if (basis_thr_val[b_row]==i) {
//             //save into tgt_col
//             //tgt_col=basis_thr_idx[b_row]; // like this? I'm a bit confused...
//             tgt_col=b_row;
//             // remove i : skip it
//             continue;
//         }
//         new_basis_val.push_back(basis_val[b_row]);
//         new_basis_idx.push_back(new_basis_val_idx++); // and increment
//     }

//     // create new_B CSR sparse matrix, with mostly data from A except at column=i
//     // row,col,value
//     thrust::device_vector<csr_triple> new_B_triplets{};

//     // copy matrix B into B_new, EXCEPT column=tgt_col
//     for (unsigned int row=0;row<sp_B.n;row++) {
//         //get row offset
//         unsigned int row_ofs=sp_B.csrRowPtr[row];
//         if (row>1 and sp_B.csrColInd[row-1]==row_ofs) {
//             // this is an empty row, skip to next one
//             //new_B_row_ofs[row]=new_B_row_ofs[row-1];
//             continue;
//         }
//         unsigned int val_idx=sp_B.csrColInd[row_ofs];
//         // overwrite if col==i: FOR NOW, SKIP only
//         if (val_idx==tgt_col) {
//             continue;
//         }
//         val_T dest_value=sp_B.csrVal[val_idx];
//         csr_triple t(row,val_idx,dest_value);
//         new_B_triplets.push_back(t);
//     }
//     // read column==i of A, write into B column=tgt_col
//     for (unsigned int row=0;row<sp_A.n;row++) {
//         //get row offset
//         unsigned int row_ofs=sp_A.csrRowPtr[row];
//         if (row>1 and sp_A.csrRowPtr[row-1]==row_ofs) {
//             // this is an empty row, skip to next one
//             //new_B_row_ofs[row]=new_B_row_ofs[row-1];
//             continue;
//         }
//         unsigned int val_idx=sp_A.csrColInd[row_ofs];
//         // overwrite if col==i:
//         if (val_idx==i) {
//             val_T dest_value=sp_A.csrVal[val_idx];
//             csr_triple t(row,tgt_col,dest_value);
//             new_B_triplets.push_back(t);
//         }
//     }
//     //assemble new_B
//     // expensive :
//     // does it compile?
//     thrust::sort(thrust::device,
//             new_B_triplets.begin(),new_B_triplets.end(),
//             thrust::less<csr_triple>()
//     );
//     thrust::device_vector<val_T> new_B_values{};
//     thrust::device_vector<int> new_B_row_ofs{};
//     thrust::device_vector<int> new_B_col_idx{};
//     unsigned int prev_row=(unsigned int) -1;
//     unsigned int row;
//     unsigned int col;
//     val_T value;
//     // CSR format
//     for (csr_triple tripl:new_B_triplets) {
//         row=thrust::get<0>(tripl);
//         col=thrust::get<1>(tripl);
//         value=thrust::get<2>(tripl);

//         new_B_values.push_back(value);
//         new_B_col_idx.push_back(col);
//         if (prev_row!=row) { //just started a new row
//             new_B_row_ofs.push_back(row);
//             prev_row=row;
//         }
//     }
//     // replace pointers
//     CHECK_CUDA(cudaFree(sp_B.csrRowPtr));
//     CHECK_CUDA(cudaFree(sp_B.csrColInd));
//     CHECK_CUDA(cudaFree(sp_B.csrVal));
//     sp_B.csrRowPtr= thrust::raw_pointer_cast(new_B_row_ofs.data());
//     sp_B.csrColInd= thrust::raw_pointer_cast(new_B_col_idx.data());
//     sp_B.csrVal=    thrust::raw_pointer_cast(new_B_values.data());
//     // WARNING: I only set the row+col pointers+values, but is there
//     // really nothing to change with the descriptor? In the case of a
//     // cusparseMatDescr_t old-style non-Sp descr
// #if true // use the newer cusparseSpMatDescr_t type
//     cusparseCsrSetPointers(sp_B.spDescr,
//             sp_B.csrRowPtr,
//             sp_B.csrColInd,
//             sp_B.csrVal
//         );
// #endif
//     return;

// /* WARNING: this is prototype not-working code, feel free to take the ideas though

//     // sp_helper contains 1 at indices we want... except i/j still needs to be swapped i out, j in
//     // "returns" this->single_int_value the index into basis, of where i was.

//     //TODO CHECK: do the blockIdx 's start counting at 0 ? If I tell it 1, will there be block 0 and bl 1 ?
//     // I think no, that's why there is a +1
//     helper_swap_i_j<<<(basis.m_)/8+1,8>>>(basis.values_vptr,basis.indices_vptr,
//             i,j,n,this->single_int_value);

//     // once we swapped i,j, need to move one column:
//     // A nonbasic and B basic
//     // B[col=offset_of_i_in_basis]<-A[col=j]
//     this->mat_one_extract_set_position(basis.offset(i),j);

//     // SpGEMM(): use mat_one to extract column j from A into mat_one
//     // matA=A, mat_B=mat_one, C=mat_one [for the RESULT, use beta=0]
//     val_T alpha=1.0;
//     val_T beta=0.0;
//     size_t bufferSize1;
//     size_t bufferSize2;
//     //void *externalBuffer1; NEEDED ON DEVICE!
//     //void *externalBuffer2; NEEDED ON DEVICE!
//     cusparseSpGEMMDescr_t* spgemmDescr;
//     cusparseSpGEMM_createDescr(spgemmDescr);
//     cusparseSpGEMM_workEstimation(handle,
//             CUSPARSE_OPERATION_NON_TRANSPOSE, // opA
//             CUSPARSE_OPERATION_NON_TRANSPOSE, // opB
//             (void *) &alpha, // TODO: make sure val_T TYPE!
//             sp_A.descr, // matA
//             basis.mat_one_descr, // matB
//             (void *) &beta, //val_T TYPE!
//             basis.mat_one_descr, // matC AND result
//             CUDA_R_64F,
//             CUSPARSE_SPGEMM_DEFAULT,
//             spgemmDescr,
//             &bufferSize1,
//             NULL
//         );
//     cudaMalloc((void **) &externalBuffer1,bufferSize1);
//     cusparseSpGEMM_workEstimation(handle,
//             CUSPARSE_OPERATION_NON_TRANSPOSE, // opA
//             CUSPARSE_OPERATION_NON_TRANSPOSE, // opB
//             (void *) &alpha, // TODO: make sure val_T TYPE!
//             sp_A.descr, // matA
//             basis.mat_one_descr, // matB
//             (void *) &beta, //val_T TYPE!
//             basis.mat_one_descr, // matC AND result
//             CUDA_R_64F,
//             CUSPARSE_SPGEMM_DEFAULT,
//             spgemmDescr,
//             &bufferSize1,
//             externalBuffer1,
//         );
//     cusparseSpGEMM_compute(handle,
//             CUSPARSE_OPERATION_NON_TRANSPOSE, // opA
//             CUSPARSE_OPERATION_NON_TRANSPOSE, // opB
//             (void *) &alpha, // TODO: make sure val_T TYPE!
//             sp_A.descr, // matA
//             basis.mat_one_descr, // matB
//             (void *) &beta, //val_T TYPE!
//             basis.mat_one_descr, // matC AND result
//             CUDA_R_64F,
//             CUSPARSE_SPGEMM_DEFAULT,
//             spgemmDescr,
//             &bufferSize2,
//             NULL
//         );
//     cudaMalloc((void **) &externalBuffer2,bufferSize2);
//     cusparseSpGEMM_compute(handle,
//             CUSPARSE_OPERATION_NON_TRANSPOSE, // opA
//             CUSPARSE_OPERATION_NON_TRANSPOSE, // opB
//             (void *) &alpha, // TODO: make sure val_T TYPE!
//             sp_A.descr, // matA
//             basis.mat_one_descr, // matB
//             (void *) &beta, //val_T TYPE!
//             basis.mat_one_descr, // matC AND result
//             CUDA_R_64F,
//             CUSPARSE_SPGEMM_DEFAULT,
//             spgemmDescr,
//             &bufferSize2,
//             externalBuffer2
//         );
//     int64_t new_num_rows,new_num_cols,new_nnz;
//     int *C_col_ptrs;
//     val_T *C_vals;
//     cusparseSpMatGetSize(
//             basis.mat_one_descr,
//             &new_num_rows,&new_num_cols,&new_nnz
//         );
//     //TODO: think
//     cudaMalloc((void **)C_col_ptrs,new_nnz*sizeof(int));
//     cudaMalloc((void **)C_vals,new_nnz*sizeof(val_T));
//     cusparseCsrSetPointers(
//             basis.mat_one_descr,
//             basis.row_ofs?, // TODO: need 3rd int * as well
//             C_col_ptrs,
//             C_vals
//         );

//     cusparseSpGEMM_copy(handle,
//             CUSPARSE_OPERATION_NON_TRANSPOSE, // opA
//             CUSPARSE_OPERATION_NON_TRANSPOSE, // opB
//             (void *) &alpha, // TODO: make sure val_T TYPE!
//             sp_A.descr, // matA
//             basis.mat_one_descr, // matB
//             (void *) &beta, //val_T TYPE!
//             //TODO: can we "just" copy into A ? NO will overwrite same values...
//             basis.mat_one_descr, // matC AND result
//             CUDA_R_64F,
//             CUSPARSE_SPGEMM_DEFAULT,
//             spgemmDescr
//         );
//     cusparseSpGEMM_destroyDescr(spgemmDescr);
//     cudaFree(externalBuffer1);
//     cudaFree(externalBuffer2);

//     // SpGEMM(): make SP matrix sum into mat_one
//     // setPointers: "return" new B as mat_one which has all the data.
//     // SpGEMM()+setPointers: overwrite B_old with what mat_one should be, to swap back again next loop iter

//     //helper_copy_cols<<<(basis.m_)/8+1,8>>>(this->single_int_value,sp_B.descr?,sp_A.descr?);

//     //cusparseSpMV(h,CUSPARSE_OPERATION_NON_TRANSPOSE,(const void*)&alpha=1,
//             //sp_A,ds_B?,(const void*)&beta=1,ds_B?,CUDA_R_64F,alg,BUFFEEEEEEER
//             //);

// }
// */
#endif
