#include "simplex.cuh"

void gatherSolution(const DenseVector* xb, const BasisVector* basis, DenseVector::value_t** solution, const int size) {

	*solution = new DenseVector::value_t[size];
	DenseVector::value_t* solution_d;
	CHECK_CUDA(cudaMalloc((void **)&solution_d, size*sizeof(DenseVector::value_t)))
	makeZeros<<<size, 1>>>(solution_d);

	int* basisVals_d;
	CHECK_CUSPARSE(cusparseSpVecGetValues((basis->descr), (void **)&basisVals_d))

	val_T* xbVals_d;
	CHECK_CUSPARSE(cusparseDnVecGetValues(xb->DnDescr, (void**)&xbVals_d))

	setBasicVals<<<basis->m_, 1>>>(xbVals_d, basisVals_d, solution_d);

	cudaMemcpy(*solution, solution_d, size*sizeof(DenseVector::value_t), cudaMemcpyDeviceToHost);

	CHECK_CUDA(cudaFree(solution_d))
	solution_d = NULL;
	
}


// Helper for removeSlacksFrom_xb
__global__ void makeZeros(DenseVector::value_t* newVals) {

	newVals[blockIdx.x*blockDim.x + threadIdx.x] = 0.0;

}

// Helper for gatherSolution
__global__ void setBasicVals(const DenseVector::value_t* xbVals, const int* basisVals, DenseVector::value_t* solution) {

	int idx = basisVals[blockIdx.x*blockDim.x + threadIdx.x];
	solution[idx] = xbVals[blockIdx.x*blockDim.x + threadIdx.x];

}
