#include "simplex.cuh"
#include <iostream>
#include <string>
#include <iomanip>
#include <vector>
#include <fstream>

//input: int status which is defined as follows:
//status == 0 is infeasible (inf)
//status == 1 is optimal (opt)
//status == 2 is ?   (best)
//status == 3 is unbounded (unbd)
//status == 4 is unknown (unkn) 
//opt_val is the optimal value of the LP unless there is no optimal value then it is just 0 (we can choose another dummy variable)
//test is a string with the name of the test that is currently executed
//numVars is the number of variables
//numConst is the number of slack variables
void write_sol(int status, val_T opt_val, DenseVector::value_t* solution, const int numVars, const int numConst){
    std::cout <<"SOLFILESTART"<<std::endl;
	switch(status){
		
		//infeasible
		case 0:
			std::cout << "=inf= -1" << std::endl;
			
		break;
		
		//optimal
		case 1:
			std::cout << "=opt= "<< opt_val << std::endl;
			
			std::cout << "=sol=" <<"\n";

    			for (int i=0; i < numVars; i++) {
                    if (std::abs(solution[i])<TOL) {
                        solution[i]=0.0f;
                    }
        			std::cout <<"VAR_"<<i<<" "<<solution[i] << std::endl;
    			}
                // Slack variables:
			//	for (int i=0; i < numConst; i++) {
            //        if (std::abs(solution[numVars+i])<TOL) {
            //            solution[numVars+i]=0.0f;
            //        }
        	//		std::cout << solution[numVars+i] << "\n";
    		//	}

		break;
		
		//best
		case 2:
			std::cout << "=best=" << std::endl;
		break;
		
		//unbd
		case 3:
			std::cout << "=unbd=" << std::endl;
		break;
		
		//unknown
		case 4:
			std::cout << "=unkn=" << std::endl;
		break;
		
		//else
		default: 
			std::cout << "some unknown error occured in obtaining the solution" << std::endl;
	}

	
	//file.close();
}
