#include "simplex.cuh"
#include <thrust/device_ptr.h>
#include <thrust/host_vector.h>

#define DEBUGGING_MEM false

BasisVector::BasisVector(unsigned int m,unsigned int n) :
        m_(m),
        n_(n),
        values_vec(thrust::device_vector<unsigned int> (m)),
        indices_vec(thrust::device_vector<unsigned int> (m)),
        idx_t{CUSPARSE_INDEX_32I},
        i_b_t{CUSPARSE_INDEX_BASE_ZERO},
        dat_t{CUDA_R_32I}
{
    // write values <- 01234..., ones<- 111111..., indices=01234...
    thrust::host_vector<int> host_values_vec(this->values_vec.size());
    thrust::host_vector<int> host_indices_vec(this->indices_vec.size());

    for (unsigned int i=0;i<m;i++) {
        host_values_vec[i]=(signed int) i+this->n_;
        //this->ones_vec[i]=1;
        host_indices_vec[i]=(signed int) i;
    }
    //initialize the vectors
    // cudaMemCopy! happens in thrust copy-constructor
    this->values_vec=host_values_vec;
    this->indices_vec=host_indices_vec;

    // for converting from thrust::device_vector to cusparse, raw ptrs needed!
    // https://stackoverflow.com/questions/67391625/pass-thrust-device-vectors-to-device-function-and-modify-them
    this->indices_vptr=thrust::raw_pointer_cast(this->indices_vec.data());
    //NOT NEEDED:
    //this->ones_vptr=thrust::raw_pointer_cast(this->ones_vec.data());
    this->values_vptr=thrust::raw_pointer_cast(this->values_vec.data());

    //NOT NEEDED:
    //cudaMalloc((void **) &(this->single_int_value),sizeof(unsigned int)*1);

    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX;
        std::cout << "creating SpVec(indices,values)"<<std::endl;
    }
    
    CHECK_CUSPARSE(cusparseCreateSpVec(&this->descr, this->m_, this->m_,
            // indices, then values
            indices_vptr, values_vptr,
            // integer indices obv, BUT ALSO integer values
            // INDEX_32I apparently is signed int
            this->idx_t,this->i_b_t,this->dat_t));

/* useless
    this->mat_one_values{1,1.0};
    //start with 1 at rightmost bottom corner
    this->mat_one_row_ofs{this->m_+1,0};
    this->mat_one_row_ofs[this->m_]=(double) this->m_;
    this->mat_one_col_ind{1,this->m_};

    this->mat_one_values_vptr=thrust::raw_pointer_cast(this->mat_one_values.data());
    this->mat_one_row_ofs_vptr=thrust::raw_pointer_cast(this->mat_one_row_ofs.data());
    this->mat_one_col_ind_vptr=thrust::raw_pointer_cast(this->mat_one_col_ind.data());
    cusparseCreateCsr(this->mat_one_descr,this->m_,this->m_,1,
            this->mat_one_row_ofs_vptr,
            this->mat_one_col_ind_vptr,
            this->mat_one_values_vptr,
            // 64float for the matrix multiply? make sure we NEVER cast 1*0.2 to 0, rather have 1.0*0.2
            CUSPARSE_INDEX_32I,CUDA_R_64F);
*/
}

BasisVector::~BasisVector() {
    // BEWARE! destroying the descriptor ALSO automatically seems to cudaFree the pointers

    cusparseDestroySpVec(this->descr);

    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout<<"Destroyed this->descr"<<std::endl;
    }

    // THIS makes errors
	//cudaFree(this->values_vptr);
	//cudaFree(this->indices_vptr);
    //PRINT_PREFIX;
    //std::cout <<"Freed this->values_vptr, this->indices_vptr"<<std::endl;

}

DenseVector::DenseVector() : size(0), valueType(CUDA_R_32F) {
    PRINT_FUNC_PREFIX;
    std::cout<<"warning, initializing 0-size vector"<<std::endl;
}

DenseVector::DenseVector(unsigned int n) : size(n), valueType(CUDA_R_32F){
    CHECK_CUDA(cudaMalloc(&values, size*sizeof(value_t)))
    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout << "malloced values, size=" << size*sizeof(val_T) << std::endl;
    }

    CHECK_CUSPARSE(cusparseCreateDnVec(&DnDescr, size, values, valueType));
    if (DEBUGGING_MEM) {
        PRINT_PREFIX; std::cout << "initialized DnVec descr" << std::endl;
    }
}

DenseVector::DenseVector(unsigned int n, value_t* val) : size(n), valueType(CUDA_R_32F){   
            if (DEBUGGING_MEM) {
                PRINT_FUNC_PREFIX; std::cout << "DenseVec init n, val    n=" << n << "  size=" << size << std::endl;}
    
    CHECK_CUDA(cudaMalloc((void**)&values, n*sizeof(value_t)));
    //std::cout << "malloced values, size=" << size*sizeof(t) << " with s ele_size=" << sizeof(t) << " vs devize size=" << size*sizeof(CUDA_R_32F) << " with single ele_size=" << sizeof(CUDA_R_32F) << std::endl;
            if (DEBUGGING_MEM) {
                PRINT_PREFIX; std::cout << "malloced values, size=" << size*sizeof(value_t) << std::endl; }

    CHECK_CUDA(cudaMemcpy(values, val, size*sizeof(value_t), cudaMemcpyHostToDevice));
            if (DEBUGGING_MEM) {
                PRINT_PREFIX; std::cout << "copied memory to device" << std::endl; }

    CHECK_CUSPARSE(cusparseCreateDnVec(&DnDescr, size, values, valueType));
            if (DEBUGGING_MEM) {
                PRINT_PREFIX; std::cout << "initialized DnVec descr" << std::endl; }
}

DenseVector::DenseVector(const DenseVector& b) : size(b.size), valueType(b.valueType) {
    val_T *b_values;
    CHECK_CUSPARSE(cusparseDnVecGetValues(b.DnDescr,(void **)&b_values));

    CHECK_CUDA(cudaMalloc((void**)&values, size*sizeof(val_T)));
            if (DEBUGGING_MEM) {
                PRINT_FUNC_PREFIX; std::cout << "malloced values, size=" << size*sizeof(val_T) << std::endl; }

    int gridDim = 1;
    int blockDim = size;
    if (blockDim > 1024) {
        gridDim = std::ceil(((float)blockDim) / 1024);
        blockDim = 1024;
    }
    copyData<<<gridDim, blockDim>>>((value_t*)b_values, (value_t*)values, size);
            if (DEBUGGING_MEM) {
                PRINT_PREFIX; std::cout << "copied other vector data" << std::endl; }

    CHECK_CUSPARSE(cusparseCreateDnVec(&DnDescr, size, values, valueType));
            if (DEBUGGING_MEM) {
                PRINT_PREFIX; std::cout << "initialized DnVec descr" << std::endl; }
}

DenseVector::~DenseVector() {
    CHECK_CUSPARSE(cusparseDestroyDnVec(DnDescr));
    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout << "descriptor-destroyed Dense Vector" << std::endl;
    }

    CHECK_CUDA(cudaFree(values))
    values = nullptr;
}

SparseMatrix::SparseMatrix() : m(0), n(0), nnz(0){
    csrVal = nullptr;
    csrRowPtr = nullptr;
    csrColInd = nullptr;
}

SparseMatrix::SparseMatrix(unsigned int dim) : m(dim), n(dim), nnz(dim) {
    int* rowPtr = new int[n+1];
    int* colInd = new int[n];
    value_t* val = new value_t[n];

    for (int i=0; i<n; i++) {
        rowPtr[i] = i;
        colInd[i] = i;
        val[i] = 1.0;
    }
    rowPtr[n] = n;

    CHECK_CUSPARSE(cusparseCreateMatDescr(&descr));
    CHECK_CUSPARSE(cusparseSetMatIndexBase(descr, CUSPARSE_INDEX_BASE_ZERO))
    CHECK_CUSPARSE(cusparseSetMatType(descr, CUSPARSE_MATRIX_TYPE_GENERAL))
    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout << "Initialized Sparse Matrix" << std::endl;
    }

    CHECK_CUDA(cudaMalloc((void**)&csrVal, nnz*sizeof(value_t)))
    CHECK_CUDA(cudaMalloc((void**)&csrRowPtr, (m+1) * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&csrColInd, nnz * sizeof(int)))
    if (DEBUGGING_MEM) {
        PRINT_PREFIX;
        std::cout << "malloced memory for vals=" << nnz*sizeof(value_t);
        std::cout << " rowPtrs=" << (m+1) * sizeof(int) << " and colInd=" << nnz * sizeof(int) << std::endl;
    }

    CHECK_CUDA(cudaMemcpy(csrVal, val, nnz*sizeof(value_t), cudaMemcpyHostToDevice))
    CHECK_CUDA(cudaMemcpy(csrRowPtr, rowPtr, (m+1) * sizeof(int), cudaMemcpyHostToDevice))
    CHECK_CUDA(cudaMemcpy(csrColInd, colInd, nnz * sizeof(int), cudaMemcpyHostToDevice))
    if (DEBUGGING_MEM) {
        PRINT_PREFIX; std::cout << "copied memory form host to device" << std::endl;
    }

    cusparseCreateCsr(&spDescr, m, n, nnz, csrRowPtr, csrColInd, csrVal, 
                      CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I,
                      CUSPARSE_INDEX_BASE_ZERO, CUDA_R_32F);
}

SparseMatrix::SparseMatrix(unsigned int rows, unsigned int cols, unsigned int nonZeros, int* rowPtr, int* colInd, value_t* val) : m(rows), n(cols), nnz(nonZeros) {
    cusparseCreateMatDescr(&descr);
    CHECK_CUSPARSE(cusparseSetMatIndexBase(descr, CUSPARSE_INDEX_BASE_ZERO))
    CHECK_CUSPARSE(cusparseSetMatType(descr, CUSPARSE_MATRIX_TYPE_GENERAL))

    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout << "Initialized Sparse Matrix" << std::endl;
    }
    csrVal = nullptr;
    csrRowPtr = nullptr;
    csrColInd = nullptr;

    CHECK_CUDA(cudaMalloc((void**)&csrVal, nnz*sizeof(value_t)))
    CHECK_CUDA(cudaMalloc((void**)&csrRowPtr, (m+1) * sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&csrColInd, nnz * sizeof(int)))
    if (DEBUGGING_MEM) {
        PRINT_PREFIX; std::cout << "malloced memory for vals=" << nnz*sizeof(value_t) << " rowPtrs=" << (m+1) * sizeof(int) << " and colInd=" << nnz * sizeof(int) << std::endl;
    }

    CHECK_CUDA(cudaMemcpy(csrVal, val, nnz*sizeof(value_t), cudaMemcpyHostToDevice))
    CHECK_CUDA(cudaMemcpy(csrRowPtr, rowPtr, (m+1) * sizeof(int), cudaMemcpyHostToDevice))
    CHECK_CUDA(cudaMemcpy(csrColInd, colInd, nnz * sizeof(int), cudaMemcpyHostToDevice))
    if (DEBUGGING_MEM) {
        PRINT_PREFIX; std::cout << "copyed memory form host to device" << std::endl;
    }

    cusparseCreateCsr(&spDescr, m, n, nnz, csrRowPtr, csrColInd, csrVal,
                      CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I,
                      CUSPARSE_INDEX_BASE_ZERO, CUDA_R_32F);
}

SparseMatrix::~SparseMatrix() {
    CHECK_CUDA(cudaFree(csrVal))
    CHECK_CUDA(cudaFree(csrRowPtr))
    CHECK_CUDA(cudaFree(csrColInd))
    csrVal = nullptr;
    csrRowPtr = nullptr;
    csrColInd = nullptr;

    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout << "Entered Matrix destructor" << std::endl;
    }

    CHECK_CUSPARSE(cusparseDestroyMatDescr(descr))
    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout << "Destroyed MatDescr" << std::endl;
    }

    CHECK_CUSPARSE(cusparseDestroySpMat(spDescr))

    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout << "freed and destroyed SparseMatrix" << std::endl;
    }
}

LU::~LU() {
    // CHECK_CUDA(cudaFree(&csrVal))
    // CHECK_CUDA(cudaFree(&csrRowPtr))
    // CHECK_CUDA(cudaFree(&csrColInd))

    CHECK_CUSPARSE(cusparseDestroySpMat(descr_L))
    CHECK_CUSPARSE(cusparseDestroySpMat(descr_U))
    
    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout << "freed and destroyed LU" << std::endl;
    }
}

LU::LU() {
    // value_t InitcsrVal[nnz];

    // cudaMemcpy(InitcsrVal, A->csrVal, sizeof(int)*(nnz), cudaMemcpyDeviceToHost);

    // cudaMemcpy(csrVal, InitcsrVal, sizeof(int)*(nnz), cudaMemcpyHostToDevice);

    // Allocate dummies for first free in performLU
    CHECK_CUDA(cudaMalloc((void**)&csrVal, sizeof(SparseMatrix::value_t)))
    CHECK_CUDA(cudaMalloc((void**)&csrColInd, sizeof(int)))
    CHECK_CUDA(cudaMalloc((void**)&csrRowPtr, sizeof(int)))
    if (DEBUGGING_MEM) {
        PRINT_FUNC_PREFIX; std::cout << "malloced LU dummies" <<std::endl;
    }
}

// Helper for the DenseVector copy constructer
__global__ void copyData(const DenseVector::value_t* src, DenseVector::value_t* dest, const int size) {
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    
    if (i < size) {
        dest[i] = src[i];
    }
}





// SparseMatrix::SparseMatrix(const SparseMatrix* SpMat) m(SpMat->m), n(SpMat->n), nnz(SpMat->nnz) {
//     cusparseCreateMatDescr(&descr);
//     std::cout << "Initialized Sparse Matrix" << std::endl;

//     CHECK_CUDA(cudaMalloc((void**)&csrVal, nnz*sizeof(value_t)))
//     CHECK_CUDA(cudaMalloc((void**)&csrRowPtr, (m+1) * sizeof(int)))
//     CHECK_CUDA(cudaMalloc((void**)&csrColInd, nnz * sizeof(int)))
//     std::cout << "malloced memory for vals=" << nnz*sizeof(value_t) << " rowPtrs=" << (m+1) * sizeof(int) << " and colInd=" << nnz * sizeof(int) << std::endl;

//     int numBlocks = nnz;
//     copyData<<<numBlocks, 1>>>((value_t*)SpMat->csrVal, (value_t*)this->csrVal);
//     copyData<<<numBlocks, 1>>>((value_t*)SpMat->csrColInd, (value_t*)this->csrColInd);

//     numBlocks = m + 1
//     copyData<<<numBlocks, 1>>>((value_t*)SpMat->csrRowPtr, (value_t*)this->csrRowPtr);

//     std::cout << "copyed other vector data" << std::endl;
//     std::cout << "copyed memory form host to device" << std::endl;
//     std::cout << " copyed Sparsematrix " << std::endl;
// }
