#!/usr/bin/python3

import pprint
fln='benchmark/miplib/results/run0.small_benchmark.scip.0threads.3600s.out'

solutions={}
after_match=7
curr_match=-1
current='UNKOWN'
with open(fln,'r') as f:
    while l:=f.readline() :
        if l.startswith('@01 ') :
            current=l.split(' ',2)[1]
            print(current)
            solutions[current]=[]
        elif l.startswith('Solution') :
            curr_match=0
        if curr_match>=0 :
            if l.find('Primal Bound')+l.find('Dual Bound')>=0 :
                text,val=l.split(':',1)
                val=float(val.strip(' ').split(' ',1)[0])
                print('\t',text,val)

            curr_match+=1
            if curr_match>after_match :
                curr_match=-1

