
#thrust library as part of cccl:
# AUTOMATICALLY included in CUDA tooklit! :)
# find /usr/local/cuda/|grep thrust

NVCC_OPTIONS=-lcusparse -G
#COMPILER_OPTIONS=-g
HOSTNAME=$(shell cat /etc/hostname)
#check if running on davinci or not
ifeq "${HOSTNAME}" 'davinci'
	#add glpk
	GLPK_INCLUDE_DIR='../felix_tests/glpk-4.35/include/'
	GLPK_LIB_DIR='../felix_tests/glpk-4.35/src/.libs'
	NVCC_OPTIONS+=-I ${GLPK_INCLUDE_DIR} -L ${GLPK_LIB_DIR} -lglpk -lcublas -lcusolver

	#add libsegfault
	SEGFAULT_LIB_DIR='../'
	NVCC_OPTIONS+=-L ${SEGFAULT_LIB_DIR} -lSegFault

	#benchmarking
	NVCC_OPTIONS+=-O 5 --use_fast_math

	#[optional=OFF] add instrument_functions.cu
	ifeq '1' '0' #false
		NVCC_OPTIONS+=-D SHOW_FUNCTION_NAMES
		COMPILER_OPTIONS+=-rdynamic -finstrument-functions
	endif
else ifeq "${HOSTNAME}" 'florian-Swift-SF313-53G'
	#whenever you're running not on davinci:
	#glpk
	GLPK_INCLUDE_DIR='../glpk-4.35/include/'
	GLPK_LIB_DIR='../glpk-4.35/src/.libs'
	NVCC_OPTIONS+=-I ${GLPK_INCLUDE_DIR} -L ${GLPK_LIB_DIR} -lglpk -lcublas -lcusolver
#	#[optional] libsegfault
#	SEGFAULT_LIB_DIR='../'
#	NVCC_OPTIONS+=-L ${SEGFAULT_LIB_DIR} -lSegFault

#	#[optional] add instrument_functions.cu
#	NVCC_OPTIONS+=-D SHOW_FUNCTION_NAMES
#	COMPILER_OPTIONS+=-rdynamic -finstrument-functions
else
	#whenever you're running not on davinci:
	#glpk
	GLPK_INCLUDE_DIR='../glpk-4.35/include/'
	GLPK_LIB_DIR='../glpk-4.35/src/.libs'
	NVCC_OPTIONS+=-I ${GLPK_INCLUDE_DIR} -L ${GLPK_LIB_DIR} -lglpk -lcublas -lcusolver
	#[optional] libsegfault
	SEGFAULT_LIB_DIR='../'
	NVCC_OPTIONS+=-L ${SEGFAULT_LIB_DIR} -lSegFault

	#[optional] add instrument_functions.cu
	NVCC_OPTIONS+=-D SHOW_FUNCTION_NAMES
	COMPILER_OPTIONS+=-rdynamic -finstrument-functions
endif

NVCC_OPTIONS+=--compiler-options='${COMPILER_OPTIONS}'
SOURCES = src/housekeeping.cu src/instrument_functions.cu \
		  src/memorymanagment.cu src/operations.cu src/reader.cu \
		  src/selectEnterLeavingVars.cu src/write_output.cu \
		  src/gatherSolution.cu src/newLu.cu src/basisfunctions.cu src/simplex.cu main.cu
all: project

#SHOW_FUNCTION_NAMES print all functions' names when they are entered
#makefile to trigger a recompile also when compiler options changed
project: $(SOURCES) src/*.cuh makefile
	nvcc ${NVCC_OPTIONS} -I src/ $(SOURCES) -o project

test_all: test_housekeeping
	echo "running ALL tests"

TEST_H_SOURCES=src/housekeeping.cu src/memorymanagment.cu
test_housekeeping: test/housekeeping/main.cu src/*.cuh ${TEST_H_SOURCES} makefile
	nvcc ${NVCC_OPTIONS} -I src/ test/housekeeping/main.cu ${TEST_H_SOURCES} -o test_housekeeping

TEST_H2_SOURCES=src/housekeeping.cu src/memorymanagment.cu test/housekeeping/TestHousekeeping.cu
test_housekeeping2: ${TEST_H2_SOURCES} src/*.cuh makefile
	nvcc ${NVCC_OPTIONS} -I src/  ${TEST_H2_SOURCES} -o test_housekeeping2

TEST_R_SOURCES = src/memorymanagment.cu src/reader.cu test/read/read.cu
test_reader: ${TEST_R_SOURCES} src/*.cuh makefile
	nvcc ${NVCC_OPTIONS} -I src/ ${TEST_R_SOURCES} -o test_reader

TEST_ONLYR_SOURCES = test/read/reader.cu test/read/main.cu
test_only_reader: ${TEST_ONLYR_SOURCES} test/read/reader.cuh makefile
	nvcc ${NVCC_OPTIONS} -I test/read/ ${TEST_ONLYR_SOURCES} -o test_only_reader

TEST_L_SOURCES = src/incompLU.cu src/instrument_functions.cu \
		  src/memorymanagment.cu test/lu/luMain.cu
test_lu: ${TEST_L_SOURCES} src/*.cuh makefile
	nvcc ${NVCC_OPTIONS} -I src/ ${TEST_L_SOURCES} -o test_lu

TEST_O_SOURCES = src/operations.cu src/memorymanagment.cu \
				 src/reader.cu test/operations/TestOperations.cu
test_op: ${TEST_O_SOURCES} src/*.cuh makefile
	nvcc ${NVCC_OPTIONS} -I src/ ${TEST_O_SOURCES} -o test_op

TEST_AUX_SOURCES = src/memorymanagment.cu \
			test/helpers_sp1/TestHelpers.cu
test_aux: ${TEST_AUX_SOURCES} src/*.cuh makefile
	nvcc ${NVCC_OPTIONS} -I src/ ${TEST_AUX_SOURCES} -o test_aux

TEST_SEL_SOURCES = src/memorymanagment.cu \
			test/selectPivot/TestSelection.cu
test_sel: ${TEST_SEL_SOURCES} src/*.cuh makefile
	nvcc ${NVCC_OPTIONS} -I src/ ${TEST_SEL_SOURCES} -o test_sel

TEST_GATH_SOURCES = src/housekeeping.cu src/gatherSolution.cu \
		  src/memorymanagment.cu  \
			test/gatherSolution/testGather.cu
test_gath: ${TEST_GATH_SOURCES} src/*.cuh makefile
	nvcc ${NVCC_OPTIONS} -I src/ ${TEST_GATH_SOURCES} -o test_gath

TEST_NL_SOURCES = src/memorymanagment.cu test/lu/newLu.cu

test_newLu: ${TEST_NL_SOURCES} src/*.cuh makefile
	nvcc ${NVCC_OPTIONS} -I src/ ${TEST_NL_SOURCES} -o test_newLu

TEST_SIMPLE_SOURCES = src/housekeeping.cu src/instrument_functions.cu \
          src/memorymanagment.cu src/operations.cu src/reader.cu \
          src/selectEnterLeavingVars.cu src/write_output.cu \
          src/gatherSolution.cu src/newLu.cu src/basisfunctions.cu src/simplex.cu test/smallProblems/test1.cu
	
test_simpleProblems: src/*.cuh test/smallProblems/test.cuh makefile
	nvcc ${NVCC_OPTIONS} -I src/ ${TEST_SIMPLE_SOURCES} -o test_simple

TEST_SIMPLEX_SOURCES = src/housekeeping.cu src/instrument_functions.cu \
          src/memorymanagment.cu src/operations.cu \
          src/selectEnterLeavingVars.cu src/gatherSolution.cu \
          src/newLu.cu src/basisfunctions.cu src/simplex.cu test/simplex/TestSimplex.cu
test_simplex: ${TEST_SIMPLEX_SOURCES} src/simplex.cuh makefile
	nvcc ${NVCC_OPTIONS} -I src/ ${TEST_SIMPLEX_SOURCES} -o test_simplex

TEST_SELECT_SOURCES = src/memorymanagment.cu src/selectEnterLeavingVars.cu test/selectPivot/TestSelection.cu

test_selectEnterLeaveVars: ${TEST_SELECT_SOURCES} src/*.cuh makefile
	nvcc ${NVCC_OPTIONS} -I src/ ${TEST_SELECT_SOURCES} -o test_selectEnterLeaveVars

TEST_BU_SOURCES = src/basisfunctions.cu src/memorymanagment.cu \
				  src/operations.cu src/housekeeping.cu \
				  test/housekeeping/TestUpdateBasis.cu
test_basisUpdate: ${TEST_BU_SOURCES} src/*.cuh makefile
	nvcc ${NVCC_OPTIONS} -I src/ ${TEST_BU_SOURCES} -o test_basisUpdate

TEST_WI_SOURCES = src/basisfunctions.cu src/memorymanagment.cu \
				  src/operations.cu src/housekeeping.cu \
				  test/housekeeping/TestWriteVectorAtIndicies.cu
test_writeIndicies: ${TEST_WI_SOURCES} src/*.cuh makefile
	nvcc ${NVCC_OPTIONS} -I src/ ${TEST_WI_SOURCES} -o test_writeIndicies



# Specify GLPK directories
g_GLPK_INCLUDE_DIR = '../glpk-4.35/include/'
g_GLPK_LIB_DIR = '../glpk-4.35/src/.libs'

# Compiler options
CC = g++
CFLAGS = -g -pg -Wall -Wextra

# GLPK options
g_GLPK_OPTIONS = -I$(g_GLPK_INCLUDE_DIR) -L$(g_GLPK_LIB_DIR) -lglpk

# Target for your test program
test_idr: test/read/TestReader.cpp makefile
	$(CC) $(CFLAGS) $(g_GLPK_OPTIONS) test/read/TestReader.cpp -o test_idr
