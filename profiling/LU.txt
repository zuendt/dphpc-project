for 50 iterations

==3177974== Profiling application: ./project ./benchmark/miplib/instances/markshare_4_0.mps 3
==3177974== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   34.04%  1.1944ms        50  23.888us  23.839us  24.192us  void cub::DeviceRadixSortSingleTileKernel<cub::DeviceRadixSortPolicy<int, int, int>::Policy800, bool=0, int, int, int>(int const *, cub::DeviceRadixSortSingleTileKernel<cub::DeviceRadixSortPolicy<int, int, int>::Policy800, bool=0, int, int, int>*, cub::DeviceRadixSortPolicy<int, int, int>::Policy800 const *, cub::DeviceRadixSortSingleTileKernel<cub::DeviceRadixSortPolicy<int, int, int>::Policy800, bool=0, int, int, int>**, bool=0, int, int)
                   21.39%  750.56us       650  1.1540us  1.0870us  1.6000us  [CUDA memcpy DtoH]
                    5.09%  178.52us        50  3.5700us  3.5190us  3.8720us  void cub::DeviceReduceByKeyKernel<cub::DispatchReduceByKey<int*, int*, cub::ConstantInputIterator<int, int>, int*, int*, cub::Equality, cub::Sum, int>::PtxReduceByKeyPolicy, int*, int*, cub::ConstantInputIterator<int, int>, int*, int*, cub::ReduceByKeyScanTileState<int, int, bool=1>, cub::Equality, cub::Sum, int>(int*, int, int, cub::ConstantInputIterator<int, int>, int*, int*, int, cub::Equality, cub::Sum, int)
                    5.00%  175.62us        50  3.5120us  3.4560us  3.8410us  void cub::DeviceScanKernel<cub::DeviceScanPolicy<int>::Policy600, int*, int*, cub::ScanTileState<int, bool=1>, cub::Sum, cub::detail::InputValue<int, int*>, int>(cub::DeviceScanPolicy<int>::Policy600, int*, int*, int, int, bool=1, cub::ScanTileState<int, bool=1>)
                    5.00%  175.52us        50  3.5100us  3.3600us  3.7130us  void cusparse::csr2csr_kernel_v1<unsigned int=128, unsigned int=896, int=0, int>(int const *, int const *, int, int const **)
                    4.12%  144.73us       150     964ns     927ns  1.2800us  [CUDA memcpy HtoD]
                    3.81%  133.79us        50  2.6750us  2.6240us  2.9760us  void cusparse::gather_kernel<unsigned int=128, int, int>(int const *, int const *, int, int*)
                    3.81%  133.67us        50  2.6730us  2.6240us  2.8800us  void cusparse::gather_kernel<unsigned int=128, float, float>(float const *, int const *, int, float*)
                    3.55%  124.42us        50  2.4880us  2.4640us  2.7520us  void cusparse::scatter_kernel<unsigned int=128, int=0, int, int>(int const *, int const *, int, int*)
                    3.35%  117.44us        50  2.3480us  2.3030us  2.6880us  void cusparse::sequence_kernel<unsigned int=128, int>(int*, int, cusparse::sequence_kernel<unsigned int=128, int>)
                    3.22%  112.87us        50  2.2570us  2.2400us  2.3050us  void cusparse::merge_path_partition_kernel<unsigned int=128, unsigned int=896, int=0, int>(int const *, int, cusparse::merge_path_partition_kernel<unsigned int=128, unsigned int=896, int=0, int>, int, int*, int)
                    3.15%  110.47us        50  2.2090us  2.1760us  2.3050us  void cub::DeviceScanInitKernel<cub::ScanTileState<int, bool=1>>(int, int)
                    3.12%  109.50us        50  2.1900us  2.1440us  2.4310us  void cub::DeviceCompactInitKernel<cub::ReduceByKeyScanTileState<int, int, bool=1>, int*>(int, int, int)
                    1.37%  47.904us        50     958ns     928ns  1.0240us  [CUDA memset]
      API calls:   56.92%  19.840ms       350  56.685us  4.1400us  302.85us  cudaMalloc
                   24.17%  8.4240ms       750  11.232us  4.9400us  27.310us  cudaMemcpy
                    8.42%  2.9365ms       550  5.3390us  3.8400us  14.860us  cudaLaunchKernel
                    3.38%  1.1785ms        50  23.569us  17.360us  33.111us  cudaMemcpyAsync
                    3.10%  1.0821ms       150  7.2140us  4.2500us  11.370us  cudaFree
                    1.63%  567.71us      3353     169ns     150ns     790ns  cudaGetLastError
                    0.94%  327.75us       751     436ns     340ns  1.5800us  cudaGetDevice
                    0.67%  234.98us        50  4.6990us  4.2800us  11.871us  cudaMemsetAsync
                    0.31%  108.75us       100  1.0870us     790ns  6.0800us  cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
                    0.27%  92.440us       500     184ns     150ns     270ns  cudaPeekAtLastError
                    0.16%  54.490us       100     544ns     410ns     860ns  cudaDeviceGetAttribute
                    0.03%  8.9000us         1  8.9000us  8.9000us  8.9000us  cudaFuncGetAttributes
                    0.00%  1.6600us         1  1.6600us  1.6600us  1.6600us  cuDeviceGetCount
                    0.00%     200ns         1     200ns     200ns     200ns  cudaGetDeviceCount